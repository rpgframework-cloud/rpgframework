/**
 * 
 */
package de.rpgframework.devices;

import de.rpgframework.media.Media;
import de.rpgframework.reality.Player;

/**
 * @author Stefan
 *
 */
public interface FunctionPlayerHandout {

	public Media.Category[] getSupportedCategories();
	
	public void sendHandout(Player player, Media handout);
	
}
