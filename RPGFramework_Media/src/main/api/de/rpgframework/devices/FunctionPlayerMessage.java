package de.rpgframework.devices;

import de.rpgframework.reality.Player;

/**
 * @author Stefan
 *
 */
public interface FunctionPlayerMessage {

	public void sendMessage(Player player, String message);
	
}
