/**
 *
 */
package de.rpgframework.adventure.encoder;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

import de.rpgframework.adventure.AdventureDocumentElement;
import de.rpgframework.adventure.Chapter;
import de.rpgframework.adventure.EnhancedAdventureXMLPlugin;
import de.rpgframework.adventure.Paragraph;
import de.rpgframework.adventure.Paragraph.Type;
import de.rpgframework.adventure.Section;
import de.rpgframework.adventure.TextFlowElement;

/**
 * @author Stefan
 *
 */
public class MainEAMLPlugin implements EnhancedAdventureXMLPlugin {

	private final static Logger logger = LogManager.getLogger(MainEAMLPlugin.class);

	public final static String NAMESPACE = "+//IDN rpgframework.de//DTD EAML 1.0//EN";

	//--------------------------------------------------------------------
	/**
	 */
	public MainEAMLPlugin() {
		// TODO Auto-generated constructor stub
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.adventure.EnhancedAdventureXMLPlugin#getNamespace()
	 */
	@Override
	public String getNamespace() {
		return NAMESPACE;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.adventure.EnhancedAdventureXMLPlugin#domDecode(org.w3c.dom.Element)
	 */
	@Override
	public AdventureDocumentElement domDecode(Element elem) {
		logger.debug("decode "+elem);
		String tag = elem.getLocalName();

		if (tag.equalsIgnoreCase("chapter")) {
			return decodeChapter(elem);
		} else if (tag.equalsIgnoreCase("section")) {
			return decodeSection(elem);
		} else if (tag.equalsIgnoreCase("p")) {
			return decodeParagraph(elem, Type.NORMAL);
		} else if (tag.equalsIgnoreCase("common")) {
			return decodeParagraph(elem, Type.COMMON);
		} else if (tag.equalsIgnoreCase("special")) {
			return decodeParagraph(elem, Type.SPECIAL);
		} else if (tag.equalsIgnoreCase("master")) {
			return decodeParagraph(elem, Type.MASTER);
		} else
			logger.error("Unknown element to decode: "+tag);
		return null;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.adventure.EnhancedAdventureXMLPlugin#domEncode(org.w3c.dom.Document, de.rpgframework.adventure.AdventureDocumentElement)
	 */
	@Override
	public Element domEncode(Document doc, AdventureDocumentElement toEncode) {
		// TODO Auto-generated method stub
		return null;
	}

	//--------------------------------------------------------------------
	private Chapter decodeChapter(Element parent) {
		Chapter data = new Chapter();

		/*
		 * Children
		 */
		List<Element> parseExternally = new ArrayList<>();
		for (int i=0; i<parent.getChildNodes().getLength(); i++) {
			Node child = parent.getChildNodes().item(i);
			if (child.getNodeType()!=Node.ELEMENT_NODE)
				continue;
			Element element = (Element)child;

			if (element.getTagName().equalsIgnoreCase("title"))
				data.setTitle( AdventureModuleParser.getChildAsCDATA(element) );
			else if (element.getTagName().equalsIgnoreCase("subtitle"))
				data.setSubtitle( AdventureModuleParser.getChildAsCDATA(element) );
//			else if (element.getTagName().equalsIgnoreCase("p"))
//				structure.add(new Paragraph(element));
//			else if (element.getTagName().equalsIgnoreCase("section"))
//				structure.add(new Section(element));
			else
				parseExternally.add(element);
		}

		// Parse rest
		AdventureModuleParser.parseChildren(data, parseExternally);

		return data;
	}

	//--------------------------------------------------------------------
	private Paragraph decodeParagraph(Element parent, Paragraph.Type type) {
		Paragraph data = new Paragraph(type);

		/*
		 * Children
		 */
//		List<Element> parseExternally = new ArrayList<>();
		for (int i=0; i<parent.getChildNodes().getLength(); i++) {
			Node child = parent.getChildNodes().item(i);
			
			switch (child.getNodeType()) {
			case Node.TEXT_NODE:
				Text text = (Text)child;
				TextFlowElement elem = new TextFlowElement();
				elem.setCharacterData(text.getNodeValue().trim().replaceAll("\\s+", " ") );
				data.add(elem);
				break;
			case Node.ELEMENT_NODE:
				Element element = (Element)child;
				AdventureDocumentElement element2 = domDecode(element);
				if (element2!=null)
					data.add(element2);
				break;
			default:
				logger.warn("Don't know how to deal with "+child.getClass());
			}
//			if (child.getNodeType()!=Node.ELEMENT_NODE)
//				continue;
//			Element element = (Element)child;
//
//			if (element.getTagName().equalsIgnoreCase("title"))
//				data.setTitle( AdventureModuleParser.getChildAsCDATA(element) );
//			else if (element.getTagName().equalsIgnoreCase("subtitle"))
//				data.setSubtitle( AdventureModuleParser.getChildAsCDATA(element) );
////			else if (element.getTagName().equalsIgnoreCase("p"))
////				structure.add(new Paragraph(element));
////			else if (element.getTagName().equalsIgnoreCase("section"))
////				structure.add(new Section(element));
//			else
//				parseExternally.add(element);
		}

//		// Parse rest
//		AdventureModuleParser.parseChildren(data, parseExternally);

		logger.warn("TODO: decode text flow children");
		return data;
	}

	//-------------------------------------------------------------------
	public Section decodeSection(Element parent) {
		Section data = new Section();

		/*
		 * Children
		 */
		List<Element> parseExternally = new ArrayList<>();
		for (int i=0; i<parent.getChildNodes().getLength(); i++) {
			Node child = parent.getChildNodes().item(i);
			if (child.getNodeType()!=Node.ELEMENT_NODE)
				continue;
			Element element = (Element)child;

			if (element.getTagName().equalsIgnoreCase("title"))
				data.setTitle( AdventureModuleParser.getChildAsCDATA(element) );
			else {
				parseExternally.add(element);
			}
		}

		// Parse rest
		AdventureModuleParser.parseChildren(data, parseExternally);

		return data;
	}

}
