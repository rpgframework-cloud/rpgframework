/**
 * 
 */
package de.rpgframework.adventure.encoder;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;

import de.rpgframework.adventure.AdventureModule;
import de.rpgframework.core.RoleplayingSystem;

/**
 * @author Stefan
 *
 */
public class AdventureModuleEncoder {
	
	private final static String NS_DEFAULT = "http://rpgframework.de/EnhancedAdventure";
	private final static String NS_SPLITTERMOND = "http://rpgframework.de/EnhancedAdventure_Splittermond";

//	private final static String SPLITTERMOND_URI = "http://rpgframework.de/xml/EnAdv_Splittermond.dtd";
	
	private static DocumentBuilderFactory factory;
	private static DocumentBuilder builder;
	private static DocumentType docType;
	private static Map<RoleplayingSystem, String> nsByRules;
	
	//--------------------------------------------------------------------
	static {
		factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DOMImplementation impl = builder.getDOMImplementation();
		docType = impl.createDocumentType("dt1", NS_DEFAULT, "foo.dtd");

		
		nsByRules = new HashMap<RoleplayingSystem, String>();
		nsByRules.put(RoleplayingSystem.SPLITTERMOND, NS_SPLITTERMOND);
	}
	
	//--------------------------------------------------------------------
	public static String transformToString(Document document) throws TransformerFactoryConfigurationError, TransformerException {
		DOMSource in = new DOMSource(document);
		StreamResult out = new StreamResult(new StringWriter());

		// Configure transformer
		Transformer transformer = TransformerFactory.newInstance()
				.newTransformer(); // An identity transformer
		transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, "testing.dtd [..]");
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
//		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
	    transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		transformer.transform(in, out);

		return out.getWriter().toString();
	}
	
	//--------------------------------------------------------------------
	public static Document encodeToXML(AdventureModule module) {
		Document doc = builder.newDocument();
		doc = builder.getDOMImplementation().createDocument(NS_DEFAULT, "adventure", docType);
//		DOMConfiguration config = doc.getDomConfig();
////		config.setParameter("error-handler",new MyErrorHandler());
//		config.setParameter("schema-type", "http://www.w3.org/2001/XMLSchema");
////		config.setParameter("validate", Boolean.TRUE);
////		config.setParameter("namespace-declarations", Boolean.TRUE);
////		doc.createElementNS(namespaceURI, qualifiedName)
		
//		Element root = doc.createElement("adventure");
//		doc.appendChild(root);
		Element root = doc.getDocumentElement();
//		root.setPrefix("xmlns");
		if (module.getLang()!=null)
			root.setAttribute("lang", module.getLang());
		root.setAttributeNS(
				  "http://www.w3.org/2000/xmlns/", // namespace
				  "xmlns:sm", // node name including prefix
				  NS_SPLITTERMOND // value
				);
		
		Element title = doc.createElementNS(NS_DEFAULT, "title");
		title.appendChild(doc.createTextNode(module.getTitle()));
		root.appendChild(title);
	
		Element smstats = doc.createElementNS(NS_SPLITTERMOND, "npcstats");
		smstats.setPrefix("sm");
		root.appendChild(smstats);
		
		return doc;
	}
}
