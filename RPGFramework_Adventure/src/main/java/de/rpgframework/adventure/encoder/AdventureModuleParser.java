/**
 *
 */
package de.rpgframework.adventure.encoder;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.function.Consumer;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLResolver;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;
import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSResourceResolver;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import de.rpgframework.adventure.AdventureDocumentElement;
import de.rpgframework.adventure.AdventureModule;
import de.rpgframework.adventure.ElementContainer;
import de.rpgframework.adventure.EnhancedAdventureXMLPlugin;

/**
 * @author prelle
 *
 */
public class AdventureModuleParser {

	private final static Logger logger = LogManager.getLogger(AdventureModuleParser.class);

	static final String JAXP_SCHEMA_SOURCE = "http://java.sun.com/xml/jaxp/properties/schemaSource";

	private static DocumentBuilderFactory factory;
	private static DocumentBuilder builder;
	private static EntityResolver entityResolver;
	private static ErrorHandler errorHandler;

	private static Map<String, EnhancedAdventureXMLPlugin> pluginsByNS;

	//-------------------------------------------------------------------
	static {
		/*
		 * Load all EnhancedAdventure XML parser plugins
		 */
		logger.debug("Setup plugins");
		MainEAMLPlugin basePlugin = new MainEAMLPlugin();
		pluginsByNS = new HashMap<String, EnhancedAdventureXMLPlugin>();
		pluginsByNS.put(basePlugin.getNamespace(), basePlugin);
		Iterator<EnhancedAdventureXMLPlugin> it = ServiceLoader.load(EnhancedAdventureXMLPlugin.class).iterator();
		while (it.hasNext()) {
			EnhancedAdventureXMLPlugin plugin = it.next();
			logger.info("Found plugin for namespace "+plugin.getNamespace());
			pluginsByNS.put(plugin.getNamespace(), plugin);
		}

		
		entityResolver = new EntityResolver() {

			@Override
			public InputSource resolveEntity(String publicId, String systemId)
					throws SAXException, IOException {
				logger.debug("try resolve: "+publicId+"   sys="+systemId);
				if (publicId!=null && publicId.equalsIgnoreCase("+//IDN rpgframework.de//DTD EnhancedAdventure 1.0//en")) {
					logger.debug("Resolve locally: "+publicId);
					return new InputSource(new FileInputStream("../EnhancedAdventure/enhanced_adventure.dtd"));
//					return new InputSource(new FileInputStream("../EnhancedAdventure/importing.dtd"));
				}
				if (publicId!=null && publicId.equalsIgnoreCase("+//IDN rpgframework.de//DTD EAML Audio 1.0//en")) {
					logger.debug("Resolve locally: "+publicId);
					return new InputSource(new FileInputStream("../EnhancedAdventure_Audio/eaml_audio_with_schema.dtd"));
//					return new InputSource(new FileInputStream("../EnhancedAdventure/importing.dtd"));
				}
				if (systemId!=null && systemId.startsWith("file://")) {
					logger.debug("Resolve locally: "+systemId);
					return new InputSource(new FileInputStream(systemId.substring(7)));
				}
				logger.warn("TODO: resolve "+publicId+"  SYSTEM "+systemId);
				return null;
			}
		};
		
		errorHandler = new ErrorHandler() {
			@Override
			public void warning(SAXParseException e) throws SAXException {
				logger.warn(String.format("pub='%s' sys='%s' line %d:%d: %s",
						e.getPublicId(), e.getSystemId(), e.getLineNumber(), e.getColumnNumber(), e.getMessage()
						));
			}

			@Override
			public void fatalError(SAXParseException e) throws SAXException {
				logger.fatal(String.format("pub='%s' sys='%s' line %d:%d: %s",
						e.getPublicId(), e.getSystemId(), e.getLineNumber(), e.getColumnNumber(), e.getMessage()
						));
			}

			@Override
			public void error(SAXParseException e) throws SAXException {
				logger.error(String.format("pub='%s' sys='%s' line %d:%d: %s",
						e.getPublicId(), e.getSystemId(), e.getLineNumber(), e.getColumnNumber(), e.getMessage()
						));
			}
		};
	}

	//-------------------------------------------------------------------
	/**
	 */
	@SuppressWarnings("unused")
	public AdventureModuleParser() {
		if (builder==null) {
			factory = DocumentBuilderFactory.newInstance();
			factory.setExpandEntityReferences(true);
			factory.setXIncludeAware(true);
			factory.setIgnoringComments(true);
			factory.setNamespaceAware(true);
			if (false) {
				factory.setValidating(true);
			} else {
				factory.setValidating(false);
//				factory.setAttribute(JAXP_SCHEMA_SOURCE, new String[]{"../EnhancedAdventure/foo.xsd", "../EnhancedAdventure_Audio/foo.xsd"});
				try {
					SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
					schemaFactory.setResourceResolver(new LSResourceResolver(){
					    @Override
					    public LSInput resolveResource(String type, String namespaceURI, String publicId, String systemId, String baseURI) {
					           logger.info("Resolve "+namespaceURI);
					           return null;
					    }

					});
					Schema schema = schemaFactory.newSchema(new Source[] {
					            new StreamSource(new FileInputStream("main.xsd")),
					            new StreamSource(new FileInputStream("extension.xsd")),
					            });
					schema.newValidator().setErrorHandler(errorHandler);
					factory.setSchema(schema);
//					factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, true);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SAXException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			try {
				builder = factory.newDocumentBuilder();
				builder.setEntityResolver(entityResolver);
				builder.setErrorHandler(errorHandler);
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	//-------------------------------------------------------------------
	public static String getChildAsCDATA(Element element) {
		Node first = element.getFirstChild();
		if (first==null)
			return null;
//		logger.debug("First child of "+element+" = "+element.getFirstChild());
//		logger.debug("First child of2 "+element+" = "+first.getTextContent());
//		logger.debug("First child of3 "+element+" = "+ ((Text)first).getWholeText());
//		logger.debug("First child of3 "+element+" = "+ ((Text)first).getData());
		return ((Text)first).getData().trim().replaceAll("\\s+", " ");
	}

	//-------------------------------------------------------------------
	public static AdventureDocumentElement decode(Element elem) {
//		logger.info("Decode "+elem.getLocalName()+" from "+elem.getNamespaceURI());
		EnhancedAdventureXMLPlugin plugin = pluginsByNS.get(elem.getNamespaceURI());

		try {
			if (plugin!=null) {
				return plugin.domDecode(elem);
			} else
				logger.warn("No plugin for "+elem.getNamespaceURI()+" / "+elem.getLocalName());
		} catch (Exception e) {
			logger.error("Failed parsing element "+elem,e);
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static void parseChildren(ElementContainer parent, List<Element> children) {
		for (Element elem : children) {
			AdventureDocumentElement parsed = decode(elem);
			if (parsed!=null) {
				parent.add(parsed);
			}
		}
	}

	//-------------------------------------------------------------------
	public AdventureModule read(InputStream xml) throws IOException {
		if (xml==null)
			throw new NullPointerException("InputStream is null");
		try {
//			XMLInputFactory inputFactory = XMLInputFactory.newInstance();
//			XMLEventReader  evRd = inputFactory.createXMLEventReader( xml );
//			return read(evRd);
			Document doc = builder.parse(xml);
			AdventureModule mod = new AdventureModule(doc);
			logger.debug("Reading done");
			return mod;
		} catch (FactoryConfigurationError e) {
			throw new IOException(e);
		} catch (SAXParseException e) {
			logger.error("Failed reading publicId='"+e.getPublicId()+"' systemId='"+e.getSystemId()+"', line "+e.getLineNumber()+" col="+e.getColumnNumber()+": "+e.getMessage());
			throw new IOException(e);
		} catch (Exception e) {
			throw new IOException(e);
		}
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	public AdventureModule read2(InputStream stream) throws IOException, XMLStreamException {
		XMLInputFactory factory = XMLInputFactory.newInstance();
		factory.setProperty(XMLInputFactory.IS_REPLACING_ENTITY_REFERENCES, true);
		factory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, true);
		factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);
		factory.setProperty(XMLInputFactory.IS_VALIDATING, true);

		factory.setXMLResolver(new XMLResolver() {

			@Override
			public Object resolveEntity(String publicId, String systemId,
					String baseURI, String namespace) throws XMLStreamException {
				try {
					if (publicId.equalsIgnoreCase("+//IDN rpgframework.de//DTD EnhancedAdventure 1.0//en")) {
						logger.debug("Resolve locally: "+publicId);
						return new InputSource(new FileInputStream("../EnhancedAdventure/enhanced_adventure.dtd"));
					}
					if (publicId!=null && publicId.equalsIgnoreCase("+//IDN rpgframework.de//DTD EAML Audio 1.0//en")) {
						logger.debug("Resolve locally: "+publicId);
						return new InputSource(new FileInputStream("../EnhancedAdventure_Audio/eaml_audio.dtd"));
//						return new InputSource(new FileInputStream("../EnhancedAdventure/importing.dtd"));
					}
					logger.warn("TODO: resolve "+publicId+"  SYSTEM "+systemId);
				} catch (FileNotFoundException e) {
					logger.error(e.toString());
				}
				return null;
			}
		});
		XMLEventReader evRd = factory.createXMLEventReader(stream);
		try {
			while( evRd.hasNext() ) {
				XMLEvent ev = evRd.nextEvent();
				logger.debug("------------------------------------------------------------------");
				logger.debug(" evType = "+ev+" // "+ev.getSchemaType());
				switch (ev.getEventType()) {
				case XMLEvent.START_DOCUMENT:
					logger.debug("Start document");
					continue;
				case XMLEvent.START_ELEMENT:
					StartElement start = ev.asStartElement();
					logger.debug(" name = "+start.getName());
					start.getAttributes().forEachRemaining(new Consumer<Attribute>() {

						@Override
						public void accept(Attribute t) {
							// TODO Auto-generated method stub
							logger.debug("  attr = "+t);

						}
					});
//					if (start.getName().getNamespaceURI()!=null && start.getName().getNamespaceURI().contains("EAML"))
//						System.exit(0);
					continue;
				case XMLEvent.CHARACTERS:
					logger.debug("CHARACTERS "+ev);
					continue;
				case XMLEvent.END_ELEMENT:
					logger.debug("END_ELEMENT "+ev);
					continue;
				case XMLEvent.END_DOCUMENT:
					logger.debug("END_DOCUMENT");
					continue;
				case XMLEvent.COMMENT:
					continue;
				case XMLEvent.DTD:
					logger.debug("DTD");
					continue;
				default:
					logger.fatal("? "+ev.getEventType()) ;
					System.exit(0);
				}

			}
		} catch (XMLStreamException e) {
			logger.error("ERROR",e);
			throw new IOException(e);
		}
		return null;
	}

}
