/**
 *
 */
package de.rpgframework.adventure;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import de.rpgframework.adventure.encoder.AdventureModuleParser;
import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
public class AdventureModule extends ArrayList<AdventureDocumentElement> implements ElementContainer {

	private String lang;
	private RoleplayingSystem system;

	private String title;
	private String subtitle;

	//-------------------------------------------------------------------
	public AdventureModule() {
	}

	//-------------------------------------------------------------------
	public AdventureModule(Document doc)  {
		this();
		String encoding = doc.getXmlEncoding();
		Logger logger = LogManager.getLogger(AdventureModule.class);
		logger.debug("Encoding = "+encoding);

		Element root = doc.getDocumentElement();

		/*
		 * Attributes
		 */
		NamedNodeMap map = root.getAttributes();
		for (int i=0; i<map.getLength(); i++) {
			Attr attr = (Attr) map.item(i);
			if (attr.getName().equalsIgnoreCase("system"))
				system = RoleplayingSystem.valueOf(attr.getValue().toUpperCase());
			else if (attr.getName().equalsIgnoreCase("lang"))
				lang = attr.getValue();
			else if (attr.getName().startsWith("xmlns"))
				continue;
			else if (attr.getName().startsWith("xsi"))
				continue;
			else
				logger.warn("Ignore attribute "+attr+" in root node");
		}

		/*
		 * Children
		 */
		List<Element> parseExternally = new ArrayList<>();
		for (int i=0; i<root.getChildNodes().getLength(); i++) {
			Node child = root.getChildNodes().item(i);
			if (child.getNodeType()!=Node.ELEMENT_NODE)
				continue;
			Element element = (Element)child;
//			logger.debug("Parse "+element.getNodeName());

			if (element.getTagName().equalsIgnoreCase("title"))
				title = AdventureModuleParser.getChildAsCDATA(element);
			else if (element.getTagName().equalsIgnoreCase("subtitle")) {
				subtitle = AdventureModuleParser.getChildAsCDATA(element);
//			} else if (element.getTagName().equalsIgnoreCase("chapter")) {
//				chapters.add(new Chapter(element));
//			} else if (element.getTagName().equalsIgnoreCase("p")) {
//				paragraphs.add(new Paragraph(element));
			} else {
				parseExternally.add(element);
//				logger.warn("Ignore element "+element+" in root node");
//				AdventureModuleParser.decode(element);
			}
		}

		// Parse rest
		AdventureModuleParser.parseChildren(this, parseExternally);
	}

	//-------------------------------------------------------------------
	public String toString() {
		return "Module "+title;
	}

	//-------------------------------------------------------------------
	public String getTitle() {
		return title;
	}

	//-------------------------------------------------------------------
	public void setTitle(String value) {
		this.title = value;
	}

	//-------------------------------------------------------------------
	public String getLang() {
		return lang;
	}

	//-------------------------------------------------------------------
	public void setLang(String value) {
		this.lang = value;
	}

	//-------------------------------------------------------------------
	public RoleplayingSystem getSystem() {
		return system;
	}

	//-------------------------------------------------------------------
	public void setSystem(RoleplayingSystem value) {
		this.system = value;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the subtitle
	 */
	public String getSubtitle() {
		return subtitle;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.adventure.AdventureDocumentElement#getNamespace()
	 */
	@Override
	public String getNamespace() {
		// TODO Auto-generated method stub
		return null;
	}

}
