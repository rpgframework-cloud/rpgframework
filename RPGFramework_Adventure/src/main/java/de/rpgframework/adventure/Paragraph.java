/**
 *
 */
package de.rpgframework.adventure;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
public class Paragraph extends StructureElement {
	
	public enum Type {
		NORMAL,
		COMMON,
		SPECIAL,
		MASTER,
	}

	private Type type;
	
	//-------------------------------------------------------------------
	public Paragraph(Type type) {
		this.type = type;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}

}
