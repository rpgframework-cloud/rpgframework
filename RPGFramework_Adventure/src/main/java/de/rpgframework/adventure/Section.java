/**
 *
 */
package de.rpgframework.adventure;

import java.util.ArrayList;
import java.util.List;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
public class Section extends StructureElement {

	private String title;
//	private String subtitle;

	private List<StructureElement> structure;

	//-------------------------------------------------------------------
	/**
	 */
	public Section() {
		structure = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	public String toString() {
		return title;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	//-------------------------------------------------------------------
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	//-------------------------------------------------------------------
	public List<StructureElement> getChildren() {
		return structure;
	}

}
