/**
 *
 */
package de.rpgframework.adventure;


/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
public class Chapter extends StructureElement implements ElementContainer {

	private String title;
	private String subtitle;

	//-------------------------------------------------------------------
	/**
	 */
	public Chapter() {
		title = "unnamed chapter";
	}

	//-------------------------------------------------------------------
	public String toString() {
		return title;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	//-------------------------------------------------------------------
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the subtitle
	 */
	public String getSubtitle() {
		return subtitle;
	}

	//--------------------------------------------------------------------
	/**
	 * @param subtitle the subtitle to set
	 */
	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

}
