package de.rpgframework.adventure;

/**
 * @author prelle
 *
 */
public class TextFlowElement implements AdventureDocumentElement {
	
	private String namespace;
	private String cdata;

	//-------------------------------------------------------------------
	/**
	 */
	public TextFlowElement() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @return the cdata
	 */
	public String getCharacterData() {
		return cdata;
	}

	//-------------------------------------------------------------------
	/**
	 * @param cdata the cdata to set
	 */
	public void setCharacterData(String cdata) {
		this.cdata = cdata;
	}

	@Override
	public String getNamespace() {
		return namespace;
	}

}
