/**
 *
 */
package de.rpgframework.adventure;

import java.util.ArrayList;

import de.rpgframework.adventure.encoder.MainEAMLPlugin;

/**
 * @author Stefan
 *
 */
@SuppressWarnings("serial")
public class StructureElement extends ArrayList<AdventureDocumentElement> implements ElementContainer {

	private String id;

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.adventure.AdventureDocumentElement#getNamespace()
	 */
	@Override
	public String getNamespace() {
		return MainEAMLPlugin.NAMESPACE;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	//--------------------------------------------------------------------
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

}
