/**
 * 
 */
package de.rpgframework.adventure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author Stefan
 *
 */
@SuppressWarnings("serial")
public class Table extends StructureElement {

	private final static Logger logger = LogManager.getLogger(Table.class);
	
	private Map<Integer,String> colWidth;
	private List<String> columnNames;
	private List<String[]> tableData;

	//--------------------------------------------------------------------
	public Table() {
		colWidth = new HashMap<Integer, String>();
		columnNames = new ArrayList<String>();
		tableData   = new ArrayList<String[]>();
	}

	//--------------------------------------------------------------------
	public Table(Element parent) {
		this();
		/*
		 * Children
		 */
		for (int i=0; i<parent.getChildNodes().getLength(); i++) {
			Node child = parent.getChildNodes().item(i);
			if (child.getNodeType()!=Node.ELEMENT_NODE)
				continue;
			Element element = (Element)child;
			
			if (element.getTagName().equalsIgnoreCase("colwidth")) {
				int col = -1;
				if (element.hasAttribute("col"))
					col = Integer.parseInt(element.getAttribute("col"));
				colWidth.put(col, element.getAttribute("width"));
			} else if (element.getTagName().equalsIgnoreCase("thead")) {
				parseTableHead(element);
			} else if (element.getTagName().equalsIgnoreCase("tbody")) {
				parseTableBody(element);
			} else
				logger.warn("Ignore element "+element.getTagName()+" in chapter ");
		}
		// TODO Auto-generated constructor stub
	}

	//--------------------------------------------------------------------
	private void parseTableHead(Element head) {
		NodeList children = head.getElementsByTagName("td");
		for (int i=0; i<children.getLength(); i++) {
			Element td = (Element)children.item(i);
			StringBuffer buf = new StringBuffer();
			NodeList tmp = td.getChildNodes();
			logger.warn("TODO: handle children of <td>");
			for (int x=0; x<tmp.getLength(); x++) {
				
//				TextFlowElement text = Paragraph.parseTextFlow(tmp.item(x));
//				if (text instanceof PlainText) {
//					buf.append( ((PlainText)text).getValue()+" ");
//				} else
//					logger.warn("Unexpected "+text.getClass());
			}
			columnNames.add(buf.toString().trim());
		}
	}

	//--------------------------------------------------------------------
	private void parseTableBody(Element body) {
		NodeList rowChildren = body.getElementsByTagName("tr");
		for (int r=0; r<rowChildren.getLength(); r++) {
			Element tr = (Element)rowChildren.item(r);
			String[] row = new String[columnNames.size()];
			NodeList children = tr.getElementsByTagName("td");
			for (int i=0; i<children.getLength(); i++) {
				Element td = (Element)children.item(i);
				StringBuffer buf = new StringBuffer();
				NodeList tmp = td.getChildNodes();
				logger.warn("TODO: handle children of <td>");
				for (int x=0; x<tmp.getLength(); x++) {
//					TextFlowElement text = Paragraph.parseTextFlow(tmp.item(x));
//					if (text instanceof PlainText) {
//						buf.append( ((PlainText)text).getValue()+" ");
//					} else
//						logger.warn("Unexpected "+text.getClass());
				}
				row[i] = buf.toString().trim();
			}
//			logger.debug("Set table data: "+Arrays.toString(row));
			tableData.add(row);
		}
	}

	//--------------------------------------------------------------------
	public List<String> getColumnNames() {
		return columnNames;
	}

	//--------------------------------------------------------------------
	public int getPercentWidth(int index) {
		String value = colWidth.get(index+1);
		if (value==null)
			return 0;
		if (value.endsWith("%"))
			return Integer.parseInt(value.substring(0, value.length()-1).trim());
		logger.warn("Don't know how to parse "+value);
		return 0;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the tableData
	 */
	public List<String[]> getTableData() {
		return tableData;
	}
}
