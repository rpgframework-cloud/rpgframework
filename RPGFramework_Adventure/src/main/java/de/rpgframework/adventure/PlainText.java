/**
 * 
 */
package de.rpgframework.adventure;

/**
 * @author prelle
 *
 */
public class PlainText extends TextFlowElement {

	private String value;
	
	//-------------------------------------------------------------------
	/**
	 */
	public PlainText(String text) {
		value = text;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	//--------------------------------------------------------------------
	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

}
