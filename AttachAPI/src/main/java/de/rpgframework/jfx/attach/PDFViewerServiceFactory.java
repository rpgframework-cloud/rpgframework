package de.rpgframework.jfx.attach;

import java.util.Optional;
import java.util.ServiceLoader;

public class PDFViewerServiceFactory {

	private static Optional<PDFViewerService> instance;

	public static Optional<PDFViewerService> create() {
		if (instance==null) {
			instance = ServiceLoader.load(PDFViewerService.class).findFirst();
		}
        return instance;
    }

}
