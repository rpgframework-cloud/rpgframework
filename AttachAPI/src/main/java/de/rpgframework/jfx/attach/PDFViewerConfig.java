package de.rpgframework.jfx.attach;

import java.util.function.BiFunction;

/**
 * @author prelle
 *
 */
public class PDFViewerConfig {
	
	public static class PathAndOffset {
		public String path;
		public int offset;
		public PathAndOffset(String path, int offset) {
			this.path = path;
			this.offset = offset;
		}
	}

	private static BiFunction<String, String, PathAndOffset> pathResolver;
	private static boolean enabled = false;
	
	//-------------------------------------------------------------------
	public static void setPDFPathResolver(BiFunction<String, String, PathAndOffset> resolver) {
		PDFViewerConfig.pathResolver = resolver;
	}
	
	//-------------------------------------------------------------------
	public static BiFunction<String, String, PathAndOffset> getPDFPathResolver() {
		return pathResolver;
	}

	//-------------------------------------------------------------------
	public static void setEnabled(boolean value) {
		PDFViewerConfig.enabled = value;
	}

	//-------------------------------------------------------------------
	public static boolean isEnabled() {
		return PDFViewerConfig.enabled;
	}

}
