package de.rpgframework.jfx.attach;

import de.rpgframework.core.RoleplayingSystem;

public interface PDFViewerService {

	void show(RoleplayingSystem rules, String productID, String lang, int page);

}
