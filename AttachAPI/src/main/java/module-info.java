module rpgframework.pdfviewer {
	exports de.rpgframework.jfx.attach;

	uses de.rpgframework.jfx.attach.PDFViewerService;

	requires transitive de.rpgframework.core;
}