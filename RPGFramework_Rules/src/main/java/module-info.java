module de.rpgframework.rules {
	requires transitive de.rpgframework.core;
	requires transitive simple.persist;
	requires java.xml;

	exports de.rpgframework.character;
	exports de.rpgframework.eden.foundry;
	exports de.rpgframework.genericrpg;
	exports de.rpgframework.genericrpg.chargen;
	exports de.rpgframework.genericrpg.chargen.ai;
	exports de.rpgframework.genericrpg.data;
	exports de.rpgframework.genericrpg.export;
	exports de.rpgframework.genericrpg.items;
	exports de.rpgframework.genericrpg.items.formula;
	exports de.rpgframework.genericrpg.modification;
	exports de.rpgframework.genericrpg.persist;
	exports de.rpgframework.genericrpg.requirements;

	opens de.rpgframework.character;
	opens de.rpgframework.genericrpg;
	opens de.rpgframework.genericrpg.data;
	opens de.rpgframework.genericrpg.items;
	opens de.rpgframework.genericrpg.chargen;
	opens de.rpgframework.genericrpg.modification;
	opens de.rpgframework.genericrpg.requirements;
	opens de.rpgframework.genericrpg.items.formula;

}