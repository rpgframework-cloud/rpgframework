package de.rpgframework.genericrpg.persist;

import org.prelle.simplepersist.StringValueConverter;

/**
 * @author prelle
 *
 */
public class StringArrayConverter implements StringValueConverter<String[]> {

	@Override
	public String write(String[] v) throws Exception {
		return String.join(",", v);
	}

	@Override
	public String[] read(String v) throws Exception {
		if (v.contains("|"))
			return v.split("\\|");
		return v.split(",");
	}

}
