/**
 * 
 */
package de.rpgframework.genericrpg.persist;

import java.util.UUID;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.StartElement;

import org.prelle.simplepersist.XMLElementConverter;
import org.prelle.simplepersist.marshaller.XmlNode;
import org.prelle.simplepersist.unmarshal.XMLTreeItem;

import de.rpgframework.genericrpg.data.Decision;

/**
 * @author prelle
 *
 */
public class DecisionConverter implements XMLElementConverter<Decision> {

	//-------------------------------------------------------------------
	/**
	 */
	public DecisionConverter() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.XMLElementConverter#write(org.prelle.simplepersist.marshaller.XmlNode, java.lang.Object)
	 */
	@Override
	public void write(XmlNode node, Decision value) throws Exception {
		node.setAttribute("ref", value.getChoiceUUID().toString());
		node.setAttribute("chosen", value.getValue());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.XMLElementConverter#read(org.prelle.simplepersist.unmarshal.XMLTreeItem, javax.xml.stream.events.StartElement)
	 */
	@Override
	public Decision read(XMLTreeItem node, StartElement ev, XMLEventReader evRd) throws Exception {
		String f1 = ev.getAttributeByName(new QName("ref")).getValue();
		String f2 = ev.getAttributeByName(new QName("chosen")).getValue();
		return new Decision(UUID.fromString(f1), f2);
	}

}
