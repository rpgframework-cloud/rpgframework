package de.rpgframework.genericrpg.persist;

import org.prelle.simplepersist.StringValueConverter;

/**
 * @author prelle
 *
 */
public class IntegerConverter implements StringValueConverter<Integer> {

	@Override
	public String write(Integer value) throws Exception {
		return String.valueOf(value);
	}

	@Override
	public Integer read(String v) throws Exception {
		if (v==null) return null;
		if (v.endsWith(".0"))
			return Integer.valueOf(v.substring(0,v.length()-2));
		if (v.indexOf(".")>-1)
			return Math.round(Float.valueOf(v));
		return Integer.valueOf(v);
	}

}
