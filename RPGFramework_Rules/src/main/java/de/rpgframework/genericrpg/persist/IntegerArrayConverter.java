package de.rpgframework.genericrpg.persist;

import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.StringValueConverter;

/**
 * @author prelle
 *
 */
public class IntegerArrayConverter implements StringValueConverter<int[]> {

	@Override
	public String write(int[] v) throws Exception {
		StringBuffer buf = new StringBuffer();
		for (int i=0; i<v.length; i++) {
			if (i>0)
				buf.append(",");
//			if (v[i]!=0) {
				buf.append(v[i]);
//			}
		}
		return buf.toString();
	}

	@Override
	public int[] read(String v) throws Exception {
		v = v.trim();
		List<Integer> vals = new ArrayList<>();

		StringBuffer buf = new StringBuffer();
		for (int i=0; i<v.length(); i++) {
			char c = v.charAt(i);
			if (c==',') {
				String toParse = buf.toString().trim();
				if (toParse.isBlank() || toParse.isEmpty())
					vals.add(0);
				else
					vals.add(Integer.parseInt(toParse));
				buf = new StringBuffer();
			} else {
				buf.append(c);
			}
		}
		String toParse = buf.toString().trim();
		if (toParse.isBlank() || toParse.isEmpty())
			vals.add(0);
		else
			vals.add(Integer.parseInt(toParse));

		int[] ret = new int[vals.size()];
		for (int i=0; i<vals.size(); i++) ret[i]=vals.get(i);

//		String[] buf = v.trim().split(",");
//		int[] ret = new int[buf.length];
//		for (int i=0; i<buf.length; i++) {
//			buf[i] = buf[i].trim();
//			if (buf[i].isEmpty()) {
//				ret[i]=0;
//			} else
//				ret[i] = Integer.parseInt(buf[i]);
//		}
		return ret;
	}

}
