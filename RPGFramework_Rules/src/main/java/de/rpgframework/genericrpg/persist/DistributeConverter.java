package de.rpgframework.genericrpg.persist;

import java.util.StringTokenizer;

import org.prelle.simplepersist.StringValueConverter;

public class DistributeConverter implements StringValueConverter<Integer[]> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(java.lang.Object)
	 */
	@Override
	public String write(Integer[] v) throws Exception {
		StringBuffer buf = new StringBuffer();
		for (int i=0; i<v.length; i++) {
			buf.append(v[i]+"");
			if ((i+1)<v.length)
				buf.append(" ");
		}
		return buf.toString();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(java.lang.String)
	 */
	@Override
	public Integer[] read(String v) throws Exception {
		StringTokenizer tok = new StringTokenizer(v,", ");
		Integer[] ret = new Integer[tok.countTokens()];
		int pos=0;
			while (tok.hasMoreTokens()) {
				ret[pos++] = Integer.parseInt(tok.nextToken());
			}
		return ret;
	}
}