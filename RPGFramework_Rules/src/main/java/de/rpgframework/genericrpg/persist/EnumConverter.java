package de.rpgframework.genericrpg.persist;

import java.lang.reflect.Method;

import org.prelle.simplepersist.StringValueConverter;

/**
 * @author prelle
 *
 */
@SuppressWarnings("rawtypes")
public class EnumConverter<T extends Enum> implements StringValueConverter<T> {
	
	private Method valueOf;
	
	//-------------------------------------------------------------------
	public EnumConverter(Class<T> e) {
		if (!e.isEnum())
			throw new IllegalArgumentException("Must be an enum");
		try {
			valueOf = e.getMethod("valueOf", String.class);
		} catch (Exception e1) {
			e1.printStackTrace();
			throw new RuntimeException(e1);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(java.lang.Object)
	 */
	@Override
	public String write(T v) throws Exception {
		return v.name();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public T read(String v) throws Exception {
		v = v.trim();
		
		return (T) valueOf.invoke(null, v);
	}

}
