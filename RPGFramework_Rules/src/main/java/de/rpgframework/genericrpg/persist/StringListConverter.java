package de.rpgframework.genericrpg.persist;

import java.util.Arrays;
import java.util.List;

import org.prelle.simplepersist.StringValueConverter;

public class StringListConverter implements StringValueConverter<List<String>> {

	@Override
	public String write(List<String> value) throws Exception {
		return String.join(",", value);
	}

	@Override
	public List<String> read(String v) throws Exception {
		String[] all = v.trim().split(",");
		for (int i=0; i<all.length; i++) {
			all[i] = all[i].strip();
		}
		return Arrays.asList(all);
	}
	
}