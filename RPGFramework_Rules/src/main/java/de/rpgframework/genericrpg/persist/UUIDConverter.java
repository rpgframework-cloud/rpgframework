package de.rpgframework.genericrpg.persist;

import java.util.UUID;

import org.prelle.simplepersist.StringValueConverter;

/**
 * @author prelle
 *
 */
public class UUIDConverter implements StringValueConverter<UUID> {

	@Override
	public String write(UUID value) throws Exception {
		return String.valueOf(value);
	}

	@Override
	public UUID read(String v) throws Exception {
		return UUID.fromString(v);
	}

}
