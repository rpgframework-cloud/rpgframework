package de.rpgframework.genericrpg.persist;

import org.prelle.simplepersist.StringValueConverter;

/**
 * @author prelle
 *
 */
public class FloatConverter implements StringValueConverter<Float> {

	@Override
	public String write(Float value) throws Exception {
		return String.valueOf(value);
	}

	@Override
	public Float read(String v) throws Exception {
		return Float.valueOf(v);
	}

}
