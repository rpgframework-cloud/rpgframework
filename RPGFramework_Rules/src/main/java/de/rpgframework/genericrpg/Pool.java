package de.rpgframework.genericrpg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import de.rpgframework.genericrpg.modification.CheckModification;

/**
 * @author prelle
 *
 */
public class Pool<T> implements Cloneable {

	private Map<ValueType, List<PoolCalculation<T>>> poolsBySet;
	private List<CheckModification> checkMods;

	//-------------------------------------------------------------------
	public Pool() {
		poolsBySet = new HashMap<>();
		checkMods = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	public Pool(T val) {
		poolsBySet = new HashMap<>();
		addStep(ValueType.NATURAL, new PoolCalculation<T>(val, null));
		checkMods = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#clone()
	 */
	public Object clone() {
		Pool<T> cloned = new Pool<T>();
		for (Entry<ValueType, List<PoolCalculation<T>>> entry : poolsBySet.entrySet()) {
			cloned.poolsBySet.put(entry.getKey(), new ArrayList<PoolCalculation<T>>(entry.getValue()));
		}
		return cloned;
	}

	//-------------------------------------------------------------------
	public void addStep(ValueType set, PoolCalculation<T> step) {
		List<PoolCalculation<T>> list = poolsBySet.get(set);
		if (list==null) {
			list = new ArrayList<>();
			poolsBySet.put(set, list);
		}
		list.add(step);
	}

	//-------------------------------------------------------------------
	public boolean hasSet(ValueType set) {
		return poolsBySet.containsKey(set);
	}

	//-------------------------------------------------------------------
	/**
	 * @param set  NATURAL or ARTIFICIAL
	 * @return
	 */
	public List<PoolCalculation<T>> getCalculation(ValueType set) {
		if (poolsBySet.containsKey(set))
			return poolsBySet.get(set);
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @param set  NATURAL or ARTIFICIAL
	 * @return
	 */
	public int getValue(ValueType set) {
		return (int)getCalculation(set).stream().collect(Collectors.summarizingInt(pc -> (int)pc.value)).getSum();
	}

	//-------------------------------------------------------------------
	public boolean isModified() {
		return poolsBySet.containsKey(ValueType.ARTIFICIAL)
				||
				poolsBySet.get(ValueType.NATURAL).size()>1;
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	public String toString() {
		StringBuffer buf = new StringBuffer();
		// Sum Non-Augmented Natural
		int nat = 0;
		int aug = 0;
		if (poolsBySet.get(ValueType.NATURAL) != null) {
			for (PoolCalculation<T> c : poolsBySet.get(ValueType.NATURAL)) {
				if (!c.augment) {
					nat += ((PoolCalculation<Integer>) c).value;
				} else {
					aug += ((PoolCalculation<Integer>) c).value;
				}
			}
			;
		}

		buf.append( String.valueOf( nat));
		if ( aug>0 ) {
			buf.append("(");
//			int sum = poolsBySet.get(ValueType.NATURAL).stream()
//				.mapToInt(pc -> ((PoolCalculation<Integer>)pc).value)
//				.sum();
			buf.append(String.valueOf(nat+aug));
			buf.append(")");
		}

		if (poolsBySet.containsKey(ValueType.ARTIFICIAL)) {
			int sum = poolsBySet.get(ValueType.ARTIFICIAL).stream()
			.mapToInt(pc -> ((PoolCalculation<Integer>)pc).value)
			.sum();
			buf.append("|"+sum);
		}
		return buf.toString();
	}

	//-------------------------------------------------------------------
	public String toExplainString() {
		if (poolsBySet.get(ValueType.NATURAL) == null) return "";
		List<String> ret = poolsBySet.get(ValueType.NATURAL).stream()
				.map(pc -> pc.value+" "+pc.source)
				.collect(Collectors.toList());
		if (poolsBySet.containsKey(ValueType.ARTIFICIAL)) {
			ret.add(" ---");
			ret.addAll(poolsBySet.get(ValueType.ARTIFICIAL).stream()
					.map(pc -> pc.value+" "+pc.source)
					.collect(Collectors.toList()));
		}
		return String.join("\n", ret);
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	public int getNatural() {
		if (poolsBySet.get(ValueType.NATURAL) == null) return 0;
		return poolsBySet.get(ValueType.NATURAL).stream()
				.mapToInt(pc -> ((PoolCalculation<Integer>)pc).value)
				.sum();
	}

	//-------------------------------------------------------------------
	public void addAll(Pool<T> other) {
		for (ValueType type : other.poolsBySet.keySet()) {
			List<PoolCalculation<T>> list = poolsBySet.get(type);
			List<PoolCalculation<T>> otherList = other.poolsBySet.get(type);
			if (list==null) {
				this.poolsBySet.put(type, otherList);
			} else {
				list.addAll(otherList);
			}
		}
	}

	//-------------------------------------------------------------------
	public void addCheckModification(CheckModification mod) {
		checkMods.add(mod);
	}

}
