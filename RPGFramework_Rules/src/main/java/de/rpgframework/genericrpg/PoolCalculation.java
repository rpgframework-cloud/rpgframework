package de.rpgframework.genericrpg;

/**
 * @author prelle
 *
 */
public class PoolCalculation<T> {

	public T value;
	public String source;
	public boolean hitLimit;
	public boolean augment;

	//-------------------------------------------------------------------
	public PoolCalculation(T val, String src) {
		this.value = val;
		this.source= src;
	}

	//-------------------------------------------------------------------
	public PoolCalculation(T val, String src, boolean augment) {
		this.value = val;
		this.source= src;
		this.augment = augment;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() { return value+":"+source+(hitLimit?"*":""); }

}
