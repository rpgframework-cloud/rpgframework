package de.rpgframework.genericrpg;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.rpgframework.genericrpg.data.DataSet;

/**
 * @author prelle
 *
 */
public class LicenseManager {

	private static List<String> globalSets = new ArrayList<>();
	private static ThreadLocal<List<String>> userDataSets = new ThreadLocal<>();

	//-------------------------------------------------------------------
	private static boolean hasGlobalLicense(DataSet set, Locale loc) {
		String key = set.getRules().name()+"/"+set.getID()+"/"+loc.getLanguage();
		return globalSets.contains(key);
	}

	//-------------------------------------------------------------------
	public static void storeGlobalLicenses(List<String> sets) {
		globalSets.addAll(sets);
	}

	//-------------------------------------------------------------------
	public static void storeUserLicensedDatasets(List<String> sets) {
		userDataSets.set(sets);
	}

	//-------------------------------------------------------------------
	private static boolean hasUserLicensed(DataSet set, Locale loc) {
		if (userDataSets.get()==null) return false;
		String key = set.getRules().name()+"/"+set.getID()+"/"+loc.getLanguage();
		return userDataSets.get().contains(key);
	}

	//-------------------------------------------------------------------
	public static boolean hasLicense(DataSet set, Locale loc) {
		return hasGlobalLicense(set,loc) || hasUserLicensed(set,loc);
	}

}
