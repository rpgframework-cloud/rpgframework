package de.rpgframework.genericrpg.export;

import java.util.List;

import de.rpgframework.ConfigOption;
import de.rpgframework.character.RuleSpecificCharacterObject;

/**
 * @author prelle
 *
 */
public interface CharacterExportPlugin<C extends RuleSpecificCharacterObject<?, ?, ?,?>> extends ExportPlugin {

	//--------------------------------------------------------------------
	public String getFileType();

	//--------------------------------------------------------------------
	public byte[] getIcon();

	//--------------------------------------------------------------------
	/**
	 * Return a list (better: tree) of configurable for actions performed
	 * by this plugin (e.g. print options)
	 */
	public List<ConfigOption<?>> getConfiguration();

	//--------------------------------------------------------------------
	public byte[] createExport(C charac);
	
}
