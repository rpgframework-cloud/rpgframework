package de.rpgframework.genericrpg.export;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import de.rpgframework.character.RuleSpecificCharacterObject;
import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
public class ExportPluginRegistry {
	
	private final static Map<RoleplayingSystem, List<ExportPlugin>> pluginsBySystem = new LinkedHashMap<>();

	//-------------------------------------------------------------------
	public static void register(ExportPlugin plugin) {
		List<ExportPlugin> list = pluginsBySystem.get(plugin.getRoleplayingSystem());
		if (list==null) {
			list = new ArrayList<>();
			pluginsBySystem.put(plugin.getRoleplayingSystem(), list);
		}
		
		if (!list.contains(plugin)) {
			list.add(plugin);
		}
	}

	//-------------------------------------------------------------------
	public static List<CharacterExportPlugin> getCharacterExportPlugins(RuleSpecificCharacterObject<?,?, ?, ?> model) {
		List<CharacterExportPlugin> ret = new ArrayList<>();
		List<ExportPlugin> list = pluginsBySystem.get(model.getRules());
		if (list!=null) {
			for (ExportPlugin plugin : list) {
				if (plugin instanceof CharacterExportPlugin)
					ret.add((CharacterExportPlugin) plugin);
			}
		}
		return ret;
	}

}
