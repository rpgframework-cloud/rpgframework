package de.rpgframework.genericrpg.export;

import java.util.Locale;

import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
public interface ExportPlugin {
	
	public RoleplayingSystem getRoleplayingSystem();
	
	public String getName(Locale loc);

}
