package de.rpgframework.genericrpg;

/**
 * @author prelle
 *
 */
public enum DataType {

	RACE,
	CULTURE,
	PROFESSION,
	ATTRIBUTE,
	SKILL,
	RESOURCE,
	/**
	 * Powers, Qualities
	 */
	TRAIT,
	SPELL,
}
