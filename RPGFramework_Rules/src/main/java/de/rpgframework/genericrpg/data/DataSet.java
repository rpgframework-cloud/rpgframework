package de.rpgframework.genericrpg.data;

import java.lang.System.Logger;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.core.CustomResourceManagerLoader;
import de.rpgframework.core.RoleplayingSystem;

/**
 * You need to implement this class to extend the functionality of RPGFramework
 * applications for specific rulesets.
 *
 *
 * @author prelle
 *
 */
public class DataSet {

	public static enum DataSetType {
		// Core Rulebooks
		RULES,
		// Additional rules
		OPT_RULES,
		LOCATION,
		BACKGROUND,
		OTHER
	}

	protected final static Logger logger = System.getLogger(DataSet.class.getPackageName());

	private MultiLanguageResourceBundle resources;
	private transient String bundleID ;

	private String id;
	private RoleplayingSystem rules;
	private List<Locale> languages;
	private DataSetType type = DataSetType.OTHER;
	private int released = 209999;

	//--------------------------------------------------------------------
	public DataSet(Object parent, RoleplayingSystem rules, String id, String resourcePrefix, Locale... locales) {
		this.id = id;
		this.rules = rules;
		if (resourcePrefix!=null)
			this.bundleID = resourcePrefix+"."+id.toLowerCase();
		else
			this.bundleID = id.toLowerCase();
		languages = Arrays.asList(locales); //.stream().map(loc -> loc.getDisplayLanguage()).collect(Collectors.toList());

		try {
			if (parent!=null)
				resources = new MultiLanguageResourceBundle(parent.getClass(), bundleID, locales);
			else
				resources = new MultiLanguageResourceBundle(bundleID, locales);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
	}

	//--------------------------------------------------------------------
	public DataSet(Object parent, RoleplayingSystem rules, String id, String resourcePrefix, MultiLanguageResourceBundle res, Locale... locales) {
		this.id = id;
		this.rules = rules;
		this.bundleID = resourcePrefix+"."+id;
		languages = Arrays.asList(locales); //.stream().map(loc -> loc.getDisplayLanguage()).collect(Collectors.toList());

		resources = res;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "dataset:"+id;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the bundleID
	 */
	public String getBundleID() {
		return bundleID;
	}

	//-------------------------------------------------------------------
	public String getName(Locale locale) {
		return getResourceString("dataset.name", locale);
	}

	//-------------------------------------------------------------------
	public String getShortName(Locale locale) {
		return getResourceString("dataset.name.short", locale);
	}

	//-------------------------------------------------------------------
	public String getDescription(Locale locale) {
		return getResourceString("dataset.desc", locale);
	}

	//--------------------------------------------------------------------
	/**
	 * Returns an identifier uniquely identifying the plugin within
	 * a RoleplayingSystem. The identifier "CORE" is reserved for the
	 * core rules. For everything else an abbreviated product name
	 * in capital letters is recommended.
	 */
	public String getID() {
		return id;
	}

	//--------------------------------------------------------------------
	/**
	 * Identify the type of rules to support.
	 */
	public RoleplayingSystem getRules() {
		return rules;
	}

	//--------------------------------------------------------------------
	/**
	 * Return a list of supported language codes
	 */
	public List<Locale> getLocales() {
		return languages;
	}

	// --------------------------------------------------------------------
	public String getResourceString(String key, Locale locale) {
		if (resources == null) {
			System.err.println("No MultiLanguageResourceBundle set for " + id);
			return key;
		}

		return resources.getString(key, locale);
	}

	// --------------------------------------------------------------------
	public String getResourceString(List<String> keys, Locale locale) {
		if (resources == null) {
			System.err.println("No MultiLanguageResourceBundle set for " + id);
			return "?"+keys+"?";
		}
		// Check for a user defined property
		if (CustomResourceManagerLoader.getInstance()!=null) {
			for (String key : keys) {
				String custom = CustomResourceManagerLoader.getInstance().getProperty(rules, key, locale);
				if (custom!=null)
					return custom;
			}
		}
		return resources.getString(keys, locale);
	}

	//--------------------------------------------------------------------
	public String getBaseBundleName() {
		return resources.getBaseBundleName();
	}

	//--------------------------------------------------------------------
	public void flushMissingKeys() {
		resources.flushMissingKeys();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public DataSetType getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @param type the type to set
	 */
	public void setType(DataSetType type) {
		this.type = type;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the released
	 */
	public int getReleased() {
		return released;
	}

	//-------------------------------------------------------------------
	/**
	 * @param released the released to set
	 */
	public void setReleased(int released) {
		this.released = released;
	}

}
