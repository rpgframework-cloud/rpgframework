package de.rpgframework.genericrpg.data;

import de.rpgframework.genericrpg.modification.ModifiedObjectType;

/**
 * @author prelle
 *
 */
public class ReferenceError {

	private ModifiedObjectType type;
	private String reference;

	//-------------------------------------------------------------------
	public ReferenceError(ModifiedObjectType type, String ref) {
		this.type = type;
		this.reference = ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return ("Invalid reference to "+type+" '"+reference+"'");
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public ModifiedObjectType getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @param type the type to set
	 */
	public void setType(ModifiedObjectType type) {
		this.type = type;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the reference
	 */
	public String getReference() {
		return reference;
	}

	//-------------------------------------------------------------------
	/**
	 * @param reference the reference to set
	 */
	public void setReference(String reference) {
		this.reference = reference;
	}
}
