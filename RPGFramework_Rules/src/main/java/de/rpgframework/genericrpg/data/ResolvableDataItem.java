package de.rpgframework.genericrpg.data;

/**
 * @author prelle
 *
 */
public interface ResolvableDataItem<T extends DataItem> {
	
	//-------------------------------------------------------------------
	/**
	 * Return the identifier of the DataItem connected with this
	 * value.
	 * @return
	 */
	public String getKey();

	//-------------------------------------------------------------------
	public void setResolved(T value);

	//-------------------------------------------------------------------
	public T getResolved();

}
