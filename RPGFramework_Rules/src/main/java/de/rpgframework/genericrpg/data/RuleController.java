package de.rpgframework.genericrpg.data;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.genericrpg.chargen.Rule;
import de.rpgframework.genericrpg.chargen.RuleConfiguration;
import de.rpgframework.genericrpg.chargen.RuleInterpretation;
import de.rpgframework.genericrpg.chargen.RuleValue;

/**
 * @author prelle
 *
 */
public class RuleController {

	private final static Logger logger = System.getLogger(RuleController.class.getName()+".rules");

	private CommonCharacter<?, ?, ?, ?> model;
	private Rule[] allRules;
	private List<RuleInterpretation> interpretations;

	private Map<Rule, RuleValue> effectiveRules;

	//-------------------------------------------------------------------
	public RuleController(CommonCharacter<?, ?, ?, ?> model, List<RuleInterpretation> interpretations, Rule[] allRules) {
		this.model = model;
		this.allRules = allRules;
		this.interpretations = interpretations;
		effectiveRules = new LinkedHashMap<>();
		updateEffectiveRules();
	}

	//-------------------------------------------------------------------
	public void setModel(CommonCharacter<?,?,?,?> model) {
		this.model = model;
		updateEffectiveRules();
	}

	//-------------------------------------------------------------------
	public void set(Rule rule, String value) {
		RuleValue rVal = getRule(rule);
		if (!rVal.isEditable())
			throw new IllegalArgumentException("Rule "+rule.getID()+" not editable");

		model.addRule(rule, value);
		updateEffectiveRules();
	}

//	//-------------------------------------------------------------------
//	public RuleConfiguration getRuleValue(Rule rule) {
//		for (RuleConfiguration rv : rules) {
//			if (rv.getRuleId().equals(rule.getID()))
//				return rv;
//		}
//		return null;
//	}
	//-------------------------------------------------------------------
	public boolean getRuleValueAsBoolean(Rule rule) {
		if (!effectiveRules.containsKey(rule))
			return rule.getDefaultAsBooleanValue();
		if (effectiveRules.get(rule).getValue() instanceof String)
			return Boolean.parseBoolean( (String)effectiveRules.get(rule).getValue());
		return (Boolean) effectiveRules.get(rule).getValue();
//		for (RuleConfiguration rv : rules) {
//			if (rv.getRuleId().equals(rule.getID())) {
//				try {
//					return Boolean.parseBoolean(rv.getValueString());
//				} catch (Exception e) {
//					logger.log(Level.WARNING, "Invalid entry for rule " + rule,e);
//				}
//				return Boolean.parseBoolean(rule.getDefaultValue());
//			}
//		}
//		return false;
	}
	//-------------------------------------------------------------------
	public int getRuleValueAsInteger(Rule rule) {
		return (Integer) effectiveRules.get(rule).getValue();
	}

	//-------------------------------------------------------------------
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <E extends Enum> E getRuleValueAsEnum(Rule rule) {
		return (E) effectiveRules.get(rule).getValue();
//		for (RuleConfiguration rv : rules) {
//			if (rv.getRuleId().equals(rule.getID())) {
//				Class<Enum> enumType = rule.getEnumClassToUse();
//				try {
//					return (E) Enum.valueOf(enumType, rv.getValueString());
//
//				} catch (Exception e) {
//					System.getLogger(getClass().getPackageName()).log(Level.WARNING, "Invalid entry for rule " + rule,e);
//				}
//				return (E) Enum.valueOf(enumType, rule.getDefaultValue());
//			}
//		}
//		return rule.getDefaultAsEnumValue();
	}

	//-------------------------------------------------------------------
	public void setRuleValue(Rule rule, Object value) {
		RuleValue rVal = effectiveRules.get(rule);
		if (!rVal.isEditable())
			throw new IllegalStateException("Rule "+rule.getID()+" not editable");

		rVal.setValue(value);
		String sVal = rule.encodeString(value);
		model.setRuleValue(rule, sVal);
		effectiveRules.put(rule, rVal);

		// mark all items dirty
		model.getCarriedItems().forEach(ci -> ci.setDirty(true));
	}

//	//-------------------------------------------------------------------
//	public void clearRuleValue(Rule rule) {
//		RuleConfiguration old = getRuleValue(rule);
//		if (old!=null)
//			rules.remove(old);
//	}

	//-------------------------------------------------------------------
	private Rule getRule(String id) {
		for (Rule rule : allRules) {
			if (rule.getID().equals(id))
				return rule;
		}
		return null;
	}

	//-------------------------------------------------------------------
	private RuleInterpretation getRuleInterpretation(String id) {
		for (RuleInterpretation rule : interpretations) {
			if (rule.getId().equals(id))
				return rule;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public RuleValue getRule(Rule rule) {
		for (Entry<Rule, RuleValue> entry : effectiveRules.entrySet()) {
			if (entry.getKey()==rule) {
				return entry.getValue();
			}
		}
		return null;
	}

	//-------------------------------------------------------------------
	public void updateEffectiveRules() {
		logger.log(Level.DEBUG, "ENTER updateEffectiveRules for "+this);
//		try {
//			throw new RuntimeException("Trace");
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		try {
			effectiveRules.clear();
			// Use hardcoded defaults first
			for (Rule rule : allRules) {
				RuleValue rVal = new RuleValue(rule);
				effectiveRules.put(rule, rVal);
				logger.log(Level.DEBUG, "start with {0}",rVal);
			}

			// Apply settings from character
			if (model != null) {
				for (RuleConfiguration rc : new ArrayList<>(model.getRuleValues())) {
					Rule rule = getRule(rc.getRuleId());
					// clean up unknown rule settings
					if (rule == null) {
						model.getRuleValues().remove(rc);
					} else {
						// Overwrite default with setting from char
						RuleValue rv = getRule(rule);
						try {
							rv.setValue(rule.parseValue(rc.getValueString()));
							logger.log(Level.DEBUG, "stored in character: "+rv);
						} catch (Exception e) {
							logger.log(Level.ERROR, "Cannot parse '"+rc.getValueString()+"' for rule "+rule,e);
							model.getRuleValues().remove(rc);
							BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 1, "I found an illegal value for rule "+rule.getID()+" in this character and removed it");
						}
					}
				}

				// Now check for chosen rule interpretation
				// If necessary, overwrite
				if (model.getStrictness() != null) {
					RuleInterpretation inter = getRuleInterpretation(model.getStrictness());
					if (inter == null) {
						logger.log(Level.ERROR, "Character uses an unknown rule interpretation: " + model.getStrictness());
					} else {
						for (RuleConfiguration set : inter.getRules()) {
							Rule rule = getRule(set.getRuleId());
							RuleValue rv = getRule(rule);
							rv.setValue(rule.parseValue(set.getValueString()));
							// If set in strictness, remove from character
							model.clearRuleValue(rule);
							rv.setEditable(false);
							logger.log(Level.DEBUG, "by strictness: "+rv);
						}
					}
				}
			}
		} finally {
			logger.log(Level.DEBUG, "LEAVE updateEffectiveRules");
		}
	}

	//-------------------------------------------------------------------
	public List<RuleValue> getValues() {
		return new ArrayList<RuleValue>(effectiveRules.values());
	}

}
