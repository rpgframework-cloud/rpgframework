package de.rpgframework.genericrpg.data;

import java.util.List;

/**
 * 
 */
public class CustomDataSetHandle {
	
	public static record DataSetEntry<E extends DataItem> (
			String key, 
			Class<E> clazz, 
			Class<? extends List<E>> listClazz) {
		
	}
	
	private DataSet dataset;
	private List<DataSetEntry<?>> orderedFileKeys;

	//-------------------------------------------------------------------
	public CustomDataSetHandle(DataSet set, List<DataSetEntry<?>> orderedFileKeys) {
		this.dataset = set;
		this.orderedFileKeys = orderedFileKeys;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the name
	 */
	public DataSet getName() {
		return dataset;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the orderedFileKeys
	 */
	public List<DataSetEntry<?>> getOrderedFileKeys() {
		return orderedFileKeys;
	}

}
