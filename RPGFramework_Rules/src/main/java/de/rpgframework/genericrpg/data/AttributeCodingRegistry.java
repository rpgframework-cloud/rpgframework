package de.rpgframework.genericrpg.data;

import java.util.HashMap;
import java.util.Map;

import org.prelle.simplepersist.StringValueConverter;

import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.genericrpg.modification.ModifiedObjectType;

/**
 * The point of this registry is to store converters that decode a string (from XML)
 * to a object value and reverse.
 * 
 * @author prelle
 *
 */
public class AttributeCodingRegistry {
	
	private static class FormulaKey {
		RoleplayingSystem rules;
		ModifiedObjectType type;
		String key;
		//-------------------------------------------------------------------
		public FormulaKey(RoleplayingSystem r, ModifiedObjectType t, String k) {
			this.rules = r;
			this.type  = t;
			this.key   = k;
		}
		//-------------------------------------------------------------------
		public String toString() {
			return rules+":"+type+":"+key;
		}
		//-------------------------------------------------------------------
		/**
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		public boolean equals(Object o) {
			if (o instanceof FormulaKey) {
				FormulaKey other = (FormulaKey)o;
				if (rules!=other.rules) return false;
				if (type!=other.type) return false;
				return key.equals(other.key);
			}
			return false;
		}
	}
	
	private static Map<FormulaKey, StringValueConverter<?>> converters = new HashMap<>();

	//-------------------------------------------------------------------
	public static void addConverter(RoleplayingSystem rules, ModifiedObjectType type, String attrib, StringValueConverter<?> conv) {
		FormulaKey key = new FormulaKey(rules, type, attrib);
		converters.put(key, conv);
	}

	//-------------------------------------------------------------------
	public static  StringValueConverter<?> getConverter(RoleplayingSystem rules, ModifiedObjectType type, String attrib) {
		FormulaKey key = new FormulaKey(rules, type, attrib);
		return converters.get(key);
	}

}
