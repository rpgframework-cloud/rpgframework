package de.rpgframework.genericrpg.data;

/**
 * @author prelle
 *
 */
public class SkillSpecializationValue<S extends ISkill> extends ComplexDataItemValue<SkillSpecialization<S>> {

	//-------------------------------------------------------------------
	public SkillSpecializationValue() {}

	//-------------------------------------------------------------------
	public SkillSpecializationValue(SkillSpecialization<S> resolved) {
		this.ref = resolved.getId();
		this.resolved = resolved;
	}

	//-------------------------------------------------------------------
	public SkillSpecializationValue(SkillSpecialization<S> resolved, int val) {
		super(resolved, val);
		this.ref = resolved.getId();
		this.resolved = resolved;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return ref+":"+getDistributed()+"("+incomingModifications+")";
	}

}
