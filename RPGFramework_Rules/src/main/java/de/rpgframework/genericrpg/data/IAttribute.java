/**
 * 
 */
package de.rpgframework.genericrpg.data;

import java.util.Locale;

/**
 * @author Stefan
 *
 */
public interface IAttribute {

	public String getName(Locale locale);
	public String getName();
	public String getShortName(Locale locale);
	
	public boolean isDerived();

}
