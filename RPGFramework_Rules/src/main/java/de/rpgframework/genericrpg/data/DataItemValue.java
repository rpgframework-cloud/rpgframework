package de.rpgframework.genericrpg.data;

import java.util.Locale;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;

import de.rpgframework.genericrpg.NumericalValue;
import de.rpgframework.genericrpg.SelectedValue;
import de.rpgframework.genericrpg.modification.DataItemModification;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModifyableImpl;

/**
 * @author prelle
 *
 */
public abstract class DataItemValue<T extends DataItem>
	extends ModifyableImpl
	implements NumericalValue<T>, ResolvableDataItem<T>, SelectedValue<T> {

	@Attribute(required=true)
	protected String ref;
	/**
	 * For items that have different language versions, this field stores
	 * the language used when selecting
	 */
	@Attribute(required=false,name="lang")
	protected String language = Locale.getDefault().getLanguage();

	protected transient T resolved;
	/** The character this item belongs to */
	protected transient CommonCharacter<?, ?, ?, ?> character;

	@Attribute(name="value")
	protected int value;

 	@Element
 	protected String customName;
	/** For packs, this is the pack item */
	protected transient Object injectedBy;

	//-------------------------------------------------------------------
	public DataItemValue() {
	}

	//-------------------------------------------------------------------
	public DataItemValue(T data) {
		this();
		this.ref = data.getId();
		this.resolved = data;
	}

	//-------------------------------------------------------------------
	public DataItemValue(T data, int val) {
		this(data);
		value = val;
	}

	//-------------------------------------------------------------------
	/**
	 * Return the identifier of the ComplexDataItem connected with this
	 * value.
	 * @return
	 * @see de.rpgframework.genericrpg.data.ResolvableDataItem#getKey()
	 */
	public String getKey() {
		return ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.ResolvableDataItem#setResolved(de.rpgframework.genericrpg.data.DataItem)
	 */
	public void setResolved(T value) {
		if (value==null) throw new NullPointerException();
		this.resolved = value;
		ref = (value!=null)?value.getId():null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.ResolvableDataItem#getResolved()
	 */
	public T getResolved() {
		return resolved;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValue#getDistributed()
	 */
	@Override
	public int getDistributed() {
		return value;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValue#setDistributed(int)
	 */
	@Override
	public void setDistributed(int points) {
		this.value = points;
	}

	//-------------------------------------------------------------------
	public boolean isAutoAdded() {
		if (getInjectedBy()!=null)
			return true;
		for (Modification mod : incomingModifications) {
			if (mod instanceof DataItemModification) {
				DataItemModification tmp = (DataItemModification)mod;
				if (tmp.getResolvedKey()==resolved)
					return true;
			}
		}
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectedValue#getModifyable()
	 */
	@Override
	public T getModifyable() {
		return getResolved();
	}

	//-------------------------------------------------------------------
	public String getNameWithoutDecisions(Locale loc) {
		if (customName!=null)
			return customName;
		if (resolved==null)
			return ref;
		return resolved.getName(loc);
	}

	//-------------------------------------------------------------------
	public String getNameWithoutRating(Locale loc) {
		if (customName!=null)
			return customName;
		if (resolved==null)
			return "?"+ref;
		return resolved.getName(loc);
	}

	//-------------------------------------------------------------------
	public String getNameWithoutRating() {
		return getNameWithoutRating(Locale.getDefault());
	}

	//-------------------------------------------------------------------
	public String getNameWithRating(Locale loc) {
		if (resolved==null)
			return ref+" "+value;
		if (value==0) return resolved.getName(loc);
		return resolved.getName(loc)+" "+value;
	}

	//-------------------------------------------------------------------
	public String getNameWithRating() {
		return getNameWithRating(Locale.getDefault());
	}

	//--------------------------------------------------------------------
	public String getShortName(Locale locale) {
		if (resolved==null)
			return ref;
		return resolved.getShortName(locale);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the character
	 */
	public CommonCharacter<?, ?, ?, ?> getCharacter() {
		return character;
	}

	//-------------------------------------------------------------------
	/**
	 * @param character the character to set
	 */
	public void setCharacter(CommonCharacter<?, ?, ?, ?> character) {
		this.character = character;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the injectedBy
	 */
	public Object getInjectedBy() {
		return injectedBy;
	}

	//-------------------------------------------------------------------
	/**
	 * @param injectedBy the injectedBy to set
	 */
	public void setInjectedBy(Object injectedBy) {
		this.injectedBy = injectedBy;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the customName
	 */
	public String getCustomName() {
		return customName;
	}

	//-------------------------------------------------------------------
	/**
	 * @param customName the customName to set
	 */
	public void setCustomName(String customName) {
		this.customName = customName;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	//-------------------------------------------------------------------
	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

}
