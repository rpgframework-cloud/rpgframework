package de.rpgframework.genericrpg.data;

import java.io.InputStream;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;

import org.prelle.simplepersist.Attribute;

import de.rpgframework.core.CustomResourceManagerLoader;
import de.rpgframework.genericrpg.LicenseManager;
import de.rpgframework.genericrpg.items.PieceOfGearVariant;

/**
 * @author prelle
 *
 */
public abstract class DataItem {

	protected final static Logger logger = System.getLogger(DataItem.class.getPackageName());

	protected List<PageReference> parentSet;

	/** An identifier within type */
	@Attribute(required=true)
	protected String id;
	/**
	 * Defines if this DataItem is only to be used for this language.
	 * This helps to model situations when there is a default and a
	 * deviant definition for a different language
	 */
	@Attribute(required=false)
	protected String lang;

	protected transient DataItem parentItem;
	/* Datasets this item is used in */
	protected transient List<DataSet> datasets = new ArrayList<>();

	protected transient Map<String, DataItem> languageAlternatives;

	//-------------------------------------------------------------------
	protected DataItem() {
		parentSet = new ArrayList<>();
		languageAlternatives = new HashMap<>();
	}

	//-------------------------------------------------------------------
	public void assignToDataSet(DataSet set) {
		if (datasets.contains(set)) {
			logger.log(Level.WARNING, "Already assigned {0} to dataset {1}", id, set);
			return;
		}
		datasets.add(set);

		// Find page references
		String key = getTypeString()+"."+id.toLowerCase()+".page";
		String key2 = getTypeString()+"."+id.toLowerCase();
		String key3 = getTypeString()+"."+id.toLowerCase()+".prodname";
		if (parentItem!=null) {
			key = parentItem.getTypeString()+"."+parentItem.getId().toLowerCase()+"."+key;
			key2 = parentItem.getTypeString()+"."+parentItem.getId().toLowerCase()+"."+key2;
			key3 = parentItem.getTypeString()+"."+parentItem.getId().toLowerCase()+"."+key3;
		}
		for (Locale locale : set.getLocales()) {
			// Check if property is present
			set.getResourceString(key2, locale);
			int page = 0;
			if (!(this instanceof PieceOfGearVariant)) {
				// Only expect .page if this is not a variant
				try {
					String pageS = set.getResourceString(key,locale);
					page = Integer.valueOf(pageS);
				} catch (NumberFormatException nfe) {
					// Ignore invalid page data
//					logger.log(Level.ERROR, "No page data in key "+key+"\t  for locale "+locale+" in "+set.getBaseBundleName());
				} catch (MissingResourceException mre) {
					logger.log(Level.ERROR, "Missing resource  "+mre.getKey()+"\t  for locale "+locale+" in "+set.getBaseBundleName());
				}
			}
			PageReference pageRef = new PageReference(set, page, locale.getLanguage());
			// Check if there is a special product name only for this item
			String prodName = set.getResourceString(key3, locale);
			if (prodName!=null && !prodName.endsWith(".prodname")) {
				pageRef.setOverwrittenProductName(prodName);
			}
			parentSet.add(pageRef);
			getName(locale);
			getDescription(locale);
		}
	}

	//-------------------------------------------------------------------
	public Collection<DataSet> getAssignedDataSets() {
		return datasets;
	}

	//-------------------------------------------------------------------
	public boolean inDataSets(Collection<DataSet> sets) {
		return parentSet.stream().anyMatch(pr -> sets.contains(pr.getProduct()));
	}

	//-------------------------------------------------------------------
	public String getTypeString() {
		if (getClass().getAnnotation(DataItemTypeKey.class)==null)
			throw new IllegalArgumentException("Missing @DataItemTypeKey in "+getClass());
		return getClass().getAnnotation(DataItemTypeKey.class).id();
//		return type;
	}

	//-------------------------------------------------------------------
	public String getId() {
		return id;
	}
	public void setId(String value) { this.id = value; }

	//-------------------------------------------------------------------
	public String toString() {
		if (lang!=null)
			return getTypeString()+":"+id+"("+lang+")";
		return getTypeString()+":"+id;
	}

	//--------------------------------------------------------------------
	/**
	 * Use getName(Locale)
	 */
	public String getName() {
		return getName(Locale.getDefault());
	}

	//--------------------------------------------------------------------
	public void setParentItem(DataItem parentItem) {
		this.parentItem = parentItem;
	}

	//--------------------------------------------------------------------
	public DataSet getFirstParent(Locale locale) {
		if (locale==null)
			throw new NullPointerException("Locale may not by null");
		PageReference defaultRef = null;
		for (PageReference set : parentSet) {
			if (defaultRef==null)
				defaultRef = set;
			if (set.getLanguage()!=null && set.getLanguage().equals(locale.getLanguage()))
				return set.getProduct();
			if (set.getLanguage()==null || set.getLanguage().equals(Locale.getDefault().getLanguage()))
				defaultRef = set;;
		}
		if (defaultRef==null && parentItem!=null) {
			for (PageReference set : parentItem.getPageReferences()) {
				if (defaultRef==null)
					defaultRef = set;
				if (set.getLanguage()!=null && set.getLanguage().equals(locale.getLanguage())) {
					return set.getProduct();
				}
				if (set.getLanguage()==null || set.getLanguage().equals(Locale.getDefault().getLanguage()))
					defaultRef = set;;
			}
		}
		if (defaultRef==null) {
//			logger.log(Level.WARNING, "No dataset for  locale "+locale+" supported for "+(getTypeString()+"."+id.toLowerCase())+"\nParent item: "+parentItem);
			return null;
		}

//		logger.log(Level.WARNING, "No locale "+locale+" supported for "+(getTypeString()+"."+id.toLowerCase())+" - using default");
		return defaultRef.getProduct();
	}

	//--------------------------------------------------------------------
	public String getLocalizedString(Locale locale, String key) {
		if (parentItem!=null)
			return parentItem.getLocalizedString(locale, key);

		if (parentSet==null) {
			System.err.println("No parent dataset for "+getTypeString()+":"+id);
			return key;
		}
		if (locale==null) {
			throw new NullPointerException("Locale was NULL");
		}
		getFirstParent(locale);
		DataSet set = null;
		int where = 0;
		try {
			set = getFirstParent(locale);
			if (set==null)
				return "?No ParentSet?"+key;
			// Check for a user defined property
			if (CustomResourceManagerLoader.getInstance()!=null) {
				String custom = CustomResourceManagerLoader.getInstance().getProperty(set.getRules(), key, locale);
				if (custom!=null)
					return custom;
			}
			where++;
			return set.getResourceString(key,locale);
		} catch (MissingResourceException mre) {
			if (where==0)
				logger.log(Level.ERROR, mre.toString());
			else
				logger.log(Level.ERROR, "Missing resource  "+mre.getKey()+"\t  for locale "+locale+" in "+set.getBaseBundleName());
			return id;
		}
	}

	//--------------------------------------------------------------------
	public InputStream getResource(String name) {
		for (PageReference ref : parentSet) {
			DataSet set = ref.getProduct();
			String resourceName = "org.prelle.rpgframework.splittermond.data." + set.getID().toLowerCase()+"."+name;
			InputStream ins = ClassLoader.getSystemResourceAsStream(resourceName);
			logger.log(Level.ERROR, "Search image resource {0} = {1}",resourceName, ins);
			resourceName = "org/prelle/rpgframework/splittermond/data/" + set.getID().toLowerCase()+"/"+name;
			ins = ClassLoader.getSystemResourceAsStream(resourceName);
			logger.log(Level.ERROR, "Search image resource {0} = {1}",resourceName, ins);
			if (ins!=null)
				return ins;
		}
		return null;
	}

	//--------------------------------------------------------------------
	public String getLocalizedString(Locale locale, List<String> keys) {
		if (parentSet==null) {
			System.err.println("No parent dataset for "+getTypeString()+":"+id);
			return keys.toString();
		}
		if (locale==null) {
			throw new NullPointerException("Locale was NULL");
		}
		getFirstParent(locale);
		DataSet set = null;
		int where = 0;
		try {
			set = getFirstParent(locale);
			if (set==null)
				return "?No ParentSet?";
			where++;
			String localized = set.getResourceString(keys,locale);
			if (localized==null) {
				logger.log(Level.ERROR, "Missing one of the follwing resources  "+keys+"\t  for locale "+locale+" in "+set.getBundleID());
				return "?No_Key_Found?";
			}
			return localized;
		} catch (MissingResourceException mre) {
			if (where==0)
				logger.log(Level.ERROR, mre.toString());
			else
				logger.log(Level.ERROR, "Missing one of the follwing resources  "+keys+"\t  for locale "+locale+" in "+set.getBaseBundleName());
			return id;
		}
	}

	//--------------------------------------------------------------------
	public String getName(Locale locale) {
		if (parentItem!=null) {
			String key = parentItem.getTypeString()+"."+parentItem.getId().toLowerCase()+"."+getTypeString()+"."+id.toLowerCase();
			return getLocalizedString(locale, key);
		}
		String key = getTypeString()+"."+id.toLowerCase();
		return getLocalizedString(locale, key);
	}

	//--------------------------------------------------------------------
	public String getShortName(Locale locale) {
		String key = getTypeString()+"."+id.toLowerCase()+".short";
		List<String> keys = new ArrayList<>();
		if (parentItem!=null) {
			keys.add(parentItem.getTypeString()+"."+parentItem.getId().toLowerCase()+"."+key);
		}
		keys.add(key);
		String result = getLocalizedString(locale, keys);
		if ("?No_Key_Found?".equals(result)) return null;
		return result;
	}

	//--------------------------------------------------------------------
	/**
	 * Use getDescription(Locale)
	 */
	public String getDescription() {
		return getDescription(Locale.getDefault());
	}

	// --------------------------------------------------------------------
	public boolean hasLicense() {
		return hasLicense(Locale.getDefault());
	}

	// --------------------------------------------------------------------
	public boolean hasLicense(Locale locale) {
		if (parentSet == null) {
			System.err.println("No parent dataset for " + getTypeString() + ":" + id);
			return false;
		}

		// Check license
		DataSet set = null;
		set = getFirstParent(locale);
		if (set == null)
			return false;
		if (!LicenseManager.hasLicense(set,locale))
			return false;
		return true;
	}

	//--------------------------------------------------------------------
	public String getDescription(Locale locale) {
		String key = getTypeString()+"."+id.toLowerCase()+".desc";
		if (parentSet==null) {
			System.err.println("No parent dataset for "+getTypeString()+":"+id);
			return key;
		}
		if (parentItem!=null) {
			key = parentItem.getTypeString()+"."+parentItem.getId().toLowerCase()+"."+key;
		}

		// Check license
		DataSet set = null;
		int where = 0;
		try {
			set = getFirstParent(locale);
			if (set==null)
				return "?No ParentSet?";
			// Check for a user defined property
			if (CustomResourceManagerLoader.getInstance()!=null) {
				String custom = CustomResourceManagerLoader.getInstance().getProperty(set.getRules(), key, locale);
				if (custom!=null)
					return custom;
			}

			where++;
			if (!LicenseManager.hasLicense(set,locale))
				return GenericRPGTools.RES.format("warning.no_license", set.getName(locale), locale.getDisplayLanguage(locale));

			String foo = set.getResourceString(key,locale);
			return foo;
		} catch (MissingResourceException mre) {
			mre.printStackTrace();
			if (where==0)
				logger.log(Level.ERROR, mre.toString());
			else
				logger.log(Level.ERROR, "Missing resource  "+mre.getKey()+"\t  for locale "+locale+" in "+set.getBaseBundleName());
			return id;
		}
	}

	//-------------------------------------------------------------------
	public String getLanguage() {
		return lang;
	}

	//-------------------------------------------------------------------
	/**
	 * Used in deriving classes to perform validation checks on loading,
	 * if necessary
	 * @return Error message or NULL
	 */
	public void validate() throws DataErrorException {
	}

	//-------------------------------------------------------------------
	/**
	 * @return the parentSet
	 */
	public List<PageReference> getPageReferences() {
		if (parentItem!=null)
			return parentItem.getPageReferences();
		return parentSet;
	}

	//-------------------------------------------------------------------
	public void addLanguageAlternative(String lang, DataItem alt) {
		languageAlternatives.put(lang, alt);
	}

	//-------------------------------------------------------------------
	public boolean hasLanguageAlternative(String lang) {
		return languageAlternatives.containsKey(lang);
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	public <T extends DataItem> T getLanguageAlternative(String lang) {
		return (T) languageAlternatives.get(lang);
	}

	//-------------------------------------------------------------------
	public Collection<DataItem> getLanguageAlternatives() {
		return languageAlternatives.values();
	}

}
