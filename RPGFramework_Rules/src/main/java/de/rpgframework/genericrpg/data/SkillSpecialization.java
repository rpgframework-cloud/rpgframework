package de.rpgframework.genericrpg.data;

import java.util.List;

import org.prelle.simplepersist.Attribute;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id="skillspec")
public class SkillSpecialization<S extends ISkill> extends ComplexDataItem {

	@Attribute(required=false)
	protected transient S skill;
	@Attribute(required=false)
	protected String subtypes;
	@Attribute(name="attr",required=false)
	protected String attribute;

	//-------------------------------------------------------------------
	public SkillSpecialization() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	public SkillSpecialization(S skill, String id) {
		this.skill = skill;
		this.id    = id;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the skill
	 */
	public S getSkill() {
		return skill;
	}

	//-------------------------------------------------------------------
	/**
	 * @param skill the skill to set
	 */
	public void setSkill(S skill) {
		this.skill = skill;
	}

	//-------------------------------------------------------------------
	public String getTypeString() {
		if (getClass().getAnnotation(DataItemTypeKey.class)==null)
			throw new IllegalArgumentException("Missing @DataItemTypeKey in "+getClass());
		if (skill==null)
			return "skill.<unknown>."+getClass().getAnnotation(DataItemTypeKey.class).id();
		return "skill."+skill.getId()+"."+getClass().getAnnotation(DataItemTypeKey.class).id();
//		return type;
	}

	//--------------------------------------------------------------------
//	public String getName(Locale locale) {
//		String key = getTypeString()+"."+id.toLowerCase();
//		if (skill!=null) {
//			key = "skill."+skill.getId()+"."+id.toLowerCase();
//		}
//		return getLocalizedString(locale, key);
//	}

	//-------------------------------------------------------------------
	public List<String> getSubtypeKeys() {
		if (subtypes==null) return List.of();
		return List.of(subtypes.split(","));
	}

	//-------------------------------------------------------------------
	public String getAttribute() {
		return attribute;
	}

}
