package de.rpgframework.genericrpg.data;

/**
 * @author prelle
 *
 */
public class CustomDataSetManagerLoader {

	private static CustomDataSetManager charProv;

	//--------------------------------------------------------------------
	public static CustomDataSetManager getInstance() {
			return charProv;
	}

	//--------------------------------------------------------------------
	public static void setResourceManager(CustomDataSetManager service) {
		charProv = service;
	}

}
