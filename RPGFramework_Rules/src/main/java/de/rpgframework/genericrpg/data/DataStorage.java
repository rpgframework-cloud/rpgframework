package de.rpgframework.genericrpg.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author prelle
 *
 */
public class DataStorage<V extends DataItem> implements DataProvider<V> {

	private String type;
	private Map<String, V> items;

	//-------------------------------------------------------------------
	public DataStorage(String type) {
		this.type = type;
		items = new HashMap<String, V>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.DataProvider#getSupportedDataType()
	 */
	@Override
	public String getSupportedDataType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.DataProvider#getItem(java.lang.String)
	 */
	@Override
	public V getItem(String key) {
		V ret = items.get(key);
		if (ret!=null) return ret;
		for (V tmp : items.values()) {
			if (tmp instanceof IReferenceResolver) {
				ret = ((IReferenceResolver)tmp).resolveItem(key);
				if (ret!=null) return ret;
			}
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.DataProvider#getItem(java.lang.String)
	 */
	@Override
	public V getItem(String key, String lang) {
		V ret = items.get(key);
		if (ret!=null) {
			V ret2 = (lang!=null)?ret.getLanguageAlternative(lang):null;
			return (ret2!=null) ? ret2 : ret;
		}

		for (V tmp : items.values()) {
			if (tmp instanceof IReferenceResolver) {
				ret = ((IReferenceResolver)tmp).resolveItem(key);
				if (ret!=null) return ret;
			}
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.DataProvider#getItems()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<V> getItems() {
		List<V> all = new ArrayList<V>();
		for (V tmp : items.values()) {
			all.add(tmp);
			all.addAll((Collection<? extends V>) tmp.getLanguageAlternatives());
		}
		return all;
	}

	//-------------------------------------------------------------------
	public List<String> getKeys() {
		return new ArrayList<>(items.keySet());
	}

	//-------------------------------------------------------------------
	public void add(V value) {
		if (value==null)
			throw new NullPointerException();
		if (!value.getTypeString().equals(type))
			throw new IllegalArgumentException("Trying to add a '"+value.getTypeString()+"' to a database of '"+type+"'");

		items.put(value.getId(), value);
	}

	//-------------------------------------------------------------------
	public void add(V value, DataItem parent) {
		if (value==null)
			throw new NullPointerException("Value");
		if (parent==null)
			throw new NullPointerException("parent DataItem");
//		if (!value.getTypeString().equals(type))
//			throw new IllegalArgumentException("Trying to add a '"+value.getTypeString()+"' to a database of '"+type+"'");

		items.put(parent.getId()+"/"+value.getId(), value);
	}

	//-------------------------------------------------------------------
	public V remove(String key) {
		return items.remove(key);
	}

	//-------------------------------------------------------------------
	public void clear() {
		items.clear();
	}

}
