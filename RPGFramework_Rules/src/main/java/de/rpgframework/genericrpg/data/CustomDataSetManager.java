package de.rpgframework.genericrpg.data;

import java.io.InputStream;
import java.util.List;

/**
 *
 */
public interface CustomDataSetManager {

	//-------------------------------------------------------------------
	/**
	 * Get a list of all datasets for custom data. A dataset named "default"
	 * covers all XML files that are in directly in the custom directory.<br/>
	 * It is expected that the returned handle has a list of "filehandles"
	 * (filenames without the suffix) and that this list is sorted in a load
	 * order.
	 * <br/>
	 * How exactly load order is determined and user-controlled is up for later.
	 * @return
	 */
	public List<CustomDataSetHandle> getCustomDataProducts();

	public InputStream getDataFile(CustomDataSetHandle product, String fileKey);

}
