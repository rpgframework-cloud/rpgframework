/**
 * 
 */
package de.rpgframework.genericrpg.data;

/**
 * @author Stefan
 *
 */
public abstract class TwoAttributeSkill<A extends IAttribute> extends OneAttributeSkill<A> {

	public abstract A getAttribute2();
	
}
