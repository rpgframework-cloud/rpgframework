/**
 *
 */
package de.rpgframework.genericrpg.data;

import de.rpgframework.genericrpg.modification.ModifiedObjectType;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
public class ReferenceException extends RuntimeException {

	private ReferenceError error;
	private Object context;

	//--------------------------------------------------------------------
	public ReferenceException(String valid, ModifiedObjectType type, String ref) {
		super("Invalid reference to "+type+" '"+ref+"'.\n  Valid are: "+valid);
		error = new ReferenceError(type, ref);
	}

	//--------------------------------------------------------------------
	public ReferenceException(ModifiedObjectType type, String ref) {
		super("Invalid reference to "+type+" '"+ref+"'");
		error = new ReferenceError(type, ref);
	}
	//--------------------------------------------------------------------
	public ReferenceException(ModifiedObjectType type, String ref, Object context) {
		super("Invalid reference to "+type+" '"+ref+"' in context "+context);
		error = new ReferenceError(type, ref);
		this.context = context;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the context
	 */
	public Object getContext() {
		return context;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the error
	 */
	public ReferenceError getError() {
		return error;
	}

}
