package de.rpgframework.genericrpg.data;

import java.lang.System.Logger.Level;
import java.util.Date;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementList;

import de.rpgframework.character.RuleSpecificCharacterObject;
import de.rpgframework.classification.Gender;
import de.rpgframework.genericrpg.Reward;
import de.rpgframework.genericrpg.chargen.DataSetMode;
import de.rpgframework.genericrpg.chargen.Rule;
import de.rpgframework.genericrpg.chargen.RuleConfiguration;
import de.rpgframework.genericrpg.chargen.ai.LevellingProfileValue;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.PieceOfGear;
import de.rpgframework.genericrpg.modification.DataItemModification;
import de.rpgframework.genericrpg.modification.ModificationList;

/**
 * @author prelle
 *
 */
@SuppressWarnings("rawtypes")
public abstract class CommonCharacter<A extends IAttribute,S extends ISkill, V extends ASkillValue<S>, T extends PieceOfGear> implements RuleSpecificCharacterObject<A,S,V,T>, Lifeform<A, S, V>, DecisionContainer {

	public static class DataSetControl {
		@Attribute
		public DataSetMode mode = DataSetMode.ALL;
		@ElementList(entry = "set", type = String.class)
		public List<String> selected = new ArrayList<>();
		public DataSetControl() {}
		public void add(String id) {
			if (!selected.contains(id)) selected.add(id);
		};
		public void remove(String id) {
			selected.remove(id);
		}
	}

	@Element
	protected String name;
	@Element(name="datasets")
	protected DataSetControl dataSets = new DataSetControl();
    @Attribute(name="gender")
    protected Gender gender = Gender.MALE;
	@ElementList(type = RuleConfiguration.class, entry = "set")
    protected List<RuleConfiguration> rules;

	@Element
	private String hairColor, eyeColor, skinColor, age;
	@Element
	private int size, weight;

	@ElementList(type = AttributeValue.class, entry = "attributes")
	private final List<AttributeValue<A>> attributes;
	/** Attributes that don't need to be serialized */
	protected Map<A, AttributeValue<A>> derivedAttributes;
    @ElementList(type = ASkillValue.class, entry = "skill")
    protected List<V> skills;
	@Element
	protected byte[] image;
	@Attribute(name="career")
	protected boolean inCareerMode;
	/** Which generator model was used */
	@Element
	private String charGenUsed;
	/** What kind of strictness shall be used for this char */
	@Element
	private String strictness;

    @ElementList(type = Decision.class, entry = "decision")
	public List<Decision> decisions;

	@ElementList(type = CarriedItem.class, entry = "item")
    protected List<CarriedItem<T>> items;
    protected transient List<CarriedItem<T>> virtualItems;

	@Element
	protected ModificationList history;
	@ElementList(type = Reward.class, entry = "reward")
	protected List<Reward> rewards;
	@ElementList(type = LevellingProfileValue.class, entry = "profile")
	protected List<LevellingProfileValue> profiles;

	protected transient List<RuleFlag> ruleFlags;
	protected transient List<T> gearDefinitions;

	//-------------------------------------------------------------------
	public CommonCharacter() {
		rules      = new ArrayList<>();
		attributes = new ArrayList<>();
		decisions = new ArrayList<>();
		skills    = new ArrayList<>();
		items     = new ArrayList<>();
		virtualItems     = new ArrayList<>();
		ruleFlags = new ArrayList<>();
		derivedAttributes = new LinkedHashMap<>();
		history   = new ModificationList();
		rewards   = new ArrayList<>();
		profiles  = new ArrayList<>();
		gearDefinitions = new ArrayList<>();
		name = "Unnamed";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.Lifeform#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.RuleSpecificCharacterObject#setName(java.lang.String)
	 */
	@Override
	public void setName(String value) {
		this.name = value;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.RuleSpecificCharacterObject#getImage()
	 */
	@Override
	public byte[] getImage() {
		return image;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.RuleSpecificCharacterObject#setImage(byte[])
	 */
	@Override
	public void setImage(byte[] data) {
		this.image = data;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.Lifeform#getAttribute(A)
	 */
	@Override
	public AttributeValue<A> getAttribute(A key) {
		for (AttributeValue<A> val : attributes) {
			if (val.getModifyable()==key)
				return val;
		}
		return derivedAttributes.get(key);
	}

	//-------------------------------------------------------------------
	public List<AttributeValue<A>> getAttributes() {
		List<AttributeValue<A>> ret = new ArrayList<AttributeValue<A>>(attributes);
		ret.addAll(derivedAttributes.values());
		return ret;
	}

	//-------------------------------------------------------------------
	public void setAttribute(AttributeValue<A> val) {
		if (val.getModifyable().isDerived())
			derivedAttributes.put(val.getModifyable(), val);
		else
			attributes.add(val);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.RuleSpecificCharacterObject#hasDecisionBeenMade(java.util.UUID)
	 */
	@Override
	public boolean hasDecisionBeenMade(UUID choice) {
		for (Decision dec : decisions) {
			if (dec.getChoiceUUID().equals(choice)) {
				return true;
			}
		}
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.RuleSpecificCharacterObject#getDecision(java.util.UUID)
	 */
	@Override
	public Decision getDecision(UUID choice) {
		for (Decision dec : decisions) {
			if (dec.getChoiceUUID().equals(choice)) {
				return dec;
			}
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.RuleSpecificCharacterObject#removeDecision(java.util.UUID)
	 */
	@Override
	public void removeDecision(UUID choice) {
		for (Decision dec : decisions) {
			if (dec.getChoiceUUID().equals(choice)) {
				decisions.remove(dec);
				return;
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.RuleSpecificCharacterObject#addDecision(de.rpgframework.genericrpg.data.Decision)
	 */
	@Override
	public void addDecision(Decision decision) {
		removeDecision(decision.getChoiceUUID());
		decisions.add(decision);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.RuleSpecificCharacterObject#getGender()
	 */
	@Override
	public Gender getGender() {
		return gender;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.RuleSpecificCharacterObject#setGender(de.rpgframework.classification.Gender)
	 */
	@Override
	public void setGender(Gender value) {
		this.gender = value;
	}

	//-------------------------------------------------------------------
	@Override
	public String getHairColor() { return hairColor; }
	public void setHairColor(String hairColor) { this.hairColor = hairColor; }

	//-------------------------------------------------------------------
	@Override
	public String getEyeColor() {return eyeColor;}
	public void setEyeColor(String eyeColor) {this.eyeColor = eyeColor;}

	//-------------------------------------------------------------------
	@Override
	public String getSkinColor() { return skinColor; }
	public void setSkinColor(String color) { this.skinColor = color;}

	//-------------------------------------------------------------------
	@Override
	public int getSize() { return size; }
	public void setSize(int size) { this.size = size; }

	//-------------------------------------------------------------------
	@Override
	public int getWeight() { return weight; }
	public void setWeight(int weight) { this.weight = weight; }

	//-------------------------------------------------------------------
	public String getAge() { return age; }
	public void setAge(String value) { this.age = value;}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.Lifeform#getSkillValues()
	 */
	@Override
	public List<V> getSkillValues() {
		return skills;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.Lifeform#getSkillValue(S)
	 */
	@Override
	public V getSkillValue(S skill) {
		assert skill!=null;
		for (V val : skills) {
			if (val.getModifyable()==skill || val.getModifyable().getId().equals(skill.getId())) {
				return val;
			}
		}
		return null;
	}

	//-------------------------------------------------------------------
	public V getSkillValue(String skillID) {
		for (V val : skills) {
			if (val.getKey()!=null && val.getKey().equals(skillID)) {
				return val;
			}
		}
		return null;
	}

	//-------------------------------------------------------------------
	public V getSkillValue(UUID uuid) {
		for (V val : skills) {
			if (val.getUuid()!=null && val.getUuid().equals(uuid)) {
				return val;
			}
		}
		return null;
	}

	//-------------------------------------------------------------------
	public void removeSkillValue(V value) {
		for (V val : skills) {
			if (val.equals(value)) {
				skills.remove(val);
				return;
			}
		}
	}

	//-------------------------------------------------------------------
	public V addSkillValue(V value) {
		if (value.getKey()==null) throw new RuntimeException("No key");
		if (value.getResolved()==null) throw new RuntimeException("Nothing resolved");
		if (skills.contains(value))
			return value;
		removeSkillValue(value);
		skills.add(value);
		return value;
	}

	//-------------------------------------------------------------------
	public DataSetControl getDataSets() {
		return dataSets;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.RuleSpecificCharacterObject#addCarriedItem(de.rpgframework.genericrpg.items.CarriedItem)
	 */
	@Override
	public void addCarriedItem(CarriedItem<T> item) {
		if (item.getUuid()==null)
			throw new NullPointerException("Cannot add item without UUID");
		// Prevent double entries
		for (CarriedItem<T> tmp : items) {
			if (tmp.getUuid()==null)
				System.err.println("CommonCharacter("+name+").addCarriedItem: found item "+tmp+" that has no UUID");
			if (tmp.getUuid()!=null && tmp.getUuid().equals(item.getUuid()))
				return;
		}
		// Add
		items.add(item);
	}

	//-------------------------------------------------------------------
	public List<CarriedItem<T>> getVirtualCarriedItems() {
		return new ArrayList<CarriedItem<T>>(virtualItems);
	}

	//-------------------------------------------------------------------
	public void addVirtualCarriedItem(CarriedItem<T> item) {
		if (item.getInjectedBy()==null)
			throw new IllegalArgumentException("Virtual items need an injectedBy property set");
		if (!virtualItems.contains(item))
			virtualItems.add(item);
	}

	//-------------------------------------------------------------------
	public void removeVirtualCarriedItem(CarriedItem<T> item) {
		if (item.getInjectedBy()==null)
			throw new IllegalArgumentException("Virtual items need an injectedBy property set");
//		try {
//			throw new RuntimeException("Remove "+item);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		virtualItems.remove(item);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.RuleSpecificCharacterObject#removeCarriedItem(de.rpgframework.genericrpg.items.CarriedItem)
	 */
	@Override
	public void removeCarriedItem(CarriedItem<T> item) {
		items.remove(item);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.RuleSpecificCharacterObject#getCarriedItems()
	 */
	public List<CarriedItem<T>> getCarriedItems() {
		List<CarriedItem<T>> ret = new ArrayList<CarriedItem<T>>(items);
		ret.addAll(virtualItems);
		return ret;
	}

	//-------------------------------------------------------------------
	public List<CarriedItem<T>> getCarriedItemsRecursive() {
		List<CarriedItem<T>> ret = new ArrayList<CarriedItem<T>>();
		for (CarriedItem<T> tmp : getCarriedItems()) {
			ret.add(tmp);
			ret.addAll(tmp.getEffectiveAccessories());
		}
		return ret;
	}

	//-------------------------------------------------------------------
	public List<CarriedItem<T>> getCarriedItems(Predicate<CarriedItem<T>> filter) {
		return getCarriedItemsRecursive().stream().filter(filter).collect(Collectors.toList());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.RuleSpecificCharacterObject#getCarriedItem(java.lang.String)
	 */
	@Override
	public CarriedItem<T> getCarriedItem(String key) {
		for (CarriedItem<T> tmp : getCarriedItems()) {
			if (tmp.getKey().equals(key)) return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public CarriedItem<T> getCarriedItem(UUID uuid) {
		if (uuid==null) throw new NullPointerException("UUID is null");
		for (CarriedItem<T> tmp : getCarriedItems()) {
			if (tmp.getUuid().equals(uuid)) return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the inCareerMode
	 */
	public boolean isInCareerMode() {
		return inCareerMode;
	}

	//-------------------------------------------------------------------
	/**
	 * @param inCareerMode the inCareerMode to set
	 */
	public void setInCareerMode(boolean inCareerMode) {
		this.inCareerMode = inCareerMode;
	}

	//-------------------------------------------------------------------
	public String getCharGenUsed() { return charGenUsed;}
	//-------------------------------------------------------------------
	public void setCharGenUsed(String charGenUsed) {
		this.charGenUsed = charGenUsed;
	}

	//-------------------------------------------------------------------
	public String getStrictness() { return strictness;}
	//-------------------------------------------------------------------
	public void setStrictness(String value) {
		this.strictness = value;
	}

	//-------------------------------------------------------------------
	public void addRuleFlag(RuleFlag flag) {
		if (!ruleFlags.contains(flag))
			ruleFlags.add(flag);
	}

	//-------------------------------------------------------------------
	public void clearRuleFlag(RuleFlag flag) {
		ruleFlags.remove(flag);
	}

	//-------------------------------------------------------------------
	public void clearRuleFlags() {
		ruleFlags.clear();
	}

	//-------------------------------------------------------------------
	public boolean hasRuleFlag(RuleFlag flag) {
		return ruleFlags.contains(flag);
	}

	//-------------------------------------------------------------------
	void addRule(Rule rule, String value) {
		RuleConfiguration old = getRuleValue(rule);
		if (old!=null)
			rules.remove(old);
		rules.add(new RuleConfiguration(rule.getID(), value));
	}

	//-------------------------------------------------------------------
	List<RuleConfiguration> getRuleValues() {
		return rules;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.RuleSpecificCharacterObject#getRuleValue(de.rpgframework.genericrpg.chargen.Rule)
	 */
	public RuleConfiguration getRuleValue(Rule rule) {
		for (RuleConfiguration rv : rules) {
			if (rv.getRuleId().equals(rule.getID()))
				return rv;
		}
		return null;
	}
	//-------------------------------------------------------------------
	boolean getRuleValueAsBoolean(Rule rule) {
		for (RuleConfiguration rv : rules) {
			if (rv.getRuleId().equals(rule.getID())) {
				try {
					return Boolean.parseBoolean(rv.getValueString());
				} catch (Exception e) {
					System.getLogger(getClass().getPackageName()).log(Level.WARNING, "Invalid entry for rule " + rule,e);
				}
				return Boolean.parseBoolean(rule.getDefaultValue());
			}
		}
		return false;
	}

	//-------------------------------------------------------------------
	@SuppressWarnings({ "unchecked" })
	<E extends Enum> E getRuleValueAsEnum(Rule rule) {
		for (RuleConfiguration rv : rules) {
			if (rv.getRuleId().equals(rule.getID())) {
				Class<Enum> enumType = rule.getEnumClassToUse();
				try {
					return (E) Enum.valueOf(enumType, rv.getValueString());

				} catch (Exception e) {
					System.getLogger(getClass().getPackageName()).log(Level.WARNING, "Invalid entry for rule " + rule,e);
				}
				return (E) Enum.valueOf(enumType, rule.getDefaultValue());
			}
		}
		return rule.getDefaultAsEnumValue();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.RuleSpecificCharacterObject#setRuleValue(de.rpgframework.genericrpg.chargen.Rule, java.lang.String)
	 */
	public void setRuleValue(Rule rule, String value) {
		clearRuleValue(rule);
		rules.add(new RuleConfiguration(rule.getID(), value));
	}

	//-------------------------------------------------------------------
	void clearRuleValue(Rule rule) {
		RuleConfiguration old = getRuleValue(rule);
		if (old!=null)
			rules.remove(old);
	}

	//--------------------------------------------------------------------
	public void addToHistory(DataItemModification mod) {
		if (mod.getDate()==null)
			mod.setDate(new Date());
		if (mod.getWhen()!=null) mod.setWhen(null);
		if (mod.getApplyTo()!=null) mod.setApplyTo(null);
		history.add(mod);
	}

	//--------------------------------------------------------------------
	public boolean removeFromHistory(DataItemModification mod) {
		return history.remove(mod);
	}

	//--------------------------------------------------------------------
	public List<DataItemModification> getHistory() {
		ArrayList<DataItemModification> ret = new ArrayList<>();
		history.forEach(tmp -> ret.add((DataItemModification) tmp));
		return ret;
	}

	//--------------------------------------------------------------------
	public void addReward(Reward rew) {
		if (!rewards.contains(rew))
			rewards.add(rew);
	}

	//--------------------------------------------------------------------
	public boolean removeReward(Reward rew) {
		return rewards.remove(rew);
	}

	//--------------------------------------------------------------------
	public List<Reward> getRewards() {
		return new ArrayList<Reward>(rewards);
	}

	//-------------------------------------------------------------------
	public void clearGearDefinitions() {
		gearDefinitions.clear();
	}

	//-------------------------------------------------------------------
	public T getGearDefinition(String key) {
		for (T tmp : gearDefinitions) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public void addGearDefinition(T gearDef) {
		if (!gearDefinitions.contains(gearDef)) {
			gearDefinitions.add(gearDef);
		}
	}

	//-------------------------------------------------------------------
	public List<LevellingProfileValue> getProfiles() {
		return profiles;
	}

	//-------------------------------------------------------------------
	public void addProfile(LevellingProfileValue value	) {
		profiles.add(value);
	}

	//-------------------------------------------------------------------
	public void removeProfile(LevellingProfileValue value	) {
		profiles.remove(value);
	}


}
