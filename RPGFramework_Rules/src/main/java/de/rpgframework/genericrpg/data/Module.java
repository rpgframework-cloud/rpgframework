package de.rpgframework.genericrpg.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.prelle.simplepersist.Element;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.requirements.Requirement;

/**
 * @author prelle
 *
 */
public abstract class Module extends DataItem {
	
	@Element
	protected List<Modification> modifications;
	
	@Element
	protected List<Requirement> requirements;

	//-------------------------------------------------------------------
	protected Module() {
		super();
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	public Collection<Modification> getModifications() {
		if (modifications==null) return new ArrayList<Modification>();
		return new ArrayList<Modification>(modifications);
	}

	//-------------------------------------------------------------------
	public Collection<Requirement> getRequirement() {
		if (requirements==null) return new ArrayList<Requirement>();
		return new ArrayList<Requirement>(requirements);
	}

}
