package de.rpgframework.genericrpg.data;

import java.util.List;
import java.util.UUID;

/**
 * 
 */
public interface DecisionContainer {

	//-------------------------------------------------------------------
	/**
	 * If the referenced ComplexDataItem had choices, these are the
	 * decisions.
	 */
	//public List<Decision> getDecisions();

	//-------------------------------------------------------------------
	public void addDecision(Decision value);

	//-------------------------------------------------------------------
	public void removeDecision(UUID choiceUUID);

	//-------------------------------------------------------------------
	public Decision getDecision(UUID choiceUUID) ;

}
