package de.rpgframework.genericrpg.data;

import java.util.UUID;

/**
 * 
 */
public interface ChoiceOrigin {


	//-------------------------------------------------------------------
	public Choice getChoice(UUID uuid);

}
