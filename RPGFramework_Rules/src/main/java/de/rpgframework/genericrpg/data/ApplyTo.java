package de.rpgframework.genericrpg.data;

public enum ApplyTo {
	/* e.g. the modified weapon or the container */
	DATA_ITEM,
	/* The container of the current container */
	PARENT,
	/* The created character */
	CHARACTER,
	// Melee without weapons
	UNARMED,
	// All melle
	MELEE,
	/* A used piece of equipment required in the current setup */
	ACTIVE_GEAR,
	/* Does not affect item itself, but rules how it is levelled/created */
	RULES,
	/* Free Generation points */
	POINTS,
	/* Shadowrun: combination of cyberdeck + commlink/cyberjack */
	PERSONA,
	/* Shadowrun: Drake body */
	DRAKE,
	/* Shadowrun: Shifters Animal body */
	ANIMAL
}