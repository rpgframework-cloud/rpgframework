package de.rpgframework.genericrpg.data;

/**
 * @author prelle
 *
 */
public interface IReferenceResolver {

	public <T> T resolveItem(String key);

}
