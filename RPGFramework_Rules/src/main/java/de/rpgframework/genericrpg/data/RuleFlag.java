package de.rpgframework.genericrpg.data;

/**
 * Flags how to handle specific rules in a character. They originate from
 * qualities, adept powers and other elements that change how rules apply
 * to a character.
 * This flags are not preconfigured, but injected by modifications.
 * 
 * @author prelle
 *
 */
public interface RuleFlag {

}
