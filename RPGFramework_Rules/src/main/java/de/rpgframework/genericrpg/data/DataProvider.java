package de.rpgframework.genericrpg.data;

import java.util.List;

/**
 * @author prelle
 *
 */
public interface DataProvider<V extends DataItem> {

	public String getSupportedDataType();

	public V getItem(String key);
	public V getItem(String key, String lang);

	public List<V> getItems();

}
