package de.rpgframework.genericrpg.data;

import java.util.Locale;

/**
 * @author prelle
 *
 */
public class PageReference {

	private DataSet product;
	private int     page;
	private String  lang;
	/** If present this name gets precedence before the product */
	private String  overwrittenProductName;

	//-------------------------------------------------------------------
	public PageReference(DataSet product, int page, String lang) {
		this.product = product;
		this.page    = page;
		this.lang    = lang;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return product.getID()+"/"+lang;
	}

	//-------------------------------------------------------------------
	public DataSet getProduct() {
		return product;
	}

	//-------------------------------------------------------------------
	public int getPage() {
		return page;
	}

	//-------------------------------------------------------------------
	public String getLanguage() {
		return lang;
	}

	//-------------------------------------------------------------------
	public void setOverwrittenProductName(String value) {
		this.overwrittenProductName = value;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the overwrittenProductName
	 */
	public String getOverwrittenProductName() {
		return overwrittenProductName;
	}

	//-------------------------------------------------------------------
	public String getProductName() {
		if (overwrittenProductName!=null) return overwrittenProductName;
		return product.getName(Locale.forLanguageTag(lang));
	}

	//-------------------------------------------------------------------
	public String getProductShortName() {
		if (overwrittenProductName!=null) return overwrittenProductName;
		return product.getShortName(Locale.forLanguageTag(lang));
	}

}
