/**
 * 
 */
package de.rpgframework.genericrpg.data;

import java.util.List;

/**
 * @author Stefan
 *
 */
public abstract class ISkill extends ComplexDataItem {

	//-------------------------------------------------------------------
	public abstract List<SkillSpecialization<? extends ISkill>> getSpecializations() ;
	
}
