package de.rpgframework.genericrpg.data;

import java.io.IOException;
import java.io.InputStream;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.SerializationException;

/**
 * @author prelle
 *
 */
public abstract class GenericCore {

	protected final static Logger logger = System.getLogger(GenericCore.class.getPackageName());

	protected static Persister serializer;

	private static Map<Class<? extends DataItem>, DataStorage<? extends DataItem>> storesByType;
	protected static List<DataSet> sets;

	//-------------------------------------------------------------------
	static {
		serializer   = new Persister();
		storesByType = new HashMap<Class<? extends DataItem>, DataStorage<? extends DataItem>>();
		sets         = new ArrayList<DataSet>();
	}

//	public static abstract Logger getLogger();

	//-------------------------------------------------------------------
	public static Persister getPersister() {
		return serializer;
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	protected static<E extends DataItem> DataStorage<E> getStorage(Class<E> cls) {
		DataStorage<E> store = (DataStorage<E>) storesByType.get(cls);
		if (store==null) {
			DataItemTypeKey anno = cls.getAnnotation(DataItemTypeKey.class);
			if (anno==null)
				throw new IllegalArgumentException("Class "+cls.getName()+" misses @DataItemTypeKey annotation");
			store = new DataStorage<E>(anno.id());
			storesByType.put(cls, store);
		}
		return store;
	}

	//-------------------------------------------------------------------
	public static List<DataSet> getDataSets() {
		return new ArrayList<>(sets);
	}

	//-------------------------------------------------------------------
	public static void removeDataSet(DataSet set) {
		System.getLogger("rpgframework").log(Level.WARNING, "Remove dataset "+ set.getID());
		sets.remove(set);
		for (Map.Entry<Class<? extends DataItem>, DataStorage<? extends DataItem>> entry : storesByType.entrySet()) {
			DataStorage<? extends DataItem> storage = entry.getValue();
			for (DataItem item : new ArrayList<>(storage.getItems())) {
				for (PageReference pageRef : new ArrayList<>( item.getPageReferences())) {
					if (pageRef.getProduct()==set) {
						item.getPageReferences().remove(pageRef);
					}
				}
				// If Item is in no product anymore, remove item
				if (item.getPageReferences().isEmpty()) {
					if (storage.remove(item.getId())==null) {
						System.getLogger("rpgframework").log(Level.WARNING, "Failed to remove item {0}", item.getId());
					}
				}
			}
		}
	}

	//-------------------------------------------------------------------
	public static <E extends DataItem> List<E> loadDataItems(Class<? extends List<E>> cls, Class<E> itemCls, DataSet plugin, Class<?> clazz, String resourceName) throws IOException {
		try {
			return loadDataItems(cls, itemCls, plugin, clazz.getResourceAsStream(resourceName));
		} catch (DataErrorException e) {
			e.setResource(resourceName);
			throw e;
		} finally {
			if (Persister.errorFound) {
				logger.log(Level.ERROR, "Error in resource {0}", resourceName);
			}
		}
	}

	//-------------------------------------------------------------------
	public static <E extends DataItem> List<E> loadDataItems(Class<? extends List<E>> cls, Class<E> itemCls, DataSet plugin, InputStream in) throws IOException {
		DataStorage<E> storage = getStorage(itemCls);
		try {
			List<E> added = serializer.read(cls, in);
			// If not empty, add the set
			if (!sets.contains(plugin)) {
				sets.add(plugin);
			}

			for (E item : added) {
				// Check if item already exists
				E already = storage.getItem(item.getId());
				if (already!=null) {
					// Item with that ID already exists - may either be due to different definition for other language
					// or due to another source
					if (already.getLanguage()!=item.getLanguage()) {
						logger.log(Level.DEBUG, "Add different language definition of {0}: {1}", item.toString(), item.getLanguage());
						already.addLanguageAlternative(item.getLanguage(), item);
					} else {
						logger.log(Level.WARNING, "Add {0} from {1} to already existing {2} from {3}", item.toString(), plugin, already.toString(), already.getAssignedDataSets());
					}
					if (!already.datasets.contains(plugin))
						already.assignToDataSet(plugin);
					if (!item.datasets.contains(plugin))
						item.assignToDataSet(plugin);
				} else {
					// Unknown item yet
					item.assignToDataSet(plugin);
					storage.add(item);
				}
			}

			// Validate all items
			added.forEach(item -> {
				try {
					item.validate();
					item.getName();
					item.getDescription();
				} catch (DataErrorException e) {
					e.setDataset(plugin);
					System.err.println("In dataset "+plugin.getID()+"\n"+e.getMessage());
					throw e;
				} catch (Exception e) {
					throw new DataErrorException(item, plugin, e);
				}
			});
			return added;
		} catch (SerializationException e) {
			System.err.println("Line "+e.getLine()+"  col "+e.getColumn());
			throw e;
		}
	}

	//-------------------------------------------------------------------
	public static <E extends DataItem> void retainItems(Class<E> itemCls, String...keys) {
		List<String> allowed = Arrays.asList(keys);
		List<String> found   = getStorage(itemCls).getKeys();
		for (String key : found) {
			if (!allowed.contains(key)) {
				getStorage(itemCls).remove(key);
			}
		}
	}

	//-------------------------------------------------------------------
	public static<E extends DataItem> E getItem(Class<E> itemCls, String key) {
		return getStorage(itemCls).getItem(key);
	}

	//-------------------------------------------------------------------
	public static<E extends DataItem> E getItem(Class<E> itemCls, String key, String lang) {
		return getStorage(itemCls).getItem(key, lang);
	}

	//-------------------------------------------------------------------
	public static<E extends DataItem> List<E> getItemList(Class<E> itemCls) {
		List<E> foo = new ArrayList<E>();
		getStorage(itemCls).getItems().forEach(item -> foo.add((E) item));
		return foo;
	}

	//-------------------------------------------------------------------
	public static <E extends DataItem> List<PageReference> getBestPageReferences(E item, Locale locale) {
		List<PageReference> ret = new ArrayList<>();
		for (PageReference tmp : item.getPageReferences()) {
			if (tmp.getLanguage().equals(locale.getLanguage())) {
				ret.add(tmp);
			}
		}
		if (ret.isEmpty()) {
			ret.addAll(item.getPageReferences());
		}
		return ret;
	}

	//-------------------------------------------------------------------
	public static <E extends DataItem> List<String> getBestPageReferenceShortNames(E item, Locale locale) {
		List<String> ret = new ArrayList<>();
		for (PageReference tmp : getBestPageReferences(item,locale)) {
			ret.add(tmp.getProduct().getShortName(locale)+" "+tmp.getPage());
		}
		return ret;
	}

	//-------------------------------------------------------------------
	public static <E extends DataItem> List<String> getBestPageReferenceLongNames(E item, Locale locale) {
		List<String> ret = new ArrayList<>();
		for (PageReference tmp : getBestPageReferences(item,locale)) {
			ret.add(tmp.getProduct().getName(locale)+" "+tmp.getPage());
		}
		return ret;
	}

}
