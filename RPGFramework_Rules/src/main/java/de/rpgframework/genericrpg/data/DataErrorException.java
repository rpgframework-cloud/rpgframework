package de.rpgframework.genericrpg.data;

/**
 * @author prelle
 *
 */
public class DataErrorException extends RuntimeException {

	private static final long serialVersionUID = 7732274685110052626L;

	private DataSet dataset;
	private DataItem item;
	private String resource;
	private ReferenceError refError;

	//-------------------------------------------------------------------
	public DataErrorException(DataItem item, String message) {
		super(message);
		this.item = item;
	}

	//-------------------------------------------------------------------
	public DataErrorException(DataItem item, DataSet plugin, Exception e) {
		super(e.getMessage(),e);
		this.item = item;
		this.dataset = plugin;
	}

	//-------------------------------------------------------------------
	public DataErrorException(DataItem item, ReferenceError refError) {
		super(refError.toString());
		this.item = item;
		this.refError = refError;
	}

	//-------------------------------------------------------------------
	public String getMessage() {
		StringBuffer buf = new StringBuffer();
		if (refError!=null)
			buf.append(refError.toString());
		else
			buf.append(super.getMessage());
		if (dataset!=null)
			buf.append("\nDataset : "+dataset.getID());
		if (resource!=null) 
			buf.append("\nResource: "+resource);		
		if (item!=null)
			buf.append("\n"+item.getTypeString()+"="+item.getId());
		else
			buf.append("\nitem=null");
		return buf.toString();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the dataset
	 */
	public DataSet getDataset() {
		return dataset;
	}

	//-------------------------------------------------------------------
	/**
	 * @param dataset the dataset to set
	 */
	public void setDataset(DataSet dataset) {
		this.dataset = dataset;		
	}

	//-------------------------------------------------------------------
	/**
	 * @return the item
	 */
	public DataItem getItem() {
		return item;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the resource
	 */
	public String getResource() {
		return resource;
	}

	//-------------------------------------------------------------------
	/**
	 * @param resource the resource to set
	 */
	public void setResource(String resource) {
		this.resource = resource;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the refError
	 */
	public ReferenceError getReferenceError() {
		return refError;
	}

}
