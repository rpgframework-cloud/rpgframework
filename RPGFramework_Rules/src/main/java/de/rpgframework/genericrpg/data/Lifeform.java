package de.rpgframework.genericrpg.data;

import java.util.List;

public interface Lifeform<A extends IAttribute, S extends ISkill, V extends ASkillValue<S>> {

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.RuleSpecificCharacterObject#getName()
	 */
	public String getName();

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.RuleSpecificCharacterObject#getAttribute(de.rpgframework.genericrpg.data.IAttribute)
	 */
	public AttributeValue<A> getAttribute(A key);

	//-------------------------------------------------------------------
	public AttributeValue<A> getAttribute(String key);

	//-------------------------------------------------------------------
	public List<V> getSkillValues();

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.RuleSpecificCharacterObject#getSkillValue(de.rpgframework.genericrpg.data.ISkill)
	 */
	public V getSkillValue(S skill);

}