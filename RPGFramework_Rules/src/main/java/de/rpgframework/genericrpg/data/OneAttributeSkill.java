/**
 * 
 */
package de.rpgframework.genericrpg.data;

/**
 * @author Stefan
 *
 */
public abstract class OneAttributeSkill<A extends IAttribute> extends ISkill {

	public abstract A getAttribute();
	
}
