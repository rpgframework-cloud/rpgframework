package de.rpgframework.genericrpg;

import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;
import de.rpgframework.genericrpg.modification.ModifiedObjectType;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id = "set")
public class SetItem extends ComplexDataItem {

	@Attribute
	private int cost;
	@Attribute
	private ModifiedObjectType type;

	//-------------------------------------------------------------------
	/**
	 */
	public SetItem() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @return the cost
	 */
	public int getCost() {
		return cost;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public ModifiedObjectType getType() {
		return type;
	}

}
