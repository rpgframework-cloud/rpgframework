package de.rpgframework.genericrpg.modification;

import de.rpgframework.genericrpg.data.CommonCharacter;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;
import de.rpgframework.genericrpg.data.DataItem;

/**
 * @author prelle
 *
 */
public interface ModifiedObjectType {

	public <T> T resolve(String key);

	public <T extends DataItem> T resolveAsDataItem(String key);

	public <T> T[] resolveAny();

	public <T> T[] resolveVariable(String varName);

	public Modification instantiateModification(Modification tmp, ComplexDataItemValue<?> complexDataItemValue, int multiplier,
			CommonCharacter<?, ?, ?,?> model);

}
