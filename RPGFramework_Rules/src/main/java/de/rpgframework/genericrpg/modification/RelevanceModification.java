package de.rpgframework.genericrpg.modification;

import org.prelle.simplepersist.Attribute;

/**
 * @author prelle
 *
 */
public class RelevanceModification extends Modification {

	@Attribute
	private String topic;

	//-------------------------------------------------------------------
	public RelevanceModification() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	public String getType() {
		return topic;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return "Relevance:"+topic+" ("+super.source+")";
	}

}
