/**
 *
 */
package de.rpgframework.genericrpg.modification;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.rpgframework.genericrpg.modification.Modification.Origin;

/**
 * @author prelle
 *
 */
public abstract class ModifyableImpl implements Modifyable {

	protected transient List<Modification> incomingModifications;
	protected transient List<Modification> outgoingModifications;

	//-------------------------------------------------------------------
	protected ModifyableImpl() {
		incomingModifications = new ArrayList<>();
		outgoingModifications = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modifyable#getIncomingModifications()
	 */
	@Override
	public List<Modification> getIncomingModifications() {
		return new ArrayList<Modification>(this.incomingModifications);
	}

	//-------------------------------------------------------------------
	public void clearIncomingModifications() {
		incomingModifications.clear();
	}

	//-------------------------------------------------------------------
	public void removeIncomingModifications(Origin origin) {
		getIncomingModifications().stream()
			.filter(mod -> mod.getOrigin()==origin)
			.forEach(mod -> removeIncomingModification(mod))
			;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modifyable#setIncomingModifications(java.util.List)
	 */
	@Override
	public void setIncomingModifications(List<Modification> mods) {
		incomingModifications = mods;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modifyable#addIncomingModification(de.rpgframework.genericrpg.modification.Modification)
	 */
	@Override
	public void addIncomingModification(Modification mod) {
//		if (mod.getOrigin()==null)
//			throw new RuntimeException("Modifications without origin are not allowed");
		incomingModifications.add(mod);
	}
	public void addModifications(Collection<Modification> mods) {
		incomingModifications.addAll(mods);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modifyable#setIncomingModifications(java.util.List)
	 */
	@Override
	public void removeIncomingModification(Modification mod) {
		incomingModifications.remove(mod);
	}

	//-------------------------------------------------------------------
	public void removeIncomingModificationForSource(Object src) {
		for (Modification mod : new ArrayList<Modification>(incomingModifications)) {
			if (mod.getSource().equals(src))
				incomingModifications.remove(src);
		}
	}

	//-------------------------------------------------------------------
	public void clearOutgoingModifications() { outgoingModifications.clear(); }
	public void addOutgoingModification(Modification mod) { outgoingModifications.add(mod); }
	public List<Modification> getOutgoingModifications() {
		return new ArrayList<Modification>(this.outgoingModifications);
	}

}
