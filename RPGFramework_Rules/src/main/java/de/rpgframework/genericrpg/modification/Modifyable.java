package de.rpgframework.genericrpg.modification;

import java.util.List;

public interface Modifyable {

	//-------------------------------------------------------------------
	public List<Modification> getIncomingModifications();

	//-------------------------------------------------------------------
	public void setIncomingModifications(List<Modification> mods);

	//-------------------------------------------------------------------
	public void addIncomingModification(Modification mod);

	//-------------------------------------------------------------------
	public void removeIncomingModification(Modification mod);

}