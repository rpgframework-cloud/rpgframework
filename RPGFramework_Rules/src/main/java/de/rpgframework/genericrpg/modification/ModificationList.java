package de.rpgframework.genericrpg.modification;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.ElementListUnion;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.data.ReferenceException;

/**
 * @author prelle
 *
 */
@Root(name="modifications")
@ElementListUnion({
    @ElementList(entry="allowmod", type=AllowModification.class),
    @ElementList(entry="checkmod", type=CheckModification.class),
    @ElementList(entry="costmod", type=CostModification.class),
    @ElementList(entry="embed", type=EmbedModification.class),
    @ElementList(entry="valmod", type=ValueModification.class),
    @ElementList(entry="itemmod", type=DataItemModification.class),
    @ElementList(entry="recommod", type=RecommendationModification.class),
    @ElementList(entry="relevancemod", type=RelevanceModification.class),
    @ElementList(entry="selmod", type=ModificationChoice.class),
    @ElementList(entry="allmod", type=ModificationGroup.class),
 })
public class ModificationList extends ArrayList<Modification> {

	private static final long serialVersionUID = 1L;

	//-------------------------------------------------------------------
	public ModificationList() {
	}

	//-------------------------------------------------------------------
	/**
	 * @param c
	 */
	public ModificationList(Collection<? extends Modification> c) {
		super(c);
		}

	//-------------------------------------------------------------------
	public List<Modification> getModificiations() {
		return this;
	}

	//-------------------------------------------------------------------
	/**
	 * Validate all modifications in this list
	 */
	public void validate() throws ReferenceException {
		this.forEach(mod -> mod.validate());
	}
}
