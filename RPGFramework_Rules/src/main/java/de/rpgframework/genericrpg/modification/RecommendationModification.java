package de.rpgframework.genericrpg.modification;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.chargen.ai.Weight;

/**
 * A modification made to the current item it is attached to
 * @author prelle
 *
 */
@Root(name = "recommod")
public class RecommendationModification extends DataItemModification {
	
	@Attribute
	private Weight level = Weight.VERY_GOOD;

	//-------------------------------------------------------------------
	public RecommendationModification() {
	}

	//-------------------------------------------------------------------
	public RecommendationModification(ModifiedObjectType type, String ref, Weight level) {
		this.type = type;
		this.ref  = ref;
		this.level= level;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return super.toString()+"("+level+")";
	}

	//-------------------------------------------------------------------
	public String[] getAsKeys() {
		return ref.trim().split(", ");
	}

	//-------------------------------------------------------------------
	public boolean isNotRecommended() {
		return level==Weight.NOT_RECOMMENDED;
	}

	//-------------------------------------------------------------------
	public Weight getWeight() {
		return level;
	}

}
