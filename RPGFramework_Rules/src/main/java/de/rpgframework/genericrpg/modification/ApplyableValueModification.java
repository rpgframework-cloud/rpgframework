package de.rpgframework.genericrpg.modification;

import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.ValueType;
import de.rpgframework.genericrpg.items.Formula;
import de.rpgframework.genericrpg.items.formula.FormulaTool;

/**
 * @author prelle
 *
 */
public class ApplyableValueModification extends DataItemModification {

	private Formula value;
	private ValueType set = ValueType.NATURAL;
	private boolean ignoreLimit;

	//-------------------------------------------------------------------
	public ApplyableValueModification(ValueModification valMod) {
		// Modification
		this.type = valMod.type;
		this.apply = valMod.apply;
		this.when  = valMod.when;
		this.conditions = valMod.conditions;
		this.source= valMod.source;
		// DataItemModification
		this.ref   = valMod.ref;
		this.choice = valMod.choice;
		this.restrictedToMode = valMod.restrictedToMode;
		// ValueModification
		this.set = valMod.getSet();
		this.ignoreLimit = valMod.isIgnoreLimit();
		
		value = FormulaTool.tokenize(valMod.getRawValue());
	}

}
