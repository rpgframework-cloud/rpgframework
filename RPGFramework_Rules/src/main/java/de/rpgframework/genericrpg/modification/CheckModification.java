package de.rpgframework.genericrpg.modification;

import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.data.CheckInfluence;

/**
 * @author prelle
 *
 */
public class CheckModification extends ValueModification {

	@Attribute(name="what", required=false)
	private CheckInfluence what;

	//-------------------------------------------------------------------
	public CheckModification() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	public String toString() {
		return "Checks: what="+what+" on "+super.toString();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the what
	 */
	public CheckInfluence getWhat() {
		return what;
	}
}
