/**
 *
 */
package de.rpgframework.genericrpg.modification;

import java.lang.System.Logger.Level;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.ValueType;
import de.rpgframework.genericrpg.data.ApplyTo;
import de.rpgframework.genericrpg.data.ApplyWhen;
import de.rpgframework.genericrpg.items.Formula;
import de.rpgframework.genericrpg.items.formula.FormulaTool;
import de.rpgframework.genericrpg.persist.StringArrayConverter;

/**
 * @author Stefan
 *
 */
@Root(name = "valmod")
public class ValueModification extends DataItemModification implements Cloneable {

	@Attribute(name="value", required=true)
	private String value;
	private transient Formula parsedValue;
	@Attribute(name="set", required=false)
	private ValueType set = ValueType.AUGMENTED;
	@Attribute(name="unlimited", required=false)
	private boolean ignoreLimit;
	/** Lookup from parsed formula, before evaluating */
	@org.prelle.simplepersist.Attribute(name="table")
	@AttribConvert(value = StringArrayConverter.class)
	private String[] lookupTable;

	/** If set to true, the modification already has been instantiated */
	private transient boolean instantiated;

	//--------------------------------------------------------------------
	public ValueModification() {}

	//-------------------------------------------------------------------
	public ValueModification(ModifiedObjectType type, String ref, int val) {
		this(type, ref, String.valueOf(val));
	}

	//-------------------------------------------------------------------
	public ValueModification(ModifiedObjectType type, String ref, int val, Object src) {
		this(type, ref, String.valueOf(val));
		this.source = src;
	}

	//-------------------------------------------------------------------
	public ValueModification(ModifiedObjectType type, String ref, int val, Object src, ValueType set) {
		this(type, ref, String.valueOf(val));
		this.source = src;
		this.set = set;
	}

	//-------------------------------------------------------------------
	public ValueModification(ModifiedObjectType type, String ref, String val, Object src) {
		this(type, ref, val);
		this.source = src;
	}

//	//-------------------------------------------------------------------
//	public ValueModification(ModifiedObjectType type, String ref, Formula parsed, Object src) {
//		this(type, ref, parsed.toString());
//		this.parsedValue = parsed;
//		this.source = src;
//	}

	//-------------------------------------------------------------------
	public ValueModification(ModifiedObjectType type, String ref, String val) {
		super(type , ref);
		this.value = val;
		this.when  = ApplyWhen.ALLCREATE;
	}

	//-------------------------------------------------------------------
	public ValueModification(ModifiedObjectType type, String ref, int val, ApplyWhen when, ValueType set) {
		this(type, ref, String.valueOf(val), when, set);
	}

	//-------------------------------------------------------------------
	public ValueModification(ModifiedObjectType type, String ref, String val, ApplyWhen when, ValueType set) {
		super(type , ref);
		this.value = val;
		this.when = when;
		this.set = set;
	}

	//-------------------------------------------------------------------
	public ValueModification(ModifiedObjectType type, String ref, int val, ApplyTo to, ValueType set) {
		this(type, ref, String.valueOf(val), to, set);
	}

	//-------------------------------------------------------------------
	public ValueModification(ModifiedObjectType type, String ref, String val, ApplyTo to, ValueType set) {
		super(type , ref);
		this.value = val;
		this.apply = to;
		this.set = set;
	}

	//-------------------------------------------------------------------
 	public ValueModification clone() {
//    	try {
    		return (ValueModification) super.clone();
//    	} catch ( CloneNotSupportedException e ) {
//    		throw new InternalError();
//    	}
    }

	//-------------------------------------------------------------------
	public boolean hasFormula() {
		return value.contains("$") || value.contains("&");
	}

	//-------------------------------------------------------------------
	public boolean isModMin() {
		return value.contains("|");
	}

	//-------------------------------------------------------------------
	public boolean isDouble() {
		if (value==null) return false;
		return value.contains(".");
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String setP = "";
		String setS = "";
		if (set!=null) {
			switch (set) {
			case MAX: setP="max."; break;
			case MIN: setP="min."; break;
			}
//			switch (set) {
//			case EDGE_BONUS: setS="EdgeBonus"; break;
//			case EDGE_COST : setS="Edge Cost"; break;
//			case EDGE_USAGE: setS="EdgeUsage"; break;
//			}
		}
//		return type+":"+setP+ref+"="+value+" "+setS;
		return type+":"+ref+"(val="+value+",set="+set+",apply="+apply+",src="+source+")";
	}

	//--------------------------------------------------------------------
	/**
	 * @return The amount by which the value must be changed
	 */
	public int getValue() {
		if (value==null) {
			if (parsedValue!=null)
				System.getLogger("rpgframework.rules").log(Level.ERROR, "Value of "+this+" is NULL");
			return 0;
		}
		if (value.contains(",")) {
			return Integer.parseInt(value.substring(0, value.indexOf(",")));
		}
		if (value.contains("$")) {
			System.getLogger("rpgframework.rules").log(Level.WARNING, "Cannot perform getValue() on formula "+value+" from "+source);
			return 0;
		}
		if (value.contains("/")) return 0;
		//if (value.contains(".")) return (int)Double.parseDouble(value);
		return Integer.parseInt(value);
	}

	//--------------------------------------------------------------------
	public double getValueAsDouble() {
		if (value==null) {
			if (parsedValue!=null)
				System.getLogger("rpgframework.rules").log(Level.ERROR, "Value of "+this+" is NULL");
			return 0;
		}
		if (value.contains(",")) {
			return Double.parseDouble(value.substring(0, value.indexOf(",")));
		}
		return Double.parseDouble(value);
	}

	//-------------------------------------------------------------------
	public String[] getValueAsKeys() {
		return value.trim().split(",");
	}

	//-------------------------------------------------------------------
	public int[] getModMinValues() {
		String[] tmp =  value.trim().split("\\|");
		int[] ret = new int[tmp.length];
		for (int i=0; i<tmp.length; i++) {
			ret[i] = Integer.parseInt(tmp[i]);
		}
		return ret;
	}

	//-------------------------------------------------------------------
	public Formula getFormula() {
		if (parsedValue==null)
			parsedValue = FormulaTool.tokenize(value);

		return parsedValue;
	}

	//-------------------------------------------------------------------
	public void setFormula(Formula value) {
		parsedValue = value;
	}

	//-------------------------------------------------------------------
	/**
	 * Returns the declaration like it appears in the XML definition.
	 * Use this method, if the value contains variables/formulas
	 */
	public String getRawValue() {
		return value;
	}
	public void setRawValue(String val) {
		value = val;
	}

	//--------------------------------------------------------------------
	/**
	 * @param value The amount by which the value must be changed
	 */
	public void setValue(String value) { this.value = value; }
	public void setValue(int value) {
		this.value = String.valueOf(value);
	}

	//-------------------------------------------------------------------
	/**
	 * Get the value set
	 * @return
	 */
	public ValueType getSet() {
		return set;
	}
	public void setSet(ValueType val) { this.set = val; }

	//-------------------------------------------------------------------
	/**
	 * @return the ignoreLimit
	 */
	public boolean isIgnoreLimit() {
		return ignoreLimit;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the lookupTable
	 */
	public String[] getLookupTable() {
		return lookupTable;
	}
	public void setLookupTable(String[] table) { this.lookupTable = table; }

	//-------------------------------------------------------------------
	/**
	 * @return the instantiated
	 */
	public boolean isInstantiated() {
		return instantiated;
	}

	//-------------------------------------------------------------------
	/**
	 * @param instantiated the instantiated to set
	 */
	public void setInstantiated(boolean instantiated) {
		this.instantiated = instantiated;
	}

}
