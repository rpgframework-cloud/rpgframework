package de.rpgframework.genericrpg.modification;

import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.data.CostType;

/**
 * @author prelle
 *
 */
public class CostModification extends Modification {

	@Attribute(name="ref", required = true)
	private String ref;
	@Attribute(name="cost", required = true)
	private CostType cost;

	//-------------------------------------------------------------------
	/**
	 */
	public CostModification() {
		// TODO Auto-generated constructor stub
	}

}
