/**
 *
 */
package de.rpgframework.genericrpg.modification;

import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

/**
 * @author Stefan
 *
 */
@Root(name = "allowmod")
public class AllowModification extends DataItemModification implements Cloneable {

	public enum AllowType {
		UNSPECIFIED,
		DRACOFORM
	}

	@Attribute(name="neg")
	private boolean negate;
	@Attribute
	private AllowType what= AllowType.UNSPECIFIED;

	//--------------------------------------------------------------------
	public AllowModification() {}

	//-------------------------------------------------------------------
	public AllowModification(ModifiedObjectType type, String ref) {
		super(type, ref);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the negate
	 */
	public boolean isNegate() {
		return negate;
	}

	//-------------------------------------------------------------------
	/**
	 * @param negate the negate to set
	 */
	public void setNegate(boolean negate) {
		this.negate = negate;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public AllowType getWhat() {
		return what;
	}

	//-------------------------------------------------------------------
	public <T> List<T> getAsValues() {
		String[] multi = ref.trim().split(",");
		List<T> list = new ArrayList<>();
		for (String key : multi) {
			list.add( type.resolve(key.trim()) );
		}
		return list;
	}

}
