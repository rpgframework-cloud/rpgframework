package de.rpgframework.genericrpg;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * @author stefa
 *
 */
@SuppressWarnings("serial")
public class CalculatedValue<T> extends ArrayList<PoolCalculation<T>> {

	//-------------------------------------------------------------------
	public CalculatedValue() {
	}

	//-------------------------------------------------------------------
	public int getAsInt() {
		return (int) stream().collect(Collectors.summarizingInt(pc -> (int)pc.value)).getSum();
	}

}
