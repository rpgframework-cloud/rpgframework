package de.rpgframework.genericrpg.requirements;

/**
 * @author prelle
 *
 */
public interface ResolvableRequirement {

	public boolean resolve();
	
}
