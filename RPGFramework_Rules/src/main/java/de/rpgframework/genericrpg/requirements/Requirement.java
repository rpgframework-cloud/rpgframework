package de.rpgframework.genericrpg.requirements;

import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.data.ApplyTo;
import de.rpgframework.genericrpg.data.ReferenceException;
import de.rpgframework.genericrpg.modification.ModifiedObjectType;

/**
 * @author prelle
 *
 */
public abstract class Requirement {
	
	@Attribute(required=false)
	protected ApplyTo apply; // = ApplyTo.CHARACTER;

	/** A DataType identifier */
	@Attribute(required = false)
	protected ModifiedObjectType type;
	
	/** Comma separated list of identifiers */
	@Attribute(name="ref",required = false)
	protected String reference;

	//-------------------------------------------------------------------
	/**
	 * @return the apply
	 */
	public ApplyTo getApply() {
		return apply;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public ModifiedObjectType getType() {
		return type;
	}

	//-------------------------------------------------------------------
	public String getKey() {
		return reference;
	}

	//-------------------------------------------------------------------
	public String[] getAsKeys() {
		if (reference==null)
			return new String[0];
		return reference.trim().split(", ");
	}

	//-------------------------------------------------------------------
	/**
	 * Validate content in this modification during load
	 */
	public void validate() throws ReferenceException {
//		if (apply==null) throw new IllegalArgumentException("Missing apply= attribute in reference to "+type+":"+reference);
	}

}
