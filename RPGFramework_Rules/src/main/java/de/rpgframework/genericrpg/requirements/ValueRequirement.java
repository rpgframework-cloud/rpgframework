package de.rpgframework.genericrpg.requirements;

import java.util.UUID;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.items.Formula;
import de.rpgframework.genericrpg.items.formula.FormulaTool;
import de.rpgframework.genericrpg.modification.ModifiedObjectType;

/**
 * @author prelle
 *
 */
@Root(name = "valuereq")
public class ValueRequirement extends Requirement {

	@Attribute(name="choice", required = false)
	private UUID choice;
	@Attribute(name="variant", required = false)
	private String variantId;
	@Attribute(required=false)
	protected String value;
	private transient Formula parsedValue;
	@Attribute(required=false)
	protected String max;

	//-------------------------------------------------------------------
	public ValueRequirement() {
	}

	//-------------------------------------------------------------------
	public ValueRequirement(ModifiedObjectType type, String ref, int min) {
		super.type = type;
		super.reference = ref;
		this.value = String.valueOf(min);
	}

	public String toString() {
		return type+":"+reference+">="+value;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the choice
	 */
	public UUID getChoice() {
		return choice;
	}

	//--------------------------------------------------------------------
	/**
	 * @return The amount by which the value must be changed
	 */
	public int getValue() { return Integer.parseInt(value); }
	public int getMaxValue() { return (max!=null && !max.isBlank())?Integer.parseInt(max):0; }

	//-------------------------------------------------------------------
	public Formula getFormula() {
		if (parsedValue==null) {
			if (value!=null) {
				parsedValue = FormulaTool.tokenize(value);
			} else if (max!=null) {
				parsedValue = FormulaTool.tokenize(max);
			}
		}

		return parsedValue;
	}

	//-------------------------------------------------------------------
	public void setFormula(Formula value) {
		parsedValue = value;
	}

	//-------------------------------------------------------------------
	/**
	 * Returns the declaration like it appears in the XML definition.
	 * Use this method, if the value contains variables/formulas
	 */
	public String getRawValue() {
		return value;
	}
	public void setRawValue(String val) {
		value = val;
	}

	//--------------------------------------------------------------------
	/**
	 * @param value The amount by which the value must be changed
	 */
	public void setValue(String value) { this.value = value; }

	//-------------------------------------------------------------------
	/**
	 * @return the variantId
	 */
	public String getVariantId() {
		return variantId;
	}
}
