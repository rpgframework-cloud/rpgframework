package de.rpgframework.genericrpg.requirements;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.modification.ModifiedObjectType;

/**
 * @author prelle
 *
 */
@Root(name = "datareq")
public class ExistenceRequirement extends Requirement {

	@Attribute(required = false)
	protected boolean negate;
	@Attribute(required = false)
	protected String variant;

	//-------------------------------------------------------------------
	public ExistenceRequirement() {
	}

	//-------------------------------------------------------------------
	public ExistenceRequirement(ModifiedObjectType type, String ref) {
		this.type = type;
		this.reference = ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the negate
	 */
	public boolean isNegate() {
		return negate;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return super.toString()+":"+type;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the variant
	 */
	public String getVariant() {
		return variant;
	}

}
