package de.rpgframework.genericrpg.requirements;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.ElementListUnion;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.data.ReferenceException;


@Root(name = "selreq")
public class AnyRequirement extends Requirement implements ResolvableRequirement {
    
	@ElementListUnion({
	    @ElementList(entry="datareq"  , type=ExistenceRequirement.class),
	    @ElementList(entry="valuereq"  , type=ValueRequirement.class),
	 })
    protected List<Requirement> optionList;
    
    //-----------------------------------------------------------------------
    public AnyRequirement() {
        optionList   = new ArrayList<Requirement>();
    }
    
    //-----------------------------------------------------------------------
    public AnyRequirement(List<Requirement> mods) {
        this.optionList   = mods;
   }
    
    //-----------------------------------------------------------------------
    public AnyRequirement(Requirement... mods) {
        this.optionList   = new ArrayList<Requirement>();
        for (Requirement tmp : mods)
        	optionList.add(tmp);
    }
    
    //-----------------------------------------------------------------------
    public void add(Requirement mod) {
        if (!optionList.contains(mod)) {
            optionList.add(mod);
        }
    }
    
    //-----------------------------------------------------------------------
    public void add(Object mod) {
        if (!optionList.contains(mod) && mod instanceof Requirement) {
            optionList.add((Requirement)mod);
        }
    }
   
    //-----------------------------------------------------------------------
    public void remove(Requirement mod) {
        optionList.remove(mod);
    }
    
    //-----------------------------------------------------------------------
    public boolean equals(Object o) {
        if (o instanceof AnyRequirement) {
            AnyRequirement mc = (AnyRequirement)o;
            return optionList.equals(mc.getOptionList());
        }
        return false;
    }
    
    //-----------------------------------------------------------------------
    public Requirement[] getOptions() {
        Requirement[] modArray = new Requirement[optionList.size()];
        modArray = (Requirement[]) optionList.toArray(modArray);
        return modArray;
    }
    
    //-----------------------------------------------------------------------
    public List<Requirement> getOptionList() {
    	return new ArrayList<Requirement>(optionList);
    }
    
    //-----------------------------------------------------------------------
    public String toString() {
        StringBuffer buf = new StringBuffer("Require any of (");
        
        Iterator<Requirement> it = optionList.iterator();
        while (it.hasNext()) {
            buf.append(it.next().toString());
            if (it.hasNext())
                buf.append("|");
        }
        
        buf.append(")");
        return buf.toString();
    }

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.requirements.Requirement#resolve()
	 */
	@Override
	public boolean resolve() {
		for (Requirement req : optionList)
			if (req instanceof ResolvableRequirement && !((ResolvableRequirement)req).resolve())
				return false;
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * Validate content in this modification during load
	 */
	public void validate() throws ReferenceException {
		for (Requirement req : optionList) {
			req.validate();
		}
	}
    
}
