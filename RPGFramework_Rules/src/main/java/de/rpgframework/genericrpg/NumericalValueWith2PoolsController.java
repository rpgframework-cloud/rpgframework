package de.rpgframework.genericrpg;

import de.rpgframework.genericrpg.chargen.OperationResult;

/**
 * @author prelle
 *
 */
public interface NumericalValueWith2PoolsController<T, V extends NumericalValue<T>> extends NumericalValueWith1PoolController<T, V> {

	public String getColumn2();

	public int getPointsLeft2();
	
	public Possible canBeIncreasedPoints2(V key);
	
	public Possible canBeDecreasedPoints2(V key);

	public OperationResult<V> increasePoints2(V value);

	public OperationResult<V> decreasePoints2(V value);

	public int getPoints2(V key);
	
}
