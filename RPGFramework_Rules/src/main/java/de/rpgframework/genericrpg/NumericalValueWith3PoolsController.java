package de.rpgframework.genericrpg;

import de.rpgframework.genericrpg.chargen.OperationResult;

/**
 * @author prelle
 *
 */
public interface NumericalValueWith3PoolsController<T, V extends NumericalValue<T>> extends NumericalValueWith2PoolsController<T, V> {

	public String getColumn3();

	public int getPointsLeft3();
	
	public Possible canBeIncreasedPoints3(V key);
	
	public Possible canBeDecreasedPoints3(V key);

	public OperationResult<V> increasePoints3(V value);

	public OperationResult<V> decreasePoints3(V value);

	public int getPoints3(V key);
	
}
