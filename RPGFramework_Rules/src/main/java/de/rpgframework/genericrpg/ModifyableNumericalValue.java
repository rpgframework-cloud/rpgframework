/**
 *
 */
package de.rpgframework.genericrpg;

import java.util.List;

import de.rpgframework.genericrpg.modification.CheckModification;
import de.rpgframework.genericrpg.modification.DataItemModification;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.Modifyable;
import de.rpgframework.genericrpg.modification.ValueModification;

/**
 * @author Stefan
 *
 */
public interface ModifyableNumericalValue<T> extends Modifyable, NumericalValue<T> {

	//--------------------------------------------------------------------
	/**
	 * Shortcut to getModifiedValue(NATURAL, AUGMENTED)
	 * @return
	 */
	default int getModifiedValue() {
		return getModifiedValue(ValueType.NATURAL, ValueType.AUGMENTED);
	}

	//--------------------------------------------------------------------
	default int getModifiedValue(ValueType... typeArray) {
		List<ValueType> types = List.of(typeArray);
		boolean isNaturalOrAugmented = types.contains(ValueType.NATURAL) || types.contains(ValueType.AUGMENTED);
		int val = isNaturalOrAugmented?getDistributed():0;
		val += getModifier(typeArray);
		return val;
	}

	//--------------------------------------------------------------------
	/**
	 * Shortcut to getModifiedValue(AUGMENTED)
	 * @return
	 */
	default int getModifier() {
		return getModifier(ValueType.NATURAL, ValueType.AUGMENTED);
	}

	//--------------------------------------------------------------------
	default int getModifier(ValueType... typeArray) {
		List<ValueType> types = List.of(typeArray);
		double val = 0;
		for (Modification mod : getIncomingModifications()) {
			if (mod instanceof CheckModification) {
				continue;
			}
			if (mod instanceof ValueModification) {
				ValueModification vMod = (ValueModification)mod;
				if (types.contains( vMod.getSet()) && !vMod.isConditional()) {
					val += vMod.getValueAsDouble();
				}
			} else if (mod instanceof DataItemModification) {
				val+=1;
			}
		}
		return (int)Math.round(val);
	}

	//-------------------------------------------------------------------
	public Pool<Integer> getPool();

}
