/**
 * 
 */
package de.rpgframework.genericrpg;

import java.util.Locale;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.ResourceI18N;

/**
 * @author prelle
 *
 */
public class ToDoElement implements Comparable<ToDoElement> {
	
	public enum Severity {
		STOPPER,
		WARNING,
		INFO
	}
	
	private Severity severity;
	private String message;
	
	private String key;
	private MultiLanguageResourceBundle bundle;
	private Object[] params;

	//-------------------------------------------------------------------
	public ToDoElement(Severity sev, String mess) {
		this.severity = sev;
		this.message  = mess;
	}

	//-------------------------------------------------------------------
	public ToDoElement(Severity sev, MultiLanguageResourceBundle bundle, String key) {
		this.severity = sev;
		this.bundle   = bundle;
		this.key      = key;
	}

	//-------------------------------------------------------------------
	public ToDoElement(Severity sev, MultiLanguageResourceBundle bundle, String key, Object... params) {
		this.severity = sev;
		this.bundle   = bundle;
		this.key      = key;
		this.params   = params;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the severity
	 */
	public Severity getSeverity() {
		return severity;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the message
	 */
	public String getMessage() {
		if (key!=null && bundle!=null) {
			if (params==null)
				return ResourceI18N.get(bundle, Locale.getDefault(), key);
			return ResourceI18N.format(bundle, Locale.getDefault(), key, params);
		}
		return message;
	}

	//-------------------------------------------------------------------
	public String getMessage(Locale locale) {
		if (key!=null && bundle!=null) {
			return ResourceI18N.format(bundle, locale, key, params);
		}
		
		return message;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return getMessage();
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(ToDoElement o) {
		return Integer.compare(severity.ordinal(), o.severity.ordinal());
	}

	//-------------------------------------------------------------------
	public String getKey() {
		return key;
	}
	
}
