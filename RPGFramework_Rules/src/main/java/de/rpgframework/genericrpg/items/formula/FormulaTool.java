package de.rpgframework.genericrpg.items.formula;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;

import de.rpgframework.genericrpg.items.Formula;
import de.rpgframework.genericrpg.items.IItemAttribute;
import de.rpgframework.genericrpg.items.ItemAttributeFloatValue;
import de.rpgframework.genericrpg.items.ItemAttributeFormulaValue;
import de.rpgframework.genericrpg.items.ItemAttributeNumericalValue;
import de.rpgframework.genericrpg.items.ItemAttributeObjectValue;
import de.rpgframework.genericrpg.items.ItemAttributeValue;
import de.rpgframework.genericrpg.items.formula.FormulaElement.Operation;
import de.rpgframework.genericrpg.items.formula.FormulaElement.Type;
import de.rpgframework.genericrpg.modification.ModifiedObjectType;

/**
 * @author prelle
 *
 */
public class FormulaTool {

	protected final static Logger logger = System.getLogger(FormulaTool.class.getPackageName());

	private final static String OPERANDS = "*/+-^";

	private static enum State {
		UNKNOWN,
		// Currently a number is parsed
		NUMBER,
		VARIABLE,
		STRING,
	}

	//-------------------------------------------------------------------
	public static ItemAttributeValue convertToAttributeValue(IItemAttribute attrib, Formula form) {
		if (form.isInteger()) {
			return new ItemAttributeNumericalValue<IItemAttribute>(attrib, form.getAsInteger());
		}
		if (form.isFloat()) {
			return new ItemAttributeFloatValue<IItemAttribute>(attrib, form.getAsFloat());
		}
		if (form.isObject()) {
			return new ItemAttributeObjectValue<IItemAttribute>(attrib, form.getValue());
		}
		return new ItemAttributeFormulaValue<IItemAttribute>(attrib, form);
	}


	//-------------------------------------------------------------------
	public static FormulaImpl tokenize(String formula) {
		FormulaImpl ret = new FormulaImpl();
		State state = State.UNKNOWN;
		StringBuffer buf = new StringBuffer();
		FormulaElement current = null;
		Stack<SubFormulaElement> subFormStack = new Stack<>();

		int last=0;
		for (int i=0; i<formula.length(); i++) {
			char c = formula.charAt(i);

			logger.log(Level.TRACE, "Found {0} while current={1} and state {2}", c, current, state);
			switch (state) {
			case UNKNOWN:
				// We don't know what we get
				if (Character.isDigit(c)) {
					// A number started
					buf = new StringBuffer();
					if (ret.getLastElement()!=null && ret.getLastElement().getType()==Type.OPERATION && ret.getElements().size()==1 && ((OperandElement)ret.getLastElement()).getOperation()==Operation.SUBSTRACT) {
						current = new NumberElement(i);
						ret.getElements().clear();
						buf.append('-');
					}
					current = new NumberElement(i);
					ret.addElement(current);
					buf.append(c);
					state = State.NUMBER;
				} else if (c=='$' || c=='&') {
					// A variable started
					current = new VariableElement(i);
					ret.addElement(current);
					buf = new StringBuffer();
					buf.append(c);
					state = State.VARIABLE;
				} else if (c=='(') {
					// A subformula started
					current = new SubFormulaElement(ret,i);
					subFormStack.push((SubFormulaElement) current);
					ret.addElement(current);
					ret = (FormulaImpl) ((SubFormulaElement)current).getValue();
					buf = new StringBuffer();
					buf.append(c);
					state = State.UNKNOWN;
				} else if (c==')') {
					// SubFormula ends
					logger.log(Level.DEBUG, "SubFormula ends: "+subFormStack.peek());
					SubFormulaElement closed = subFormStack.pop();
					ret = closed.getParent();
					logger.log(Level.DEBUG, "       ret now : "+ret);
				} else if (OPERANDS.indexOf(c)>=0) {
					switch (c) {
					case '+': ret.addElement(new OperandElement(Operation.ADD, i)); state=State.UNKNOWN; break;
					case '-': ret.addElement(new OperandElement(Operation.SUBSTRACT, i)); state=State.UNKNOWN; break;
					case '*': ret.addElement(new OperandElement(Operation.MULTIPLY, i)); state=State.UNKNOWN; break;
					case '/': ret.addElement(new OperandElement(Operation.DIVIDE, i)); state=State.UNKNOWN; break;
					case '^': ret.addElement(new OperandElement(Operation.EXPONENTIATE, i)); state=State.UNKNOWN; break;
					}
					state = State.UNKNOWN;
				} else if (Character.isWhitespace(c)) {
					// Continue state UNKNOWN
				} else {
					// A number started
					current = new StringElement(i);
					ret.addElement(current);
					buf = new StringBuffer();
					buf.append(c);
					state = State.STRING;
				}
				break;
			case NUMBER:
				if (Character.isDigit(c) || c=='.') {
					// Continue number
					buf.append(c);
				} else {
					((NumberElement)current).setRaw(buf.toString());
					try {
						((NumberElement)current).setValue( Float.parseFloat(buf.toString()) );
					} catch (NumberFormatException e) {
						throw new FormulaException(formula, "Not a valid number '"+current.getRaw()+"' @"+current.getStartPos(),current.getStartPos());
					}
					if (Character.isWhitespace(c)) {
						state = State.UNKNOWN;
					} else if (OPERANDS.indexOf(c)>=0) {
						switch (c) {
						case '+': ret.addElement(new OperandElement(Operation.ADD, i)); state=State.UNKNOWN; break;
						case '-': ret.addElement(new OperandElement(Operation.SUBSTRACT, i)); state=State.UNKNOWN; break;
						case '*': ret.addElement(new OperandElement(Operation.MULTIPLY, i)); state=State.UNKNOWN; break;
						case '/': ret.addElement(new OperandElement(Operation.DIVIDE, i)); state=State.UNKNOWN; break;
						case '^': ret.addElement(new OperandElement(Operation.EXPONENTIATE, i)); state=State.UNKNOWN; break;
						}
						state = State.UNKNOWN;
					} else if (c==')') {
						// SubFormula ends
						if (subFormStack.isEmpty()) {
							logger.log(Level.ERROR, "Closing bracket, but there seems to be no open one left: "+formula);
							throw new IllegalArgumentException("Closing bracket "+last+", but there seems to be no open one left: "+formula);
						}
						logger.log(Level.DEBUG, "SubFormula ends: "+subFormStack.peek());
						SubFormulaElement closed = subFormStack.pop();
						ret = closed.getParent();
						state = State.UNKNOWN;
						logger.log(Level.DEBUG, "       ret now : "+ret);
					} else {
						// A number started
						current = new StringElement(i);
						ret.addElement(current);
						buf = new StringBuffer();
						buf.append(c);
						state = State.STRING;
					}
				}
				break;
			case VARIABLE:
				if (Character.isJavaIdentifierPart(c)) {
					// Continue variable
					buf.append(c);
				} else {
					((VariableElement)current).setRaw(buf.toString());
					if (Character.isWhitespace(c)) {
						state = State.UNKNOWN;
					} else if (OPERANDS.indexOf(c)>=0) {
						switch (c) {
						case '+': ret.addElement(new OperandElement(Operation.ADD, i)); state=State.UNKNOWN; break;
						case '-': ret.addElement(new OperandElement(Operation.SUBSTRACT, i)); state=State.UNKNOWN; break;
						case '*': ret.addElement(new OperandElement(Operation.MULTIPLY, i)); state=State.UNKNOWN; break;
						case '/': ret.addElement(new OperandElement(Operation.DIVIDE, i)); state=State.UNKNOWN; break;
						case '^': ret.addElement(new OperandElement(Operation.EXPONENTIATE, i)); state=State.UNKNOWN; break;
						}
						state = State.UNKNOWN;
					} else if (Character.isDigit(c)) {
						// A number started
						current = new NumberElement(i);
						ret.addElement(current);
						buf = new StringBuffer();
						buf.append(c);
						state = State.NUMBER;
					} else if (c=='(') {
						// A subformula started
						current = new SubFormulaElement(ret,i);
						subFormStack.push((SubFormulaElement) current);
						ret.addElement(current);
						ret = (FormulaImpl) ((SubFormulaElement)current).getValue();
						buf = new StringBuffer();
						buf.append(c);
						state = State.UNKNOWN;
					} else {
						// A number started
						current = new StringElement(i);
						ret.addElement(current);
						buf = new StringBuffer();
						buf.append(c);
						state = State.STRING;
					}
				}
				break;
			case STRING:
				if (Character.isWhitespace(c)) {
					current.setRaw(buf.toString());
					state = State.UNKNOWN;
				} else if (Character.isDigit(c)) {
					current.setRaw(buf.toString());
					// A number started
					current = new NumberElement(i);
					ret.addElement(current);
					buf = new StringBuffer();
					buf.append(c);
					state = State.NUMBER;
				} else if (c=='$') {
					current.setRaw(buf.toString());
					// A variable started
					current = new VariableElement(i);
					ret.addElement(current);
					buf = new StringBuffer();
					buf.append(c);
					state = State.VARIABLE;
				} else if (c=='(') {
					// A subformula started
					current.setRaw(buf.toString());
					current = new SubFormulaElement(ret,i);
					subFormStack.push((SubFormulaElement) current);
					ret.addElement(current);
					ret = (FormulaImpl) ((SubFormulaElement)current).getValue();
					buf = new StringBuffer();
					buf.append(c);
					state = State.UNKNOWN;
				} else {
					buf.append(c);
				}
				break;
			default:
				throw new FormulaException(formula, "Parser error: Don't know how to parse in type "+current.getType(),i);
			}
		}

		// Close formula
		switch (state) {
		case NUMBER:
			current.setRaw(buf.toString());
			try {
				((NumberElement)current).setValue( Float.parseFloat(buf.toString()) );
			} catch (NumberFormatException e) {
				throw new FormulaException(formula, "Not a valid number '"+current.getRaw()+"' @"+current.getStartPos(),current.getStartPos());
			}
			break;
		case VARIABLE:
		case STRING:
			current.setRaw(buf.toString());
			break;
		case UNKNOWN:
			break;
		default:
			throw new FormulaException(formula, "Parser error: Don't know how to close "+current.getType(),formula.length());
		}


		return ret;
	}

	//-------------------------------------------------------------------
	private static FormulaImpl calculate(FormulaImpl formula, FormulaElement.Operation...allowed) {
		// If not all elements are resolved, return self
		if (!formula.isResolved())
			return formula;

		List<FormulaElement.Operation> allAllowed = Arrays.asList(allowed);
		FormulaImpl resolvedFormula = new FormulaImpl();
		FormulaElement last = null;
		for (int i=0; i<formula.getElements().size(); i++) {
			FormulaElement elem = formula.getElements().get(i);
			logger.log(Level.DEBUG, "calculate: Check "+elem);
			if (elem.getType()==Type.OPERATION && allAllowed.contains( ((OperandElement)elem).getOperation() )) {
				NumberElement op1 = null;
				FormulaElement rawOp1 = resolvedFormula.getLastElement();
				if (rawOp1==null) {
					// Assume missing 0
					rawOp1 = new NumberElement(0, 0);
				}

				if (rawOp1 instanceof NumberElement) {
					op1 = (NumberElement)rawOp1;
				} else if (rawOp1 instanceof StringElement) {
					Object attribute = ((StringElement)rawOp1).getValue();
					logger.log(Level.ERROR, "Parse string"+attribute+" / "+attribute.getClass());
					System.exit(1);
				} else {
					logger.log(Level.ERROR, "Parse "+rawOp1+" / "+((rawOp1!=null)?rawOp1.getClass():"null"));
					System.exit(1);
				}
				NumberElement op2 = (NumberElement) formula.getElements().get(++i);
				resolvedFormula.getElements().remove(op1);
				switch (((OperandElement)elem).getOperation()) {
				case ADD:
					resolvedFormula.addElement(new NumberElement(op1.getValueAsFloat() + op2.getValueAsFloat(), -1));
					break;
				case SUBSTRACT:
					resolvedFormula.addElement(new NumberElement(op1.getValueAsFloat() - op2.getValueAsFloat(), -1));
					break;
				case MULTIPLY:
					resolvedFormula.addElement(new NumberElement(op1.getValueAsFloat() * op2.getValueAsFloat(), -1));
					break;
				case DIVIDE:
					resolvedFormula.addElement(new NumberElement(op1.getValueAsFloat() / op2.getValueAsFloat(), -1));
					break;
				}
			} else
				resolvedFormula.addElement(elem);
		}
		return resolvedFormula;
	}

	//-------------------------------------------------------------------
	/**
	 * @param formula
	 * @param contextItem
	 * @param contextUser
	 * @return
	 */
	public static String resolve(ModifiedObjectType toSetAttrib, Formula formula, VariableResolver resolver) {
		if (logger.isLoggable(Level.TRACE)) logger.log(Level.TRACE, "ENTER: Resolve formula: {0}",formula);

		FormulaImpl resolvedFormula = new FormulaImpl();
		for (FormulaElement elem : ((FormulaImpl)formula).getElements()) {
			logger.log(Level.TRACE, "  Element {0}",elem);
			if (elem.needsResolving()) {
				if (elem instanceof VariableElement) {
					VariableElement var = (VariableElement)elem;
					FormulaElement resolvForm = resolver.resolve(toSetAttrib, var.getRaw());
//					logger.log(Level.DEBUG, "Variable "+var+" resolved to "+resolvForm);
					if (resolvForm!=null)
						resolvedFormula.addElement(resolvForm);
					else {
						// Resolution failed - return null
						if (logger.isLoggable(Level.TRACE)) logger.log(Level.TRACE, "LEAVE: Failed resolving variable "+var);
							return null;
							//resolvedFormula.addElement(elem);
					}
				} else if (elem instanceof SubFormulaElement) {
					SubFormulaElement sub = (SubFormulaElement)elem;
					String foo = resolve(toSetAttrib, sub.getSubFormula(), resolver);
					sub.setRaw(foo);
					logger.log(Level.DEBUG, "    Subformula "+sub+" resolved to "+foo);
					if (foo==null) {
						if (logger.isLoggable(Level.TRACE)) logger.log(Level.TRACE, "LEAVE: Failed resolving subformula "+sub);
						return null;
					}
					try {
						NumberElement toAdd = new NumberElement(Float.parseFloat(foo), elem.getStartPos());
						logger.log(Level.DEBUG, "    replace {0} with {1}", sub, toAdd);
						resolvedFormula.getElements().remove(sub);
						resolvedFormula.addElement(toAdd);
						logger.log(Level.DEBUG, "    formula now {0}", resolvedFormula);
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						System.exit(1);
					}
				}
			} else if (elem instanceof SubFormulaElement) {
				SubFormulaElement sub = (SubFormulaElement)elem;
				String foo = resolve(toSetAttrib, sub.getSubFormula(), resolver);
				sub.setRaw(foo);
				logger.log(Level.DEBUG, "Subformula "+sub+" resolved to "+foo);
				try {
					NumberElement toAdd = new NumberElement(Float.parseFloat(foo), elem.getStartPos());
					logger.log(Level.DEBUG, "  replace {0} with {1}", sub, toAdd);
					resolvedFormula.getElements().remove(sub);
					resolvedFormula.addElement(toAdd);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.exit(1);
				}
			} else {
				resolvedFormula.addElement(elem);
			}
//
//			// Add to return string
//			ret.append(elem.value);
		}
		if (logger.isLoggable(Level.TRACE)) logger.log(Level.TRACE, "Resolved formula; "+resolvedFormula);

		// Perform MULTIPLY or DIVIDE
		FormulaImpl r2 = calculate(resolvedFormula, FormulaElement.Operation.DIVIDE, FormulaElement.Operation.MULTIPLY);
		if (logger.isLoggable(Level.TRACE)) logger.log(Level.TRACE, "Resolve formula2: "+r2);

		// Perform ADD or SUBSTRICT
		FormulaImpl r3 = calculate(r2, FormulaElement.Operation.ADD, FormulaElement.Operation.SUBSTRACT);
		if (logger.isLoggable(Level.TRACE)) logger.log(Level.TRACE, "Resolve formula3: "+r3);

		// Convert to String
		if (r3.getElements().size()==1) {
			FormulaElement elem = r3.getLastElement();
			if (elem.getType()==Type.NUMBER) {
				NumberElement num = (NumberElement)elem;
				if (num.getValueAsFloat()!=num.getValueAsInt()) {
					if (logger.isLoggable(Level.INFO)) logger.log(Level.INFO, "LEAVE: Resolve formula: {0}",num.getValueAsFloat());
					return  String.valueOf( num.getValueAsFloat() );
				}
				if (logger.isLoggable(Level.INFO)) logger.log(Level.INFO, "LEAVE: Resolve formula: {0}",num.getValueAsInt());
				return  String.valueOf( num.getValueAsInt() );
			}
			if (logger.isLoggable(Level.INFO)) logger.log(Level.INFO, "LEAVE: Resolve formula: "+elem.toString());
			return elem.toString();
		} else {
			if (logger.isLoggable(Level.INFO)) logger.log(Level.INFO, "LEAVE: Resolve formula: "+String.join(" ", r3.getElements().stream().map(e -> e.toString()).collect(Collectors.toList())));
			return String.join(" ", r3.getElements().stream().map(e -> e.toString()).collect(Collectors.toList()));
		}
	}

}
