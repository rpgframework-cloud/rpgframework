package de.rpgframework.genericrpg.items;

import java.util.List;

import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.Lifeform;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModifiedObjectType;

/**
 * @author prelle
 *
 */
public interface CarriedItemProcessor {

	/**
	 * @param unprocessed Unprocessed modifications from previous steps
	 * @return Unprocessed modification
	 */
	public OperationResult<List<Modification>> process(boolean strict, ModifiedObjectType refType, Lifeform charac, CarriedItem<?> model, List<Modification> unprocessed);

}
