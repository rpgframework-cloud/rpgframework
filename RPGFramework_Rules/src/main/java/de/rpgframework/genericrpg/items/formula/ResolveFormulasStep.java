package de.rpgframework.genericrpg.items.formula;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.prelle.simplepersist.StringValueConverter;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.Lifeform;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.CarriedItemProcessor;
import de.rpgframework.genericrpg.items.IItemAttribute;
import de.rpgframework.genericrpg.items.ItemAttributeDefinition;
import de.rpgframework.genericrpg.items.ItemAttributeFloatValue;
import de.rpgframework.genericrpg.items.ItemAttributeNumericalValue;
import de.rpgframework.genericrpg.items.ItemAttributeObjectValue;
import de.rpgframework.genericrpg.items.ItemAttributeValue;
import de.rpgframework.genericrpg.items.PieceOfGear;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModifiedObjectType;
import de.rpgframework.genericrpg.persist.IntegerConverter;

/**
 * @author prelle
 *
 */
public class ResolveFormulasStep implements CarriedItemProcessor {

	protected final static Logger logger = System.getLogger(FormulaTool.class.getPackageName());

	//-------------------------------------------------------------------
	public ResolveFormulasStep() {
	}


	//-------------------------------------------------------------------
	private FormulaElement resolveVariable(CarriedItem model, IItemAttribute toSetAttrib, String var) {
		String tail = var.substring(1);
		if (tail.startsWith("$")) {
			logger.log(Level.WARNING, "TODO: ask parent");
			System.exit(0);
		} else {
			// Try to get ItemAttribute by name
			IItemAttribute attrib = toSetAttrib.resolve(tail);

			logger.log(Level.INFO, var+" resolved to ItemAttribute "+attrib);
			ItemAttributeValue val = model.getAttributeRaw(attrib);
			if (val!=null) {
				logger.log(Level.INFO, var+" resolved to value "+val);
				if (val instanceof ItemAttributeNumericalValue) {
					return new NumberElement( ((ItemAttributeNumericalValue)val).getDistributed()  , -1);
				} else {
					return new StringElement(val.toString(), -1);
				}
			} else {
				logger.log(Level.WARNING, var+" cannot be resolved - attribute is not set");
				return null;
			}
		}
		return null;
	}


	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.CarriedItemProcessor#process(java.lang.String, de.rpgframework.genericrpg.items.CarriedItem, java.util.List)
	 */
	@Override
	public OperationResult<List<Modification>> process(boolean strict, ModifiedObjectType refType, Lifeform user, CarriedItem<?> model, List<Modification> unprocessed) {
		OperationResult<List<Modification>> ret = new OperationResult<>(unprocessed);
//		logger.log(Level.WARNING, "resolveFormulas");

		PieceOfGear<?,?,?,?> template = model.getResolved();
		Collection<ItemAttributeDefinition> attribs = template.getAttributes(model.getVariantID());
		// Get all attributes from the default item template
		attribs.stream().forEach( val -> {
			IItemAttribute attrib = val.getModifyable();
			logger.log(Level.DEBUG, "Check attribute {0} with {1}", val, val.getFormula());
			FormulaImpl formula = (FormulaImpl) val.getFormula();
			if (!formula.isResolved()) {
				String resolvedString = FormulaTool.resolve(refType, formula, new VariableResolver(model, user));
				if (resolvedString==null) {
					logger.log(Level.DEBUG, "Not resolved yet: "+formula);
				} else {
					if (val.getLookupTable()!=null) {
						logger.log(Level.DEBUG, "Lookup '{0}' in table {1}", resolvedString, Arrays.toString(val.getLookupTable()));
						String tmp = val.getLookupTable()[Integer.parseInt(resolvedString)-1];
						logger.log(Level.DEBUG, "Resolved {0} maps to {1}", resolvedString,tmp);
						resolvedString = tmp;
					}
					// Convert to expected value
					logger.log(Level.DEBUG, "{0}: Handing ''{1}'' to converter {2}", attrib.name(), resolvedString, attrib.getConverter());
					try {
						StringValueConverter<?> conv = attrib.getConverter();
						if (conv==null)
							conv = new IntegerConverter();
						Object resolved = conv.read(resolvedString);

						logger.log(Level.INFO, val.getModifyable() + ": RAW " + val.getRawValue() + " ==> " + formula + " ==> "
								+ resolved);
						if (resolved instanceof Integer) {
							ItemAttributeNumericalValue<?> aVal = new ItemAttributeNumericalValue<>(
									val.getModifyable());
							aVal.setDistributed((int) resolved);
							model.setAttribute(aVal.getModifyable(), aVal);
							logger.log(Level.DEBUG, "Set attribute "+val+" to numerical "+aVal+" because type is "+resolved.getClass());
						} else if (resolved instanceof Float) {
							ItemAttributeFloatValue<?> aVal = new ItemAttributeFloatValue<>(
									val.getModifyable());
							aVal.setDistributed((float) resolved);
							model.setAttribute(aVal.getModifyable(), aVal);
							logger.log(Level.DEBUG, "Set attribute "+val+" to float "+aVal+" because type is "+resolved.getClass());
						} else {
							ItemAttributeObjectValue<?> aVal = new ItemAttributeObjectValue<>(val);
							aVal.setValue(resolved);
							model.setAttribute(aVal.getModifyable(), aVal);
							logger.log(Level.DEBUG, "Set attribute "+val+" to object "+aVal+" because type is "+resolved.getClass());
						}
					} catch (Exception e) {
						logger.log(Level.ERROR, "Failed converting '" + resolvedString + "' using " + attrib.getConverter()
								+ " of attribute " + attrib, e);
						ret.addMessage(new ToDoElement(Severity.STOPPER, "Failed converting '" + resolvedString
								+ "' using " + attrib.getConverter() + " of attribute " + attrib));
					}
				}
			}
		});

		// Now modifications
		for (Modification tmp : model.getIncomingModifications()) {
			logger.log(Level.INFO, model.getKey()+": "+tmp+" Modification -> "+tmp);
			System.exit(1);
		}

		return ret;
	}

}
