package de.rpgframework.genericrpg.items;

/**
 * Needs to be extended by implementing RPGs
 * 
 * <code>
 * public class SR6WeaponData implements GearTypeData {
 *   ...
 * }
 * </code>
 * 
 * @author prelle
 *
 */
public interface IGearTypeData {

	public void copyToAttributes(AGearData copyTo);
	
}
