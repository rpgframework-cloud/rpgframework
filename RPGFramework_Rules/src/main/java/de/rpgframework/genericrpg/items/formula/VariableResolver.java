package de.rpgframework.genericrpg.items.formula;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;

import de.rpgframework.genericrpg.NumericalValue;
import de.rpgframework.genericrpg.data.AttributeValue;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.data.IAttribute;
import de.rpgframework.genericrpg.data.Lifeform;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.IItemAttribute;
import de.rpgframework.genericrpg.items.ItemAttributeNumericalValue;
import de.rpgframework.genericrpg.items.ItemAttributeValue;
import de.rpgframework.genericrpg.items.ItemEnhancementValue;
import de.rpgframework.genericrpg.modification.ModifiedObjectType;

/**
 * @author prelle
 *
 */
public class VariableResolver {

	protected final static Logger logger = System.getLogger(VariableResolver.class.getPackageName());

	private ComplexDataItemValue<? extends ComplexDataItem> model;
	private Lifeform user;

	//-------------------------------------------------------------------
	public VariableResolver(ComplexDataItemValue<? extends ComplexDataItem> contextItem, Lifeform contextUser) {
		this.model = contextItem;
		this.user  = contextUser;
	}

	//-------------------------------------------------------------------
	private FormulaElement getItemAttributeFromParents(ModifiedObjectType toSetAttrib, String var) {
		CarriedItem<?> parent = ((CarriedItem<?>)model).getParent();
		if (parent==null) {
			logger.log(Level.ERROR, "Need to determine item attribute {0} from parent of CarriedItem {1}, but parent is NULL", var, model.getKey());
			return null;
		}
		VariableResolver res = new VariableResolver(parent, user);
		return res.resolve(toSetAttrib, var);
	}

	//-------------------------------------------------------------------
	public FormulaElement resolve(ModifiedObjectType toSetAttrib, String var) {
		String tail = var.substring(1);
		if (tail.startsWith("$")) {
			logger.log(Level.DEBUG, "ask parent of {0} for {1}", model, tail);
			if (model instanceof CarriedItem) {
				FormulaElement ret = getItemAttributeFromParents(toSetAttrib, tail);
				logger.log(Level.DEBUG, "{0} is resolved to {1}", tail, ret);
				return ret;
			} else {
				logger.log(Level.ERROR, "Don't support obtaining a parent for "+model.getClass());
			}
		} else if (var.startsWith("&")) {
			logger.log(Level.TRACE, "ask character for {0}", tail);
			AttributeValue<?> val = user.getAttribute(tail);
			if (val!=null) {
				logger.log(Level.INFO, var+" resolved to value "+val);
				//System.err.println("VariableResolver: ToDo: implement listening to character attribute changes");
				return new NumberElement( val.getDistributed()  , -1);
			} else {
				logger.log(Level.WARNING, var+" cannot be resolved - attribute is not set");
				System.err.println("VariableResolver: "+tail+" cannot be resolved - attribute is not set");
				System.exit(0);
				return null;
			}
		} else {
			// Question: Prefer decision or eventually modified attribute value

			// Check decisions
			Decision dec = model.getDecisionByRef(tail);
			if (dec!=null) {
				try {
					return new NumberElement(Integer.parseInt(dec.getValue())  , -1);
				} catch (NumberFormatException e) {
				}
			} else {
				logger.log(Level.DEBUG, "Resolve as {0}: [{1}] in {2}]",toSetAttrib, tail,model.getResolved());
			}

			// Now find out if reference type of eventually matching choices differs from ModifiedObjectType from caller
			if (model.getResolved()!=null) {
				Choice choice = ((ComplexDataItem)model.getResolved()).getChoice(tail);
				if (choice!=null && choice.getChooseFrom()!=toSetAttrib) {
					return null;
				}
			}

			if ("LEVEL".equals(tail)) {
				return  new NumberElement(model.getDistributed(), -1);
			}

			Object unknownType = toSetAttrib.resolve(tail);
			if (unknownType instanceof IAttribute) {
				AttributeValue<?> val = (AttributeValue<?>) user.getAttribute((IAttribute) unknownType);
				logger.log(Level.DEBUG, "resolved ATTRIBUTE {0} to {1}",tail,val);
				if (val instanceof NumericalValue) {
					return new NumberElement( ((NumericalValue<?>)val).getDistributed()  , -1);
				} else {
					return new StringElement(val.toString(), -1);
				}
			}
			// Try to get ItemAttribute by name
			IItemAttribute attrib = toSetAttrib.resolve(tail);

			if (model instanceof ItemEnhancementValue) {
				ItemEnhancementValue<?> enhVal = (ItemEnhancementValue<?>) model;
				if (tail.equals("RATING"))
					return new NumberElement( enhVal.getDistributed(), -1);
				logger.log(Level.ERROR, "Don't know how to resolve {0} in ItemEnhancementValue {1}", tail,enhVal);
				System.exit(1);
			}

			logger.log(Level.INFO, var+" resolved to ItemAttribute "+attrib);
			ItemAttributeValue<?> val = ((CarriedItem)model).getAttributeRaw(attrib);
			if (val!=null) {
				logger.log(Level.INFO, var+" resolved to value "+val);
				if (val instanceof ItemAttributeNumericalValue) {
					return new NumberElement( ((ItemAttributeNumericalValue<?>)val).getDistributed()  , -1);
				} else {
					return new StringElement(val.toString(), -1);
				}
			} else {
				//logger.log(Level.WARNING, var+" cannot be resolved - attribute is not set");
				return null;
			}
		}
		return null;

	}

}
