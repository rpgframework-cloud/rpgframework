package de.rpgframework.genericrpg.items;

import de.rpgframework.genericrpg.data.ComplexDataItemValue;

public class ItemEnhancementValue<E extends AItemEnhancement> extends ComplexDataItemValue<E> {

	//--------------------------------------------------------------------
	public ItemEnhancementValue() {
	}

	//--------------------------------------------------------------------
	public ItemEnhancementValue(E ref) {
		setResolved(ref);
	}

//	//--------------------------------------------------------------------
//	public ItemEnhancementValue(ItemEnhancement ref, boolean auto) {
//		setResolved(ref);
//		this.autoAdded = auto;
//	}
//
//	//-------------------------------------------------------------------
//	/**
//	 * @return the autoAdded
//	 */
//	public boolean isAutoAdded() {
//		return autoAdded;
//	}

}
