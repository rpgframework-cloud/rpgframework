package de.rpgframework.genericrpg.items.formula;

/**
 * @author prelle
 *
 */
public class OperandElement extends FormulaElement {
	
	private Operation operation;

	//-------------------------------------------------------------------
	/**
	 * @param type
	 * @param startPos
	 */
	public OperandElement(Operation op, int startPos) {
		super(Type.OPERATION, startPos);
		this.operation = op;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.formula.FormulaElement#needsResolving()
	 */
	@Override
	public boolean needsResolving() {
		return false;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return operation+"";
	}

	//-------------------------------------------------------------------
	/**
	 * @return the operation
	 */
	public Operation getOperation() {
		return operation;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.formula.FormulaElement#getValue()
	 */
	@Override
	public Object getValue() {
		return null;
	}

}
