package de.rpgframework.genericrpg.items;

import java.util.List;

/**
 * @author prelle
 *
 */
public class OperationModeOption {
	
	private CarriedItem<?> source;
	private List<OperationMode> modes;

	//-------------------------------------------------------------------
	public OperationModeOption(CarriedItem<?> model, List<OperationMode> modes) {
		this.source = model;
		this.modes  = modes;
	}

	//-------------------------------------------------------------------
	public String toString( ) {
		return source.getKey()+":"+modes;
	}

	//-------------------------------------------------------------------
	public CarriedItem<?> getSource() {
		return source;
	}

	//-------------------------------------------------------------------
	public List<OperationMode> getModes() {
		return modes;
	}

}
