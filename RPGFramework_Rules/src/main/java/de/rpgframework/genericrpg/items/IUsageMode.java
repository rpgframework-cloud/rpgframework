package de.rpgframework.genericrpg.items;

import java.util.Locale;

/**
 * Is the item used e.g. in Melee or Ranged combat.
 * Intended as a selector e.g. for character sheet sections
 * 
 * @author prelle
 *
 */
public interface IUsageMode {

	public String getName(Locale locale);
	public String getName();

}
