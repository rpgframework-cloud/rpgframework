package de.rpgframework.genericrpg.items.formula;

/**
 * @author prelle
 *
 */
public class StringElement extends FormulaElement {

	//-------------------------------------------------------------------
	public StringElement(int startPos) {
		super(Type.STRING, startPos);
	}

	//-------------------------------------------------------------------
	public StringElement(String raw, int startPos) {
		super(Type.STRING, startPos);
		setRaw(raw);
	}

	//-------------------------------------------------------------------
	public String toString() {
		return getRaw();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.formula.FormulaElement#needsResolving()
	 */
	@Override
	public boolean needsResolving() {
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.formula.FormulaElement#getValue()
	 */
	@Override
	public Object getValue() {
		return super.getRaw();
	}

}
