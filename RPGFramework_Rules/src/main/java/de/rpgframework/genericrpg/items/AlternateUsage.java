package de.rpgframework.genericrpg.items;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.IgnoreMissingAttributes;

/**
 * @author prelle
 *
 */
@IgnoreMissingAttributes("id")
public abstract class AlternateUsage<U extends IUsageMode> extends AGearData {

	// TODO: Definition wie Nahkampf, Fernkampf
	// oder normal, montiert,
	@Attribute
	protected U mode;
	
	//-------------------------------------------------------------------
	public U getUsageMode() {
		return mode;
	}
	
}
