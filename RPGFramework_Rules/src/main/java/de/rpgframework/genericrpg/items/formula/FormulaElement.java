package de.rpgframework.genericrpg.items.formula;

/**
 * @author prelle
 *
 */
public abstract class FormulaElement {

	public static enum Type {
		OPERATION,
		VARIABLE,
		NUMBER,
		OBJECT,
		STRING,
		FORMULA
	}

	public static enum Operation {
		ADD,
		SUBSTRACT,
		MULTIPLY,
		DIVIDE,
		EXPONENTIATE
	}

	private Type type;
	private int startPos;
	private String raw;

	//-------------------------------------------------------------------
	public FormulaElement(Type type, int startPos) {
		this.type = type;
		this.startPos = startPos;
	}

	//-------------------------------------------------------------------
	public Type getType() {
		return type;
	}

	//-------------------------------------------------------------------
	public abstract boolean needsResolving();

	//-------------------------------------------------------------------
	/**
	 * @return the raw
	 */
	public String getRaw() {
		return raw;
	}

	//-------------------------------------------------------------------
	/**
	 * @param raw the raw to set
	 */
	public void setRaw(String raw) {
		this.raw = raw;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the startPos
	 */
	public int getStartPos() {
		return startPos;
	}

	//-------------------------------------------------------------------
	public abstract Object getValue();

}
