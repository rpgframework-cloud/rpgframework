package de.rpgframework.genericrpg.items;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.List;

import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.Lifeform;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModifiedObjectType;

/**
 * This whole class may be pointless, since resolving is already done elsewhere
 * @author prelle
 *
 */
public class ResolveVariantStep implements CarriedItemProcessor {

	private final static Logger logger = CarriedItem.logger;

	//-------------------------------------------------------------------
	/**
	 */
	public ResolveVariantStep() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.CarriedItemProcessor#process(java.lang.String, de.rpgframework.genericrpg.items.CarriedItem, java.util.List)
	 */
	@Override
	@SuppressWarnings("rawtypes")
	public OperationResult<List<Modification>> process(boolean strict, ModifiedObjectType refType, Lifeform user, CarriedItem<?> model, List<Modification> unprocessed) {
		OperationResult<List<Modification>> ret = new OperationResult<>(unprocessed);
		// Nothing to do, if there is no given variant
		if (model.getVariantID()==null)
			return ret;
		// Is it already resolved?
		if (model.getVariant()!=null && model.getVariant().getId().equals(model.getVariantID()))
			return ret;

		PieceOfGear template = model.getModifyable();
		logger.log(Level.DEBUG, "Resolve variant {0}/{1}", model.getTemplateID(), model.getVariantID());
		model.setVariant( template.getVariant(model.getVariantID()) );

		return ret;
	}

}
