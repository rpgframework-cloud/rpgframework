package de.rpgframework.genericrpg.items;

import java.util.Locale;

/**
 * @author stefa
 *
 */
public interface ItemFlag {

	/** Enum method  - returns String for XML */
	public String name();

	public String getName();

	public String getName(Locale loc);

}
