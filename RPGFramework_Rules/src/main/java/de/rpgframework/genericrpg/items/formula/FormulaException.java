package de.rpgframework.genericrpg.items.formula;

/**
 * @author prelle
 *
 */
public class FormulaException extends IllegalArgumentException {
	
	private String formula;
	private int position;

	//-------------------------------------------------------------------
	public FormulaException(String form, String message) {
		super(message);
		this.formula = form;
	}

	//-------------------------------------------------------------------
	public FormulaException(String form, String message, int pos) {
		this(form, message);
		this.position = pos;
	}

	//-------------------------------------------------------------------
	public String getFormula() {
		return formula;
	}

}
