package de.rpgframework.genericrpg.items;

import java.util.List;

import de.rpgframework.genericrpg.data.Decision;

/**
 * @author prelle
 *
 */
public class ItemConfiguration {
	
	private PieceOfGearVariant variant;
	private List<Decision> decisions; 

	//-------------------------------------------------------------------
	public ItemConfiguration() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @return the variant
	 */
	public PieceOfGearVariant getVariant() {
		return variant;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the decisions
	 */
	public List<Decision> getDecisions() {
		return decisions;
	}

}
