package de.rpgframework.genericrpg.items;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.List;

import org.prelle.simplepersist.StringValueConverter;

import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.Lifeform;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModifiedObjectType;
import de.rpgframework.genericrpg.persist.FloatConverter;
import de.rpgframework.genericrpg.persist.IntegerConverter;

/**
 * @author prelle
 *
 */
public class CopyResolvedAttributesStep implements CarriedItemProcessor {

	private final static Logger logger = CarriedItem.logger;

	//-------------------------------------------------------------------
	/**
	 */
	public CopyResolvedAttributesStep() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	@SuppressWarnings({ "rawtypes", "unchecked"})
	private void process(CarriedItem<?> model, ItemAttributeDefinition val) {
		IItemAttribute attrib = val.getModifyable();
		Formula form = val.getFormula();
		if (!form.isResolved()) {
			logger.log(Level.INFO, "Not resolved yet: "+form);
		}
		if (form.isResolved()) {
			if (attrib.getConverter()!=null) {
//				logger.log(Level.WARNING, "Convert "+form+" of "+val+" using "+attrib.getConverter());
				Class<? extends StringValueConverter> cls = attrib.getConverter().getClass();
				if (cls==FloatConverter.class) {
					Float flt = form.getAsObject(attrib.getConverter());
					model.setAttribute(val.getModifyable(), new ItemAttributeFloatValue(attrib, flt));
				} else if (cls==IntegerConverter.class) {
					Integer flt = form.getAsObject(attrib.getConverter());
					model.setAttribute(val.getModifyable(), new ItemAttributeNumericalValue(attrib, flt));
				} else {
					Object foo = form.getAsObject(attrib.getConverter());
					if (foo==null) {
						logger.log(Level.ERROR, "Converter {0} resolved {1} to NULL", attrib.getConverter(), form.toString());
					} if (foo instanceof ItemAttributeValue) {
						model.setAttribute(val.getModifyable(), (ItemAttributeValue) foo);
					} else
						model.setAttribute(val.getModifyable(), new ItemAttributeObjectValue(attrib, foo));
				}
			} else {
				if (form.isInteger()) {
					model.setAttribute(val.getModifyable(), new ItemAttributeNumericalValue(attrib, form.getAsInteger()));
				} else if (form.isFloat()) {
					model.setAttribute(val.getModifyable(), new ItemAttributeFloatValue(attrib, form.getAsFloat()));
				} else {
					model.setAttribute(val.getModifyable(), new ItemAttributeObjectValue(attrib, form.getValue()));
				}
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.CarriedItemProcessor#process(java.lang.String, de.rpgframework.genericrpg.items.CarriedItem, java.util.List)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public OperationResult<List<Modification>> process(boolean strict, ModifiedObjectType refType, Lifeform user, CarriedItem<?> model, List<Modification> unprocessed) {
		OperationResult<List<Modification>> ret = new OperationResult<>(unprocessed);

		logger.log(Level.DEBUG, "item mods {0}", model.getResolved().getOutgoingModifications());
		PieceOfGear template = model.getResolved();
		if (template==null) {
			logger.log(Level.ERROR, "No resolved template for ''{0}'' in item {1}", model.getKey(), model.getUuid());
			return new OperationResult<>();
		}
		// Get all attributes from the default item template
		template.getAttributes().stream().forEach( val -> {
			logger.log(Level.DEBUG, "Operate {0} ", val);
			try {
				process(model, val);
			} catch (Exception e) {
				logger.log(Level.ERROR, "Error converting "+val,e);
			}
			});

		// Copy flags
		model.clearAutoFlags();
		model.getResolved().getFlags().forEach(flag -> {
			logger.log(Level.INFO, "Add flag {0}", flag);
			model.setAutoFlag(flag, true);
		});

		// Apply data from eventually existing variant
		if (model.getVariant()!=null) {
			logger.log(Level.DEBUG, "Apply variant "+model.getVariantID());
			logger.log(Level.DEBUG, "-->"+model.getVariant().getAttributes());
			model.getVariant().getAttributes().stream().forEach( val -> process(model, val));

			// Copy flags
			model.getVariant().getFlags().forEach(flag -> {
				logger.log(Level.INFO, "Add variant's flag {0}", flag);
				model.setAutoFlag(flag,true);
			});
		}

		return ret;
	}

}
