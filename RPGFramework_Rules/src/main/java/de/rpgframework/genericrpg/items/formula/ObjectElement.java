package de.rpgframework.genericrpg.items.formula;

/**
 * @author prelle
 *
 */
public class ObjectElement extends FormulaElement {
	
	private Object value;

	//-------------------------------------------------------------------
	public ObjectElement(Object obj) {
		super(Type.OBJECT, -1);
		this.value = obj;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return String.valueOf(value);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.formula.FormulaElement#needsResolving()
	 */
	@Override
	public boolean needsResolving() {
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.formula.FormulaElement#getValue()
	 */
	@Override
	public Object getValue() {
		return value;
	}

}
