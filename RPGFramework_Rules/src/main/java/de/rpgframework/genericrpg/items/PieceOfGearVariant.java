package de.rpgframework.genericrpg.items;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.ElementList;

/**
 * @author prelle
 *
 */
public abstract class PieceOfGearVariant<M extends IVariantMode> extends AGearData {

	/* Child classes should add there Usage lists here */

	@ElementList(entry = "alternate", type = AlternateUsage.class, inline = true)
	protected List<AlternateUsage> alternates = new ArrayList<>();

	@Attribute
	protected M mode;

	private transient String globalI18NKey;

	//-------------------------------------------------------------------
	public PieceOfGearVariant() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	public M getEquipMode() {
		return mode;
	}

	//-------------------------------------------------------------------
	public List<? extends AlternateUsage> getAlternates() {
		return alternates;
	}

	//-------------------------------------------------------------------
	public abstract List<? extends IGearTypeData> getTypeData();

	//-------------------------------------------------------------------
	public String getName(Locale locale) {
		String key = parentItem.getTypeString()+"."+parentItem.getId().toLowerCase()+".variant."+id.toLowerCase();
		if (globalI18NKey!=null)
			key = globalI18NKey;
		return getLocalizedString(locale, key);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the globalI18NKey
	 */
	public String getGlobalI18NKey() {
		return globalI18NKey;
	}

	//-------------------------------------------------------------------
	/**
	 * @param globalI18NKey the globalI18NKey to set
	 */
	public void setGdlobalI18NKey(String globalI18NKey) {
		this.globalI18NKey = globalI18NKey;
	}

}
