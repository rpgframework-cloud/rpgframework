package de.rpgframework.genericrpg.items;

/**
 * @author stefa
 *
 */
public enum CarryMode {

	/** A piece somewhere in the inventory of the character */
	CARRIED,
	/** It is used as an accessory of a larger container and might be removed rather easily */
	EMBEDDED,
	/** It is put inside the body of the user, like Cyberware or a modification of a vehilce */
	IMPLANTED,
	/** To be used in a temporary virtual item, like 'Software library' */
	VIRTUAL
}
