package de.rpgframework.genericrpg.items;

import de.rpgframework.genericrpg.Pool;
import de.rpgframework.genericrpg.SelectedValue;
import de.rpgframework.genericrpg.modification.ModifyableImpl;

/**
 * @author prelle
 *
 */
public abstract class ItemAttributeValue<A extends IItemAttribute> extends ModifyableImpl implements SelectedValue<A>, Cloneable {

	protected A attribute;

	/**
	 * This is calculated by RPG implementations to follow their
	 * rules on limits and how things interoperate
	 */
	private transient Pool<Integer> pool;

	//-------------------------------------------------------------------
	public ItemAttributeValue(A attr) {
		this.attribute = attr;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectedValue#getModifyable()
	 */
	@Override
	public A getModifyable() {
		return attribute;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#clone()
	 */
	@Override
	public abstract Object clone() ;

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableNumericalValue#getPool()
	 */
	public Pool<Integer> getPool() {
		return pool;
	}

	//-------------------------------------------------------------------
	public void setPool(Pool<Integer> pool) {
		this.pool = pool;
	}

}
