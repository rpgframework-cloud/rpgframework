package de.rpgframework.genericrpg.items;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.ChoiceOption;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.data.Lifeform;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModifiedObjectType;
import de.rpgframework.genericrpg.modification.ValueModification;

/**
 * @author prelle
 *
 */
public class ApplyDecisionsStep implements CarriedItemProcessor {

	private final static Logger logger = CarriedItem.logger;

	//-------------------------------------------------------------------
	/**
	 */
	public ApplyDecisionsStep() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.CarriedItemProcessor#process(java.lang.String, de.rpgframework.genericrpg.items.CarriedItem, java.util.List)
	 */
	@Override
	public OperationResult<List<Modification>> process(boolean strict, ModifiedObjectType refType, Lifeform user, CarriedItem<?> model, List<Modification> unprocessed) {
		OperationResult<List<Modification>> ret = new OperationResult<>(unprocessed);

		List<Decision> decisions = model.getDecisions();
		PieceOfGear template = model.getModifyable();
		PieceOfGearVariant<?> variant = model.getVariant();
		Map<UUID, Choice> openChoices = new LinkedHashMap<>();
		template.getChoices().forEach(ch -> openChoices.put(ch.getUUID(), ch));
		if (variant!=null) {
			variant.getChoices().forEach(ch -> openChoices.put(ch.getUUID(), ch));
		}

		if (decisions!=null) {
			for (Decision dec : decisions) {
				Choice choice = template.getChoice(dec.getChoiceUUID());
				logger.log(Level.DEBUG, "Decision {0} for choice {1}", dec.getValue(), choice);
				if (choice==null && variant!=null)
					choice = variant.getChoice(dec.getChoiceUUID());
				if (choice!=null) {
					openChoices.remove(choice.getUUID());
					Object obj = null;
					if (choice.getChooseFrom()!=null && choice.getTypeReference()!=null) {
						if ("CHOICE".equals(choice.getTypeReference())) {
							obj = choice.getChooseFrom().resolve(dec.getValue());
						} else {
							obj = choice.getChooseFrom().resolve(choice.getTypeReference());
						}
					}

					//logger.log(Level.WARNING, "Found "+obj+" for "+choice.getTypeReference()+" in "+choice.getChooseFrom().getClass());
					// ToDo: Was mache ich, wenn es KEINE ItemAttributes sind?????
					if (obj==null) {
						logger.log(Level.DEBUG, "No need to process {0} here", choice.getChooseFrom());
						continue;
					}
					if (obj instanceof String) {
						ChoiceOption subOpt = choice.getSubOption((String)dec.getValue());
						if (subOpt==null) {
							 logger.log(Level.ERROR, "Reference to unknown suboption {0}", obj);
						} else {
							 logger.log(Level.DEBUG, "  Found suboption {0}", obj);
							 if (subOpt.getCost()!=0) {
								 logger.log(Level.DEBUG, "  Found cost {0}", refType);
								 try {
									IItemAttribute attr = refType.resolve("PRICE");
									ItemAttributeNumericalValue<?> aVal = model.getAsValue(attr);
									logger.log(Level.DEBUG, "Add {0} to price of {1}", (int)subOpt.getCost(), aVal.getDistributed());
									aVal.setDistributed( aVal.getDistributed() +(int)subOpt.getCost());
								} catch (Exception e) {
								}
							 }
							 if (!subOpt.getOutgoingModifications().isEmpty()) {
								 List<Modification> toAdd = new ArrayList<>();
								 for (Modification mod : subOpt.getOutgoingModifications()) {
									 if (mod instanceof ValueModification && String.valueOf(((ValueModification)mod).getReferenceType()).equals("ITEM_ATTRIBUTE")) {
										 ValueModification valMod = (ValueModification)mod;
										 obj = mod.getReferenceType().resolve(valMod.getKey());
										 logger.log(Level.DEBUG, "    Set attribute {0} to {1}", obj, valMod.getValue());
										 ItemAttributeNumericalValue<?> aVal = new ItemAttributeNumericalValue<>((IItemAttribute) obj);
										 aVal.setDistributed(valMod.getValue());
										 model.setAttribute(aVal.attribute, aVal);
									 } else
										 toAdd.add(mod);
								 }
								 if (!toAdd.isEmpty()) {
									 logger.log(Level.DEBUG, "    Add {0} modifications from suboption {1}", toAdd.size(), subOpt);
								 	ret.get().addAll(toAdd);
								 }
							 }
						}
						continue;
					}
					if (obj==null || !(obj instanceof IItemAttribute)) {
						logger.log(Level.DEBUG, "No need to process {0} here: {1}", choice.getChooseFrom()+"/"+choice.getTypeReference(),dec.getValue());
						continue;
					}
					logger.log(Level.DEBUG, "Set {0} to {1}", choice.getChooseFrom()+"/"+choice.getTypeReference(),dec.getValue());
					try {
						ItemAttributeNumericalValue<?> aVal = new ItemAttributeNumericalValue<>((IItemAttribute) obj);
						aVal.setDistributed(Integer.parseInt(dec.getValue()));
						model.setAttribute(aVal.attribute, aVal);
					} catch (NumberFormatException nfe) {
						ItemAttributeObjectValue<?> aVal = new ItemAttributeObjectValue<>((IItemAttribute) obj, dec);
						//aVal.setValue(dec);
						model.setAttribute(aVal.attribute, aVal);
					}
				} else {
					logger.log(Level.WARNING, "Decision for unknown choice "+dec.getChoiceUUID()+" in item "+this);
					ret.addMessage(new ToDoElement(Severity.INFO, "Decision for unknown choice "+dec.getChoiceUUID()+" in item "+this));
				}
			}
		}

		// If a PARENT_OR_ALTERNATES decision is missing, default it
		for (UUID choiceID : new ArrayList<UUID>(openChoices.keySet())) {
			Choice open = openChoices.get(choiceID);
			if ("PARENT_OR_ALTERNATES".equals(open.getTypeReference())) {
				// Ignore that this decision is missing - it will be defaulted to PARENT in selection code
				openChoices.remove(choiceID);
			}
			if (open.getTypeReference()==null) {
				logger.log(Level.DEBUG, "Missing type reference in choice {0} of {1}", open.getUUID(), template);
			}
		}

		// Warn about missing choices
		for (Choice open : openChoices.values()) {
//			logger.log(Level.WARNING, "Missing decision for choice "+open.getUUID()+": "+open.getTypeReference());
			ret.addMessage(new ToDoElement(Severity.INFO, "Missing decision for choice "+open.getUUID()+": "+open.getTypeReference()));
		}

		return ret;
	}

}
