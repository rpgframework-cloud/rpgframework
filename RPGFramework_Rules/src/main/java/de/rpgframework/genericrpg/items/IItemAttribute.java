/**
 * 
 */
package de.rpgframework.genericrpg.items;

import java.util.List;
import java.util.Locale;

import org.prelle.simplepersist.StringValueConverter;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * Super interface for all attributes a piece of gear can have
 * 
 * @author Stefan
 *
 */
public interface IItemAttribute {

	public String name();

	public String getName(Locale locale);
	public String getName();

	public String getShortName(Locale locale);

	public <T extends IItemAttribute> T resolve(String key);

	public <T> StringValueConverter<T> getConverter();
	
	public <T> T calculateModifiedValue(Object base, List<Modification> mods);

}
