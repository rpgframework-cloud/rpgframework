package de.rpgframework.genericrpg.items;

import java.util.List;

import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.Lifeform;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModifiedObjectType;

/**
 * @author prelle
 *
 */
public class CopyUsagesStep implements CarriedItemProcessor {

	//-------------------------------------------------------------------
	public CopyUsagesStep() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.CarriedItemProcessor#process(java.lang.String, de.rpgframework.genericrpg.items.CarriedItem, java.util.List)
	 */
	@Override
	public OperationResult<List<Modification>> process(boolean strict, ModifiedObjectType refType, Lifeform user, CarriedItem<?> model, List<Modification> unprocessed) {
		OperationResult<List<Modification>> ret = new OperationResult<>(unprocessed);

		PieceOfGear template = model.getResolved();
		model.setAllowedHooks(template.getUsages());

		if (model.getVariant()!=null) {
			model.setAllowedHooks(model.getVariant().getUsages());
		}
		return ret;
	}

}
