package de.rpgframework.genericrpg.items;

import de.rpgframework.genericrpg.data.Lifeform;

/**
 * @author prelle
 *
 */
public class CarriedItemController<T extends PieceOfGear> {
	
	private Lifeform<? , ?, ?> lifeform;
	private CarriedItem<T> model;

	//-------------------------------------------------------------------
	public CarriedItemController(Lifeform<? , ?, ?> lifeform, CarriedItem<T> model) {
		this.lifeform = lifeform;
		this.model    = model;
	}

	//-------------------------------------------------------------------
	public OperationMode getModeByID(String id) {
		for (OperationModeOption modeOpt : model.getOperationModes(true)) {
			for (OperationMode mode : modeOpt.getModes()) {
				if (mode.getId().equals(id))
					return mode;
			}
		}
		return null;
	}

//	//-------------------------------------------------------------------
//	public boolean canChangeMode(OperationMode mode, boolean newState) {
//		if (!model.getOperationModes().contains(mode))
//			return false;
//		
//		return true;
//	}

}
