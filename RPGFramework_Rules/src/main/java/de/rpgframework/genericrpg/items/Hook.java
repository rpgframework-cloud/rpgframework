package de.rpgframework.genericrpg.items;

/**
 * @author prelle
 *
 */
public interface Hook {

	public String name();
	public String getName();
	
	public boolean hasCapacity();
	
}
