package de.rpgframework.genericrpg.items;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.List;

import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.ApplyTo;
import de.rpgframework.genericrpg.data.Lifeform;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModifiedObjectType;
import de.rpgframework.genericrpg.requirements.Requirement;

/**
 * @author prelle
 *
 */
public class CopyRequirementsStep implements CarriedItemProcessor {

	private final static Logger logger = CarriedItem.logger;

	//-------------------------------------------------------------------
	public CopyRequirementsStep() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.CarriedItemProcessor#process(java.lang.String, de.rpgframework.genericrpg.items.CarriedItem, java.util.List)
	 */
	@Override
	public OperationResult<List<Modification>> process(boolean strict, ModifiedObjectType refType, Lifeform user, CarriedItem<?> model, List<Modification> unprocessed) {
		OperationResult<List<Modification>> ret = new OperationResult<>(unprocessed);

		PieceOfGear template = model.getResolved();
		for (Requirement req : template.getRequirements()) {
			if (req.getApply()==null || req.getApply()==ApplyTo.CHARACTER || req.getApply()==ApplyTo.DATA_ITEM) {
				model.addRequirement(req);
				logger.log(Level.DEBUG, "Add requirement "+req);
			} else {
				logger.log(Level.INFO, "Forget requirement "+req);
			}
		}

		if (model.getVariant()!=null) {
			for (Requirement req : model.getVariant().getRequirements()) {
				if (req.getApply()==null || req.getApply()==ApplyTo.CHARACTER || req.getApply()==ApplyTo.DATA_ITEM) {
					model.addRequirement(req);
					logger.log(Level.DEBUG, "Add requirement "+req);
				} else {
					logger.log(Level.INFO, "Forget requirement "+req);
				}
			}
		}
		return ret;
	}

}
