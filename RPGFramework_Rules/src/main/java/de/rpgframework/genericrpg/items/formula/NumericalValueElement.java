package de.rpgframework.genericrpg.items.formula;

/**
 * @author prelle
 *
 */
public interface NumericalValueElement {

	//-------------------------------------------------------------------
	public float getValueAsFloat() ;

	//-------------------------------------------------------------------
	public int getValueAsInt();

}
