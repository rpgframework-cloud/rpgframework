package de.rpgframework.genericrpg.items.formula;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.prelle.simplepersist.StringValueConverter;

import de.rpgframework.genericrpg.items.Formula;
import de.rpgframework.genericrpg.items.formula.FormulaElement.Operation;
import de.rpgframework.genericrpg.items.formula.FormulaElement.Type;
import de.rpgframework.genericrpg.persist.IntegerArrayConverter;

/**
 * @author prelle
 *
 */
public class FormulaImpl implements Formula {

	protected final static Logger logger = System.getLogger(FormulaTool.class.getPackageName());

	private List<FormulaElement> elements;
	private Object cached;

	//-------------------------------------------------------------------
	public FormulaImpl() {
		elements = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	public void addElement(FormulaElement val) {
		if (val==null)
			throw new NullPointerException("Trying to add NULL to "+toString());
		this.elements.add(val);
	}

	//-------------------------------------------------------------------
	List<FormulaElement> getElements() {
		return elements;
	}

	//-------------------------------------------------------------------
	FormulaElement getLastElement() {
		if (elements.isEmpty()) return null;
		return elements.get(elements.size()-1);
	}

	//-------------------------------------------------------------------
	public String toString() {
		return String.valueOf(elements);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.Formula#isResolved()
	 */
	@Override
	public boolean isResolved() {
		return elements.stream().allMatch(el -> !el.needsResolving());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.Formula#size()
	 */
	@Override
	public int size() {
		return elements.size();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.Formula#getValue()
	 */
	@Override
	public Object getValue() {
//		logger.log(Level.INFO, "getValue {0}",elements);
		if (elements.isEmpty())
			return null;
		if (cached!=null)
			return cached;

		// Fix that a single negative value is initially parsed as operator and operand
		if (elements.size()==2 && elements.get(0).getType()==Type.OPERATION && ((OperandElement)elements.get(0)).getOperation()==Operation.SUBSTRACT) {
			NumberElement toNegate = (NumberElement) elements.get(1);
			if (toNegate.isFloat()) {
				toNegate.setValue( toNegate.getValueAsFloat() * -1);
			} else {
				toNegate.setValue( toNegate.getValueAsInt() * -1);
			}
			elements.remove(0);
		}

		Iterator<FormulaElement> it = elements.iterator();
		FormulaElement last = it.next();

		while (it.hasNext()) {
			// Expect operation
			FormulaElement tmp = it.next();
			logger.log(Level.INFO, "New element {0}",tmp);
			if (tmp.getType()!=Type.OPERATION) {
				logger.log(Level.ERROR, "Error converting {0}",tmp);
				throw new FormulaException(tmp.toString(), "Syntax error - expect operand after "+last+" but found "+tmp.getType(), tmp.getStartPos());
			}

			// Expect next value
			FormulaElement op2 = it.next();
			last = performOperation( ((OperandElement)tmp).getOperation(), last, op2);
		}

		cached = last.getValue();
		return cached;
	}

	//-------------------------------------------------------------------
	private static FormulaElement performOperation(Operation operation, FormulaElement op1, FormulaElement op2) {
		if (op1 instanceof NumericalValueElement && op2 instanceof NumericalValueElement) {
			float num1 = ((NumericalValueElement)op1).getValueAsFloat();
			float num2 = ((NumericalValueElement)op2).getValueAsFloat();
			switch (operation) {
			case ADD         : return new NumberElement(num1+num2, op1.getStartPos());
			case SUBSTRACT   : return new NumberElement(num1-num2, op1.getStartPos());
			case MULTIPLY    : return new NumberElement(num1*num2, op1.getStartPos());
			case DIVIDE      : return new NumberElement(num1/num2, op1.getStartPos());
			case EXPONENTIATE: return new NumberElement( ((int)num1)^((int)num2), op1.getStartPos());
			}
		}
		return null;
	}

	@Override
	public boolean isInteger() {
		if (elements.isEmpty()) return false;
		if (elements.get(0) instanceof NumberElement) {
			NumberElement num = ((NumberElement)elements.get(0));
			return num.getValueAsFloat()==num.getValueAsInt();
		}
		return false;
	}

	@Override
	public int getAsInteger() {
		try {
			return ((NumberElement)elements.get(0)).getValueAsInt();
		} catch (Exception e) {
		}
		return 0;
	}

	@Override
	public boolean isFloat() {
		if (elements.isEmpty()) return false;
		if (elements.get(0) instanceof NumberElement) {
			if (elements.get(0).getRaw()!=null)
				return elements.get(0).getRaw().contains(".");
		}
		return false;
	}

	@Override
	public float getAsFloat() {
		try {
			return ((NumberElement)elements.get(0)).getValueAsFloat();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.Formula#isObject()
	 */
	@Override
	public boolean isObject() {
		return !elements.isEmpty() && elements.get(0) instanceof ObjectElement;
	}

	//-------------------------------------------------------------------
	public String getAsString() {
		StringBuffer buf = new StringBuffer();
		for (FormulaElement e : elements) {
			buf.append(e.getRaw());
		}
		return buf.toString();
//		try {
//			if (elements.get(0) instanceof NumberElement) {
//				return ((NumberElement)elements.get(0)).getRaw();
//			}
//			if (elements.get(0) instanceof VariableElement) {
//				return ((VariableElement)elements.get(0)).getRaw();
//			}
//			return ((StringElement)elements.get(0)).getRaw();
//		} catch (Exception e) {
//			logger.log(Level.ERROR, "This failed for "+elements,e);
//		}
//		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.Formula#getAsObject(org.prelle.simplepersist.StringValueConverter)
	 */
	@Override
	public <T> T getAsObject(StringValueConverter<T> convert) {
		if (cached!=null)
			return (T)cached;

		if (!isResolved())
			throw new IllegalStateException("Not resolved yet: "+elements);

		if (isObject()) {
			cached = ((ObjectElement)elements.get(0)).getValue();
			return (T)cached;
		}

		String toParse = getAsString();
		try {
			if (toParse.contains(",")) {
				if (convert instanceof IntegerArrayConverter) {
					cached = convert.read(getAsString());
				} else {
					String[] elements = toParse.trim().split(",");
					List<T> ret = new ArrayList<>();
					for (String key : elements)
						ret.add(convert.read(key.trim()));
					cached = ret;
				}
			} else {
				cached = convert.read(getAsString());
			}
		} catch (Exception e) {
			logger.log(Level.WARNING, "Failed converting formula \""+elements+"\" to object using "+convert,e);
			throw new RuntimeException(e);
		}

		return (T)cached;
	}

}
