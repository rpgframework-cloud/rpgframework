package de.rpgframework.genericrpg.items.formula;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.List;

import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.Lifeform;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.CarriedItemProcessor;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModifiedObjectType;
import de.rpgframework.genericrpg.modification.ValueModification;

/**
 * @author prelle
 *
 */
public class ResolveFormulasInModificationsStep implements CarriedItemProcessor {

	protected final static Logger logger = System.getLogger(FormulaTool.class.getPackageName());

	//-------------------------------------------------------------------
	public ResolveFormulasInModificationsStep() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.CarriedItemProcessor#process(java.lang.String, de.rpgframework.genericrpg.items.CarriedItem, java.util.List)
	 */
	@Override
	public OperationResult<List<Modification>> process(boolean strict, ModifiedObjectType refType, Lifeform user, CarriedItem<?> model, List<Modification> unprocessed) {
		OperationResult<List<Modification>> ret = new OperationResult<>(unprocessed);

		for (Modification tmp : model.getOutgoingModifications()) {
			if (tmp instanceof ValueModification) {
				ValueModification mod = (ValueModification)tmp;
				if (mod.getFormula()!=null && !mod.getFormula().isResolved()) {
					logger.log(Level.ERROR, "TODO: check for formulas in requirements");
				}
			}
		}
		return ret;
	}

}
