package de.rpgframework.genericrpg.items;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.List;

import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.ApplyTo;
import de.rpgframework.genericrpg.data.Lifeform;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModifiedObjectType;

/**
 * @author prelle
 *
 */
public class RecurseEmbeddedItems implements CarriedItemProcessor {

	protected final static Logger logger = System.getLogger(RecurseEmbeddedItems.class.getPackageName());

	//-------------------------------------------------------------------
	/**
	 */
	public RecurseEmbeddedItems() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.CarriedItemProcessor#process(java.lang.String, de.rpgframework.genericrpg.data.Lifeform, de.rpgframework.genericrpg.items.CarriedItem, java.util.List)
	 */
	@Override
	public OperationResult<List<Modification>> process(boolean strict, ModifiedObjectType refType, Lifeform charac, CarriedItem<?> model, List<Modification> unprocessed) {
		OperationResult<List<Modification>> ret = new OperationResult<>(unprocessed);

		for (CarriedItem<? extends PieceOfGear> accessory : model.getAccessories()) {
			Hook hook = accessory.getUsedSlot();

			logger.log(Level.INFO, "accessory {0} in slot {1}", accessory.getKey(), hook);
			OperationResult<List<Modification>> sub = GearTool.recalculate("", refType, charac, accessory, strict);
			AAvailableSlot<Hook,?> slot = (AAvailableSlot<Hook, ?>) model.getSlot(hook);
			if (slot==null) {
				logger.log(Level.ERROR, "Item {0} shall be put in non-existing slot {1}", accessory.getKey(), hook);
			} else {
				slot.addEmbeddedItem(accessory);
				logger.log(Level.INFO, "Embed ''{0}'' into slot {1} of ''{2}''", accessory.getKey(), hook, model.getKey());
			}
			ret.get().addAll(sub.get());
			logger.log(Level.INFO, "Added mods = "+sub.get());
			for (Modification mod : accessory.getOutgoingModifications()) {
				ApplyTo target = mod.getApplyTo();
				if (target==null)
					target = ApplyTo.PARENT;

				switch (target) {
				case CHARACTER:
				case PERSONA:
				case MELEE:
				case UNARMED:
				case POINTS:
				case DRAKE:
					logger.log(Level.INFO, "Added char mod {0}", mod);
					model.addOutgoingModification(mod);
					break;
				case PARENT:
				case ACTIVE_GEAR:
				case DATA_ITEM:
					logger.log(Level.INFO, "Added gear mod {0}", mod);
					model.addIncomingModification(mod);
					break;
				default:
					System.err.println("RecurseEmbeddedItems: Unknown target "+target);
				}
			}
//			accessory.getOperationModes().forEach(mod -> model.addOperationMode(mod));
//			logger.log(Level.INFO, indent+"Added op modes = "+accessory.getOperationModes());
		}

//		ret.get().addAll(model.getModifications());
		return ret;
	}

}
