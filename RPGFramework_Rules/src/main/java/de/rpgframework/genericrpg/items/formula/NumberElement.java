package de.rpgframework.genericrpg.items.formula;

/**
 * @author prelle
 *
 */
public class NumberElement extends FormulaElement implements NumericalValueElement {
	
	private float value;

	//-------------------------------------------------------------------
	public NumberElement(int startPos) {
		super(Type.NUMBER, startPos);
	}

	//-------------------------------------------------------------------
	public NumberElement(float value, int startPos) {
		super(Type.NUMBER, startPos);
		setRaw(String.valueOf(value));
		this.value = value;
	}

	//-------------------------------------------------------------------
	public String toString() {
//		if (Math.floor(value)==value)
//			return String.valueOf((int)Math.floor(value));
//		return String.valueOf(value);
		
		return String.valueOf(Math.round(value));
//		return String.valueOf(Math.round(value*1000)/1000);
//		return String.valueOf(value);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.formula.FormulaElement#needsResolving()
	 */
	@Override
	public boolean needsResolving() {
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.formula.FormulaElement#getValue()
	 */
	@Override
	public Object getValue() {
		if (Math.floor(value)==value)
			return (int)Math.floor(value);
		return (float)value;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.formula.NumericalValueElement#getValueAsFloat()
	 */
	@Override
	public float getValueAsFloat() {
		return value;
	}

	//-------------------------------------------------------------------
	public boolean isFloat() {
		return ((int)value) != value;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.formula.NumericalValueElement#getValueAsInt()
	 */
	@Override
	public int getValueAsInt() {
		return Math.round(value);
	}

	//-------------------------------------------------------------------
	/**
	 * @param value the value to set
	 */
	public void setValue(float value) {
		this.value = value;
	}

}
