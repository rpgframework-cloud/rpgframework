package de.rpgframework.genericrpg.items;

import org.prelle.simplepersist.StringValueConverter;

public interface Formula {

	//-------------------------------------------------------------------
	public boolean isResolved();

	//-------------------------------------------------------------------
	public Object getValue();

	//-------------------------------------------------------------------
	public int size();

	//-------------------------------------------------------------------
	public boolean isInteger();
	public int getAsInteger();

	//-------------------------------------------------------------------
	public boolean isFloat();
	public float getAsFloat();

	//-------------------------------------------------------------------
	public boolean isObject();
	public <T> T getAsObject(StringValueConverter<T> convert);

}