package de.rpgframework.genericrpg.items;

import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.data.ComplexDataItem;

public class AItemEnhancement extends ComplexDataItem {

	@Attribute(name = "price")
	private int cost;
	@Attribute
	protected int size = 1;

	public AItemEnhancement() {
		super();
	}

	/**
	 * @return the price
	 */
	public int getPrice() {
		return cost;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(int price) {
		this.cost = price;
	}

	/**
	 * When creating an item how many item quality slots does the effect
	 * need.
	 */
	public int getSize() {
		return size;
	}

}