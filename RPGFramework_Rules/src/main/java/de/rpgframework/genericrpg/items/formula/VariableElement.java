package de.rpgframework.genericrpg.items.formula;

/**
 * @author prelle
 *
 */
public class VariableElement extends FormulaElement {
	
	private boolean needsResolving;
	private Object resolved;

	//-------------------------------------------------------------------
	/**
	 * @param type
	 * @param startPos
	 */
	public VariableElement(int startPos) {
		super(Type.VARIABLE, startPos);
		needsResolving = true;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return getRaw();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.formula.FormulaElement#needsResolving()
	 */
	@Override
	public boolean needsResolving() {
		return needsResolving;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.formula.FormulaElement#getValue()
	 */
	@Override
	public Object getValue() {
		if (needsResolving)
			throw new IllegalStateException("Not resolved");
		return resolved;
	}


}
