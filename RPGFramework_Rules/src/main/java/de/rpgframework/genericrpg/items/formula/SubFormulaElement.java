package de.rpgframework.genericrpg.items.formula;

import de.rpgframework.genericrpg.items.Formula;
import de.rpgframework.genericrpg.items.formula.FormulaElement.Type;

/**
 * @author prelle
 *
 */
public class SubFormulaElement extends FormulaElement {

	private FormulaImpl parent;
	private FormulaImpl subFormula;

	//-------------------------------------------------------------------
	public SubFormulaElement(FormulaImpl parent, int startPos) {
		super(Type.FORMULA, startPos);
		this.parent = parent;
		subFormula = new FormulaImpl();
	}

	//-------------------------------------------------------------------
	public String toString() {
		return "SUB"+subFormula.toString();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.formula.FormulaElement#needsResolving()
	 */
	@Override
	public boolean needsResolving() {
		return !subFormula.isResolved();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.formula.FormulaElement#getValue()
	 */
	@Override
	public Object getValue() {
		return subFormula;
	}
	public Formula getSubFormula() {
		return subFormula;
	}

	public FormulaImpl getParent() { return parent; }

}
