package de.rpgframework.genericrpg.items;

import java.util.ArrayList;
import java.util.Arrays;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.Modifyable;

public class ItemAttributeObjectValue<A extends IItemAttribute> extends ItemAttributeValue<A> implements Modifyable {

	protected Object value;

	//--------------------------------------------------------------------
	public ItemAttributeObjectValue(ItemAttributeDefinition def) {
		super((A)def.getModifyable());
		value = null;
	}

	//--------------------------------------------------------------------
	public ItemAttributeObjectValue(IItemAttribute attrib, Object value) {
		super((A)attrib);
		this.value = value;
		if (value instanceof Float)
			throw new RuntimeException("Use ItemAttributeFloatValue instead");
		if ((value instanceof String) && ((String)value).contains("WEAPON_CLOSE_COMBAT")) {
			try {
				throw new RuntimeException("Invalid String: "+value);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.exit(1);
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() {
		ItemAttributeObjectValue<A> copy = new ItemAttributeObjectValue<A>( this.attribute, value);
		copy.addModifications( new ArrayList<Modification>(this.incomingModifications) );
		return copy;
	}

	//--------------------------------------------------------------------
	public String toString() {
		String modS = "";
		String valS = String.valueOf(value);
		if (value instanceof int[]) {
			valS = Arrays.toString((int[])value);
		}
		if (getModifiedValue() instanceof int[]) {
			modS = Arrays.toString((int[])getModifiedValue());
		}
		return "OBJ:"+valS+"("+modS+") ("+incomingModifications+")";
	}

	//-------------------------------------------------------------------
	/**
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T> T getValue() {
		return (T) value;
	}

	//-------------------------------------------------------------------
	public void setValue(Object value) {
		this.value = value;
		if (value instanceof Float)
			throw new RuntimeException("Use ItemAttributeFloatValue instead");
	}

	//--------------------------------------------------------------------
	public <T> T getModifiedValue() {
		return attribute.calculateModifiedValue(value, incomingModifications);
	}

}
