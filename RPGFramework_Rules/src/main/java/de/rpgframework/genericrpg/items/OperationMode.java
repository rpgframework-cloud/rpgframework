package de.rpgframework.genericrpg.items;

import java.util.List;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModificationList;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.genericrpg.requirements.RequirementList;

/**
 * Mode a given ItemTemplate can be operated in
 * @author prelle
 *
 */
public class OperationMode {

	@Attribute(required=true)
	protected String id;
	@Element
	protected ModificationList modifications;
	@Element
	protected RequirementList requires;

	//-------------------------------------------------------------------
	public OperationMode() {
		modifications = new ModificationList();
		requires      = new RequirementList();
	}

	//-------------------------------------------------------------------
	public OperationMode(String id) {
		this();
		this.id = id;
	}

	//-------------------------------------------------------------------
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return "OpMode:"+id;
	}

	//-------------------------------------------------------------------
	public List<Modification> getModifications() {
		return modifications;
	}

	//-------------------------------------------------------------------
	public List<Requirement> getRequirements() {
		return requires;
	}

}
