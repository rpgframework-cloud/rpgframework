package de.rpgframework.genericrpg.items.formula;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.List;

import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.data.Lifeform;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.CarriedItemProcessor;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModifiedObjectType;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.genericrpg.requirements.ValueRequirement;

/**
 * @author prelle
 *
 */
public class ResolveFormulasInRequirementsStep implements CarriedItemProcessor {

	protected final static Logger logger = System.getLogger(FormulaTool.class.getPackageName());

	//-------------------------------------------------------------------
	public ResolveFormulasInRequirementsStep() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.CarriedItemProcessor#process(java.lang.String, de.rpgframework.genericrpg.items.CarriedItem, java.util.List)
	 */
	@Override
	public OperationResult<List<Modification>> process(boolean strict, ModifiedObjectType refType, Lifeform user, CarriedItem<?> model, List<Modification> unprocessed) {
		OperationResult<List<Modification>> ret = new OperationResult<>(unprocessed);

		for (Requirement tmp : model.getRequirements()) {
			if (tmp instanceof ValueRequirement) {
				ValueRequirement req = (ValueRequirement)tmp;
				if (req.getFormula()!=null && !req.getFormula().isResolved()) {
					logger.log(Level.ERROR, "TODO: check for formulas in requirements: "+req);
				}
			}
		}
//		template.getAttributes().stream().forEach( val -> {
//			IItemAttribute attrib = val.getModifyable();
//			logger.debug("Check attribute "+val);
//			FormulaImpl formula = (FormulaImpl) val.getFormula();
//			if (!formula.isResolved()) {
//				logger.info("######"+attrib+"\nFormula = "+formula);
//
//				String resolvedString = FormulaTool.resolve(val.getModifyable(), formula, new VariableResolver(model, null));
//				if (resolvedString==null) {
//					logger.debug("Not resolved yet: "+formula);
//				} else {
//					logger.debug("Handing '"+resolvedString+"' to converter "+attrib.getConverter());
//					try {
//						Object resolved = attrib.getConverter().read(resolvedString);
//
//						logger.info(val.getModifyable() + ": RAW " + val.getRawValue() + " ==> " + formula + " ==> "
//								+ resolved);
//						if (resolved instanceof Integer) {
//							ItemAttributeNumericalValue<?> aVal = new ItemAttributeNumericalValue<>(
//									val.getModifyable());
//							aVal.setDistributed((int) resolved);
//							model.setAttribute(aVal.getModifyable(), aVal);
//						} else {
//							ItemAttributeObjectValue<?> aVal = new ItemAttributeObjectValue<>(val);
//							aVal.setValue(resolved);
//							model.setAttribute(aVal.getModifyable(), aVal);
//						}
//					} catch (Exception e) {
//						logger.log(Level.ERROR, "Failed converting '" + resolvedString + "' using " + attrib.getConverter()
//								+ " of attribute " + attrib, e);
//						ret.addMessage(new ToDoElement(Severity.STOPPER, "Failed converting '" + resolvedString
//								+ "' using " + attrib.getConverter() + " of attribute " + attrib));
//					}
//				}
//			}
//		});
		return ret;
	}

}
