package de.rpgframework.genericrpg.items;

import java.util.Locale;

/**
 * How is the item carried by the character - e.g. HELD or as a ACCESSORY within another gear piece?
 * This decision is made when the item is added to the character.
 * @author prelle
 *
 */
public interface IVariantMode {

	public String getName(Locale locale);
	public String getName();

}
