package de.rpgframework.genericrpg.items;

import java.util.ArrayList;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class ItemAttributeFormulaValue<A extends IItemAttribute> extends ItemAttributeValue<A> {

	private Formula value;

	//-------------------------------------------------------------------
	public ItemAttributeFormulaValue(A attr, Formula value) {
		super(attr);
		this.value = value;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the value
	 */
	public Formula getFormula() {
		return value;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() {
		ItemAttributeFormulaValue<A> copy = new ItemAttributeFormulaValue<A>( this.attribute, value);
		copy.addModifications( new ArrayList<Modification>(this.incomingModifications) );
		return copy;
	}

}
