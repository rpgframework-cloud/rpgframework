package de.rpgframework.genericrpg;

import de.rpgframework.genericrpg.data.ComplexDataItemValue;

/**
 * @author prelle
 *
 */
public class SetItemValue extends ComplexDataItemValue<SetItem> {

	//-------------------------------------------------------------------
	public SetItemValue() {
	}

	//-------------------------------------------------------------------
	public SetItemValue(SetItem data) {
		super(data);
	}

}
