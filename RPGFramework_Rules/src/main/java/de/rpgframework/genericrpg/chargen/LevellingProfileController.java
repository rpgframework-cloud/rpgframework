package de.rpgframework.genericrpg.chargen;

import de.rpgframework.genericrpg.chargen.ai.LevellingProfile;
import de.rpgframework.genericrpg.chargen.ai.LevellingProfileValue;

/**
 * Let the user choose which levelling profiles are configured 
 */
public interface LevellingProfileController extends ComplexDataItemController<LevellingProfile, LevellingProfileValue> {

}
