package de.rpgframework.genericrpg.chargen.ai;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.rpgframework.genericrpg.chargen.RecommendationState;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.IAttribute;
import de.rpgframework.genericrpg.data.OneAttributeSkill;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.RecommendationModification;

public class Recommender {
	
	private static Logger logger = System.getLogger(Recommender.class.getPackageName());
	
	private List<RecommendationModification> recommendations;
	
	private Map<String, Map<DataItem, RecommendationCache<? extends DataItem>>> mapsByType;
	private Map<IAttribute, RecommendationCache<IAttribute>> mapByAttribute;

	//-------------------------------------------------------------------
	public Recommender() {
		recommendations = new ArrayList<>();
		mapsByType = new HashMap<>();
		mapByAttribute = new HashMap<>();
	}

	//-------------------------------------------------------------------
	public void clearConfiguration() {
		recommendations.clear();
		mapsByType.clear();
		mapByAttribute.clear();
	}

	//-------------------------------------------------------------------
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void addConfiguration(List<Modification> mods) {
		for (Modification tmp : mods) {
			RecommendationModification mod = (RecommendationModification) tmp; 
			recommendations.add(mod);
			
			if (mod.getResolvedKey() instanceof DataItem) {
				DataItem item = mod.getResolvedKey();

				// Get or create a map for the type
				String typeKey = item.getTypeString();
				Map<DataItem, RecommendationCache<? extends DataItem>> map = mapsByType.get(typeKey);
				if (map == null) {
					map = new HashMap<>();
					mapsByType.put(typeKey, map);
				}
				// Get or create a RecommendationCache for the item
				RecommendationCache newCache = new RecommendationCache(item, mod.getWeight());
				RecommendationCache oldCache = map.get(item);
				if (oldCache == null) {
					map.put(item, newCache);
					logger.log(Level.INFO, "Recommend {0}:{1} with level {2}", typeKey, item.getId(), newCache.getLevel());
				} else {
					Weight old = oldCache.getLevel();
					oldCache.merge(newCache);
					logger.log(Level.INFO, "Recommendation {0}:{1} upgraded from {2} to {3}", typeKey, item.getId(), old, newCache.getLevel());
				}
				// Now call for derived calculations
				calculateDerivedeffects(mod, item);
			} else if (mod.getResolvedKey() instanceof IAttribute) {
				IAttribute item = mod.getResolvedKey();
				// Get or create a RecommendationCache for the item
				RecommendationCache newCache = new RecommendationCache(item, mod.getWeight());
				RecommendationCache oldCache = mapByAttribute.get(item);
				if (oldCache == null) {
					logger.log(Level.INFO, "Recommend attribute {0} with level {1}", item, newCache.getLevel());
					mapByAttribute.put(item, newCache);
				} else {
					Weight old = oldCache.getLevel();
					oldCache.merge(newCache);
					logger.log(Level.INFO, "Recommend attribute {0} upgraded from {1} to {2}", item, old, newCache.getLevel());
				}
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * Called to allow derived recommendations ( e.g. an attribute when
	 * having a skill recommendation) to happen
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void calculateDerivedeffects(RecommendationModification mod, DataItem resolved) {
		if (resolved instanceof OneAttributeSkill) {
			OneAttributeSkill<IAttribute> skill = (OneAttributeSkill<IAttribute>)resolved;
			// Get or create a RecommendationCache for the item - using one level lower than the original
			Weight level = mod.getWeight();
			if (level.ordinal()>1)
				level = Weight.values()[level.ordinal()-1];
			RecommendationCache<IAttribute> newCache = new RecommendationCache(skill.getAttribute(), level);
			RecommendationCache<IAttribute> oldCache = mapByAttribute.get(skill.getAttribute());
			if (oldCache==null) {
				logger.log(Level.INFO, "Recommend attribute {0} with level {1}", skill.getAttribute(), newCache.getLevel());
				mapByAttribute.put(skill.getAttribute(), newCache);
			} else {
				Weight old = oldCache.getLevel();
				oldCache.merge(newCache);
				logger.log(Level.INFO, "Recommend attribute {0} upgraded from {1} to {2}", skill.getAttribute(), old, newCache.getLevel());
			}
		}
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("rawtypes")
	public RecommendationState getRecommendationState(DataItem key) {
		String typeKey = key.getTypeString();
		Map<DataItem, RecommendationCache<? extends DataItem>> map = mapsByType.get(typeKey);
		if (map!=null) {
			RecommendationCache cache = map.get(key);
			if (cache!=null) {
				switch (cache.getLevel()) {
				case NOT_RECOMMENDED: return RecommendationState.UNRECOMMENDED;
				case HOBBY:  return RecommendationState.NEUTRAL;
				case GOOD:
				case VERY_GOOD:
					return RecommendationState.RECOMMENDED;
				case MASTER:
				case INSANE:
					return RecommendationState.STRONGLY_RECOMMENDED;
				}
			}
		}
		
		return RecommendationState.NEUTRAL;
	}

	// -------------------------------------------------------------------
	@SuppressWarnings("rawtypes")
	public RecommendationState getRecommendationState(IAttribute key) {
		RecommendationCache cache = mapByAttribute.get(key);
		if (cache != null) {
			switch (cache.getLevel()) {
			case NOT_RECOMMENDED:
				return RecommendationState.UNRECOMMENDED;
			case HOBBY:
				return RecommendationState.NEUTRAL;
			case GOOD:
			case VERY_GOOD:
				return RecommendationState.RECOMMENDED;
			case MASTER:
			case INSANE:
				return RecommendationState.STRONGLY_RECOMMENDED;
			}
		}

		return RecommendationState.NEUTRAL;
	}

}