package de.rpgframework.genericrpg.chargen;

import de.rpgframework.character.RuleSpecificCharacterObject;
import de.rpgframework.genericrpg.data.IAttribute;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.CarryMode;
import de.rpgframework.genericrpg.items.IUsageMode;
import de.rpgframework.genericrpg.items.IVariantMode;
import de.rpgframework.genericrpg.items.ItemConfiguration;
import de.rpgframework.genericrpg.items.PieceOfGear;
import de.rpgframework.genericrpg.items.AlternateUsage;
import de.rpgframework.genericrpg.items.PieceOfGearVariant;

/**
 * @author prelle
 *
 */
public class AEquipmentController<E extends IVariantMode,U extends IUsageMode,A extends PieceOfGearVariant<E>,B extends AlternateUsage<U>,I extends PieceOfGear<E,U,A,B>> {

	protected RuleSpecificCharacterObject<? extends IAttribute,?,?,?> model;
	
	//-------------------------------------------------------------------
	protected <A extends IAttribute> AEquipmentController(RuleSpecificCharacterObject<A,?,?,?> model) {
		this.model = model;
	}

	//-------------------------------------------------------------------
	public CarriedItem equip(I item, ItemConfiguration config) {
		// TODO Auto-generated constructor stub
		CarriedItem carried = new CarriedItem(item, config.getVariant(), CarryMode.CARRIED);
		carried.setDecisions(config.getDecisions());
		
		model.addCarriedItem(carried);
		
		return carried;
	}

}
