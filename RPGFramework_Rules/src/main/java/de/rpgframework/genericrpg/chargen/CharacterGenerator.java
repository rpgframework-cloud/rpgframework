package de.rpgframework.genericrpg.chargen;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.character.RuleSpecificCharacterObject;
import de.rpgframework.genericrpg.data.IAttribute;

/**
 * @author stefa
 *
 */
public interface CharacterGenerator<A extends IAttribute, M extends RuleSpecificCharacterObject<A,?,?,?>> extends CharacterController<A,M> {

	//-------------------------------------------------------------------
	public String getId();

	//-------------------------------------------------------------------
	/**
	 * Name to display users
	 */
	public String getName();

	//-------------------------------------------------------------------
	/**
	 * Description to display users
	 */
	public String getDescription();

	//-------------------------------------------------------------------
	public void setModel(M model, CharacterHandle handle);

//	//-------------------------------------------------------------------
//	public void start(M model);
//
//	//-------------------------------------------------------------------
//	public void continueCreation(M model);
	
	//-------------------------------------------------------------------
	public boolean canBeFinished();
	
	//-------------------------------------------------------------------
	/**
	 * Finalize creation process. If there are open ToDos/Warnings, they
	 * are ignored.
	 */
	public void finish();
	
}
