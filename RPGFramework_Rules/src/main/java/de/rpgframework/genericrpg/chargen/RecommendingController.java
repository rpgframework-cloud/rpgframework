package de.rpgframework.genericrpg.chargen;

/**
 * @author prelle
 *
 */
public interface RecommendingController<T> {

	//-------------------------------------------------------------------
	public RecommendationState getRecommendationState(T item);

}
