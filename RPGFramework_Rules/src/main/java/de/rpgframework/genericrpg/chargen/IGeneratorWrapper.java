package de.rpgframework.genericrpg.chargen;

import de.rpgframework.character.RuleSpecificCharacterObject;
import de.rpgframework.genericrpg.data.IAttribute;

/**
 * @author prelle
 *
 */
public interface IGeneratorWrapper<A extends IAttribute,M extends RuleSpecificCharacterObject<A,?,?,?>, G extends CharacterGenerator<A,M>> extends CharacterGenerator<A,M> {

	public void setWrapped(G value);

	public G getWrapped();
	
}
