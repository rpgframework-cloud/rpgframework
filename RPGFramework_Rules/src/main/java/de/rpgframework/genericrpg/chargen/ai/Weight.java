/**
 * 
 */
package de.rpgframework.genericrpg.chargen.ai;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import de.rpgframework.ResourceI18N;

/**
 * @author prelle
 *
 */
public enum Weight {

	NOT_RECOMMENDED,
	NEUTRAL,
	HOBBY,
	GOOD,
	VERY_GOOD,
	MASTER,
	INSANE
	;
	
	public String getName() {
		return ResourceI18N.get((PropertyResourceBundle) ResourceBundle.getBundle(Weight.class.getName()), "ailevel."+name().toLowerCase());
	}
}
