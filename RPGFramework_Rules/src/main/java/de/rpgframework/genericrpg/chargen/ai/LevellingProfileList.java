/**
 * 
 */
package de.rpgframework.genericrpg.chargen.ai;

import java.util.ArrayList;
import java.util.Collection;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="profiles")
@ElementList(entry="profile",type=LevellingProfile.class)
public class LevellingProfileList extends ArrayList<LevellingProfile> {

	private static final long serialVersionUID = 1L;

	//-------------------------------------------------------------------
	public LevellingProfileList() {
	}

	//-------------------------------------------------------------------
	public LevellingProfileList(Collection<? extends LevellingProfile> c) {
		super(c);
	}

}
