package de.rpgframework.genericrpg.chargen.ai;

import java.util.ArrayList;
import java.util.List;

public class RecommendationCache<T> implements Comparable<RecommendationCache<T>> {
	T data;
	Weight level;
	int count;
	List<Object> sources;
	
	public RecommendationCache(T data, Weight level) {
		this.data = data;
		this.level = level;
		sources = new ArrayList<Object>();
		count=1;
	}
	
	public void merge(RecommendationCache<T> other) {
		if (data!=other.data)
			throw new IllegalArgumentException("does not match");
		if (other.level.ordinal()>level.ordinal()) {
			level = other.level;
			count = 1;
		} else if (other.level==level)
			count++;
	}
	
	public  String toString() { return data+"("+level+","+count+")"; }
	
	@Override
	public int compareTo(RecommendationCache<T> other) {
		int cmp = level.compareTo(other.level);
		if (cmp!=0)
			return cmp;
		return ((Integer)count).compareTo(other.count);
	}
	
	public Weight getLevel() { return level; }
}