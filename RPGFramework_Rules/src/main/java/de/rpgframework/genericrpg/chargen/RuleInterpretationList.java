package de.rpgframework.genericrpg.chargen;

import java.util.ArrayList;
import java.util.Collection;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author stefan
 *
 */
@SuppressWarnings("serial")
@Root(name="interpretations")
@ElementList(entry="interpretation",type=RuleInterpretation.class,inline=true)
public class RuleInterpretationList extends ArrayList<RuleInterpretation> {

	//-------------------------------------------------------------------
	/**
	 */
	public RuleInterpretationList() {
	}

	//-------------------------------------------------------------------
	/**
	 * @param c
	 */
	public RuleInterpretationList(Collection<? extends RuleInterpretation> c) {
		super(c);
	}

}
