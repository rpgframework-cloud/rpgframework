package de.rpgframework.genericrpg.chargen;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import de.rpgframework.character.CharacterIOException;
import de.rpgframework.character.RuleSpecificCharacterObject;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.IAttribute;
import de.rpgframework.genericrpg.data.RuleController;

/**
 * @author Stefan
 *
 */
public interface CharacterController<A extends IAttribute, M extends RuleSpecificCharacterObject<A,?,?,?>> {

	//-------------------------------------------------------------------
	public M getModel();
	public void setModel(M data);
	public Locale getLocale();

	//-------------------------------------------------------------------
	/**
	 * Has the user the content pack and correct language for the item?
	 */
	public default boolean showDataItem(DataItem item) { return true; }

	//-------------------------------------------------------------------
	public void addListener(ControllerListener callback);

	//-------------------------------------------------------------------
	public void removeListener(ControllerListener callback);

	//-------------------------------------------------------------------
	public boolean hasListener(ControllerListener callback);

	//-------------------------------------------------------------------
	public Collection<ControllerListener> getListener();

	//-------------------------------------------------------------------
	public void fireEvent(ControllerEvent type, Object...param);

	//-------------------------------------------------------------------
	public List<ToDoElement> getToDos();

	//-------------------------------------------------------------------
	public void setAllowRunProcessor(boolean value);

	//-------------------------------------------------------------------
	public void runProcessors();

	//-------------------------------------------------------------------
	/**
	 * Save the current version of the character to a long time storage.
	 * If implemented by a CharacterGenerator, an unfinished version is
	 * saved so creation can be continued later.
	 *
	 * @param data Native save format (XML bytes)
	 * @return TRUE, if saving has been successful
	 * @throws IOException
	 */
	public boolean save(byte[] data) throws IOException, CharacterIOException;

	//-------------------------------------------------------------------
	public RuleController getRuleController();

//	//-------------------------------------------------------------------
//	public RuleValue getRule(Rule rule);
//
//	//-------------------------------------------------------------------
//	public List<RuleValue> getRules();

	//-------------------------------------------------------------------
	public <T> RecommendingController<T> getRecommendingControllerFor(T item);

	//-------------------------------------------------------------------
	public LevellingProfileController getProfileController();

	//-------------------------------------------------------------------
	public Optional<IRecommender<A>> getRecommender();

}
