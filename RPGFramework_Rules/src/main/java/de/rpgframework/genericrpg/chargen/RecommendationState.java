package de.rpgframework.genericrpg.chargen;

/**
 * @author Stefan
 * TODO: Statt enum könnte man auch ein Recommendation Object inkl. Quellen
 *   zurückliefern, um Usern zu zeigen WOHER die Empfehlung kommt. Erfordert
 *   aber Umbau vieler Methoden und daher erst einmal Out-of-scope
 *
 */
public enum RecommendationState {
	
	UNRECOMMENDED,
	NEUTRAL,
	RECOMMENDED,
	STRONGLY_RECOMMENDED

}
