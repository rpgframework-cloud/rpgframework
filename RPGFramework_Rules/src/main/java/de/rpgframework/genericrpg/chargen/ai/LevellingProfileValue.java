/**
 *
 */
package de.rpgframework.genericrpg.chargen.ai;

import de.rpgframework.genericrpg.data.ComplexDataItemValue;

/**
 * @author prelle
 *
 */
public class LevellingProfileValue extends ComplexDataItemValue<LevellingProfile> {

	//-------------------------------------------------------------------
	public LevellingProfileValue() {
	}

	//-------------------------------------------------------------------
	public LevellingProfileValue(LevellingProfile ref) {
		super(ref);
	}

}
