package de.rpgframework.genericrpg.chargen.ai;

import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;

/**
 * @author prelle
 *
 */
@Root(name="profile")
@DataItemTypeKey(id="profile")
public class LevellingProfile extends ComplexDataItem {
	
	//-------------------------------------------------------------------
	public LevellingProfile() {
	}
	
	//-------------------------------------------------------------------
	public LevellingProfile(String id) {
		this();
		this.id  = id;
	}

}
