package de.rpgframework.genericrpg.chargen;

import java.util.List;

import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.data.IAttribute;

/**
 * This interfaces contains the intelligence to take the levelling profiles
 * from a character and turn it into recommendations
 */
public interface IRecommender<A extends IAttribute> {
	
	public void update();

	public RecommendationState getRecommendationState(A key);
	
	public RecommendationState getRecommendationState(DataItem item);
	
//	public <D extends DataItem> List<D> sort(Class<D> cls, List<D> input);
	
	public List<A> getMostRecommended(List<A> input);
	
	public <D extends DataItem> List<D> getMostRecommended(Class<D> cls, List<? extends DataItem> input);
	
	public Decision decide(Choice choice);
}
