package de.rpgframework.genericrpg.chargen;

import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;

/**
 * @author prelle
 *
 */
public interface NumericalDataItemController<D extends ComplexDataItem, V extends ComplexDataItemValue<D>> extends ComplexDataItemController<D, V> {

	public boolean canBeIncreased(V value);
	
	public boolean increase(V value);

	public boolean canBeDecreased(V value);
	
	public boolean decrease(V value);
	
}
