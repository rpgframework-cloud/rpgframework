package de.rpgframework.genericrpg.chargen;

import java.util.List;

import de.rpgframework.genericrpg.data.DataSet;

/**
 * @author prelle
 *
 */
public interface DataSetController {
	
	public void setMode(DataSetMode mode);
	
	public DataSetMode getMode();
	
	
	public List<DataSet> getAvailable();

	public List<DataSet> getSelected();
	
	public DataSetController select(DataSet set);
	
	public DataSetController deselect(DataSet set);
	
}
