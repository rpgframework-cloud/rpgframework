package de.rpgframework.genericrpg.chargen;

/**
 * @author stefa
 *
 */
public enum BasicControllerEvents implements ControllerEvent {

	GENERATOR_CHANGED,
	CHARACTER_CHANGED,
	CHARACTER_PROFILES_CHANGED,
	CREATURE_CHANGED,
	ITEM_CHANGED

}
