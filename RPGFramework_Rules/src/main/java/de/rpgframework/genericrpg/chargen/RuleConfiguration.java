package de.rpgframework.genericrpg.chargen;

import org.prelle.simplepersist.Attribute;

/**
 * @author stefan
 *
 */
public class RuleConfiguration {
	
	@Attribute
	private String rule;
	@Attribute
	private String to;

	//-------------------------------------------------------------------
	public RuleConfiguration() {
	}

	//-------------------------------------------------------------------
	public RuleConfiguration(String id, String  val) {
		this.rule = id;
		this.to   = val;
	}

	//-------------------------------------------------------------------
	public String getRuleId() {
		return rule;
	}

	//-------------------------------------------------------------------
	public String getValueString() {
		return to;
	}

}
