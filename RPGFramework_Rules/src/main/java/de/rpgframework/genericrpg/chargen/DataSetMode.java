package de.rpgframework.genericrpg.chargen;

public enum DataSetMode {
	/** Get all available in the users language */
	LANGUAGE,
	/** All from all languages */
	ALL,
	/** Only selected plugins */
	SELECTED
}