package de.rpgframework.genericrpg.chargen;

import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;
import de.rpgframework.genericrpg.persist.StringListConverter;

/**
 * @author prelle
 *
 */
@Root(name="interpretation")
@DataItemTypeKey(id="interpretation")
public class RuleInterpretation extends DataItem {
	
	@Attribute(name="restrict")
	@AttribConvert(value =StringListConverter.class)
	private List<String> restrictGenTo;
	
	@ElementList(entry = "set", type=RuleConfiguration.class)
	private List<RuleConfiguration> rules;

	//-------------------------------------------------------------------
	public RuleInterpretation() {
		rules = new ArrayList<RuleConfiguration>();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the restrictGenTo
	 */
	public List<String> getRestrictGenTo() {
		return restrictGenTo;
	}

	//-------------------------------------------------------------------
	/**
	 * @param restrictGenTo the restrictGenTo to set
	 */
	public void setRestrictGenTo(List<String> restrictGenTo) {
		this.restrictGenTo = restrictGenTo;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the rules
	 */
	public List<RuleConfiguration> getRules() {
		return rules;
	}

	//-------------------------------------------------------------------
	/**
	 * @param rules the rules to set
	 */
	public void setRules(List<RuleConfiguration> rules) {
		this.rules = rules;
	}

	//-------------------------------------------------------------------
	public boolean defines(Rule rule) {
		for (RuleConfiguration conf : rules) {
			if (conf.getRuleId().equals(rule.getID()))
				return true;
		}
		return false;
	}

}
