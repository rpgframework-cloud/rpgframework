package de.rpgframework.genericrpg.chargen;

import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.PieceOfGear;

/**
 * @author prelle
 *
 */
public interface EquipmentController<I extends PieceOfGear, C extends CarriedItem<I>> {
	
	public OperationResult<Boolean> canBeEquipped(C model);

}
