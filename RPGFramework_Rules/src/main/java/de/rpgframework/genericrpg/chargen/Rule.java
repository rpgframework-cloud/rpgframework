package de.rpgframework.genericrpg.chargen;

import java.util.Locale;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.genericrpg.data.GenericRPGTools;

/**
 * @author prelle
 *
 */
public class Rule {

	private MultiLanguageResourceBundle RES;

	public enum EffectOn {
		CHARGEN,
		CAREER,
		// Chargen and career
		COMMON,
		//
		UI
		;

		private static MultiLanguageResourceBundle RES = GenericRPGTools.RES;

		public String getName() {
			return RES.getString("rulecategory."+this.name().toLowerCase());
		}

	}

	public enum Type {
		BOOLEAN,
		INTEGER,
		FLOAT,
		ENUM
	}

	private EffectOn what;
	private String id;
	private Type type;
	private String defaultValue;
	private Class<? extends Enum> enumToUse;
	private Enum defEnumValue;

	//-------------------------------------------------------------------
	public Rule(EffectOn what, String id, Type type, MultiLanguageResourceBundle res, String defVal) {
		this.what = what;
		this.id   = id;
		this.type = type;
		this.defaultValue = defVal;
		this.RES  = res;
	}

	//-------------------------------------------------------------------
	public <E extends Enum<E>> Rule(EffectOn what, String id, Class<E> enumToUse, MultiLanguageResourceBundle res, E defVal) {
		this.what = what;
		this.id = id;
		this.type = Type.ENUM;
		this.enumToUse = enumToUse;
		defEnumValue = defVal;
		this.defaultValue = defVal.name();
		this.RES  = res;
	}

	//-------------------------------------------------------------------
	public String getID() { return id; }
	public Type getType() { return type; }

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	public <T> T parseValue(String value) {
		switch (type) {
		case BOOLEAN:
			return (T) Boolean.valueOf(value);
		case INTEGER:
			return (T) Integer.valueOf(value);
		case FLOAT:
			return (T) Float.valueOf(value);
		case ENUM:
			return (T) Enum.valueOf(enumToUse, value);
		}
		return null;
	}

	//-------------------------------------------------------------------
	public String encodeString(Object value) {
		switch (type) {
		case BOOLEAN:
			return String.valueOf( (Boolean)value);
		case INTEGER:
			return String.valueOf( (Integer)value);
		case FLOAT:
			return String.valueOf( (Float)value);
		case ENUM:
			return ((Enum)value).name();
		}
		return null;
	}

	//-------------------------------------------------------------------
	public String getName(Locale loc) {
		return RES.getString("rule."+id.toLowerCase(), loc);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the defaultValue
	 */
	public String getDefaultValue() {
		return defaultValue;
	}

	//-------------------------------------------------------------------
	public boolean getDefaultAsBooleanValue() {
		return Boolean.parseBoolean(defaultValue);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the defaultValue
	 */
	public <E extends Enum> E getDefaultAsEnumValue() {
		return (E) defEnumValue;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the enumToUse
	 */
	@SuppressWarnings("unchecked")
	public <E extends Enum<E>> Class<E> getEnumClassToUse() {
		return (Class<E>) enumToUse;
	}

	//-------------------------------------------------------------------
	public EffectOn getAffected() {
		return what;
	}

}
