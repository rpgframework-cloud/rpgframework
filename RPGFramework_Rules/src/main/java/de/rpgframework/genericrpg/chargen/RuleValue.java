package de.rpgframework.genericrpg.chargen;

import java.lang.System.Logger.Level;

/**
 * The current setting of a rule
 * @author Stefan
 *
 */
public class RuleValue {
	
	private Rule rule;
	private Object value;
	private boolean editable;

	//-------------------------------------------------------------------
	public RuleValue(Rule rule) {
		this.rule = rule;
		value = rule.parseValue(rule.getDefaultValue());
		editable = true;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return rule.getID()+" = "+value+"  "+(editable?"":"(locked)");
	}

	//-------------------------------------------------------------------
	/**
	 * @return the value
	 */
	public Object getValue() {
		return value;
	}

	//-------------------------------------------------------------------
	/**
	 * @param value the value to set
	 */
	public void setValue(Object value) {
		this.value = value;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the editable
	 */
	public boolean isEditable() {
		return editable;
	}

	//-------------------------------------------------------------------
	/**
	 * @param editable the editable to set
	 */
	public void setEditable(boolean editable) {
		System.getLogger(RuleValue.class.getPackageName()+".rules").log(Level.DEBUG, "Change editable of {0} to {1}",rule.getID(), editable);
		this.editable = editable;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the rule
	 */
	public Rule getRule() {
		return rule;
	}

}
