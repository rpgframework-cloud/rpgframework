package de.rpgframework.genericrpg.chargen;

/**
 * Allows getting an up to date instance of the character controller.
 * @author prelle
 *
 */
public interface CharacterControllerProvider<C extends CharacterController> {
	
	public C getCharacterController();

}
