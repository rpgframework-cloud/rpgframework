package de.rpgframework.genericrpg.chargen;

import java.util.List;

import de.rpgframework.character.ProcessingStep;
import de.rpgframework.character.RuleSpecificCharacterObject;
import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.data.DataItemValue;
import de.rpgframework.genericrpg.data.IAttribute;

/**
 * @author prelle
 *
 */
public interface SelectedValueController<V extends DataItemValue<?>> extends ProcessingStep {

	//-------------------------------------------------------------------
	/**
	 * Get all no yet selected items - either even with those whose
	 * requirements are not met, or without them-
	 * @param ignoreRequirements Also return items whose requirements are not met
	 * @return
	 */
	public List<V> getAvailable();

	//-------------------------------------------------------------------
	/**
	 * Get a list of all currently selected items
	 */
	public List<V> getSelected();

	//-------------------------------------------------------------------
	public RecommendationState getRecommendationState(V value);

	//-------------------------------------------------------------------
	/**
	 * Check if the user is allowed to select the item
	 * @param value  Item to select
	 * @return Selection allowed or not
	 * @throws IllegalArgumentException Thrown if a decision is missing or invalid
	 */
	public Possible canBeSelected(V value);

	//-------------------------------------------------------------------
	/**
	 * Add/Select the item using the given decisions
	 * @param value  Item to select
	 * @return value instance of selected item
	 * @throws IllegalArgumentException Thrown if a decision is missing or invalid
	 */
	public OperationResult<V> select(V value);

	//-------------------------------------------------------------------
	/**
	 * Check if the user is allowed to deselect the item
	 * @param value  ItemValue to deselect
	 * @return Deselection allowed or not
	 */
	public Possible canBeDeselected(V value);

	//-------------------------------------------------------------------
	/**
	 * Remove/Deselect the item
	 * @param value  Item to select
	 * @return TRUE if item has been deselected
	 * @throws IllegalArgumentException Thrown if a decision is missing or invalid
	 */
	public boolean deselect(V value);

	//-------------------------------------------------------------------
//	public <C extends RuleSpecificCharacterObject<? extends IAttribute,?,?,?>> C getModel();

	//-------------------------------------------------------------------
	/**
	 * Returns a list of steps to do in this controller
	 */
	public List<ToDoElement> getToDos();

}
