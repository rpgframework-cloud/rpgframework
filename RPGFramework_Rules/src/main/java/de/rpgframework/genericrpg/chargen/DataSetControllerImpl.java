package de.rpgframework.genericrpg.chargen;

import java.util.List;

import de.rpgframework.genericrpg.data.DataSet;
import de.rpgframework.genericrpg.data.GenericCore;

/**
 * @author prelle
 *
 */
public class DataSetControllerImpl implements DataSetController {
	
	private GenericCore core;

	//-------------------------------------------------------------------
	/**
	 */
	public DataSetControllerImpl(GenericCore core) {
		this.core = core;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.DataSetController#setMode(de.rpgframework.genericrpg.chargen.DataSetController.DataSetMode)
	 */
	@Override
	public void setMode(DataSetMode mode) {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.DataSetController#getMode()
	 */
	@Override
	public DataSetMode getMode() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.DataSetController#getAvailable()
	 */
	@SuppressWarnings("static-access")
	@Override
	public List<DataSet> getAvailable() {
		List<DataSet> ret = core.getDataSets();
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.DataSetController#getSelected()
	 */
	@Override
	public List<DataSet> getSelected() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.DataSetController#select(de.rpgframework.genericrpg.data.DataSet)
	 */
	@Override
	public DataSetController select(DataSet set) {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.chargen.DataSetController#deselect(de.rpgframework.genericrpg.data.DataSet)
	 */
	@Override
	public DataSetController deselect(DataSet set) {
		// TODO Auto-generated method stub
		return null;
	}

}
