package de.rpgframework.genericrpg.chargen;

import java.util.ArrayList;
import java.util.List;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.Decision;

/**
 * @author prelle
 *
 */
public abstract class ControllerUtil {

	//-------------------------------------------------------------------
	/**
	 * Test if there are decisions for all choices
	 * @see de.rpgframework.genericrpg.chargen.ComplexDataItemController#canBeSelected(de.rpgframework.genericrpg.data.ComplexDataItem, de.rpgframework.genericrpg.data.Decision[])
	 */
	public <D extends ComplexDataItem> OperationResult<Boolean> canBeSelected(D value, Decision... decisions) {
		OperationResult<Boolean> ret = new OperationResult<>(true);
		
		List<Decision> badDecisions = new ArrayList<>();
		
		for (Choice choice : value.getChoices()) {
			// Search matching decision
			boolean found=false;
			for (Decision dec : decisions) {
				// Only process decisions that have a UUID referring to the choice
				if (dec.getChoiceUUID()==null) {
					if (!badDecisions.contains(dec)) {
						badDecisions.add(dec);
						ret.addMessage(new ToDoElement(Severity.STOPPER, "Decision without a choice UUID found: " + dec));
					}
					continue;
				}
				if (dec.getChoiceUUID().equals(choice.getUUID())) { 
					found=true;
					break;
				}
			}
			if (!found) {
				ret.addMessage(new ToDoElement(Severity.WARNING, "No decision made for choice "+choice));
				ret.set(false);
			}
		}
		return ret;
	}

}
