package de.rpgframework.genericrpg.chargen;

/**
 * @author Stefan Prelle
 *
 */
public interface ControllerListener {

	//-------------------------------------------------------------------
	public void handleControllerEvent(ControllerEvent type, Object...param);

}
