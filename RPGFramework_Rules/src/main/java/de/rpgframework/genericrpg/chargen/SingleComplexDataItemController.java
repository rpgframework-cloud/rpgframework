package de.rpgframework.genericrpg.chargen;

import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.DataItemValue;
import de.rpgframework.genericrpg.data.Decision;

/**
 *
 */
public interface SingleComplexDataItemController<D extends DataItem, V extends DataItemValue<D>> {

	//-------------------------------------------------------------------
	public boolean canBeUsed();

	//-------------------------------------------------------------------
	public void selectClicked();

	//-------------------------------------------------------------------
	/**
	 * Add/Select the item using the given decisions
	 * @param value  Item to select
	 * @param decisions Decisions made
	 * @return value instance of selected item
	 * @throws IllegalArgumentException Thrown if a decision is missing or invalid
	 */
	public OperationResult<V> select(D value, Decision... decisions);

	//-------------------------------------------------------------------
	/**
	 * Check if the user is allowed to deselect the item
	 * @param value  ItemValue to deselect
	 * @return Deselection allowed or not
	 */
	public Possible canBeDeselected(V value);

	//-------------------------------------------------------------------
	/**
	 * Remove/Deselect the item
	 * @param value  Item to select
	 * @return TRUE if item has been deselected
	 * @throws IllegalArgumentException Thrown if a decision is missing or invalid
	 */
	public boolean deselect(V value);


}
