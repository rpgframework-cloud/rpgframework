/**
 *
 */
package de.rpgframework.genericrpg.chargen;

import java.util.List;
import java.util.UUID;

import de.rpgframework.character.ProcessingStep;
import de.rpgframework.character.RuleSpecificCharacterObject;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.data.IAttribute;
import de.rpgframework.genericrpg.modification.ModificationChoice;

/**
 * @author prelle
 *
 */
public interface PartialController<A> extends ProcessingStep {

	//-------------------------------------------------------------------
	public <A extends IAttribute, M extends RuleSpecificCharacterObject<A,?,?,?>> CharacterController<A,M> getCharacterController();

	//-------------------------------------------------------------------
	public <C extends RuleSpecificCharacterObject<? extends IAttribute,?,?,?>> C getModel();

	//-------------------------------------------------------------------
	/**
	 * Returns a list of steps to do in this controller
	 */
	public List<ToDoElement> getToDos();

	//-------------------------------------------------------------------
	/**
	 * Completely randomize every selection
	 */
	public void roll();

	//-------------------------------------------------------------------
	public List<UUID> getChoiceUUIDs();

	//-------------------------------------------------------------------
	public default Choice getAsChoice(ComplexDataItem value, UUID uuid) {
		return value.getChoice(uuid);
	}

	//-------------------------------------------------------------------
	public default List<Choice> getChoices(ComplexDataItem value) {
		return value.getChoices();
	}

	//-------------------------------------------------------------------
	public default ModificationChoice getAsModificationChoice(ComplexDataItem value, UUID uuid) {
		return value.getModificationChoice(uuid);
	}

	//-------------------------------------------------------------------
	public void decide(A decideFor, UUID choice, Decision decision);

}
