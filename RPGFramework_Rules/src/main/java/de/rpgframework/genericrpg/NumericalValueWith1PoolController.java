package de.rpgframework.genericrpg;

import de.rpgframework.genericrpg.chargen.OperationResult;

/**
 * @author prelle
 *
 */
public interface NumericalValueWith1PoolController<T, V extends NumericalValue<T>> extends NumericalValueController<T, V> {

	public String getColumn1();

	public int getPointsLeft();
	
	public Possible canBeIncreasedPoints(V key);
	
	public Possible canBeDecreasedPoints(V key);

	public OperationResult<V> increasePoints(V value);

	public OperationResult<V> decreasePoints(V value);

	public int getPoints(V key);
	
}
