package de.rpgframework.genericrpg;

import java.util.ArrayList;
import java.util.Collection;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="sets")
@ElementList(entry="set",type=SetItem.class,inline=true)
public class SetItemList extends ArrayList<SetItem> {

	private static final long serialVersionUID = 3760337271023314048L;

	//-------------------------------------------------------------------
	/**
	 */
	public SetItemList() {
	}

	//-------------------------------------------------------------------
	/**
	 * @param c
	 */
	public SetItemList(Collection<? extends SetItem> c) {
		super(c);
	}

}
