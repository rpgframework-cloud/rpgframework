/**
 *
 */
package de.rpgframework.genericrpg;

import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.chargen.RecommendingController;

/**
 * This controller provides methods to handle a static list of
 * values.
 *
 * @author Stefan
 *
 */
public interface NumericalValueController<T,V extends NumericalValue<T>> extends RecommendingController<T> {
	
	public int getValue(V value);

	public Possible canBeIncreased(V value);

	public Possible canBeDecreased(V value);

	public OperationResult<V> increase(V value);

	public OperationResult<V> decrease(V value);

}
