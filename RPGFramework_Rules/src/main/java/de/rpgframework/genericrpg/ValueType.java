package de.rpgframework.genericrpg;

/**
 * @author stefa
 *
 */
public enum ValueType {

	MIN,
	MAX,
	
	NATURAL,
	ARTIFICIAL,
	AUGMENTED,
//	EDGE_COST,
//	EDGE_BONUS,
//	EDGE_USAGE,
	
}
