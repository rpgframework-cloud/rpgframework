package de.rpgframework.genericrpg;

import java.util.function.Function;
import java.util.function.Supplier;

/**
 *
 */
public class MappedNumericalValue<T> implements NumericalValue<T> {

	private T value;
	private Supplier<Integer> callback;

	//-------------------------------------------------------------------
	public MappedNumericalValue(T value, Supplier<Integer> callback) {
		this.value = value;
		this.callback = callback;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectedValue#getModifyable()
	 */
	@Override
	public T getModifyable() {
		return value;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValue#getDistributed()
	 */
	@Override
	public int getDistributed() {
		return callback.get();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValue#setDistributed(int)
	 */
	@Override
	public void setDistributed(int points) {
	}

}
