package de.rpgframework.character;

import java.util.List;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author stefa
 *
 */
public interface ProcessingStep {

	//-------------------------------------------------------------------
	/**
	 * 
	 * @param unprocessed Modifications left over from previous steps
	 * @return Modifications to hand to following steps
	 */
	public List<Modification> process(List<Modification> unprocessed);
}
