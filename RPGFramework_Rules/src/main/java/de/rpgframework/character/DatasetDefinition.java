package de.rpgframework.character;

import java.util.List;

/**
 *
 */
public class DatasetDefinition {

	public static enum FileType {
		GEAR,
		QUALITIES
	}


	public static class DatasetFileDefinition {
		private String name;
		private FileType type;
		//-------------------------------------------------------------------
		public String getName() { return name;}
		public void setName(String name) { this.name = name;}
		//-------------------------------------------------------------------
		public FileType getType() { return type; }
		public void setType(FileType type) { this.type = type; }
	}


	private String name;
	private List<String> languages;
	private List<DatasetFileDefinition> files;
	private boolean shared;

	//-------------------------------------------------------------------
	public DatasetDefinition() {
	}
	//-------------------------------------------------------------------
	public String getName() { return name;}
	public DatasetDefinition setName(String name) { this.name = name; return this;}
	//-------------------------------------------------------------------
	public List<String> getLanguages() { return languages;}
	public DatasetDefinition setLanguages(List<String> value) { this.languages = value; return this;}
	//-------------------------------------------------------------------
	public List<DatasetFileDefinition> getFiles() { return files;}
	public DatasetDefinition setFiles(List<DatasetFileDefinition> value) { this.files = value; return this;}
	//-------------------------------------------------------------------
	public boolean isName() { return shared;}
	public DatasetDefinition setShared(boolean value) { this.shared = value; return this;}

}
