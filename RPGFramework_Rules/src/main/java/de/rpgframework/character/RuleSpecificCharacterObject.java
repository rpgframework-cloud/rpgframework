/**
 *
 */
package de.rpgframework.character;

import java.util.List;
import java.util.UUID;

import de.rpgframework.classification.Gender;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.genericrpg.chargen.Rule;
import de.rpgframework.genericrpg.chargen.RuleConfiguration;
import de.rpgframework.genericrpg.chargen.ai.Recommender;
import de.rpgframework.genericrpg.data.ASkillValue;
import de.rpgframework.genericrpg.data.AttributeValue;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.data.IAttribute;
import de.rpgframework.genericrpg.data.ISkill;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.PieceOfGear;

/**
 * This interface must be implemented by a character object of a plugin
 * adding support to serialize characters.
 *
 * @author prelle
 *
 */
public interface RuleSpecificCharacterObject<A extends IAttribute, S extends ISkill, V extends ASkillValue<S>, T extends PieceOfGear> {

	//-------------------------------------------------------------------
	public RoleplayingSystem getRules();

	//-------------------------------------------------------------------
	/**
	 * Returns a name by which a user interface shall display this character.
	 *
	 * @return A printable name
	 */
	public String getName();
	public void setName(String value);

	//-------------------------------------------------------------------
	public byte[] getImage();
	public void setImage(byte[] data);

	//-------------------------------------------------------------------
	/**
	 * Return text description suitable for character overview pages.
	 */
	public String getShortDescription();

	//-------------------------------------------------------------------
	public boolean isInCareerMode();

	//-------------------------------------------------------------------
	public AttributeValue<A> getAttribute(A key);

	//-------------------------------------------------------------------
	public V getSkillValue(S key);

	//-------------------------------------------------------------------
	public boolean hasDecisionBeenMade(UUID choice);

	//-------------------------------------------------------------------
	public Decision getDecision(UUID choice);

	//-------------------------------------------------------------------
	public void removeDecision(UUID choice);

	//-------------------------------------------------------------------
	public void addDecision(Decision decision);

	//-------------------------------------------------------------------
	public Gender getGender();
	public void setGender(Gender value);

	//-------------------------------------------------------------------
	public String getHairColor();
	public void setHairColor(String value);

	//-------------------------------------------------------------------
	public String getEyeColor();
	public void setEyeColor(String value);

	//-------------------------------------------------------------------
	public String getSkinColor();
	public void setSkinColor(String value);

	//-------------------------------------------------------------------
	public int getWeight();
	public void setWeight(int value);

	//-------------------------------------------------------------------
	public int getSize();
	public void setSize(int value);

	//-------------------------------------------------------------------
	public String getAge();
	public void setAge(String value);

	//-------------------------------------------------------------------
	public void addCarriedItem(CarriedItem<T> item);
	public void removeCarriedItem(CarriedItem<T> item);
	public List<CarriedItem<T>> getCarriedItems();
	public CarriedItem<T> getCarriedItem(String id);

	//-------------------------------------------------------------------
	public RuleConfiguration getRuleValue(Rule rule);
	public void setRuleValue(Rule rule, String value);

}
