package de.rpgframework.character;

/**
 * @author prelle
 *
 */
public interface CharacterProviderListener {

	public void characterAdded(CharacterHandle handle);

	public void characterModified(CharacterHandle handle);

	public void characterRemoved(CharacterHandle handle);
	
}
