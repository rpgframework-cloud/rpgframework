package de.rpgframework.character;

import java.util.List;

import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author stefa
 *
 */
public interface ExtendedProcessingStep {

	//-------------------------------------------------------------------
	/**
	 * 
	 * @param unprocessed Modifications left over from previous steps
	 * @return Modifications to hand to following steps
	 */
	public OperationResult<List<Modification>> process(List<Modification> unprocessed);
	
}
