/**
 *
 */
package de.rpgframework.worldinfo;

import java.util.List;

/**
 * @author Stefan
 *
 */
public interface Generator<T> {

	public String getName();

	public WorldInformationType getType();

	public List<Filter> getSupportedFilter();

	public boolean willWork(AppliedFilter[] choices);

	public T generate(AppliedFilter[] choices);

}
