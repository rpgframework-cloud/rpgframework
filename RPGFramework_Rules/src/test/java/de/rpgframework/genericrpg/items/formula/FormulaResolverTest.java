package de.rpgframework.genericrpg.items.formula;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

import de.rpgframework.genericrpg.data.DummyItemAttribute;
import de.rpgframework.genericrpg.data.TestObjectType;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.ItemAttributeNumericalValue;

class FormulaResolverTest {

	//-------------------------------------------------------------------
	@Test
	void testNumberOnly() {
		FormulaImpl form = FormulaTool.tokenize("3");
		assertNotNull(form);
		String toConvert = FormulaTool.resolve(TestObjectType.ITEM_ATTRIBUTE, form, new VariableResolver(null, null));
		assertNotNull(toConvert);
//		System.out.println(toConvert);
		assertEquals("3", toConvert);

		try {
			int result = (int)DummyItemAttribute.RATING.getConverter().read(toConvert);
			assertEquals(3, result);
		} catch (Exception e) {
			fail(e.toString());
		}
	}

	//-------------------------------------------------------------------
	@Test
	void testVariablesAndOperationList() {
		CarriedItem carried = new CarriedItem<>();
		carried.setAttribute(DummyItemAttribute.RATING, new ItemAttributeNumericalValue<>(DummyItemAttribute.RATING,7));

		FormulaImpl form = FormulaTool.tokenize("$RATING/3,$RATING,$RATING+2,,$RATING/2");
		assertNotNull(form);
		String toConvert = FormulaTool.resolve(TestObjectType.ITEM_ATTRIBUTE, form, new VariableResolver(carried, null));
		assertNotNull(toConvert);
//		System.out.println(toConvert);
		assertEquals("2 , 7 , 9 ,, 4", toConvert);

		try {
			int[] result = (int[])DummyItemAttribute.ATTACK_RATING.getConverter().read(toConvert);
			int[] expect = new int[] {2,7,9,0,4};
			assertArrayEquals(expect, result);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

	//-------------------------------------------------------------------
	@Test
	void testVariablesAndOperationListComplex() {
		CarriedItem carried = new CarriedItem<>();
		carried.setAttribute(DummyItemAttribute.RATING, new ItemAttributeNumericalValue<>(DummyItemAttribute.RATING,11));

		FormulaImpl form = FormulaTool.tokenize("($RATING/2)+1,($RATING+2),($RATING/3),($RATING/4),");
		assertNotNull(form);
		String toConvert = FormulaTool.resolve(TestObjectType.ITEM_ATTRIBUTE, form, new VariableResolver(carried, null));
		assertNotNull(toConvert);
//		System.out.println(toConvert);
		assertEquals("7 , 13 , 4 , 3 ,", toConvert);

		try {
			int[] result = (int[])DummyItemAttribute.ATTACK_RATING.getConverter().read(toConvert);
			int[] expect = new int[] {7,13,4,3,0};
			assertArrayEquals(expect, result);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

	//-------------------------------------------------------------------
	/** Not implemented yet */
	//@Test
	void testParentResolution() {
		CarriedItem parent = new CarriedItem<>();
		parent.setAttribute(DummyItemAttribute.RATING, new ItemAttributeNumericalValue<>(DummyItemAttribute.RATING,7));
		CarriedItem carried = new CarriedItem<>();
		//parent.a

		FormulaImpl form = FormulaTool.tokenize("$$RATING");
		assertNotNull(form);
		String toConvert = FormulaTool.resolve(TestObjectType.ITEM_ATTRIBUTE, form, new VariableResolver(carried, null));
		assertNotNull(toConvert);
//		System.out.println(toConvert);
		assertEquals("2 , 7 , 9 ,, 4", toConvert);
	}

}
