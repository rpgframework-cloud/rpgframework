package de.rpgframework.genericrpg.items.formula;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import de.rpgframework.genericrpg.data.ComplexDataItemValue;
import de.rpgframework.genericrpg.data.DummyGear;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.CarryMode;
import de.rpgframework.genericrpg.items.Formula;
import de.rpgframework.genericrpg.modification.ModifiedObjectType;

class FormulaTokenizerTest {

	//-------------------------------------------------------------------
	@Test
	void testNumberOnly() {
		Formula form = FormulaTool.tokenize("3");
		assertNotNull(form);
		System.out.println(form);
		assertTrue(form.isResolved());
		assertNotNull(form.getValue());
		assertEquals(3, (int)form.getValue());

		form = FormulaTool.tokenize("345");
		assertNotNull(form);
		System.out.println(form);
		assertTrue(form.isResolved());
		assertNotNull(form.getValue());
		assertEquals(345, (int)form.getValue());

		form = FormulaTool.tokenize("3.5");
		assertNotNull(form);
		System.out.println(form);
		assertTrue(form.isResolved());
		assertEquals(3.5f, (float)form.getValue());
	}

	//-------------------------------------------------------------------
	@Test
	void testVariableOnly() {
		Formula form = FormulaTool.tokenize("$BODY");
		assertNotNull(form);
		assertFalse(form.isResolved());

		form = FormulaTool.tokenize("$$BODY_2");
		assertNotNull(form);
		assertFalse(form.isResolved());
	}

	//-------------------------------------------------------------------
	@Test
	void testCharacterVariableOnly() {
		Formula form = FormulaTool.tokenize("&BODY");
		assertNotNull(form);
		assertEquals(1, ((FormulaImpl)form).getElements().size());
		assertFalse(form.isResolved());
	}

	//-------------------------------------------------------------------
	@Test
	void testNumbersWithOperation() {
		Formula form = FormulaTool.tokenize("3 * 5");
		assertNotNull(form);
		assertTrue(form.isResolved());
		assertNotNull(form.getValue());
		assertEquals(15, (int)form.getValue());
	}

	//-------------------------------------------------------------------
	@Test
	void testNumberList() {
		FormulaImpl form = FormulaTool.tokenize("3,4,,6,1");
		assertNotNull(form);
		assertTrue(form.isResolved());
		assertEquals(7,form.getElements().size());

		form = FormulaTool.tokenize(",,,,1");
		assertNotNull(form);
		assertTrue(form.isResolved());
		assertEquals(2,form.getElements().size());
	}

	//-------------------------------------------------------------------
	@Test
	void testVariablesAndOperationList() {
		FormulaImpl form = FormulaTool.tokenize("$BODY/3,$BODY,$BODY+2,,");
		assertNotNull(form);
		assertFalse(form.isResolved());
		System.out.println(form);
		assertEquals(10,form.getElements().size());
	}

	//-------------------------------------------------------------------
	@Test
	void testVariablesAndOperationList2() {
		FormulaImpl form = FormulaTool.tokenize("(($RATING/2)+1),($RATING+2),($RATING/3),($RATING/4),");
		assertNotNull(form);
		assertFalse(form.isResolved());
		System.out.println(form);
		assertEquals(8,form.getElements().size());

	}

	//-------------------------------------------------------------------
	@Test
	void testVariablesAndOperationAndFloat() {
		FormulaImpl form = FormulaTool.tokenize("$RATING*0.3");
		assertNotNull(form);
		assertFalse(form.isResolved());
		assertEquals(3,form.getElements().size());
		assertTrue( form.getElements().get(0) instanceof VariableElement);
		assertTrue( form.getElements().get(1) instanceof OperandElement);
		assertTrue( form.getElements().get(2) instanceof NumericalValueElement);
		assertEquals( 0.3f, ((NumericalValueElement)form.getElements().get(2)).getValueAsFloat());
		assertTrue( ((NumberElement)form.getElements().get(2)).isFloat());
	}

	//-------------------------------------------------------------------
	@Test
	void testSimpleSubFormula1() {
		FormulaImpl form = FormulaTool.tokenize("(14)*0.3");
		assertNotNull(form);
		assertEquals(3,form.getElements().size());
		System.out.println("Got "+form.getElements());
		assertEquals(SubFormulaElement.class, form.getElements().get(0).getClass());
		assertEquals(OperandElement.class   , form.getElements().get(1).getClass());
		assertEquals(NumberElement.class    , form.getElements().get(2).getClass());
		assertTrue(form.isResolved());
	}

	//-------------------------------------------------------------------
	@Test
	void testSimpleSubFormula2() {
		FormulaImpl form = FormulaTool.tokenize("(14*2)");
		assertNotNull(form);
		assertEquals(1,form.getElements().size());
		System.out.println("Got "+form.getElements());
		assertEquals(SubFormulaElement.class, form.getElements().get(0).getClass());
		assertTrue(form.isResolved());
	}

	//-------------------------------------------------------------------
	@Test
	void testComplexSubFormula() {
		FormulaImpl form = FormulaTool.tokenize("(14*2)/7");
		assertNotNull(form);
		System.out.println("Got "+form.getElements());
		assertEquals(3,form.getElements().size());
		assertTrue( form.getElements().get(0) instanceof SubFormulaElement);
		assertEquals(OperandElement.class, form.getElements().get(1).getClass());
		assertTrue( form.getElements().get(2) instanceof NumericalValueElement);
		assertTrue(form.isResolved());

		String resolvedString = FormulaTool.resolve(null, form, null);
		assertEquals("4",resolvedString);
	}

	//-------------------------------------------------------------------
	@Test
	void testMultiBrackets() {
		FormulaImpl form = FormulaTool.tokenize("(600 + ( ($RATING-4) *100) + ( ( $MAGIC-4)*50 ) ) *4");
		assertNotNull(form);
		System.out.println(form);
		assertFalse(form.isResolved());
		assertEquals(3,form.getElements().size());
		assertEquals(SubFormulaElement.class, form.getElements().get(0).getClass());
//		assertTrue( form.getElements().get(0) instanceof VariableElement);
//		assertTrue( form.getElements().get(1) instanceof OperandElement);
//		assertTrue( form.getElements().get(2) instanceof NumericalValueElement);
//		assertEquals( 0.3f, ((NumericalValueElement)form.getElements().get(2)).getValueAsFloat());
//		assertTrue( ((NumberElement)form.getElements().get(2)).isFloat());

		DummyGear gear = new DummyGear("dummy");
		CarriedItem<DummyGear> model = new CarriedItem<DummyGear>(gear,null,CarryMode.CARRIED);
		String resolvedString = FormulaTool.resolve(null, form, new VariableResolver(model, null) {
			public FormulaElement resolve(ModifiedObjectType toSetAttrib, String var) {
				if ("$RATING".equals(var)) return new NumberElement(4, -1);
				if ("$MAGIC".equals(var)) return new NumberElement(5, -1);
				System.out.println("Resolve "+toSetAttrib+":"+var);
				return new NumberElement(3, -1);
			}
		});
		assertEquals("2600",resolvedString);
		assertFalse(form.isResolved());
	}

	//-------------------------------------------------------------------
	@Test
	void testNegativeNumber() {
		FormulaImpl form = FormulaTool.tokenize("(-5)");
		assertNotNull(form);
		assertEquals(1,form.getElements().size());
		assertEquals(SubFormulaElement.class, form.getElements().get(0).getClass());
		assertEquals(1,((SubFormulaElement)form.getElements().get(0)).getSubFormula().size());
		assertTrue(form.isResolved());
	}

}
