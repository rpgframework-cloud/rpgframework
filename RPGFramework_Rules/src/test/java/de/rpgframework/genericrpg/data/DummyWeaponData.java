package de.rpgframework.genericrpg.data;

import java.util.List;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.items.AGearData;
import de.rpgframework.genericrpg.items.IGearTypeData;
import de.rpgframework.genericrpg.persist.IntegerArrayConverter;
import de.rpgframework.genericrpg.persist.StringListConverter;

public class DummyWeaponData implements IGearTypeData {

	@Attribute(name="dmg")
	@AttribConvert(WeaponDamageConverter.class)
	private Damage damage;
	@Attribute
	private int weight;
	@Attribute(name="feat")
	@AttribConvert(StringListConverter.class)
	private List<String> features;
	@Attribute(name="attack")
	private String attackRating;
	@Attribute
	private String skill;
	
	public DummyWeaponData() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void copyToAttributes(AGearData copyTo) {
		System.out.println("DummyWeaponData.copyToAttributes: "+damage+" , "+weight+" , "+features);
		if (damage!=null)
			copyTo.setAttribute(DummyItemAttribute.DAMAGE, damage);
		if (weight!=0)
			copyTo.setAttribute(DummyItemAttribute.WEIGHT, weight);
		if (features!=null)
			copyTo.setAttribute(DummyItemAttribute.FEATURES, features);
	}

}
