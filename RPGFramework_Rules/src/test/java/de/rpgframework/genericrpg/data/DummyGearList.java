package de.rpgframework.genericrpg.data;

import java.util.ArrayList;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

@SuppressWarnings("serial")
@Root(name = "items")
@ElementList(entry="item",type=DummyGear.class,inline=true)
public class DummyGearList extends ArrayList<DummyGear> {
	
	public DummyGearList() {}
}