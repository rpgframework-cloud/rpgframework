package de.rpgframework.genericrpg.data;

import java.util.List;

import de.rpgframework.genericrpg.items.IGearTypeData;
import de.rpgframework.genericrpg.items.AlternateUsage;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id="usage")
public class DummyGearUsage extends AlternateUsage<TestUsageMode> {

	@Override
	public List<? extends IGearTypeData> getTypeData() {
		// TODO Auto-generated method stub
		return null;
	}

}
