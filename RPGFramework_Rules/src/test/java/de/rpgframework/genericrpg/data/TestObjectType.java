package de.rpgframework.genericrpg.data;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.function.Function;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.StringValueConverter;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModifiedObjectType;

/**
 * @author stefa
 *
 */
public enum TestObjectType implements ModifiedObjectType {

	ATTRIBUTE(TestAttribute.class,0),
	HOOK(TestItemHook.class,0),
	ITEM_ATTRIBUTE(DummyItemAttribute.class,0),
	SLOT
	;

	Class<? extends DataItem> typeClass;
	String typeId;
	Class<? extends Enum<?>> enumType;
	StringValueConverter<? extends Object> converter;
	Function<String, ? extends DataItem> resolver;


	//-------------------------------------------------------------------
	TestObjectType() {
	}

	//-------------------------------------------------------------------
	TestObjectType(Class<? extends DataItem> cls) {
		this.typeClass = cls;
	}

	//-------------------------------------------------------------------
	TestObjectType(Class<? extends Enum<?>> enumType, int x) {
		this.enumType = enumType;
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("rawtypes")
	private static StringValueConverter getConverter(Class cls, String enumName) {
		try {
			Field field = cls.getDeclaredField(enumName);
			if (field==null)
				return null;
			AttribConvert attrib = field.getAnnotation(AttribConvert.class);
			if (attrib!=null && attrib.value()!=null) {
				StringValueConverter ret = attrib.value().getDeclaredConstructor().newInstance();
				return ret;
			}
		} catch (NoSuchFieldException | SecurityException | InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException | NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.ModifiedObjectType#resolve(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> T resolve(String key) {
		return (T)TestObjectType.resolve(this, key);
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	public static <T> T resolve(TestObjectType type, String key) {
//		if (type.typeClass!=null) {
//			return (T) Shadowrun6Core.getItem(type.typeClass, key);
//		} else
			if (type.enumType!=null) {
			try {
				Method valueOf = type.enumType.getMethod("valueOf", String.class);
				return (T) valueOf.invoke(null, key);
			} catch (InvocationTargetException ivte) {
				Throwable ee = ivte.getTargetException();
				if (ee instanceof IllegalArgumentException) {
					throw new ReferenceException(type, key);
				}
				System.err.println(TestObjectType.class.getSimpleName()+".resolve()-1:");
				ivte.printStackTrace();
			} catch (Exception e) {
				System.err.println(TestObjectType.class.getSimpleName()+".resolve()-1:");
				e.printStackTrace();
			}
		} else if (type.resolver!=null) {
			return (T)type.resolver.apply(key);
		} else {
			if (type.converter==null)
				throw new RuntimeException("Neither class, nor enumType nor converter  class nor StringConverter set for type "+type);
			try {
				return (T) type.converter.read(key);
			} catch (ReferenceException e) {
				throw new ReferenceException(type, key);
			} catch (Exception e) {
				System.err.println(TestObjectType.class.getSimpleName()+".resolve()-2:");
				e.printStackTrace();
			}
		}
		return (T)DummyItemAttribute.valueOf(key);
//		throw new ReferenceException(type, key);
	}

	@Override
	public <T extends DataItem> T resolveAsDataItem(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T[] resolveAny() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T[] resolveVariable(String varName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Modification instantiateModification(Modification tmp, ComplexDataItemValue<?> complexDataItemValue, int multi,
			CommonCharacter<?, ?, ?,?> model) {
		// TODO Auto-generated method stub
		return tmp;
	}


}
