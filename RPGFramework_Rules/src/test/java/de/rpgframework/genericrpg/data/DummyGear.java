package de.rpgframework.genericrpg.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;

import de.rpgframework.genericrpg.items.IGearTypeData;
import de.rpgframework.genericrpg.items.PieceOfGear;
import de.rpgframework.genericrpg.items.AlternateUsage;

@DataItemTypeKey(id="item")
public class DummyGear extends PieceOfGear {
	@Element
	private DummyWeaponData weapon;
	@Attribute(name="avail",required=false)
	private String availability;
	@Attribute
	private String type;
	@Attribute
	private String subtype;

	public DummyGear() {

	}
	public DummyGear(String id) {
		this.id=id;
	}
	@Override
	public List<? extends AlternateUsage> getAlternates() {
		return null;
	}

	@Override
	public List<? extends IGearTypeData> getTypeData() {
		if (weapon==null)
			return new ArrayList<>();
		return Arrays.asList(weapon);
	}

	//-------------------------------------------------------------------
	@Override
	public void validate() {
//		attributes.clear();

		setAttribute(DummyItemAttribute.PRICE, super.price);

//		if (availability!=null)
//			setAttribute(SR6ItemAttribute.AVAILABILITY, availability);
//
//		/* If there is no USAGE assume a NORMAL mode and no slot */
//		if (equips.isEmpty()) {
//			PieceOfGearEquip<SR6EquipMode> add = new SR6GearEquip(SR6EquipMode.NORMAL);
//			equips.add(add);
//		}
//		if (usages.isEmpty()) {
//			SR6GearUsage add = new SR6GearUsage(SR6UsageMode.NORMAL);
//			usages.add(add);
//		}


		super.validate();
	}

}