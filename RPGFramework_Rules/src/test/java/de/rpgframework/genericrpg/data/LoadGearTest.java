package de.rpgframework.genericrpg.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.prelle.simplepersist.Persister;

import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.genericrpg.items.AlternateUsage;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.CarryMode;
import de.rpgframework.genericrpg.items.GearTool;
import de.rpgframework.genericrpg.items.IItemAttribute;
import de.rpgframework.genericrpg.items.PieceOfGearVariant;
import de.rpgframework.genericrpg.modification.ModifiedObjectType;

class LoadGearTest {

	private static String SIMPLE1 = "<item id=\"lame_sword\">"
			+ "<attrdef id=\"DAMAGE\"   value=\"5\" />"
			+ "<attrdef id=\"WEIGHT\"   value=\"3\" />"
			+ "<attrdef id=\"FEATURES\" value=\"BLUNT ,  FRAGILE\" />"
			+ "</item>";

	private static String SIMPLE2 = "<item id=\"lame_sword\">"
			+ "<weapon dmg=\"6\" weight=\"3\" feat=\"BLUNT,FRAGILE\"/>"
			+ "<attrdef id=\"DAMAGE\"   value=\"5\" />"
			+ "</item>";

	private static String VARIANT1 = "<item id=\"lame_sword\">\n"
			+ "<weapon dmg=\"6\" weight=\"3\" feat=\"BLUNT,FRAGILE\"/>\n"
			+ "<variant id=\"egyptian\" >\n"
			+ "  <weapon dmg=\"7\"/>\n"
			+ "</variant>\n"
			+ "</item>\n";

	private static String CHOICE = "<item id=\"variable_sword\">\n"
			+ "<choices>\n"
			+ "  <choice type=\"ITEM_ATTRIBUTE\" uuid=\"82808f76-bd9c-4698-9714-cb21e3c75246\" ref=\"RATING\" options=\"1,2,3,4\" />\n"
			+ "</choices>\n"
			+ "<attrdef id=\"DAMAGE\" value=\"$RATING*2\"  />"
			+ "</item>\n";

	//-------------------------------------------------------------------
	private static byte[] bytes(String data) {
		return ("<items>\n"+data+"\n</items>").getBytes();
	}

	//-------------------------------------------------------------------
	@BeforeAll
	public static void beforeAll() {
		System.setProperty("logdir", "/tmp");
		Locale.setDefault(Locale.ENGLISH);
		Persister.putContext(Persister.PREFIX_KEY_INTERFACE+"."+IItemAttribute.class.getName(), DummyItemAttribute.class);
		Persister.putContext(Persister.PREFIX_KEY_ABSTRACT+"."+PieceOfGearVariant.class.getName(), DummyGearVariant.class);
		Persister.putContext(Persister.PREFIX_KEY_INTERFACE+"."+ModifiedObjectType.class.getName(), TestObjectType.class);
		Persister.putContext(Persister.PREFIX_KEY_ABSTRACT+"."+AlternateUsage.class.getName(), DummyGearUsage.class);
	}

	//-------------------------------------------------------------------
	@BeforeEach
	public void setup() {
		GenericCore.getStorage(DummyGear.class).clear();
	}

	//-------------------------------------------------------------------
	@Test
	void testSimple() throws IOException {
		ByteArrayInputStream in = new ByteArrayInputStream(bytes(SIMPLE1));
		DataSet plugin = new DataSet(this, RoleplayingSystem.SHADOWRUN6, "CORE", null, Locale.ENGLISH);
		List<DummyGear> list = GenericCore.loadDataItems(DummyGearList.class, DummyGear.class, plugin, in);
		assertFalse(list.isEmpty());
		assertEquals(1, list.size());

		DummyGear item = list.get(0);
		assertNotNull(item);
		assertNotNull(item.getAttribute(DummyItemAttribute.DAMAGE));
		assertEquals(5, item.getAttribute(DummyItemAttribute.DAMAGE).getFormula().getAsFloat());
		assertEquals(3, item.getAttribute(DummyItemAttribute.WEIGHT).getFormula().getAsFloat());
		assertNotNull(item.getAttribute(DummyItemAttribute.FEATURES));
//		List<String> list2 = item.getAttribute(DummyItemAttribute.FEATURES).getFormula().getAsObject();
//		assertNotNull(list2);
//		assertFalse(list2.isEmpty());
//		assertEquals(2, list2.size());
//		assertEquals("BLUNT", list2.get(0));
//		assertEquals("FRAGILE", list2.get(1));
	}

	//-------------------------------------------------------------------
	@Test
	void testWithShortcut() throws IOException {
		ByteArrayInputStream in = new ByteArrayInputStream(bytes(SIMPLE2));
		DataSet plugin = new DataSet(this, RoleplayingSystem.SHADOWRUN6, "CORE", null, Locale.ENGLISH);
		List<DummyGear> list = GenericCore.loadDataItems(DummyGearList.class, DummyGear.class, plugin, in);
		assertFalse(list.isEmpty());
		assertEquals(1, list.size());

		DummyGear item = list.get(0);
		assertNotNull(item);
		item.validate();
		assertTrue(item.getChoices().isEmpty());
		assertNotNull(item.getAttribute(DummyItemAttribute.DAMAGE));
		assertEquals(5, item.getAttribute(DummyItemAttribute.DAMAGE).getFormula().getAsInteger());
		assertEquals(3, item.getAttribute(DummyItemAttribute.WEIGHT).getFormula().getAsInteger());
		assertNotNull(item.getAttribute(DummyItemAttribute.FEATURES));
//		List<String> list2 = item.getAttribute(DummyItemAttribute.FEATURES).getFormula().getAsObject();
//		assertNotNull(list2);
//		assertFalse(list2.isEmpty());
//		assertEquals(2, list2.size());
//		assertEquals("BLUNT", list2.get(0));
//		assertEquals("FRAGILE", list2.get(1));
	}

	//-------------------------------------------------------------------
	@Test
	void testCarriedItem1() throws IOException {
		testWithShortcut();
		DummyGear template = GenericCore.getItem(DummyGear.class, "lame_sword");
		assertTrue(template.getChoices().isEmpty());

		CarriedItem<DummyGear> item = GearTool.buildItem(template, CarryMode.CARRIED, null, true).get();
		assertNotNull(item);
		System.out.println("LoadGearTest: "+item.dump());
		assertNotNull(item.getAsValue(DummyItemAttribute.DAMAGE));
		assertEquals(5, ((Damage)item.getAsValue(DummyItemAttribute.DAMAGE)).getDistributed());
		assertEquals(3, item.getAsValue(DummyItemAttribute.WEIGHT).getDistributed());
		assertNotNull(item.getAsObject(DummyItemAttribute.FEATURES));
		List<String> list2 = item.getAsObject(DummyItemAttribute.FEATURES).getValue();
		assertNotNull(list2);
		assertFalse(list2.isEmpty());
		assertEquals(2, list2.size());
		assertEquals("BLUNT", list2.get(0));
		assertEquals("FRAGILE", list2.get(1));
	}

	//-------------------------------------------------------------------
	@Test
	void testCarriedItemWithVariant() throws IOException {
		ByteArrayInputStream in = new ByteArrayInputStream(bytes(VARIANT1));
		DataSet plugin = new DataSet(this, RoleplayingSystem.SHADOWRUN6, "CORE", null, Locale.ENGLISH);
		List<DummyGear> list = GenericCore.loadDataItems(DummyGearList.class, DummyGear.class, plugin, in);
		assertFalse(list.isEmpty());
		assertEquals(1, list.size());

		DummyGear template = GenericCore.getItem(DummyGear.class, "lame_sword");
		PieceOfGearVariant variant = template.getVariant("egyptian");
		assertNotNull(variant);
		assertTrue(template.getChoices().isEmpty());

		CarriedItem<DummyGear> item = new CarriedItem<DummyGear>();
		item.setResolved(template, variant);
		assertNotNull(item);
		assertNotNull(item.getAsValue(DummyItemAttribute.DAMAGE));
		assertEquals(7, ((Damage)item.getAsValue(DummyItemAttribute.DAMAGE)).getValue());
		assertEquals(3, item.getAsValue(DummyItemAttribute.WEIGHT).getDistributed());
		assertNotNull(item.getAsObject(DummyItemAttribute.FEATURES));
		List<String> list2 = item.getAsObject(DummyItemAttribute.FEATURES).getValue();
		assertNotNull(list2);
		assertFalse(list2.isEmpty());
		assertEquals(2, list2.size());
		assertEquals("BLUNT", list2.get(0));
		assertEquals("FRAGILE", list2.get(1));
	}

	//-------------------------------------------------------------------
	@Test
	void testChoice1() throws IOException {
		ByteArrayInputStream in = new ByteArrayInputStream(bytes(CHOICE));
		DataSet plugin = new DataSet(this, RoleplayingSystem.SHADOWRUN6, "CORE", null, Locale.ENGLISH);
		List<DummyGear> list = GenericCore.loadDataItems(DummyGearList.class, DummyGear.class, plugin, in);
		assertFalse(list.isEmpty());
		assertEquals(1, list.size());

		DummyGear item = list.get(0);
		assertNotNull(item);
		assertNotNull(item.getAttribute(DummyItemAttribute.DAMAGE));
		assertFalse(item.getAttribute(DummyItemAttribute.DAMAGE).getFormula().isResolved());

		assertFalse(item.getChoices().isEmpty());
	}

	//-------------------------------------------------------------------
	@Test
	void testCarriedItemWithChoice() throws IOException {
		testChoice1();
		DummyGear template = GenericCore.getItem(DummyGear.class, "variable_sword");
		assertNotNull(template);

		CarriedItem item = new CarriedItem(template, null, CarryMode.CARRIED);
		System.out.println("LoadGearTest: "+item.dump());
		assertNull(item.getAsValue(DummyItemAttribute.RATING));
//		assertEquals(0,item.getAsValue(DummyItemAttribute.DAMAGE).getDistributed());

		Choice choice = template.getChoices().get(0);
		Integer rating = Integer.parseInt(choice.getChoiceOptions()[2]);
		assertEquals(3, rating);
		item.setDecisions(Arrays.asList(new Decision(choice, choice.getChoiceOptions()[2])));
		GearTool.recalculate("", TestObjectType.ITEM_ATTRIBUTE, null, item);
		assertEquals(3, item.getAsValue(DummyItemAttribute.RATING).getDistributed());
		assertEquals(6, ((Damage)item.getAsObject(DummyItemAttribute.DAMAGE).getValue()).getValue());
	}
}
