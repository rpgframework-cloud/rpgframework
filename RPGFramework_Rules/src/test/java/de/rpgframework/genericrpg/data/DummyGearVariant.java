package de.rpgframework.genericrpg.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.prelle.simplepersist.Element;

import de.rpgframework.genericrpg.items.IGearTypeData;
import de.rpgframework.genericrpg.items.IVariantMode;
import de.rpgframework.genericrpg.items.PieceOfGearVariant;

@DataItemTypeKey(id="variant")
public class DummyGearVariant extends PieceOfGearVariant<IVariantMode> {
	@Element
	private DummyWeaponData weapon;
	
	public DummyGearVariant() {
		
	}
	
	@Override
	public List<? extends IGearTypeData> getTypeData() {
		if (weapon==null)
			return new ArrayList<>();
		return Arrays.asList(weapon);
	}
	
}