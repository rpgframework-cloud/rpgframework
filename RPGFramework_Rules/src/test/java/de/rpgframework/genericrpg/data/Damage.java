package de.rpgframework.genericrpg.data;

import java.util.List;

import de.rpgframework.genericrpg.items.ItemAttributeNumericalValue;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class Damage extends ItemAttributeNumericalValue<DummyItemAttribute> implements Cloneable {

	enum DamageElement {
		REGULAR,
		FIRE,
		COLD,
		ELECTRICITY,
		CHEMICAL
	}
	enum DamageType {
		PHYSICAL,
		STUN,
		PHYSICAL_SPECIAL,
		STUN_SPECIAL,
	}

	private DamageType type;
	private DamageElement element = DamageElement.REGULAR;

	//--------------------------------------------------------------------
	public Damage() {
		super(DummyItemAttribute.DAMAGE);
	}

	//--------------------------------------------------------------------
	public Damage(int val, DamageType type, DamageElement element) {
		super(DummyItemAttribute.DAMAGE);
		this.type = type;
		this.element = element;
	}

	//--------------------------------------------------------------------
	public Damage(Damage copy, List<Modification> mods) {
		super(DummyItemAttribute.DAMAGE);
		super.incomingModifications.addAll(mods);
		type        = copy.getType();
		element     = copy.getElement();
		incomingModifications.addAll(mods);
	}

	//--------------------------------------------------------------------
	public String toString() {
		if (getModifier()==0)
			return value+" "+type;
		return value+" ("+getModifiedValue()+")"+type;
	}

	//-------------------------------------------------------------------
 	public Damage clone() {
			return (Damage) super.clone();
    }

	//--------------------------------------------------------------------
	public int getValue() {
		return Math.round(value);
	}

	//--------------------------------------------------------------------
	public DamageType getType() {
		return type;
	}

	//--------------------------------------------------------------------
	/**
	 * @param value the value to set
	 */
	public void setValue(int value) {
		this.value = value;
	}

	//--------------------------------------------------------------------
	/**
	 * @param type the type to set
	 */
	public void setType(DamageType type) {
		this.type = type;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the weaponDamageType
	 */
	public DamageElement getElement() {
		return element;
	}

	//--------------------------------------------------------------------
	/**
	 * @param weaponDamageType the weaponDamageType to set
	 */
	public void setElement(DamageElement element) {
		this.element = element;
	}


}
