package de.rpgframework.genericrpg.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import de.rpgframework.genericrpg.ValueType;
import de.rpgframework.genericrpg.modification.ValueModification;

class AttributeValueTest {

	//-------------------------------------------------------------------
	@Test
	void testIdle() {
		AttributeValue val = new AttributeValue(TestAttribute.STRENGTH);
		assertEquals(0, val.getDistributed());
		assertEquals(0, val.getModifiedValue());
		assertEquals(0, val.getModifiedValue(ValueType.NATURAL));
		assertEquals(0, val.getModifiedValue(ValueType.ARTIFICIAL));
		assertTrue(val.getIncomingModifications().isEmpty());
		assertEquals(TestAttribute.STRENGTH, val.getModifyable());
	}

	//-------------------------------------------------------------------
	@Test
	void testNatural() {
		AttributeValue val = new AttributeValue(TestAttribute.STRENGTH);
		val.setDistributed(3);
		val.addIncomingModification(new ValueModification(TestObjectType.ATTRIBUTE, TestAttribute.STRENGTH.name(), 2, ApplyWhen.ALLCREATE, ValueType.NATURAL));
		assertEquals(3, val.getDistributed());
		assertEquals(5, val.getModifiedValue());
		assertEquals(5, val.getModifiedValue(ValueType.NATURAL));
		assertEquals(0, val.getModifiedValue(ValueType.ARTIFICIAL));
		assertFalse(val.getIncomingModifications().isEmpty());
		assertEquals(1,val.getIncomingModifications().size());
		assertEquals(TestAttribute.STRENGTH, val.getModifyable());
	}

	//-------------------------------------------------------------------
	@Test
	void testArtificial() {
		AttributeValue val = new AttributeValue(TestAttribute.STRENGTH);
		val.setDistributed(3);
		val.addIncomingModification(new ValueModification(TestObjectType.ATTRIBUTE, TestAttribute.STRENGTH.name(), 1, ApplyWhen.ALLCREATE, ValueType.NATURAL));
		val.addIncomingModification(new ValueModification(TestObjectType.ATTRIBUTE, TestAttribute.STRENGTH.name(), 2, ApplyWhen.ALLCREATE, ValueType.ARTIFICIAL));
		val.addIncomingModification(new ValueModification(TestObjectType.ATTRIBUTE, TestAttribute.STRENGTH.name(), 4, ApplyWhen.ALLCREATE, ValueType.ARTIFICIAL));
		assertEquals(3, val.getDistributed());
		assertEquals(4, val.getModifiedValue());
		assertEquals(4, val.getModifiedValue(ValueType.NATURAL));
		assertEquals(6, val.getModifiedValue(ValueType.ARTIFICIAL));
		assertFalse(val.getIncomingModifications().isEmpty());
		assertEquals(3,val.getIncomingModifications().size());
		assertEquals(TestAttribute.STRENGTH, val.getModifyable());
	}

}
