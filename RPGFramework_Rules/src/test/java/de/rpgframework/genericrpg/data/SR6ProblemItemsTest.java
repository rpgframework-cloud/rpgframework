package de.rpgframework.genericrpg.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.prelle.simplepersist.Persister;

import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.items.AlternateUsage;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.CarryMode;
import de.rpgframework.genericrpg.items.GearTool;
import de.rpgframework.genericrpg.items.IItemAttribute;
import de.rpgframework.genericrpg.items.IUsageMode;
import de.rpgframework.genericrpg.items.PieceOfGearVariant;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModifiedObjectType;

class SR6ProblemItemsTest {

	private static String BOW = "<item id=\"bow\" price=\"100\" >\n"
			+ "		<choices>\n"
			+ "			<choice type=\"ITEM_ATTRIBUTE\" ref=\"RATING\" uuid=\"59cc40fa-c6f6-4fcb-b283-3d5239864e25\" options=\"1,2,3,4,5,6,7,8,9,10,11,12,13,14\"/> \n"
			+ "		</choices>\n"
			+ "		<requires>\n"
			+ "			<valuereq type=\"ATTRIBUTE\" ref=\"STRENGTH\" min=\"$RATING\" apply=\"CHARACTER\"/>\n"
			+ "		</requires>\n"
			+ "      <modifications>\n"
			+ "         <!--itemmod type=\"SLOT\" ref=\"TOP\"/>\n"
			+ "         <itemmod type=\"SLOT\" ref=\"UNDER\"/-->\n"
			+ "      </modifications>\n"
			+ "		<attrdef id=\"PRICE\"         value=\"$RATING*10 +100\" />\n"
			+ "		<attrdef id=\"DAMAGE\"        value=\"$RATING/2 P\" />\n"
			+ "		<attrdef id=\"ATTACK_RATING\" value=\"$RATING/2,$RATING,$RATING/4,,\" />\n"
			+ "		<attrdef id=\"AVAILABILITY\"  value=\"$RATING/3\" />\n"
			//+ "		<weapon skill=\"close_combat\" spec=\"close_combat/blades\" />\n"
			+ "	</item>";

	private static String AXE = "<item id=\"combat_axe\" avail=\"4\" price=\"500\" type=\"WEAPON_CLOSE_COMBAT\" subtype=\"BLADES\">\n"
			+ "		<!--equip mode=\"NORMAL\"/-->\n"
			+ "		<weapon dmg=\"5P\" attack=\"9,,,,\" skill=\"close_combat\" spec=\"close_combat/blades\" />\n"
			+ "	</item>";

	private static String RIOT = "<item avail=\"4\" price=\"1200\" id=\"riot_shield\" type=\"ARMOR\" subtype=\"ARMOR_SHIELD\">\n"
			+ "		<attrdef id=\"DEFENSE_PHYSICAL\" value=\"2\" />\n"
			+ "		<attrdef id=\"DEFENSE_SOCIAL\" value=\"-2\" />\n"
			+ "			<weapon dmg=\"4S(e)\" attack=\"4,,,,\" skill=\"exotic_weapons\" />\n"
			+ "		\n"
			+ "		<modifications>\n"
			+ "			<valmod type=\"HOOK\" ref=\"ARMOR\" value=\"2\" />\n"
			+ "		</modifications>\n"
			+ "		\n"
			+ "		<usage mode=\"WEAPON\" id=\"foo\">\n"
			+ "		</usage>\n"
			+ "	</item>";

	//-------------------------------------------------------------------
	private static byte[] bytes(String data) {
		return ("<items>\n"+data+"\n</items>").getBytes();
	}

	//-------------------------------------------------------------------
	@BeforeAll
	public static void beforeAll() {
		System.setProperty("logdir", "/tmp");
		Locale.setDefault(Locale.ENGLISH);
		Persister.putContext(Persister.PREFIX_KEY_INTERFACE+"."+IItemAttribute.class.getName(), DummyItemAttribute.class);
		Persister.putContext(Persister.PREFIX_KEY_INTERFACE+"."+IUsageMode.class.getName(), TestUsageMode.class);
		Persister.putContext(Persister.PREFIX_KEY_ABSTRACT+"."+PieceOfGearVariant.class.getName(), DummyGearVariant.class);
		Persister.putContext(Persister.PREFIX_KEY_INTERFACE+"."+ModifiedObjectType.class.getName(), TestObjectType.class);
		Persister.putContext(Persister.PREFIX_KEY_ABSTRACT+"."+AlternateUsage.class.getName(), DummyGearUsage.class);
	}

	//-------------------------------------------------------------------
	@BeforeEach
	public void setup() {
		GenericCore.getStorage(DummyGear.class).clear();
	}

	//-------------------------------------------------------------------
	@Test
	void testBow() throws IOException {
		ByteArrayInputStream in = new ByteArrayInputStream(bytes(BOW));
		DataSet plugin = new DataSet(this, RoleplayingSystem.SHADOWRUN6, "CORE", null, Locale.ENGLISH);
		List<DummyGear> list = GenericCore.loadDataItems(DummyGearList.class, DummyGear.class, plugin, in);
		assertFalse(list.isEmpty());
		assertEquals(1, list.size());

		// Check raw item
		DummyGear item = list.get(0);
		assertNotNull(item);
		assertNotNull(item.getAttribute(DummyItemAttribute.DAMAGE));
//		assertTrue(item.getAttribute(DummyItemAttribute.DAMAGE).isFormula());
//		assertTrue(item.getAttribute(DummyItemAttribute.PRICE).isFormula());
//		assertTrue(item.getAttribute(DummyItemAttribute.ATTACK_RATING).isFormula());
//		assertTrue(item.getAttribute(DummyItemAttribute.AVAILABILITY).isFormula());

		// Decide rating
		Decision decide = new Decision(UUID.fromString("59cc40fa-c6f6-4fcb-b283-3d5239864e25"), "5");

		// Check cooked item
		CarriedItem cooked = new CarriedItem(item, null, CarryMode.CARRIED);
		cooked.setDecisions(List.of(decide));
		OperationResult<List<Modification>> result =  GearTool.recalculate("", TestObjectType.ITEM_ATTRIBUTE, null, cooked);
		assertNotNull(result);
		assertTrue(result.getMessages().isEmpty(), "Errors in recalculation result");
		assertEquals(150, cooked.getAsValue(DummyItemAttribute.PRICE).getDistributed());
		assertNotNull(cooked.getAsObject(DummyItemAttribute.DAMAGE));

		assertEquals(3, ((Damage)cooked.getAsObject(DummyItemAttribute.DAMAGE).getModifiedValue()).getValue());
	}

	//-------------------------------------------------------------------
	@Test
	void testAxe() throws IOException {
		ByteArrayInputStream in = new ByteArrayInputStream(bytes(AXE));
		DataSet plugin = new DataSet(this, RoleplayingSystem.SHADOWRUN6, "CORE", null, Locale.ENGLISH);
		List<DummyGear> list = GenericCore.loadDataItems(DummyGearList.class, DummyGear.class, plugin, in);
		assertFalse(list.isEmpty());
		assertEquals(1, list.size());

		// Check raw item
		DummyGear item = list.get(0);
		assertNotNull(item);
		assertNotNull(item.getAttribute(DummyItemAttribute.DAMAGE));
//		assertTrue(item.getAttribute(DummyItemAttribute.DAMAGE).isFormula());
//		assertTrue(item.getAttribute(DummyItemAttribute.PRICE).isFormula());
//		assertTrue(item.getAttribute(DummyItemAttribute.ATTACK_RATING).isFormula());
//		assertTrue(item.getAttribute(DummyItemAttribute.AVAILABILITY).isFormula());

		// Check cooked item
		CarriedItem cooked = GearTool.buildItem(item, CarryMode.CARRIED, null, true).get();
		assertEquals(500, cooked.getAsValue(DummyItemAttribute.PRICE).getDistributed());
		assertNotNull(cooked.getAsValue(DummyItemAttribute.DAMAGE));

		assertEquals(5, ((Damage)cooked.getAsValue(DummyItemAttribute.DAMAGE)).getValue());
	}

//	//-------------------------------------------------------------------
//	/**
//	 * Primary function: Armor
//	 * Secondary function: Melee Weapon
//	 */
//	@Test
//	void testRiotShield() throws IOException {
//		ByteArrayInputStream in = new ByteArrayInputStream(bytes(RIOT));
//		DataSet plugin = new DataSet(this, RoleplayingSystem.SHADOWRUN6, "CORE", null, Locale.ENGLISH);
//		List<DummyGear> list = GenericCore.loadDataItems(DummyGearList.class, DummyGear.class, plugin, in);
//		assertFalse(list.isEmpty());
//		assertEquals(1, list.size());
//
//		// Check raw item
//		DummyGear item = list.get(0);
//		assertNotNull(item);
//		assertNotNull(item.getAttribute(DummyItemAttribute.DAMAGE));
////		assertTrue(item.getAttribute(DummyItemAttribute.DAMAGE).isFormula());
////		assertTrue(item.getAttribute(DummyItemAttribute.PRICE).isFormula());
////		assertTrue(item.getAttribute(DummyItemAttribute.ATTACK_RATING).isFormula());
////		assertTrue(item.getAttribute(DummyItemAttribute.AVAILABILITY).isFormula());
//
//		// Check cooked item
//		CarriedItem cooked = new CarriedItem(item, null);
//		assertEquals(1200, cooked.getAsValue(DummyItemAttribute.PRICE).getDistributed());
//		assertNotNull(cooked.getAsObject(DummyItemAttribute.DAMAGE));
//
//		assertEquals(5, ((Damage)cooked.getAsObject(DummyItemAttribute.DAMAGE).getModifiedValue()).getValue());
//	}

}
