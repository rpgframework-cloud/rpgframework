package de.rpgframework.genericrpg.data;

import java.util.Locale;

import de.rpgframework.genericrpg.data.IAttribute;

/**
 * @author stefa
 *
 */
public enum TestAttribute implements IAttribute {

	STRENGTH,
	WILLPOWER,;

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.HardcopyPluginData#getName()
	 */
	@Override
	public String getName() {
		return this.name();
	}

	@Override
	public String getName(Locale locale) {
		return this.name();
	}

	@Override
	public String getShortName(Locale locale) {
		return this.name();
	}

	@Override
	public boolean isDerived() {
		// TODO Auto-generated method stub
		return false;
	}
}
