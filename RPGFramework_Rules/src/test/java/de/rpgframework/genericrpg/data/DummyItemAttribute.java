package de.rpgframework.genericrpg.data;

import java.util.List;
import java.util.Locale;

import org.prelle.simplepersist.StringValueConverter;

import de.rpgframework.genericrpg.items.IItemAttribute;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.persist.IntegerArrayConverter;
import de.rpgframework.genericrpg.persist.IntegerConverter;
import de.rpgframework.genericrpg.persist.StringListConverter;

/**
 * @author prelle
 *
 */
public enum DummyItemAttribute implements IItemAttribute {

	ATTACK_RATING(new IntegerArrayConverter()),
	AVAILABILITY,
	DAMAGE(new WeaponDamageConverter()),
	DEFENSE_PHYSICAL,
	DEFENSE_SOCIAL,
	FEATURES(new StringListConverter()),
	PRICE,
	RATING,
	WEIGHT,
	;

	//-------------------------------------------------------------------
	DummyItemAttribute() {
	}
	//-------------------------------------------------------------------
	DummyItemAttribute(StringValueConverter<?> conv) {
		converter = conv;
	}
	private StringValueConverter<?> converter = new IntegerConverter();

	
	@Override
	public String getName(Locale locale) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getShortName(Locale locale) {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.IItemAttribute#resolve(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public DummyItemAttribute resolve(String key) {
		return DummyItemAttribute.valueOf(key);
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @see de.rpgframework.genericrpg.items.IItemAttribute#resolve(java.lang.String)
//	 */
//	@Override
//	public <T> T resolve(String key) {
//		try {
//			if (converter!=null)
//				return (T) converter.read(key);
//		} catch (Exception e1) {
//			throw new IllegalArgumentException(e1);
//		}
//		try {
//			return (T)Integer.valueOf(key);
//		} catch (NumberFormatException e) {
//			return (T)key;
//		}
//	}
//	//-------------------------------------------------------------------
//	/**
//	 * @see de.rpgframework.genericrpg.items.IItemAttribute#toString(java.lang.Object)
//	 */
//	@Override
//	public <E extends Object> String toString(E value) {
//		if (converter!=null) {
//			try {
//				return converter.write(value);
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//		// TODO Auto-generated method stub
//		return String.valueOf(value);
//	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.IItemAttribute#getConverter()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> StringValueConverter<T> getConverter() {
		return (StringValueConverter<T>) converter;
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.items.IItemAttribute#calculateModifiedValue(java.lang.Object, java.util.List)
	 */
	public <T> T calculateModifiedValue(Object base, List<Modification> mods) {
//		if (this==AVAILABILITY)
//			return (T) SR6GearTool.calculateModifiedValue((Availability) base, mods);
		System.err.println("DummyItemAttribute: Don't know how to calculate modified value for "+this);
		return (T)base;
	}

}
