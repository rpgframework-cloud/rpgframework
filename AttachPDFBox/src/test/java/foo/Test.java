package foo;

import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.pdfbox.Loader;
import org.apache.pdfbox.io.RandomAccessReadBufferedFile;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;

import com.mortennobel.imagescaling.ResampleOp;

public class Test {

	public Test() {
		// TODO Auto-generated constructor stub
	}

	private static PDDocument pdDocument;
	private static PDFRenderer renderer;

	public static void main(String[] args) throws IOException {
		pdDocument = Loader.loadPDF(
				 new RandomAccessReadBufferedFile("/home/data/Rollenspiel/Shadowrun/SR6_Deutsch/Regeln/Grundregelwerk_2024.pdf"),
				 null,
				 null,
				 null, null);
		renderer = new PDFRenderer(pdDocument);
		System.err.println("ImageDownscalingOptimizationThreshold: "+renderer.getImageDownscalingOptimizationThreshold());
		System.err.println("RenderingHints: "+renderer.getRenderingHints());
		System.err.println("SubsamplingAllowed: "+renderer.isSubsamplingAllowed());
		renderer.setSubsamplingAllowed(true);
		RenderingHints r = new RenderingHints(null);
//		r.put(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
//		r.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		r.put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
//		renderer.setRenderingHints(r);

		BufferedImage image = renderer.renderImageWithDPI(77, 300,ImageType.RGB);
//		BufferedImage image = renderer.renderImage(nextPage, 1.5f);
//		image.createGraphics();
//		Image fxImg = SwingFXUtils.toFXImage(image, null);


//		float SCALE = 0.333f;
//		 BufferedImage bi = new BufferedImage(
//				 Math.round(SCALE * image.getWidth(null)),
//				 Math.round(SCALE * image.getHeight(null)),
//				 BufferedImage.TYPE_INT_ARGB);
//
//		Graphics2D grph = (Graphics2D) bi.getGraphics();
//	    grph.scale(SCALE, SCALE);
//
//	    grph.drawImage(image, 0, 0, null);
//	    grph.dispose();
//
//	    ImageIO.write(bi, "png", new File("/tmp/half_size.png"));

//		File outputfile = new File("/tmp/Scalr.jpg");
//		ImageIO.write(converted, "jpg", outputfile);
//
//		int w = image.getWidth();
//		int h = image.getHeight();
//		BufferedImage after = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
//		AffineTransform at = new AffineTransform();
//		at.scale(2.0, 2.0);
//		AffineTransformOp scaleOp =
//		   new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
//		after = scaleOp.filter(image, after);
//		outputfile = new File("/tmp/affine_transform.jpg");
//		ImageIO.write(converted, "jpg", outputfile);
//
//		ResampleOp  resampleOp = new ResampleOp (image.getWidth()/3,image.getHeight()/3);
//		after= resampleOp.filter(image, null);
//		outputfile = new File("/tmp/ImageScaling.jpg");
//		ImageIO.write(after, "jpg", outputfile);

	}

}
