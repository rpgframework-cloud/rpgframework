module rpgframework.pdfviewer.pdfxbox {
	exports de.rpgframework.jfx.attach.impl;

	provides de.rpgframework.jfx.attach.PDFViewerService with de.rpgframework.jfx.attach.impl.DesktopPDFViewerService;

	requires transitive de.rpgframework.core;
	requires javafx.controls;
	requires javafx.graphics;
	requires javafx.extensions;
	requires javafx.swing;
	requires java.desktop;
	requires rpgframework.pdfviewer;
	requires org.apache.pdfbox;
	requires org.apache.pdfbox.io;
	requires java.image.scaling;
}