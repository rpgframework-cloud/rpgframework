
module de.rpgframework.core {
	requires simple.persist;

	exports de.rpgframework;
	exports de.rpgframework.classification;
	exports de.rpgframework.core;
	exports de.rpgframework.reality;
	exports de.rpgframework.world;

	opens de.rpgframework.reality;
}