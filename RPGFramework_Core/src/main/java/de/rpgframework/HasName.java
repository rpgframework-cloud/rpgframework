package de.rpgframework;

import java.util.Locale;

/**
 * @author prelle
 *
 */
public interface HasName {

	public String getName(Locale loc);
	public String getId();

}
