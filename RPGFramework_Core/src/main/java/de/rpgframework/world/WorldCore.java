package de.rpgframework.world;

import java.lang.System.Logger;
import java.util.HashMap;
import java.util.Map;

/**
 * @author prelle
 *
 */
public abstract class WorldCore {

	protected final static Logger logger = System.getLogger(WorldCore.class.getPackageName());

	private static Map<String,World> worlds;

	//-------------------------------------------------------------------
	static {
		worlds       = new HashMap<>();
	}

	//-------------------------------------------------------------------
	public static void registerWorld(World world) {
		worlds.put(world.getId(), world);
	}

}
