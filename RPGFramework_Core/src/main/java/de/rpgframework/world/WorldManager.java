package de.rpgframework.world;

import java.util.List;

/**
 *
 */
public interface WorldManager {



	public List<World> getWorlds();

	public World getOrCreateWorld(String id);

}
