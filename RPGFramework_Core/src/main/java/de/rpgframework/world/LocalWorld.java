package de.rpgframework.world;

import java.io.FileReader;
import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.nio.file.Files;
import java.nio.file.Path;

import org.prelle.simplepersist.Persister;

import de.rpgframework.classification.Taxonomy;

/**
 *
 */
public class LocalWorld implements World {

	private final static Logger logger = System.getLogger(LocalWorld.class.getPackageName());

	private Persister serializer;
	private Path worldDir;
	private String id;

	private Taxonomy worldTaxonomy;

	//-------------------------------------------------------------------
	public LocalWorld(String id, Path dataDir) {
		this.id       = id;
		this.worldDir = dataDir;
		serializer = new Persister();
		worldTaxonomy = new Taxonomy();

		Path regionsFile = worldDir.resolve("regions.xml");
		if (Files.exists(regionsFile)) {
			try {
				HierarchicTagTable table = serializer.read(HierarchicTagTable.class, new FileReader(regionsFile.toFile()));
				logger.log(Level.WARNING, "Loaded {0} regions", table.size());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			logger.log(Level.INFO, "Did not load regions");
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.world.World#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.world.World#getTaxonomy()
	 */
	@Override
	public Taxonomy getTaxonomy() {
		return worldTaxonomy;
	}

}
