package de.rpgframework.world;

import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 *
 */
@Root(name="table")
@ElementList(entry = "entry", type = HierarchicTag.class)
public class HierarchicTagTable extends ArrayList<HierarchicTag> {

	@Attribute
	private String type;

//	@ElementList(entry = "entry", type = HierarchicTag.class, inline=true)
//	private List<HierarchicTag> children = new ArrayList<>();

	//-------------------------------------------------------------------
	/**
	 */
	public HierarchicTagTable() {
		// TODO Auto-generated constructor stub
	}

}
