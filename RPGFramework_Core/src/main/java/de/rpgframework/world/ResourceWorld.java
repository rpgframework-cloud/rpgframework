package de.rpgframework.world;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.prelle.simplepersist.Persister;

import de.rpgframework.classification.Taxonomy;

/**
 *
 */
public class ResourceWorld implements World {

	private final static Logger logger = System.getLogger(ResourceWorld.class.getPackageName());

	private Persister serializer;
	private Class<?> cls ;
	private String id;

	//-------------------------------------------------------------------
	public ResourceWorld(String id, Class<?> cls) {
		this.id    = id;
		this.cls   = cls;
		serializer = new Persister();

		InputStream in = cls.getResourceAsStream(id+".regions.xml");
		if (in!=null) {
			try {
				HierarchicTagTable table = serializer.read(HierarchicTagTable.class,in);
				logger.log(Level.WARNING, "Loaded {0} regions", table.size());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			logger.log(Level.INFO, "Did not load regions");
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.world.World#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.world.World#getTaxonomy()
	 */
	@Override
	public Taxonomy getTaxonomy() {
		return null;
	}

}
