package de.rpgframework.world;

import de.rpgframework.classification.Taxonomy;

/**
 *
 */
public interface World {

	public String getId();

	//-------------------------------------------------------------------
	/**
	 * Get world specific tagging schema
	 * @return
	 */
	public Taxonomy getTaxonomy();

}
