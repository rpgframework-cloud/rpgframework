package de.rpgframework.world;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.RPGFrameworkConstants;
import de.rpgframework.classification.ClassificationType;

/**
 * @author prelle
 *
 */
public interface WorldClassificationType {

	final static MultiLanguageResourceBundle RES = RPGFrameworkConstants.MULTI;

	public final static ClassificationType REGION = new ClassificationType("REGION", RES);
	public final static ClassificationType FACTION= new ClassificationType("FACTION", RES);

}
