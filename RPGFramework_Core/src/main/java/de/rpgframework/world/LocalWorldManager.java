package de.rpgframework.world;

import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.prelle.simplepersist.Persister;

/**
 *
 */
public class LocalWorldManager implements WorldManager {

	private final static Logger logger = System.getLogger(LocalWorldManager.class.getPackageName());

	private Persister serializer;
	private Path dataDir;

	private Map<String,World> worlds;

	//-------------------------------------------------------------------
	public LocalWorldManager(Path dataDir) {
		this.dataDir = dataDir;
		serializer = new Persister();
		worlds     = new HashMap<>();

		try {
			logger.log(Level.DEBUG, "Search worlds in "+dataDir.toAbsolutePath());
			DirectoryStream<Path> stream = Files.newDirectoryStream(dataDir, new DirectoryStream.Filter<Path>() {
				public boolean accept(Path entry) throws IOException {
					return Files.isDirectory(entry);
				}});
			for (Path worldDir : stream) {
				String id = worldDir.getFileName().toString();
				logger.log(Level.DEBUG, "Loading world {0} from {1}", id, worldDir);
				LocalWorld world = new LocalWorld(id, worldDir);
				logger.log(Level.INFO, "Successfully loaded world {0}", world.getId());
				worlds.put(id,world);
			}
			stream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.world.WorldManager#getWorlds()
	 */
	@Override
	public List<World> getWorlds() {
		return new ArrayList<World>(worlds.values());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.world.WorldManager#getOrCreateWorld(java.lang.String)
	 */
	@Override
	public World getOrCreateWorld(String id) {
		if (worlds.get(id)!=null) return worlds.get(id);

		Path worldDir = dataDir.resolve(id);
		try {
			Files.createDirectories(worldDir);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		LocalWorld world = new LocalWorld(id, worldDir);
		worlds.put(id, world);
		return world;
	}

	//-------------------------------------------------------------------
	public void loadHardcodedWorld(Class<?> cls, String id) {
		ResourceWorld world = new ResourceWorld(id, cls);
		worlds.put(id, world);
	}

}
