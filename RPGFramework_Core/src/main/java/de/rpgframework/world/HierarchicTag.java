package de.rpgframework.world;

import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.ElementList;

/**
 *
 */
public class HierarchicTag {

	/** An identifier within type */
	@Attribute(required=true)
	protected String id;

	@ElementList(entry = "entry", type = HierarchicTag.class, inline=true)
	private List<HierarchicTag> children = new ArrayList<>();

	private transient HierarchicTag parent;

	//-------------------------------------------------------------------
	/**
	 */
	public HierarchicTag() {
		// TODO Auto-generated constructor stub
	}

}
