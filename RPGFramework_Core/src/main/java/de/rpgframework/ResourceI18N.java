package de.rpgframework;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingFormatArgumentException;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * @author Stefan Prelle
 *
 */
public class ResourceI18N {

	private final static Logger logger = System.getLogger("de.rpgframework.RPGFramework");

	private static Map<String,PrintWriter> MISSING = new HashMap<>();
	private static Map<String,List<String>> reportedKeys = new HashMap<>();
	private static Path myPath;

	//--------------------------------------------------------------------
	static {
		try {
			if (System.getProperty("logdir")==null) {
				myPath = Files.createTempDirectory("rpgframework");
				myPath.toFile().deleteOnExit();
			} else {
				myPath = Paths.get(System.getProperty("logdir"));
			}
			System.err.println("Missing keys written to "+myPath);
		} catch (IOException e) {
			logger.log(Level.ERROR,"Failed setting up file for missing keys",e);
		}
	}

	//-------------------------------------------------------------------
	public static String get(ResourceBundle res, String key) {
		if (res==null)
			return key;
		try {
			return res.getString(key);
		} catch (MissingResourceException e) {
//			BasePluginData.reportKey(e, res);
			System.err.println("Missing key '"+key+"' in "+res.getBaseBundleName()+" locale "+res.getLocale());
			logger.log(Level.ERROR, "Missing key '"+key+"' in "+res.getBaseBundleName());
			logger.log(Level.ERROR, " => "+e.getStackTrace()[4]);
			reportKey(res,key);
		}
		return key;
	}

	//-------------------------------------------------------------------
	public static String format(ResourceBundle res, String key, Object...objects) {
		try {
			return String.format(res.getString(key), objects);
		} catch (MissingResourceException e) {
//			BasePluginData.reportKey(e, res);
//			System.err.println("Missing key '"+key+"' in "+res.getBaseBundleName());
			logger.log(Level.ERROR, "Missing key '"+key+"' in "+res.getBaseBundleName());
			logger.log(Level.ERROR, " => "+e.getStackTrace()[4]);
//			LogManager.getLogger("shadowrun6").error(" => "+e.getStackTrace()[3]);
			reportKey(res,key);
		}
		return key;
	}

	//-------------------------------------------------------------------
	public static String get(MultiLanguageResourceBundle res, Locale loc, String key) {
		if (res==null)
			return key;
		try {
			return res.getString(key, loc);
		} catch (MissingResourceException e) {
//			BasePluginData.reportKey(e, res);
//			System.err.println("Missing key '"+key+"' in "+res.getBaseBundleName());
			logger.log(Level.ERROR, "Missing key '"+key+"' in "+res.getBaseBundleName());
			logger.log(Level.ERROR, " => "+e.getStackTrace()[4]);
		}
		return key;
	}

	//-------------------------------------------------------------------
	public static String format(MultiLanguageResourceBundle res, Locale loc, String key, Object...objects) {
		try {
			return String.format(res.getString(key, loc), objects);
		} catch (MissingFormatArgumentException e) {
			logger.log(Level.ERROR, "Missing argument "+e.getMessage()+" in key \""+key+"\" , which resolved to \""+res.getString(key, loc)+"\"");
			return res.getString(key, loc);
		} catch (MissingResourceException e) {
//			BasePluginData.reportKey(e, res);
//			System.err.println("Missing key '"+key+"' in "+res.getBaseBundleName());
			logger.log(Level.ERROR, "Missing key '"+key+"' in "+res.getBaseBundleName());
//			LogManager.getLogger("shadowrun6").error(" => "+e.getStackTrace()[3]);
		}
		return key;
	}

	//-------------------------------------------------------------------
	private static void reportKey(ResourceBundle res, String key) {
		String fileKey = "bundle-"+res.getBaseBundleName();
		List<String> reported = reportedKeys.get(fileKey);
		if (reported==null) {
			reported=new ArrayList<>();
			reportedKeys.put(fileKey, reported);
		}
		if (!reported.contains(key)) {
			PrintWriter out = MISSING.get(fileKey);
			try {
				if (out==null) {
					out = new PrintWriter(new FileWriter(myPath+System.getProperty("file.separator")+fileKey+".txt"));
					MISSING.put(fileKey, out);
				}
				out.println(key+"= ");
				out.flush();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

}
