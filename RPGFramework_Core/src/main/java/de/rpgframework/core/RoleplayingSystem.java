/**
 *
 */
package de.rpgframework.core;

import java.util.Locale;
import java.util.MissingResourceException;

import org.prelle.simplepersist.Root;

import de.rpgframework.RPGFrameworkConstants;
import de.rpgframework.classification.Classification;
import de.rpgframework.classification.ClassificationType;
import de.rpgframework.classification.GenericClassificationType;
import de.rpgframework.classification.Genre;

/**
 * @author prelle
 *
 */
@Root(name = "rules")
public enum RoleplayingSystem implements Classification<RoleplayingSystem> {

	ALL(Genre.values()),
	CHRONOPIA_TT(Genre.FANTASY, Genre.STEAMPUNK),
	CORIOLIS(Genre.SCIFI),
	CTHULHU(Genre.VINTAGE_1920, Genre.HORROR),
	DEADLANDS(Genre.WESTERN),
	DRESDEN_FILES(Genre.TODAY),
	DSA(Genre.FANTASY),
	DSA5(Genre.FANTASY),
	GENESYS(Genre.values()),
	HEX(Genre.VINTAGE_1930),
	MURPG(Genre.TODAY, Genre.SUPERHEROES),
	MUTANT_YEAR_ZERO(Genre.APOCALYPTIC, Genre.TODAY),
	SPLITTERMOND(Genre.FANTASY),
	SHADOWRUN(Genre.CYBERPUNK),
	SHADOWRUN6(Genre.CYBERPUNK),
	SHADOWRUN7(Genre.CYBERPUNK),
	SPACE1889(Genre.STEAMPUNK),
	STARWARS(Genre.SCIFI),
	STEAM_ARCANA(Genre.STEAMPUNK, Genre.FANTASY),
	TALES_FROM_THE_LOOP(Genre.TODAY, Genre.SCIFI, Genre.VINTAGE_1980),
	TERRINOTH(Genre.FANTASY),
	TORG_ETERNITY(Genre.TODAY, Genre.FANTASY, Genre.SCIFI, Genre.CYBERPUNK),
	;

	private Genre[] genres;

	//--------------------------------------------------------------------
	private RoleplayingSystem(Genre... genre) {
		this.genres = genre;
	}

	//--------------------------------------------------------------------
	public Genre[] getGenre() {
		return genres;
	}

	//--------------------------------------------------------------------
	@Deprecated
	public String getName() {
		return getName(Locale.getDefault());
	}

	//--------------------------------------------------------------------
	public String getName(Locale loc) {
		try {
			return RPGFrameworkConstants.RES.getString("roleplayingsystem."+this.name().toLowerCase());
		} catch (MissingResourceException e) {
			System.err.println("Missing "+e.getKey()+" in "+RPGFrameworkConstants.RES.getBaseBundleName());
		}
		return "roleplayingsystem."+this.name().toLowerCase();
	}

	//--------------------------------------------------------------------
	public String toString() {
		return name();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.Classification#getType()
	 */
	@Override
	public ClassificationType getType() {
		return GenericClassificationType.RULES;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.Classification#getValue()
	 */
	@Override
	public RoleplayingSystem getValue() {
		return this;
	}

}
