package de.rpgframework.core;

import java.io.ByteArrayInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.System.Logger.Level;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

/**
 *
 */
public class CustomDescriptionHandle {

	static class TranslationFile {
		/** NULL indicates default */
		String lang;
		byte[] data;
	}

	private String name;
	private Map<String, byte[]> fileContent;

	private Map<String, Properties> parsed;

	//-------------------------------------------------------------------
	public CustomDescriptionHandle(String name) {
		this.name = name;
		fileContent = new HashMap<>();
		parsed = new LinkedHashMap<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	//-------------------------------------------------------------------
	public void addLanguage(String filename, byte[] data) {
		fileContent.put(filename, data);

		Properties pro = new Properties();
		try {
			pro.load(new ByteArrayInputStream(data));
			parsed.put(filename, pro);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//-------------------------------------------------------------------
	public synchronized String get(String key, Locale loc) {
		String filename = name+"_"+loc.getLanguage()+".properties";
		Properties pro = parsed.get(filename);
		if (pro!=null && pro.containsKey(key)) return pro.getProperty(key);

		filename = name+".properties";
		pro = parsed.get(filename);
		if (pro!=null && pro.containsKey(key)) return pro.getProperty(key);

		return null;
	}

	//-------------------------------------------------------------------
	public void put(String key, Locale loc, String value, Path localBaseDir) {
		String filename = name+"_"+loc.getLanguage()+".properties";
		Properties pro = parsed.get(filename);
		if (pro==null) {
			pro = new Properties();
			parsed.put(filename, pro);
		}

		synchronized (pro) {
			pro.put(key, value);
		}

		Path path = null;
		try {
			path = localBaseDir.resolve(filename);
			System.getLogger(getClass().getPackageName()).log(Level.DEBUG, "Write {0}",path);
			pro.store(new FileWriter(path.toFile()), filename);
		} catch (IOException e) {
			System.getLogger(getClass().getPackageName()).log(Level.WARNING, "Writing custom properties failed",e);
			BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 2, "Cannot write to "+path,e);
		}
	}

}
