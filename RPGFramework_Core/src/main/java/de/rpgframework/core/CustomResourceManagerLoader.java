package de.rpgframework.core;

/**
 * @author prelle
 *
 */
public class CustomResourceManagerLoader {

	private static CustomResourceManager charProv;

	//--------------------------------------------------------------------
	public static CustomResourceManager getInstance() {
			return charProv;
	}

	//--------------------------------------------------------------------
	public static void setResourceManager(CustomResourceManager service) {
		charProv = service;
	}

}
