package de.rpgframework.core;

import java.util.Locale;

/**
 *
 */
public interface CustomResourceManager {

	public String getProperty(RoleplayingSystem rules, String key, Locale loc);

	public boolean setProperty(RoleplayingSystem rules, String key, Locale loc, String value);
	
}
