package de.rpgframework.core;

/**
 * @author prelle
 *
 */
public interface StartupStep extends Runnable {

	public boolean canRun();

}
