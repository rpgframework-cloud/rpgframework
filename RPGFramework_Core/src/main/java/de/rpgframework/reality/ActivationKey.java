package de.rpgframework.reality;

import java.time.Instant;
import java.util.UUID;

/**
 * @author prelle
 *
 */
public class ActivationKey {

	private UUID   id;
	private String itemID;
	private Instant timestamp;

	//-------------------------------------------------------------------
	public ActivationKey() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	public ActivationKey(UUID id, String item, Instant timestamp) {
		this.id = id;
		this.itemID = item;
		this.timestamp = timestamp;
	}

	//-------------------------------------------------------------------
	public String toString() { return itemID; }

	//-------------------------------------------------------------------
	/**
	 * @return the playerUUID
	 */
	public UUID getID() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @param playerUUID the playerUUID to set
	 */
	public void setID(UUID id) {
		this.id = id;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the itemID
	 */
	public String getItemID() {
		return itemID;
	}

	//-------------------------------------------------------------------
	/**
	 * @param itemID the itemID to set
	 */
	public void setItemID(String itemID) {
		this.itemID = itemID;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the timestamp
	 */
	public Instant getTimestamp() {
		return timestamp;
	}

	//-------------------------------------------------------------------
	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Instant timestamp) {
		this.timestamp = timestamp;
	}

}
