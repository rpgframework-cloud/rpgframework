package de.rpgframework.reality;

/**
 *
 */
public enum EdenConnectionState {

	NOT_CONNECTED,
	CONNECTED
}
