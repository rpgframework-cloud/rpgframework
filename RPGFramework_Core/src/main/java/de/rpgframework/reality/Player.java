package de.rpgframework.reality;

/**
 * @author prelle
 *
 */
public interface Player {
	
	public Player setFirstName(String value);
	public String getFirstName();
	
	public Player setLastName(String value);
	public String getLastName();
	
	public Player setEmail(String value);
	public String getEmail();
	
	public Player setLogin(String value);
	public String getLogin();
	
	public Player setPassword(String value);
	public String getPassword();

	public Player setVerified(boolean state);
	public boolean isVerified();

}
