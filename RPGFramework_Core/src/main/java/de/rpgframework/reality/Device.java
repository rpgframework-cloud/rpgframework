package de.rpgframework.reality;

/**
 * @author prelle
 *
 */
public class Device {
	
	private String model;
	private String uuid;
	private String platform;
	private String version;

	//-------------------------------------------------------------------
	public Device() {
	}

	//-------------------------------------------------------------------
	/**
	 * @return the model
	 */
	public String getModel() {
		return model;
	}

	//-------------------------------------------------------------------
	/**
	 * @param model the model to set
	 */
	public void setModel(String model) {
		this.model = model;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the uuid
	 */
	public String getUuid() {
		return uuid;
	}

	//-------------------------------------------------------------------
	/**
	 * @param uuid the uuid to set
	 */
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the platform
	 */
	public String getPlatform() {
		return platform;
	}

	//-------------------------------------------------------------------
	/**
	 * @param platform the platform to set
	 */
	public void setPlatform(String platform) {
		this.platform = platform;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	//-------------------------------------------------------------------
	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}

}
