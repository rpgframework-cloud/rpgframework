package de.rpgframework.reality;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.rpgframework.core.RoleplayingSystem;

/**
 * @author stefa
 *
 */
public class CatalogItem {

	public enum State {
		HIDDEN,
		AVAILABLE
	}

	public enum Category {
		CHARGEN,
		FVTT,
		CHARGEN_FVTT
	}

	private String id;
	private Category category;
	private RoleplayingSystem rules;
	private String language;
	private String name;
	private String description;
	/** In Cent */
	private int price;
	private State state;
	private List<String> datasets;

	//-------------------------------------------------------------------
	public CatalogItem(String id) {
		this.id = id;
		this.datasets = new ArrayList<String>();
	}

	//-------------------------------------------------------------------
	public CatalogItem(String id, Category category, RoleplayingSystem rules, String lang, String name, int price) {
		this.id = id;
		this.category = category;
		this.rules    = rules;
		this.language = lang;
		this.name     = name;
		this.price    = price;
		this.state    = State.AVAILABLE;
		this.datasets = new ArrayList<String>();
	}

	//-------------------------------------------------------------------
	public CatalogItem(String id, Category category, RoleplayingSystem rules, String lang, String name, int price, State state, String...plugins) {
		this.id = id;
		this.category = category;
		this.rules    = rules;
		this.language = lang;
		this.name     = name;
		this.price    = price;
		this.state    = state;
		this.datasets  = Arrays.asList(plugins);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the category
	 */
	public Category getCategory() {
		return category;
	}

	//-------------------------------------------------------------------
	/**
	 * @param category the category to set
	 */
	public void setCategory(Category category) {
		this.category = category;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the rules
	 */
	public RoleplayingSystem getRules() {
		return rules;
	}

	//-------------------------------------------------------------------
	/**
	 * @param rules the rules to set
	 */
	public void setRules(RoleplayingSystem rules) {
		this.rules = rules;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	//-------------------------------------------------------------------
	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	//-------------------------------------------------------------------
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the price
	 */
	public int getPrice() {
		return price;
	}

	//-------------------------------------------------------------------
	/**
	 * @param price the price to set
	 */
	public void setPrice(int price) {
		this.price = price;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the state
	 */
	public State getState() {
		return state;
	}

	//-------------------------------------------------------------------
	/**
	 * @param state the state to set
	 */
	public void setState(State state) {
		this.state = state;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the plugins
	 */
	public List<String> getDatasets() {
		return datasets;
	}

	//-------------------------------------------------------------------
	/**
	 * @param plugins the plugins to set
	 */
	public void setPlugins(List<String> plugins) {
		this.datasets = plugins;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	//-------------------------------------------------------------------
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}
