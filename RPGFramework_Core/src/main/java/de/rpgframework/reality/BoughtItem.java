package de.rpgframework.reality;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

/**
 * @author prelle
 *
 */
public class BoughtItem {

	private final static DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(ZoneId.of("Europe/Berlin"));

	private UUID   playerUUID;
	private String itemID;
	private String timestamp;
	private int    price;
	/** Paypal */
	private String transactionID;
	/** Bought in Shops */
	private UUID licenseKey;
	/** Display name - to be set before sending to client */
	private String name;

	//-------------------------------------------------------------------
	public BoughtItem() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	public BoughtItem(UUID player, String item, Instant timestamp, int price) {
		this.playerUUID = player;
		this.itemID = item;
		this.timestamp = FORMATTER.format(timestamp);
		this.price = price;
	}

	//-------------------------------------------------------------------
	public String toString() { return (name!=null)?name:itemID; }

	//-------------------------------------------------------------------
	/**
	 * @return the playerUUID
	 */
	public UUID getPlayerUUID() {
		return playerUUID;
	}

	//-------------------------------------------------------------------
	/**
	 * @param playerUUID the playerUUID to set
	 */
	public void setPlayerUUID(UUID playerUUID) {
		this.playerUUID = playerUUID;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the itemID
	 */
	public String getItemID() {
		return itemID;
	}

	//-------------------------------------------------------------------
	/**
	 * @param itemID the itemID to set
	 */
	public void setItemID(String itemID) {
		this.itemID = itemID;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the timestamp
	 */
	public Instant getTimestamp() {
		return FORMATTER.parse(timestamp, Instant::from);
	}

	//-------------------------------------------------------------------
	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Instant timestamp) {
		this.timestamp = FORMATTER.format(timestamp);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the price
	 */
	public int getPrice() {
		return price;
	}

	//-------------------------------------------------------------------
	/**
	 * @param price the price to set
	 */
	public void setPrice(int price) {
		this.price = price;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the transactionID
	 */
	public String getTransactionID() {
		return transactionID;
	}

	//-------------------------------------------------------------------
	/**
	 * @param transactionID the transactionID to set
	 */
	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the licenseKey
	 */
	public UUID getLicenseKey() {
		return licenseKey;
	}

	//-------------------------------------------------------------------
	/**
	 * @param licenseKey the licenseKey to set
	 */
	public void setLicenseKey(UUID licenseKey) {
		this.licenseKey = licenseKey;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	//-------------------------------------------------------------------
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

}
