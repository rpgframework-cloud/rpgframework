/**
 *
 */
package de.rpgframework.classification;

import de.rpgframework.RPGFrameworkConstants;

/**
 * @author prelle
 *
 */
public enum Difficulty implements MediaTag, Classification<Difficulty> {

	VERY_EASE,
	EASE,
	MEDIUM,
	HARD,
	VERY_HARD,
	EXTREME
	;

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.MediaTag#getName()
	 */
	@Override
	public String getName() {
		return RPGFrameworkConstants.MULTI.getString("tag.difficulty."+this.name().toLowerCase());
	}

	//-------------------------------------------------------------------
	public static String getTagTypeName() {
		return RPGFrameworkConstants.MULTI.getString("tag.difficulty");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.Classification#getType()
	 */
	@Override
	public ClassificationType getType() {
		return GenericClassificationType.DIFFICULTY;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.Classification#getValue()
	 */
	@Override
	public Difficulty getValue() {
		return this;
	}

}
