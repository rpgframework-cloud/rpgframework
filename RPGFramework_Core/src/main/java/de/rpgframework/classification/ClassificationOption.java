package de.rpgframework.classification;

public class ClassificationOption<T> implements Comparable<ClassificationOption<T>> {
	
	private T id;
	private String translatedName;
	
	//-------------------------------------------------------------------
	public ClassificationOption(T id, String translatedName) {
		this.id = id;
		this.translatedName = translatedName;
	}
	
	//-------------------------------------------------------------------
	public T getId() { return id; }
	public String getName() { return translatedName; }
	
	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(ClassificationOption<T> other) {
		return translatedName.compareToIgnoreCase(other.translatedName);
	}

}