package de.rpgframework.classification;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.RPGFrameworkConstants;

/**
 * @author prelle
 *
 */
public interface GenericClassificationType {

	final static MultiLanguageResourceBundle RES = RPGFrameworkConstants.MULTI;

	public final static ClassificationType ACTOR_ROLE = new ClassificationType("ACTOR_ROLE", RES, ActorRole.class);
	public final static ClassificationType ACTOR_TYPE = new ClassificationType("ACTOR_TYPE", RES);
	public final static ClassificationType ACTOR_PERSONALITY = new ClassificationType("PERSONALITY", RES);
	public final static ClassificationType AGE        = new ClassificationType("AGE", RES);
	public final static ClassificationType DIFFICULTY = new ClassificationType("DIFFICULTY", RES);
	public final static ClassificationType GENDER = new ClassificationType("GENDER", RES);
	public final static ClassificationType GENRE  = new ClassificationType("GENRE", RES);
	public final static ClassificationType PLACE  = new ClassificationType("PLACE", RES, TagPlaces.class);
	//public final static ClassificationType ROLE   = new ClassificationType("ROLE", RES);
	public final static ClassificationType RULES  = new ClassificationType("RULES", RES, true);
	public final static ClassificationType LANDSCAPE = new ClassificationType("LANDSCAPE", RES);
	public final static ClassificationType CONTACT_TYPE  = new ClassificationType("CONTACT", RES, TagContactType.class);
	public final static ClassificationType WILDNESS  = new ClassificationType("WILDNESS", RES);

}
