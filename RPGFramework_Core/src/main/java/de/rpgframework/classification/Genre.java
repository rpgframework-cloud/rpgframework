/**
 * 
 */
package de.rpgframework.classification;

import java.util.MissingResourceException;

import org.prelle.simplepersist.Root;

import de.rpgframework.RPGFrameworkConstants;

/**
 * @author prelle
 *
 */
@Root(name="genre")
public enum Genre implements Classification<Genre> {

	APOCALYPTIC,
	FANTASY,
	HORROR,
	SCIFI,
	CYBERPUNK,
	TODAY,
	VINTAGE_1920,
	VINTAGE_1930,
	VINTAGE_1980,
	WESTERN,
	STEAMPUNK,
	SUPERHEROES,
	;
	

	//-------------------------------------------------------------------
	public String getName() {
		try {
			return RPGFrameworkConstants.RES.getString("genre."+this.name().toLowerCase());
		} catch (MissingResourceException e) {
			System.err.println("Missing "+e.getKey()+" in "+RPGFrameworkConstants.RES.getBaseBundleName());
		}
		return "genre."+this.name().toLowerCase();
	}

//	@Override
//	public List<ClassificationOption<Genre>> getOptions(Locale loc) {
//		return List.of( Genre.values() ).stream().map( g -> new ClassificationOption<Genre>(g,g.getName(loc))).collect(Collectors.toList());
//	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.Classification#getType()
	 */
	@Override
	public ClassificationType getType() {
		return GenericClassificationType.GENRE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.Classification#getValue()
	 */
	@Override
	public Genre getValue() {
		return this;
	}

}
