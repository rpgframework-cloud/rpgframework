package de.rpgframework.classification;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.function.Function;

import de.rpgframework.MultiLanguageResourceBundle;

/**
 * The definition of a classification
 *
 * @author prelle
 */
public class ClassificationType {

	public static class EnumValidator implements Function<String, Object> {
		private Class<? extends Enum> enumCls;
		private Method valueOf;
		public EnumValidator(Class<? extends Enum> enumCls) {
			this.enumCls = enumCls;
			try {
				valueOf = enumCls.getMethod("valueOf", String.class);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		public Object apply(String value) {
			if (value.contains(",")) {
				String[] splitted = value.split(",");
				List<Object> ret = new ArrayList<>();
				for (String s : splitted) {
					try {
						ret.add(valueOf.invoke(enumCls, s.trim()));
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						e.printStackTrace();
					}
				}
				return ret;
			}

			try {
				return valueOf.invoke(enumCls, value);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}
			return null;
		}
	}

	private String id;
	private MultiLanguageResourceBundle res;
	private boolean ruleSpecific;
	private Function<String, Object> resolver;

	//-------------------------------------------------------------------
	public ClassificationType(String id, MultiLanguageResourceBundle res) {
		this.id = id;
		this.res= res;
	}
	//-------------------------------------------------------------------
	public ClassificationType(String id, MultiLanguageResourceBundle res, boolean ruleSpecific) {
		this.id = id;
		this.res= res;
		this.ruleSpecific = ruleSpecific;
	}
	//-------------------------------------------------------------------
	public ClassificationType(String id, MultiLanguageResourceBundle res, Class<? extends Enum> enumCls) {
		this.id = id;
		this.res= res;
		this.resolver = new EnumValidator(enumCls);
	}

	//-------------------------------------------------------------------
	public String toString() {
		if (ruleSpecific) return id+"(var)";
		return id;
	}

	//-------------------------------------------------------------------
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	public String getName(Locale loc) {
		return res.getString(id, loc);
	}

	//-------------------------------------------------------------------
	public String getName() {
		return getName(Locale.getDefault());
	}

	//-------------------------------------------------------------------
	public boolean isRuleSpecific() {
		return ruleSpecific;
	}

	//-------------------------------------------------------------------
	public Object resolve(String value) {
		if (resolver==null)
			return null;
		return resolver.apply(value);
	}

}
