/**
 * 
 */
package de.rpgframework.classification;

/**
 * @author prelle
 *
 */
public interface MediaTag {

	//-------------------------------------------------------------------
	/**
	 * Identifier for value of tag
	 */
	public String name();

	//-------------------------------------------------------------------
	public String getName();
	
}
