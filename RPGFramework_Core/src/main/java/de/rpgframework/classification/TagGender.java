/**
 *
 */
package de.rpgframework.classification;

import java.util.MissingResourceException;

import de.rpgframework.RPGFrameworkConstants;

/**
 * @author prelle
 *
 */
public enum TagGender implements MediaTag, Classification<TagGender> {

	MALE,
	FEMALE,
	OTHER,
	;


	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.MediaTag#getName()
	 */
	@Override
	public String getName() {
		try {
			return RPGFrameworkConstants.RES.getString("tag.gender."+this.name().toLowerCase());
		} catch (MissingResourceException e) {
			System.err.println("Missing "+e.getKey()+" in "+RPGFrameworkConstants.RES.getBaseBundleName());
		}
		return "tag.gender."+this.name().toLowerCase();
	}

	//-------------------------------------------------------------------
	public static String getTagTypeName() {
		try {
			return RPGFrameworkConstants.RES.getString("tag.gender");
		} catch (MissingResourceException e) {
			System.err.println("Missing "+e.getKey()+" in "+RPGFrameworkConstants.RES.getBaseBundleName());
		}
		return "tag.gender";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.Classification#getType()
	 */
	@Override
	public ClassificationType getType() {
		return GenericClassificationType.GENDER;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.Classification#getValue()
	 */
	@Override
	public TagGender getValue() {
		return this;
	}

}
