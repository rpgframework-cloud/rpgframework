package de.rpgframework.classification;

import java.util.Locale;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.RPGFrameworkConstants;

/**
 * @author prelle
 *
 */
public enum Gender implements Classification<Gender>{
	
	MALE,
	FEMALE,
	DIVERSE;
	
	private static MultiLanguageResourceBundle RES = RPGFrameworkConstants.MULTI;

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.Classification#getType()
	 */
	@Override
	public ClassificationType getType() {
		return GenericClassificationType.GENDER;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.Classification#getValue()
	 */
	@Override
	public Gender getValue() {		
		return this;
	}

	//-------------------------------------------------------------------
	public String getName(Locale loc) {
		return RES.getString("gender."+this.name().toLowerCase(), loc);
	}

	//-------------------------------------------------------------------
	public String getPreName(Locale loc) {
		return RES.getString("gender.pre."+this.name().toLowerCase(), loc);
	}
}
