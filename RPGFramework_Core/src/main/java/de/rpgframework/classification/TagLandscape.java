/**
 *
 */
package de.rpgframework.classification;

import de.rpgframework.RPGFrameworkConstants;

/**
 * @author prelle
 *
 */
public enum TagLandscape implements MediaTag, Classification<TagLandscape> {

	TOWN,
	VILLAGE,
	FOREST,
	CLEARING,
	MEADOW,
	SWAMP,
	LAKE,
	SEA,
	ISLE,
	HILLS,
	MOUNTAINS,
	CAVE,
	TUNNEL,
	;

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.MediaTag#getName()
	 */
	@Override
	public String getName() {
		return RPGFrameworkConstants.MULTI.getString("tag.landscape."+this.name().toLowerCase());
	}

	//-------------------------------------------------------------------
	public static String getTagTypeName() {
		return RPGFrameworkConstants.MULTI.getString("tag.landscape");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.Classification#getType()
	 */
	@Override
	public ClassificationType getType() {
		return GenericClassificationType.LANDSCAPE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.Classification#getValue()
	 */
	@Override
	public TagLandscape getValue() {
		return this;
	}

}
