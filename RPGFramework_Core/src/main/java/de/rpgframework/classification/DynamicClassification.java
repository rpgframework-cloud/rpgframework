package de.rpgframework.classification;

/**
 *
 */
public class DynamicClassification implements Classification<String> {

	private ClassificationType type;
	private String value;

	//-------------------------------------------------------------------
	/**
	 */
	public DynamicClassification(ClassificationType type, String value) {
		this.type = type;
		this.value = value;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return "dyn:"+type+"="+value;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.Classification#getType()
	 */
	@Override
	public ClassificationType getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.Classification#getValue()
	 */
	@Override
	public String getValue() {
		return value;
	}

}
