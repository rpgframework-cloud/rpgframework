/**
 *
 */
package de.rpgframework.classification;

/**
 * @author prelle
 *
 */
public enum TagContactType implements Classification<TagContactType> {

	CRIMINAL,
	CORPORATE,
	GOVERNMENT,
	MAGIC,
	MATRIX,
	MEDICAL,
	STREET
	;

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.Classification#getType()
	 */
	@Override
	public ClassificationType getType() {
		return GenericClassificationType.CONTACT_TYPE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.Classification#getValue()
	 */
	@Override
	public TagContactType getValue() {
		return this;
	}

}
