/**
 *
 */
package de.rpgframework.classification;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author prelle
 *
 */
public class Taxonomy {

	private final static Logger logger = System.getLogger(Taxonomy.class.getPackageName());

	private record TypeData(ClassificationType type, Object[] values) {

	};

	private static Taxonomy genericInstance;


	private Map<String, TypeData> database;

	//-------------------------------------------------------------------
	static {
		genericInstance = new Taxonomy();
		genericInstance.register(GenericClassificationType.ACTOR_ROLE, ActorRole.values());
		genericInstance.register(GenericClassificationType.ACTOR_TYPE, ActorType.values());
		genericInstance.register(GenericClassificationType.AGE, TagAge.values());
		genericInstance.register(GenericClassificationType.GENDER, Gender.values());
		genericInstance.register(GenericClassificationType.GENRE, Genre.values());
		genericInstance.register(GenericClassificationType.LANDSCAPE, TagLandscape.values());
//		genericInstance.register(GenericClassificationType.LIFEFORM);
//		genericInstance.register(GenericClassificationType.MOOD);
		genericInstance.register(GenericClassificationType.CONTACT_TYPE, TagContactType.values());
		genericInstance.register(GenericClassificationType.PLACE, TagPlaces.values());
//		genericInstance.register(GenericClassificationType.TEMPO);
		genericInstance.register(GenericClassificationType.WILDNESS, TagWildness.values());
	}
	//-------------------------------------------------------------------
	public static Taxonomy getGeneric() { return genericInstance; }

	//-------------------------------------------------------------------
	public Taxonomy() {
		database = new HashMap<>();
	}

	//-------------------------------------------------------------------
	public <T> void register(ClassificationType cls, T[] values) {
		String key = cls.getId();
		if (database.containsKey(key)) {
			return;
		}

		TypeData data = new TypeData(cls, values);
		database.put(key, data);

		logger.log(Level.WARNING, "Registered {0} with {1} values", key, values.length);
	}

	//-------------------------------------------------------------------
	public Collection<String> getKnownKeys() {
		return database.keySet();
	}

	//-------------------------------------------------------------------
	public ClassificationType getClassificationTypeFor(String key) {
		if (database.containsKey(key))
			return database.get(key).type;
		return null;
	}

	//-------------------------------------------------------------------
	public Object[] getValidValuesFor(String key) {
		if (database.containsKey(key))
			return database.get(key).values();
		return null;
	}

	//-------------------------------------------------------------------
//	public static String getTagTypeID(MediaTag tag) {
//		return tag.getClass().getSimpleName();
//	}
//
//	//-------------------------------------------------------------------
//	public static MediaTag resolveTag(String simpleClsName, String value) {
//		for (Class<? extends MediaTag> cls : taxonomy) {
//			if (cls.getSimpleName().equals(simpleClsName)) {
//				try {
//					return (MediaTag)cls.getMethod("valueOf", String.class).invoke(null, value);
//				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
//						| NoSuchMethodException | SecurityException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//		}
//		return null;
//	}

}
