/**
 * 
 */
package de.rpgframework.classification;

import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.stream.Collectors;

import de.rpgframework.RPGFrameworkConstants;

/**
 * @author prelle
 *
 */
public enum TagAge implements MediaTag, Classification<TagAge> {
	
	CHILD,
	YOUNG,
	MEDIUM,
	OLD,
	VERY_OLD
	;

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.MediaTag#getName()
	 */
	@Override
	public String getName() {
		try {
			return RPGFrameworkConstants.RES.getString("tag.age."+this.name().toLowerCase());
		} catch (MissingResourceException e) {
			System.err.println("Missing "+e.getKey()+" in "+RPGFrameworkConstants.RES.getBaseBundleName());
		}
		return "tag.age."+this.name().toLowerCase();
	}

	//-------------------------------------------------------------------
	public static String getTagTypeName() {
		try {
			return RPGFrameworkConstants.RES.getString("tag.age");
		} catch (MissingResourceException e) {
			System.err.println("Missing "+e.getKey()+" in "+RPGFrameworkConstants.RES.getBaseBundleName());
		}
		return "tag.age";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.Classification#getType()
	 */
	@Override
	public ClassificationType getType() {
		return GenericClassificationType.AGE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.Classification#getValue()
	 */
	@Override
	public TagAge getValue() {
		return this;
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @see de.rpgframework.classification.ClassificationType#getOptions(java.util.Locale)
//	 */
//	@Override
//	public List<ClassificationOption<TagAge>> getOptions(Locale loc) {
//		return List.of( TagAge.values() ).stream().map( g -> new ClassificationOption<TagAge>(g,g.getName(loc))).collect(Collectors.toList());
//	}

	
}
