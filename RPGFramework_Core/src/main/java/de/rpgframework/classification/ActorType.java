package de.rpgframework.classification;

public enum ActorType implements Classification<ActorType> {

	/** A sentient being */
	NPC,
	/** A creature, neither good nor bad */
	ANIMAL,
	/** A creature that clearly is a foe */
	MONSTER
	;

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.Classification#getType()
	 */
	@Override
	public ClassificationType getType() {
		return GenericClassificationType.ACTOR_TYPE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.Classification#getValue()
	 */
	@Override
	public ActorType getValue() {
		return this;
	}

}