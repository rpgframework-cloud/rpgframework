package de.rpgframework.classification;

public enum ActorRole implements Classification<ActorRole> {

	/**
	 * Someone who can help
	 */
	ALLY,
	/**
	 * The entity asking the party to work for him
	 */
	CLIENT,
	/**
	 * An armed opponent to make the run more interesting
	 */
	GRUNT,
	/**
	 * The entity to be acquired, transported, killed, protected ...
	 */
	TARGET,
	/**
	 * The nemesis, the evil to bue killed or overwhelmed
	 */
	VILLAIN,
	UNDEFINED
	;

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.Classification#getType()
	 */
	@Override
	public ClassificationType getType() {
		return GenericClassificationType.ACTOR_ROLE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.Classification#getValue()
	 */
	@Override
	public ActorRole getValue() {
		return this;
	}

}