/**
 * 
 */
package de.rpgframework.classification;

import java.util.MissingResourceException;

import de.rpgframework.RPGFrameworkConstants;

/**
 * @author prelle
 *
 */
public enum TagPlaces implements MediaTag {
	
	CASTLE,
	TOWER,
	MARKET,
	SHOP,
	BAR_TAVERN,
	ENCAMPMENT,
	HOTEL_INN,
	CLINIC_SPITAL,
	
	CEMETARY,
	CRYPT,
	CAVE,     // Natural
	MINE,     // Natural, expedient modified
	DUNGEON,  // Built, has artificial walls etc.
	SETTLEMENT,
	
	SUBWAY,
	TRAIN,
	PLANE,
	HANGAR,
	
	BOAT,
	SHIP,
	HARBOR,
	
	INDUSTRIAL,
	;

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.classification.MediaTag#getName()
	 */
	@Override
	public String getName() {
		try {
			return RPGFrameworkConstants.RES.getString("tag.places."+this.name().toLowerCase());
		} catch (MissingResourceException e) {
			System.err.println("Missing "+e.getKey()+" in "+RPGFrameworkConstants.RES.getBaseBundleName());
		}
		return "tag.places."+this.name().toLowerCase();
	}

	//-------------------------------------------------------------------
	public static String getTagTypeName() {
		try {
			return RPGFrameworkConstants.RES.getString("tag.places");
		} catch (MissingResourceException e) {
			System.err.println("Missing "+e.getKey()+" in "+RPGFrameworkConstants.RES.getBaseBundleName());
		}
		return "tag.places";
	}
	
}
