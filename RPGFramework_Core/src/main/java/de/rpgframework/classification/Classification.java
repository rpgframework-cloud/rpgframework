package de.rpgframework.classification;

/**
 * @author prelle
 *
 */
public interface Classification<T> {

	public ClassificationType getType();
	
	public T getValue(); 
	
}
