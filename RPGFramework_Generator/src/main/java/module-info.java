
module de.rpgframework.generator {
	exports  de.rpgframework.random;
	opens  de.rpgframework.random;
	opens  de.rpgframework.random.withoutnumber;
	opens  de.rpgframework.random.withoutnumber.cwn;
	provides de.rpgframework.random.GeneratorInitializer with de.rpgframework.random.GenericGeneratorInitializer;

	requires simple.persist;
	requires de.rpgframework.core;
	requires de.rpgframework.rules;

	uses de.rpgframework.random.GeneratorInitializer;

}