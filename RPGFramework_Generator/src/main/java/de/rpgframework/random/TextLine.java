package de.rpgframework.random;

import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.ElementList;

/**
 * @author prelle
 *
 */
public class TextLine {

	@ElementList(entry="key", type = String.class, inline=true)
	private List<String> i18nKeys;
	
	//-------------------------------------------------------------------
	public TextLine() {
		i18nKeys = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	public void addText(String key) {
		i18nKeys.add(key);
	}

	//-------------------------------------------------------------------
	public List<String> getKeys() {
		return i18nKeys;
	}

}
