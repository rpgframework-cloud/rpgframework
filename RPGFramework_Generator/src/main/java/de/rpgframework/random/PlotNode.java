package de.rpgframework.random;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.ElementList;

import de.rpgframework.classification.Classification;
import de.rpgframework.classification.ClassificationType;

/**
 * @author prelle
 *
 */
public class PlotNode extends VariableHolderNode {
		
	public enum Type {
		STORY,
		EXPOSITION,
		RISE,
		CRISIS,
		FALL,
		CONCLUSION,
		OPTIONAL,
		REWARD
	}

	@Attribute
	private Type type;
	@Attribute
	private String i18n;
	@ElementList(entry = "actor", type = Actor.class)
	private List<Actor> actors;
	private List<Object> locations;
	private PlotNode parent;
	@ElementList(entry = "section", type = PlotNode.class, inline=true)
	private List<PlotNode> children;

	//-------------------------------------------------------------------
	public PlotNode(Type type) {
		super();
		this.type = type;
		actors = new ArrayList<Actor>();
		children = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	public Type getType() {
		return type;
	}

	//-------------------------------------------------------------------
	public void addNode(PlotNode child) {
		child.setParent(this);
		children.add(child);
	}

	//-------------------------------------------------------------------
	public List<PlotNode> getChildNodes() {
		return children;
	}

	//-------------------------------------------------------------------
	public PlotNode getNode(Type type) {
		for (PlotNode tmp : children) {
			if (tmp.getType()==type)
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public PlotNode getParent() {
		return parent;
	}

	//-------------------------------------------------------------------
	/**
	 * @param parent the parent to set
	 */
	public void setParent(PlotNode parent) {
		this.parent = parent;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the i18n
	 */
	public String getI18n() {
		return i18n;
	}

	//-------------------------------------------------------------------
	/**
	 * @param i18n the i18n to set
	 */
	public void setI18n(String i18n) {
		this.i18n = i18n;
	}

	//-------------------------------------------------------------------
	public void addActor(Actor value) {
		actors.add(value);
	}

	//-------------------------------------------------------------------
	public List<Actor> getActors() {
		List<Actor> ret = new ArrayList<Actor>(actors);
		for (PlotNode child : children)
			ret.addAll(child.getActors());
		return ret;
	}
	
}
