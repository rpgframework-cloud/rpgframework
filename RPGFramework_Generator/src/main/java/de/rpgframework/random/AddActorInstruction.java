package de.rpgframework.random;

import org.prelle.simplepersist.Attribute;

import de.rpgframework.classification.ActorRole;

/**
 * @author prelle
 *
 */
public class AddActorInstruction {

	@Attribute(required=true)
	private ActorRole role;
	@Attribute(required=true)
	private String id;
	@Attribute
	private String variables;
	
	//-------------------------------------------------------------------
	public AddActorInstruction() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @return the role
	 */
	public ActorRole getRole() {
		return role;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the variables
	 */
	public String getVariables() {
		return variables;
	}

}
