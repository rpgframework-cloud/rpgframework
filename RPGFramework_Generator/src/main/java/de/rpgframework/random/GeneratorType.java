package de.rpgframework.random;

/**
 * @author prelle
 *
 */
public enum GeneratorType {

	NAME_PERSON,
	LOCATION(
		DataType.LOCATION_DESCRIPTION,
		DataType.BATTLEMAP
		),
	NSC(
		DataType.ACTOR_BASEDATA,
		DataType.ACTOR_PERSONALITY,
		DataType.ACTOR_VISUAL,
		DataType.RULEDATA
			),
	RUN
	;

	DataType[] possibleData;

	GeneratorType(DataType...poss) {
		possibleData = poss;
	}

}
