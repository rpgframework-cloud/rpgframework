package de.rpgframework.random;

import java.io.OutputStream;
import java.util.Locale;

/**
 * @author prelle
 *
 */
public interface ResultExport {

	public void export(Object toExport, OutputStream out, Locale loc);
	
}
