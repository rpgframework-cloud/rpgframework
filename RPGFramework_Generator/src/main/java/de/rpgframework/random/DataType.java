package de.rpgframework.random;

/**
 *
 */
public enum DataType {

	ACTOR_BASEDATA,
	ACTOR_PERSONALITY,
	ACTOR_VISUAL,
	BATTLEMAP,
	LOCATION_DESCRIPTION,
	LOCATION_ROOM_DESCRIPTION,
	PLOT_ABSTRACT,
	PLOT_DETAIL,
	RULEDATA,

}
