package de.rpgframework.random;

import java.util.Collection;
import java.util.List;

import de.rpgframework.classification.Classification;
import de.rpgframework.classification.ClassificationType;

/**
 * @author prelle
 *
 */
public interface RandomGenerator {

	static RandomGeneratorBuilder builder() {
		return new RandomGeneratorBuilder();
	}

	//-------------------------------------------------------------------
	public static boolean contains(List<Classification<?>> list, ClassificationType type) {
		return list.stream().anyMatch(e -> e.getType()==type);
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	public static <T> T getValueOf(List<Classification<?>> list, ClassificationType type) {
		for (Classification<?> cls : list) {
			if (cls.getType()==type)
				return (T)cls.getValue();
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * Short and unique identifier for this generator
	 */
	public String getId();

	//-------------------------------------------------------------------
	public GeneratorType getType();

	//-------------------------------------------------------------------
	public Collection<ClassificationType> getRequiredVariables();

	//-------------------------------------------------------------------
	/**
	 * What kind of data is provided
	 */
	public Collection<DataType> getProvidedData();

	//-------------------------------------------------------------------
	public boolean matchesFilter(Classification<?> filter);

	//-------------------------------------------------------------------
	public boolean understandsHint(ClassificationType filter);

//	//-------------------------------------------------------------------
//	public default RandomGenerator withHints(Classification<?>...hints) {
//		return withHints(List.of(hints));
//	}
//	public RandomGenerator withHints(List<Classification<?>> hints);
//	public default RandomGenerator withHint(ClassificationType type, String value) {
//		return withHints(new DynamicClassification(type, value));
//	}
//
//	//-------------------------------------------------------------------
//	public RandomGenerator withVariables(Map<GeneratorVariable,Integer> variables);
//
//	//-------------------------------------------------------------------
//	/**
//	 * @param classifier
//	 * @return Generated objected
//	 */
//	public Object generate();

	public Object generate(VariableHolderNode context);

}
