package de.rpgframework.random;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import de.rpgframework.classification.ActorRole;
import de.rpgframework.classification.Classification;
import de.rpgframework.classification.ClassificationType;
import de.rpgframework.classification.Gender;
import de.rpgframework.classification.GenericClassificationType;
import de.rpgframework.classification.Genre;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.world.WorldClassificationType;

/**
 * @author prelle
 *
 */
public class NSCGenerator implements RandomGenerator {

	private final static Logger logger = System.getLogger("rpgframework.random");

	public final static Random RANDOM = new Random();

	//-------------------------------------------------------------------
	public NSCGenerator() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#getId()
	 */
	@Override
	public String getId() {
		return "GenericNSC";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#getType()
	 */
	@Override
	public GeneratorType getType() {
		return GeneratorType.NSC;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#matchesFilter(de.rpgframework.classification.Classification)
	 */
	@Override
	public boolean matchesFilter(Classification<?> filter) {
		// TODO Auto-generated method stub
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#understandsHint(de.rpgframework.classification.Classification)
	 */
	@Override
	public boolean understandsHint(ClassificationType filter) {
		if (filter==GenericClassificationType.ACTOR_ROLE) return true;
		return false;
	}

	//-------------------------------------------------------------------
	protected Classification<?> getHint(ClassificationType needle, Collection<Classification<?>> haystack) {
		for (Classification<?> tmp : haystack) {
			if (tmp.getType()==needle)
				return tmp;
		}
		return null;

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#generate()
	 */
	@Override
	public Object generate(VariableHolderNode context) {
		logger.log(Level.INFO, "ENTER: createNSC("+context.getHints()+")");

		VariableHolderNode newContext = new VariableHolderNode(context);
		/*
		 * Roll gender
		 */
		Gender gender = null;
		Classification<?> cls = getHint(GenericClassificationType.GENDER, newContext.getHints());
		if (cls!=null) {
			gender = Gender.valueOf( (String)cls.getValue());
		} else {
			gender = switch(RANDOM.nextInt(20)) {
		    case  1, 2, 3, 4, 5, 6, 7, 8, 9 -> Gender.MALE;
		    case 11,12,13,14,15,16,17,18,19 -> Gender.FEMALE;
		    default -> Gender.DIVERSE;
			};
			newContext.setHint(gender);
		}
		logger.log(Level.INFO, "Gender is {0}", gender);

		/*
		 * Roll a name
		 */
		logger.log(Level.DEBUG, "Call NAME_PERSON generator");
		RandomGenerator generator = RandomGenerator.builder()
				.thatGenerates(GeneratorType.NAME_PERSON)
				.optionallySupportsHints(newContext.getHints().stream().map(h -> h.getType()).toList())
				.build();
		String name = (String) generator.generate(newContext);
//		String name = (String) RandomGeneratorRegistry.generate(GeneratorType.NAME_PERSON, List.of(), hints, variables);

		/*
		 * Determine roll or take it from hints
		 */
		ActorRole role = ActorRole.UNDEFINED;
		if (RandomGenerator.contains(newContext.getHints(), GenericClassificationType.ACTOR_ROLE)) {
			role = RandomGenerator.getValueOf(newContext.getHints(), GenericClassificationType.ACTOR_ROLE);
		}

		// Create an actor
		Actor actor = new Actor(role, name);
		actor.copyHints(newContext);
		actor.setGender(gender);
		for (Classification<?> cls2 : context.getHints()) {
			if (cls2.getType().isRuleSpecific()) {
				actor.setHint(cls2);
//				if (cls.getValue() instanceof Enum) {
//					logger.log(Level.WARNING, "as enum: "+ ((Enum)cls.getValue()).name());
//					actor.setGenericVariable(cls.getType().getId(), ((Enum)cls.getValue()).name());
//				} else {
//					logger.log(Level.WARNING, "as String: "+ cls.getValue().toString());
//					actor.setGenericVariable(cls.getType().getId(), cls.getValue().toString());
//				}
			}
		}

		logger.log(Level.INFO, "LEAVE createNSC()="+actor);
		return actor;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#getRequiredVariables()
	 */
	@Override
	public Collection<ClassificationType> getRequiredVariables() {
		return List.of();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#getProvidedData()
	 */
	@Override
	public Collection<DataType> getProvidedData() {
		return List.of(DataType.ACTOR_BASEDATA);
	}

}
