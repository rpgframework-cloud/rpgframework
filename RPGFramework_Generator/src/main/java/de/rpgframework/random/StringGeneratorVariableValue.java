package de.rpgframework.random;

import java.util.Locale;

import org.prelle.simplepersist.Attribute;

/**
 *
 */
public class StringGeneratorVariableValue implements GeneratorVariableValue {

	@Attribute(name="gen")
	private String generator;
	private GeneratorVariable type;
	private String value;

	//-------------------------------------------------------------------
	public StringGeneratorVariableValue(RandomGenerator generator, GeneratorVariable type, String value) {
		if (generator!=null)
			this.generator = generator.getId();
		this.type = type;
		this.value = value;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.GeneratorVariableValue#getGeneratorId()
	 */
	@Override
	public String getGeneratorId() {
		return generator;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.GeneratorVariableValue#getVariable()
	 */
	@Override
	public GeneratorVariable getVariable() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.GeneratorVariableValue#toXMLIdentifier()
	 */
	@Override
	public String toXMLIdentifier() {
		return value;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.GeneratorVariableValue#toString(java.util.Locale)
	 */
	@Override
	public String toString(Locale loc) {
		System.err.println("TODO: StringGeneratorVariable.toString(loc) for "+type.name()+" = "+value);
		return value;
	}

	//-------------------------------------------------------------------
	@Override
	public String toString() {
		System.err.println("TODO: StringGeneratorVariable.toString() for "+type.name()+" = "+value);
		return value;
	}

	//-------------------------------------------------------------------
	/**
	 * @param generator the generator to set
	 */
	public void setGenerator(RandomGenerator generator) {
		this.generator = generator.getId();
	}

}
