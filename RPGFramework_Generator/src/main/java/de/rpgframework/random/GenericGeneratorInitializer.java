package de.rpgframework.random;

import java.util.List;
import java.util.Locale;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.classification.Genre;
import de.rpgframework.random.withoutnumber.SWNNPCGenerator;
import de.rpgframework.random.withoutnumber.StoryTagGenerator;

/**
 *
 */
public class GenericGeneratorInitializer implements GeneratorInitializer {

	private static List<RandomGenerator> generators;

	//-------------------------------------------------------------------
	public GenericGeneratorInitializer() {
		generators = List.of(
			new NSCGenerator(),
			new SWNNPCGenerator(new MultiLanguageResourceBundle("de.rpgframework.random.withoutnumber.StarsWithoutNumbers", Locale.ENGLISH), "swn", Genre.CYBERPUNK),
			new StoryTagGenerator( new MultiLanguageResourceBundle("de.rpgframework.random.withoutnumber.StarsWithoutNumbers", Locale.ENGLISH), "swn_mission", Genre.CYBERPUNK)
			);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.GeneratorInitializer#getGeneratorsToRegister()
	 */
	@Override
	public List<RandomGenerator> getGeneratorsToRegister() {
		return generators;
	}

}
