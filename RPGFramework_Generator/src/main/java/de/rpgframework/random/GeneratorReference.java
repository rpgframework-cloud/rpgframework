package de.rpgframework.random;

import de.rpgframework.genericrpg.data.CommonCharacter;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModifiedObjectType;

/**
 *
 */
public enum GeneratorReference implements ModifiedObjectType {

	VARIABLE,
	;

	@Override
	public <T> T resolve(String key) {
		return (T)GeneratorReference.valueOf(key);
	}

	@Override
	public <T extends DataItem> T resolveAsDataItem(String key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T[] resolveAny() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T[] resolveVariable(String varName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Modification instantiateModification(Modification tmp, ComplexDataItemValue<?> complexDataItemValue,
			int multiplier, CommonCharacter<?, ?, ?, ?> model) {
		// TODO Auto-generated method stub
		return null;
	}

}
