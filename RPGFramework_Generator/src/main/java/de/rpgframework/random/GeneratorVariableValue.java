package de.rpgframework.random;

import java.util.Locale;

/**
 *
 */
public interface GeneratorVariableValue {

	//-------------------------------------------------------------------
	/**
	 * Return the identifier of the generator who created this
	 */
	public String getGeneratorId();

	//-------------------------------------------------------------------
	public GeneratorVariable getVariable();

	//-------------------------------------------------------------------
	/**
	 * Convert to an identifier suitable for storing in an XML file
	 */
	public String toXMLIdentifier();

	//-------------------------------------------------------------------
	/**
	 * Convert this into a human readable text description.
	 */
	public String toString(Locale loc);

}
