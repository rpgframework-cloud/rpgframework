package de.rpgframework.random;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementList;

import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;
import de.rpgframework.genericrpg.modification.ModificationList;

/**
 * @author prelle
 *
 */
@DataItemTypeKey(id="rollResult")
public class RollResult extends ComplexDataItem {

	@Attribute(required=false)
	private String value;
	@Attribute
	protected String nextTable;

	@Element
	private AddActorInstruction addActor;
	@ElementList(entry="rollTable",type=RollTable.class,inline=false)
	private RollTableList subtables;

	//-------------------------------------------------------------------
	public RollResult() {
		modifications = new ModificationList();
		subtables = new RollTableList();
	}

	//-------------------------------------------------------------------
	public boolean isInRange(int num) {
		if (value.contains("-")) {
			String[] pair = value.split("-");
			int min = Integer.parseInt(pair[0].trim());
			int max = Integer.parseInt(pair[1].trim());
			return min<=num && num<=max;
		}
		return num==Integer.parseInt(value.trim());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the value
	 */
	public String toString() {
		return super.id+"("+value+", subtables="+subtables+")";
	}

	//-------------------------------------------------------------------
	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the nextTable
	 */
	public String getNextTable() {
		return nextTable;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the subtables
	 */
	public RollTableList getSubtables() {
		return subtables;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the addActor
	 */
	public AddActorInstruction getAddActor() {
		return addActor;
	}

}
