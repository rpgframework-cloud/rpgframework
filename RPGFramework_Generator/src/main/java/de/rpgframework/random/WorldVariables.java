package de.rpgframework.random;

/**
 *
 */
public enum WorldVariables implements GeneratorVariable {

	REGION,
	FACTION(false),
	;

	boolean integer;

	//-------------------------------------------------------------------
	WorldVariables() {
	}

	//-------------------------------------------------------------------
	WorldVariables(boolean integer) {
		this.integer = integer;
	}

	@Override
	public boolean isInteger() {
		return integer;
	}

}
