package de.rpgframework.random.mine;

import java.util.List;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.ElementList;

import de.rpgframework.classification.Difficulty;

/**
 * 
 */
public class Story {
	
	private static class AspectedDifficulty {
		@Attribute(required=true)
		private StoryFocus aspect;
		@Attribute(required=true, name="diff")
		private Difficulty difficulty;
	}
	
	/**
	 * Declare relevant aspects of the story and assign a difficulty from 3-8 to it
	 */
	@ElementList(entry = "difficulty", type = AspectedDifficulty.class, inline=true)
	private List<Difficulty> difficuly;

	//-------------------------------------------------------------------
	/**
	 */
	public Story() {
		// TODO Auto-generated constructor stub
	}

}
