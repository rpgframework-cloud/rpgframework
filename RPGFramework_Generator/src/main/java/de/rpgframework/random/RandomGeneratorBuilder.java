package de.rpgframework.random;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;

import de.rpgframework.classification.Classification;
import de.rpgframework.classification.ClassificationType;
import de.rpgframework.classification.GenericClassificationType;
import de.rpgframework.classification.Genre;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.world.World;

/**
 *
 */
public class RandomGeneratorBuilder {

	private final static Logger logger = System.getLogger(RandomGeneratorBuilder.class.getPackageName());

	private GeneratorType type;
	private List<Classification<?>> requirements;
	private World world;
	private RoleplayingSystem rules;
	private List<ClassificationType> optionalSupport;

	//-------------------------------------------------------------------
	public RandomGeneratorBuilder() {
		optionalSupport = new ArrayList<>();
		requirements = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	public RandomGeneratorBuilder thatGenerates(GeneratorType type) {
		this.type = type;
		return this;
	}

	//-------------------------------------------------------------------
	public RandomGeneratorBuilder thatSupports(Classification<?>... requires) {
		this.requirements = List.of(requires);
		return this;
	}

	//-------------------------------------------------------------------
	public RandomGeneratorBuilder forSetting(World world) {
		this.world = world;
		return this;
	}

	//-------------------------------------------------------------------
	public RandomGeneratorBuilder forRules(RoleplayingSystem rules) {
		this.rules = rules;
		optionalSupport.add(GenericClassificationType.GENRE);
		return this;
	}

	//-------------------------------------------------------------------
	public RandomGeneratorBuilder optionallySupportsHints(ClassificationType... hints) {
		for (ClassificationType hint : hints) {
			if (!optionalSupport.contains(hint))
				this.optionalSupport.add(hint);
		}
		return this;
	}

	//-------------------------------------------------------------------
	public RandomGeneratorBuilder optionallySupportsHints(List<ClassificationType> hints) {
		for (ClassificationType hint : hints) {
			if (!optionalSupport.contains(hint))
				this.optionalSupport.add(hint);
		}
		return this;
	}

	//-------------------------------------------------------------------
	public RandomGenerator build() {
		RandomGenerator generator = RandomGeneratorRegistry.findGenerator(
				type,
				requirements,
				optionalSupport);
		return generator;
	}

	//-------------------------------------------------------------------
	public List<RandomGenerator> buildChain() {
		List<RandomGenerator> generator = RandomGeneratorRegistry.findGenerators(
				type,
				requirements,
				optionalSupport);
		List<RandomGenerator> filtered = new ArrayList<>();
		List<DataType> covered = new ArrayList<>();

		generator.forEach( gen -> {
			if (gen.getProvidedData().stream().noneMatch(type -> covered.contains(type))) {
				filtered.add(gen);
				logger.log(Level.DEBUG, "Generator {0} has only data types not covered yet: {1}", gen.getClass().getSimpleName(), gen.getProvidedData());
				covered.addAll(gen.getProvidedData());
			}
		});

		return filtered;
	}
}
