package de.rpgframework.random.withoutnumber;

import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.ElementList;

import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;

@DataItemTypeKey(id="enemy")
public class Enemy extends ComplexDataItem {

	@ElementList(entry="var",type=VariableDefinition.class,inline=true)
	private List<VariableDefinition> variables = new ArrayList<>();

}
