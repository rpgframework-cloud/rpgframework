package de.rpgframework.random.withoutnumber;

import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.ElementList;

import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;

/**
 *
 */
@DataItemTypeKey(id="tag")
public class StoryTag extends ComplexDataItem {

	@ElementList(entry="enemy",type=StoryTagElement.class)
	private List<StoryTagElement> enemies;
	@ElementList(entry="friend",type=StoryTagElement.class)
	private List<StoryTagElement> friends;
	@ElementList(entry="complication",type=StoryTagElement.class)
	private List<StoryTagElement> complications;
	@ElementList(entry="thing",type=StoryTagElement.class)
	private List<StoryTagElement> things;
	@ElementList(entry="place",type=StoryTagElement.class)
	private List<StoryTagElement> places;

	//-------------------------------------------------------------------
	public StoryTag() {
		enemies = new ArrayList<>();
		friends = new ArrayList<>();
		complications = new ArrayList<>();
		things = new ArrayList<>();
		places = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	public List<StoryTagElement> getEnemies() {
		return enemies;
	}

	//-------------------------------------------------------------------
	public List<StoryTagElement> getFriends() {
		return friends;
	}

	//-------------------------------------------------------------------
	public List<StoryTagElement> getComplications() {
		return complications;
	}

	//-------------------------------------------------------------------
	public List<StoryTagElement> getThings() {
		return things;
	}

	//-------------------------------------------------------------------
	public List<StoryTagElement> getPlaces() {
		return places;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.ComplexDataItem#validate()
	 */
	@Override
	public void validate() {
		super.validate();
		enemies.forEach(tmp -> {tmp.setParentItem(this); tmp.validate();});
		friends.forEach(tmp -> {tmp.setParentItem(this); tmp.validate();});
		complications.forEach(tmp -> {tmp.setParentItem(this); tmp.validate();});
		things.forEach(tmp -> {tmp.setParentItem(this); tmp.validate();});
		places.forEach(tmp -> {tmp.setParentItem(this); tmp.validate();});
	}

}
