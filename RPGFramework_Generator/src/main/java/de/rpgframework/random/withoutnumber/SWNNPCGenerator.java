package de.rpgframework.random.withoutnumber;

import java.io.InputStream;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.classification.Classification;
import de.rpgframework.classification.ClassificationType;
import de.rpgframework.classification.GenericClassificationType;
import de.rpgframework.classification.Genre;
import de.rpgframework.random.Actor;
import de.rpgframework.random.DataType;
import de.rpgframework.random.GeneratorType;
import de.rpgframework.random.GeneratorVariable;
import de.rpgframework.random.RandomGenerator;
import de.rpgframework.random.RollTable;
import de.rpgframework.random.RollTableGenerator;
import de.rpgframework.random.VariableHolderNode;

/**
 *
 */
public class SWNNPCGenerator extends RollTableGenerator implements RandomGenerator {

	private final static Logger logger = System.getLogger(SWNNPCGenerator.class.getPackageName());

	//-------------------------------------------------------------------
	/**
	 * @param res
	 */
	public SWNNPCGenerator(MultiLanguageResourceBundle res, String prefix, Genre genre) {
		super(res);
		String key = prefix+"_npc_tables.xml";
		logger.log(Level.INFO, "Try load "+key);
		InputStream ins = getClass().getResourceAsStream(key);
		super.loadTables(ins);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#getId()
	 */
	@Override
	public String getId() {
		return "SWNNSC";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#getType()
	 */
	@Override
	public GeneratorType getType() {
		return GeneratorType.NSC;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#getRequiredVariables()
	 */
	@Override
	public Collection<ClassificationType> getRequiredVariables() {
		return List.of(GenericClassificationType.GENRE);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#matchesFilter(de.rpgframework.classification.Classification)
	 */
	@Override
	public boolean matchesFilter(Classification<?> filter) {
		if (filter==Genre.CYBERPUNK) return true;
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#understandsHint(de.rpgframework.classification.Classification)
	 */
	@Override
	public boolean understandsHint(ClassificationType filter) {
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#generate()
	 */
	@Override
	public Object generate(VariableHolderNode context) {
		logger.log(Level.INFO, "ENTER: generate()");
		logger.log(Level.INFO, "variables = "+context.getVariables());
		logger.log(Level.INFO, "classifier = "+context.getHints());

		Actor actor = new Actor();
		actor.copyHints(context);
		for (Classification<?> tmp : context.getHints()) actor.setHint(tmp);
		try {
			RollTable table = tables.get(WithoutNumberVariable.NPC_STRENGTH.name());
			logger.log(Level.DEBUG,"Roll with classifier "+actor.getHints());
			super.simpleRoll(table, actor, Locale.getDefault());

			return actor;
		} finally {
			logger.log(Level.DEBUG, "LEAVE: generate");
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RollTableGenerator#resolveModifier(java.lang.String)
	 */
	@Override
	protected GeneratorVariable resolveModifier(String name) {
		return WithoutNumberVariable.valueOf(name);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#getProvidedData()
	 */
	@Override
	public Collection<DataType> getProvidedData() {
		return List.of(DataType.ACTOR_PERSONALITY);
	}

}
