package de.rpgframework.random.withoutnumber;

import de.rpgframework.random.GeneratorVariable;

/**
 *
 */
public enum WithoutNumberVariable implements GeneratorVariable{

	NPC_STRENGTH,
	NPC_VIRTUE,
	NPC_FLAW,
	NPC_PROBLEM,
	NPC_DESIRE,
	MISSION_TAG(false),
	ENEMY(false),
	;

	boolean integer;

	WithoutNumberVariable() {
	}

	WithoutNumberVariable(boolean integer) {
		this.integer = integer;
	}

	@Override
	public boolean isInteger() {
		return integer;
	}

}
