package de.rpgframework.random.withoutnumber;

import java.io.IOException;
import java.io.InputStream;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.SerializationException;

import de.rpgframework.MultiLanguageResourceBundle;
import de.rpgframework.classification.ActorRole;
import de.rpgframework.classification.Classification;
import de.rpgframework.classification.ClassificationType;
import de.rpgframework.classification.GenericClassificationType;
import de.rpgframework.classification.Genre;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.genericrpg.data.DataSet;
import de.rpgframework.genericrpg.data.GenericCore;
import de.rpgframework.random.Actor;
import de.rpgframework.random.DataType;
import de.rpgframework.random.GeneratorType;
import de.rpgframework.random.GeneratorVariable;
import de.rpgframework.random.Plot;
import de.rpgframework.random.RandomGenerator;
import de.rpgframework.random.RollTable;
import de.rpgframework.random.RollTableGenerator;
import de.rpgframework.random.VariableHolderNode;

/**
 *
 */
public class StoryTagGenerator extends RollTableGenerator implements RandomGenerator {

	private final static Logger logger = System.getLogger(StoryTagGenerator.class.getPackageName());

	private List<Classification<?>> classifications = new ArrayList<>();

	private GenericCore core;

	//-------------------------------------------------------------------
	/**
	 * @param genre
	 * @param res
	 */
	public StoryTagGenerator(MultiLanguageResourceBundle RES, String prefix, Genre genre) {
		super(RES);
		classifications.add(genre);

		// Load data
		logger.log(Level.INFO, "START -------------------------------CWN-----------------------------------------------");
		DataSet core = new DataSet(this, RoleplayingSystem.ALL, "cwn", "cwn", Locale.ENGLISH);
//		PluginSkeleton CORE = new PluginSkeleton("CORE", "Splittermond Core Rules");
		Class<StoryTagGenerator> clazz = StoryTagGenerator.class;
		List<StoryTag> list  = null;
		try {
			list = GenericCore.loadDataItems(StoryTagList.class, StoryTag.class, core, clazz.getResourceAsStream("cwn_mission_tags.xml"));
			logger.log(Level.DEBUG, "Loaded {0} missions tags", list.size());
		} catch (Exception e) {
			logger.log(Level.ERROR, "Failed loading missions tags",e);
		}


		String key = prefix+"_tags_table.xml";
		logger.log(Level.INFO, "Try load "+key);
		InputStream ins = getClass().getResourceAsStream(key);
		super.loadTables(ins);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#getId()
	 */
	@Override
	public String getId() {
		return "SWNMission";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#getType()
	 */
	@Override
	public GeneratorType getType() {
		return GeneratorType.RUN;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#getRequiredVariables()
	 */
	@Override
	public Collection<ClassificationType> getRequiredVariables() {
		return List.of(GenericClassificationType.GENRE);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#matchesFilter(de.rpgframework.classification.Classification)
	 */
	@Override
	public boolean matchesFilter(Classification<?> filter) {
		for (Classification<?> cls : classifications) {
			if (cls.getType()==filter.getType()) {
				return cls.getValue()==filter.getValue();
			}
		}
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#understandsHint(de.rpgframework.classification.Classification)
	 */
	@Override
	public boolean understandsHint(ClassificationType filter) {
		// TODO Auto-generated method stub
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#generate()
	 */
	@Override
	public Object generate(VariableHolderNode context) {
		logger.log(Level.INFO, "ENTER: generate({0})", context);
		Plot plot = new Plot();
		// Copy global data
		for (Classification<?> tmp : context.getHints()) plot.setHint(tmp);
		try {
			RollTable table = tables.get(WithoutNumberVariable.MISSION_TAG.name());
			logger.log(Level.DEBUG,"Roll with classifier "+plot.getHints());
			super.simpleRoll(table, plot, Locale.getDefault());

			Object foo = plot.getGenericVariable(WithoutNumberVariable.MISSION_TAG);
			System.err.println("Generatated misson tag =" +foo);
			List<String> list = (List<String>) plot.getGenericVariable(WithoutNumberVariable.MISSION_TAG);
			List<String> enemyList = (List<String>) plot.getGenericVariable(WithoutNumberVariable.ENEMY);
			if (list.size()>0) {
				StoryTag tag = core.getItem(StoryTag.class, list.get(0));
				if (tag==null) logger.log(Level.ERROR, "Unknown tag {0}", list.get(0));
				StoryTagValue tagValue = new StoryTagValue(tag);
				plot.setTag1(tagValue);

				// Roll all options
				if (!tag.getEnemies().isEmpty()) {
					StoryTagElement enemy = tag.getEnemies().get( random.nextInt( tag.getEnemies().size() ) );
					tagValue.setEnemy ( enemy );
					VariableHolderNode context2 = new VariableHolderNode(plot);
					context2.setHint(ActorRole.VILLAIN);
					List<RandomGenerator> generators = RandomGenerator.builder()
							.thatGenerates(GeneratorType.NSC)
							.forRules(RoleplayingSystem.SHADOWRUN6)
							.buildChain();
					Actor actor = null;
					logger.log(Level.WARNING, "Creating the villain returns {0} generators", generators.size());
					for (RandomGenerator generator : generators) {
						if (actor==null) {
							actor = (Actor)generator.generate(context2);
						} else {
							Actor tmpActor = (Actor) generator.generate(context2);
							logger.log(Level.DEBUG, "Added gen.vars  {0}", tmpActor.getGenericVariables());
							actor.copyVariables(tmpActor);
						}
					}
					plot.addActor(actor);
				}
				if (!tag.getFriends().isEmpty()) {
					StoryTagElement friend = tag.getEnemies().get( random.nextInt( tag.getFriends().size() ) );
					tagValue.setFriend( friend.getId() );
					VariableHolderNode context2 = new VariableHolderNode(plot);
					context2.setHint(ActorRole.CLIENT);
					RandomGenerator generator = RandomGenerator.builder()
							.thatGenerates(GeneratorType.NSC)
							.forRules(RoleplayingSystem.SHADOWRUN6)
							.build();
					Actor actor = (Actor)generator.generate(context2);
					plot.addActor(actor);
				}
				if (!tag.getComplications().isEmpty())
					tagValue.setComplication( tag.getComplications().get( random.nextInt( tag.getComplications().size() ) ).getId() );
				if (!tag.getThings().isEmpty())
					tagValue.setThing ( tag.getThings().get( random.nextInt( tag.getThings().size() ) ).getId() );
				if (!tag.getPlaces().isEmpty())
					tagValue.setPlace ( tag.getPlaces().get( random.nextInt( tag.getPlaces().size() ) ).getId() );
			}
			if (list.size()>1) {
				StoryTag tag = core.getItem(StoryTag.class, list.get(1));
				StoryTagValue tagValue = new StoryTagValue(tag);
				plot.setTag2(tagValue);

				// Roll all options
				if (!tag.getEnemies().isEmpty())
					tagValue.setEnemy ( tag.getEnemies().get( random.nextInt( tag.getEnemies().size() ) ) );
				if (!tag.getFriends().isEmpty())
					tagValue.setFriend( tag.getFriends().get( random.nextInt( tag.getFriends().size() ) ).getId() );
				if (!tag.getComplications().isEmpty())
					tagValue.setComplication( tag.getComplications().get( random.nextInt( tag.getComplications().size() ) ).getId() );
				if (!tag.getThings().isEmpty())
					tagValue.setThing ( tag.getThings().get( random.nextInt( tag.getThings().size() ) ).getId() );
				if (!tag.getPlaces().isEmpty())
					tagValue.setPlace ( tag.getPlaces().get( random.nextInt( tag.getPlaces().size() ) ).getId() );
			}

			try {
				(new Persister()).write(plot, System.out);
			} catch (SerializationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return plot;
		} finally {
			logger.log(Level.DEBUG, "LEAVE: generate");
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RollTableGenerator#resolveModifier(java.lang.String)
	 */
	@Override
	protected GeneratorVariable resolveModifier(String name) {
		logger.log(Level.TRACE, "resolve {0}", name);
		return WithoutNumberVariable.valueOf(name);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.random.RandomGenerator#getProvidedData()
	 */
	@Override
	public Collection<DataType> getProvidedData() {
		return List.of(DataType.PLOT_ABSTRACT);
	}

}
