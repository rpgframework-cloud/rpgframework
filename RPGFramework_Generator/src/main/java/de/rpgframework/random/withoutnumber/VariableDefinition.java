package de.rpgframework.random.withoutnumber;

import org.prelle.simplepersist.Attribute;

/**
 *
 */
public class VariableDefinition {

	@Attribute
	private String name;
	@Attribute
	private String value;

	//-------------------------------------------------------------------
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	//-------------------------------------------------------------------
	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

}
