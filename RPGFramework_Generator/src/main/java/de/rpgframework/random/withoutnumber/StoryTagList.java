package de.rpgframework.random.withoutnumber;

import java.util.ArrayList;
import java.util.Collection;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 *
 */
@Root(name="tags")
@ElementList(entry="tag",type=StoryTag.class,inline=true)
public class StoryTagList extends ArrayList<StoryTag> {

	private static final long serialVersionUID = 5924935289849377578L;

	//-------------------------------------------------------------------
	public StoryTagList() {
	}

	//-------------------------------------------------------------------
	public StoryTagList(Collection<? extends StoryTag> c) {
		super(c);
	}

}
