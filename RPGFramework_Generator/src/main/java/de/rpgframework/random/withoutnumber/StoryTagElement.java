package de.rpgframework.random.withoutnumber;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.ElementList;

import de.rpgframework.classification.ClassificationType;
import de.rpgframework.classification.Taxonomy;
import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;

@DataItemTypeKey(id="tagelement")
public class StoryTagElement extends ComplexDataItem {

	private final static Logger logger = System.getLogger(StoryTagElement.class.getPackageName());

	@ElementList(entry="var",type=VariableDefinition.class,inline=true)
	private List<VariableDefinition> variables = new ArrayList<>();

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.data.ComplexDataItem#validate()
	 */
	@Override
	public void validate() {
		getName();
		for (VariableDefinition def : variables) {
			ClassificationType type = Taxonomy.getGeneric().getClassificationTypeFor(def.getName());
			if (type==null) {
				logger.log(Level.WARNING, "Unknown variable type ''{0}'' in {1}", def.getName(), this);
				continue;
			}
			Object valid = type.resolve(def.getValue());
			if (valid==null) {
				logger.log(Level.WARNING, "Unknown value ''{0}'' for variable ''{1}'' in {2}", def.getValue(), def.getName(), this);
			}
		}
	}

	//-------------------------------------------------------------------
	public List<VariableDefinition> getVariables() {
		return variables;
	}
}
