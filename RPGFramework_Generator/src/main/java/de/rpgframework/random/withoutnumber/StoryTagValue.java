package de.rpgframework.random.withoutnumber;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Optional;

import org.prelle.simplepersist.Element;

import de.rpgframework.genericrpg.data.ComplexDataItemValue;

/**
 *
 */
public class StoryTagValue extends ComplexDataItemValue<StoryTag> {

	private final static Logger logger = System.getLogger(StoryTagGenerator.class.getPackageName());

	@Element
	private String enemy;
	@Element
	private String friend;
	@Element
	private String complication;
	@Element
	private String thing;
	@Element
	private String place;

	//-------------------------------------------------------------------
	public StoryTagValue() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @param data
	 */
	public StoryTagValue(StoryTag data) {
		super(data);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the enemy
	 */
	public StoryTagElement getEnemy() {
		Optional<StoryTagElement> opt = resolved.getEnemies().stream().filter(tmp -> tmp.getId().equals(enemy)).findFirst();
		if (opt.isPresent()) {
			StoryTagElement ret = opt.get();
			ret.setParentItem(resolved);
			return opt.get();
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @param enemy the enemy to set
	 */
	public void setEnemy(StoryTagElement enemy) {
		logger.log(Level.DEBUG, "Set enemy of "+getKey()+" to "+enemy);
		if (enemy.getVariables()!=null && !enemy.getVariables().isEmpty()) {
			logger.log(Level.WARNING, "TODO: Set variables: "+enemy.getVariables());
		}
		logger.log(Level.DEBUG, "Set enemy of "+getKey()+" to "+enemy);

		this.enemy = enemy.getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the friend
	 */
	public StoryTagElement getFriend() {
		Optional<StoryTagElement> opt = resolved.getFriends().stream().filter(tmp -> tmp.getId().equals(friend)).findFirst();
		if (opt.isPresent()) {
			StoryTagElement ret = opt.get();
			ret.setParentItem(resolved);
			return opt.get();
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @param friend the friend to set
	 */
	public void setFriend(String friend) {
		logger.log(Level.DEBUG, "Set friend of "+getKey()+" to "+friend);
		this.friend = friend;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the complication
	 */
	public StoryTagElement getComplication() {
		Optional<StoryTagElement> opt = resolved.getComplications().stream().filter(tmp -> tmp.getId().equals(complication)).findFirst();
		if (opt.isPresent()) {
			StoryTagElement ret = opt.get();
			ret.setParentItem(resolved);
			return opt.get();
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @param complication the complication to set
	 */
	public void setComplication(String complication) {
		logger.log(Level.DEBUG, "Set complication of "+getKey()+" to "+complication);
		this.complication = complication;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the thing
	 */
	public StoryTagElement getThing() {
		Optional<StoryTagElement> opt = resolved.getThings().stream().filter(tmp -> tmp.getId().equals(thing)).findFirst();
		if (opt.isPresent()) {
			StoryTagElement ret = opt.get();
			ret.setParentItem(resolved);
			return opt.get();
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @param thing the thing to set
	 */
	public void setThing(String thing) {
		logger.log(Level.DEBUG, "Set thing of "+getKey()+" to "+thing);
		this.thing = thing;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the place
	 */
	public StoryTagElement getPlace() {
		Optional<StoryTagElement> opt = resolved.getPlaces().stream().filter(tmp -> tmp.getId().equals(place)).findFirst();
		if (opt.isPresent()) {
			StoryTagElement ret = opt.get();
			ret.setParentItem(resolved);
			return opt.get();
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @param place the place to set
	 */
	public void setPlace(String place) {
		logger.log(Level.DEBUG, "Set place of "+getKey()+" to "+place);
		this.place = place;
	}

}
