package de.rpgframework.random;

import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.Root;

import de.rpgframework.random.withoutnumber.StoryTagValue;

/**
 * @author prelle
 *
 */
@Root(name = "plot")
public class Plot extends PlotNode {

	@Element
	private StoryTagValue tag1;
	@Element
	private StoryTagValue tag2;

	//-------------------------------------------------------------------
	public Plot() {
		super(Type.STORY);
	}

	//-------------------------------------------------------------------
	public String toString() {
		return getGenericVariables().toString();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the tag1
	 */
	public StoryTagValue getTag1() {
		return tag1;
	}

	//-------------------------------------------------------------------
	/**
	 * @param tag1 the tag1 to set
	 */
	public void setTag1(StoryTagValue tag1) {
		this.tag1 = tag1;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the tag2
	 */
	public StoryTagValue getTag2() {
		return tag2;
	}

	//-------------------------------------------------------------------
	/**
	 * @param tag2 the tag2 to set
	 */
	public void setTag2(StoryTagValue tag2) {
		this.tag2 = tag2;
	}

}
