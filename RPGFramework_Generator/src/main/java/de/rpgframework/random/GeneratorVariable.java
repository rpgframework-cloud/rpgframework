package de.rpgframework.random;

/**
 * @author prelle
 *
 */
public interface GeneratorVariable {

	public String name();
	public boolean isInteger();

}
