/**
 * 
 */
package de.rpgframework.random;

import java.util.ArrayList;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Root(name="rollTables")
@ElementList(entry="rollTable",type=RollTable.class,inline=true)
public class RollTableList extends ArrayList<RollTable> {

	//-------------------------------------------------------------------
	public RollTableList() {
	}

}
