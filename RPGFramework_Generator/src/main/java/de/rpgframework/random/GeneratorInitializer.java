package de.rpgframework.random;

import java.util.List;

/**
 *
 */
public interface GeneratorInitializer {

	public List<RandomGenerator> getGeneratorsToRegister();

}
