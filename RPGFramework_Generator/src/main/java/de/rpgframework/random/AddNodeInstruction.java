package de.rpgframework.random;

import org.prelle.simplepersist.Attribute;

/**
 * @author prelle
 *
 */
public class AddNodeInstruction {

	@Attribute(required=true)
	private PlotNode.Type type;
	@Attribute(required=true)
	private String title;
	
	//-------------------------------------------------------------------
	public AddNodeInstruction() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public PlotNode.Type getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

}
