package de.rpgframework.random;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.HashMap;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.rpgframework.classification.Genre;

class AdventureGeneratorTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		System.out.println("Before All");
		RandomGeneratorRegistry.initialize();
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testNPC() {
		System.out.println("I can generate "+RandomGeneratorRegistry.getSupportedTypes());
		for (GeneratorType type : RandomGeneratorRegistry.getSupportedTypes()) {
			System.out.println(String.format("I can generate %s with %d generators",type, RandomGeneratorRegistry.getGenerators(type).size()));
		}

		Object result = RandomGeneratorRegistry.generate(GeneratorType.NSC, List.of(Genre.CYBERPUNK), List.of(), new HashMap<>());
		System.out.println("Generated "+result);
		assertNotNull(result);
	}

	@Test
	void testMission() {
		System.out.println("I can generate "+RandomGeneratorRegistry.getSupportedTypes());
		for (GeneratorType type : RandomGeneratorRegistry.getSupportedTypes()) {
			System.out.println(String.format("I can generate %s with %d generators",type, RandomGeneratorRegistry.getGenerators(type).size()));
		}

		Object result = RandomGeneratorRegistry.generate(GeneratorType.RUN, List.of(Genre.CYBERPUNK), List.of(), new HashMap<>());
		System.out.println("Generated "+result);
		assertNotNull(result);
	}

}
