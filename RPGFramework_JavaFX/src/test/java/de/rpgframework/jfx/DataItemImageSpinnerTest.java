package de.rpgframework.jfx;

import java.util.Arrays;

import org.prelle.javafx.ResponsiveControlManager;

import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.DataItemTypeKey;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.util.Callback;

// slides a frost pane in on scroll or swipe up; slides it out on scroll or swipe down.
public class DataItemImageSpinnerTest extends Application {

	@DataItemTypeKey(id="meta")
	class SR6MetaType extends DataItem {
		public SR6MetaType(String string) {
			super.id = string;
		}
		public String toString() { return id; }
	}


    public static void main(String[] args) { launch(args); }


    //-------------------------------------------------------------------
    /**
     * @see javafx.application.Application#start(javafx.stage.Stage)
     */
    @Override
    public void start(Stage stage) {
    	DataItemPane<SR6MetaType> contentPane = new DataItemPane<SR6MetaType>( r->null, m->null);
		contentPane.setImageConverter(new Callback<SR6MetaType,Image>(){
			public Image call(SR6MetaType value) {
				System.out.println("apply "+value);
				return new Image(ClassLoader.getSystemResourceAsStream("openduke.png"));
			}});
		contentPane.setItems(Arrays.asList(new SR6MetaType("ork"), new SR6MetaType("elf")));

//    	spinner.setStyle("-fx-background-color: pink");
//    	spinner.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

//    	Label label = new Label("Node 2");
//    	label.setStyle("-fx-background-color: lime");
//    	label.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
//
//    	ResponsiveBox box = new ResponsiveBox(contentPane, label);
    	ResponsiveControlManager.setBreakpoints(600, 1200);
        Scene scene = new Scene(contentPane, 590,1200);
    	ResponsiveControlManager.manageResponsiveControls(scene);
//        FlexibleApplication.setStyle(scene, FlexibleApplication.DARK_STYLE);
//        scene.getStylesheets().add(DataItemImageSpinnerTest.class.getResource("css/rpgframework.css").toString());

        stage.setScene(scene);
        stage.show();

    }

}