package de.rpgframework.jfx;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.text.Collator;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.NavigButtonControl;
import org.prelle.javafx.ResponsiveControl;
import org.prelle.javafx.ResponsiveControlManager;
import org.prelle.javafx.WindowMode;

import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.Possible.State;
import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.DataItemValue;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.jfx.cells.ComplexDataItemListCell;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;

/**
 * @author prelle
 *
 */
public abstract class Selector<T extends DataItem, V extends DataItemValue<T>> extends Pane implements ResponsiveControl, IRefreshableList {

	private final static Logger logger = System.getLogger(Selector.class.getPackageName());

	private final static Collator COLLATOR = Collator.getInstance(Locale.getDefault());

	protected ComplexDataItemController<T, V> control;
	protected NavigButtonControl btnCtrl;
	protected Function<Requirement,String> resolver;
	protected Function<Modification,String> mResolver;
	protected ListView<T> listPossible;
	protected Predicate<T> baseFilter ;

	protected AFilterInjector<T> filter;
	protected Label phAvailable;
	protected VBox col1, col2;
	protected GenericDescriptionVBox genericDescr;
	protected Pane help;

	protected Label lbNotPossible;

	//-------------------------------------------------------------------
	public Selector(ComplexDataItemController<T, V> ctrl, Function<Requirement,String> resolver, Function<Modification,String> mResolver, AFilterInjector<T> filter) {
		if (ctrl==null) throw new NullPointerException("Controller may not be NULL");
		this.control = ctrl;
		this.filter  = filter;
		this.resolver = resolver;
		this.mResolver = mResolver;
		initComponents();
		initLayout();
		listPossible.getItems().addAll(ctrl.getAvailable());
		if (filter!=null) {
			filter.updateChoices(ctrl.getAvailable());
		}

		initInteractivity();

		updateLayout();
		refreshList();
	}

	//-------------------------------------------------------------------
	public Selector(ComplexDataItemController<T, V> ctrl, Predicate<T> baseFilter, Function<Requirement,String> resolver, Function<Modification,String> mResolver, AFilterInjector<T> filter) {
		if (ctrl==null) throw new NullPointerException("Controller may not be NULL");
		this.control = ctrl;
		this.filter  = filter;
		this.resolver = resolver;
		this.mResolver = mResolver;
		this.baseFilter = baseFilter;
		initComponents();
		initLayout();

		List<T> list = ctrl.getAvailable();
		if (baseFilter!=null) {
			list = list.stream().filter(baseFilter).collect(Collectors.toList());
		}
		if (filter!=null) {
			filter.updateChoices(list);
		}
		Collections.sort(list, new Comparator<T>() {
			public int compare(T o1, T o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		listPossible.getItems().setAll(list);

		initInteractivity();

		updateLayout();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		phAvailable = new Label();
		phAvailable.setWrapText(true);

		listPossible = new ListView<T>();
		listPossible.setPlaceholder(phAvailable);
		listPossible.setStyle("-fx-border-width: 1px; -fx-border-color: white; -fx-border-style:solid; -fx-pref-width: 24em");
		listPossible.setCellFactory( (lv) -> new ComplexDataItemListCell( () -> control, resolver, this));

		listPossible.setMaxHeight(Double.MAX_VALUE);

		logger.log(Level.ERROR, "Instantiate GenericDescriptionVBox with {0} and {1}", resolver, mResolver);
		genericDescr = new GenericDescriptionVBox(resolver, mResolver);

		lbNotPossible = new Label();
		lbNotPossible.setStyle("-fx-text-fill: highlight;");
		lbNotPossible.setWrapText(true);
		lbNotPossible.setMaxWidth(Double.MAX_VALUE);
		lbNotPossible.setAlignment(Pos.BASELINE_RIGHT);
		lbNotPossible.setTextAlignment(TextAlignment.RIGHT);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		getChildren().clear();

		col1 = new VBox(listPossible);
		col1.setFillWidth(true);
		col1.setMaxHeight(Double.MAX_VALUE);
		col1.setStyle("-fx-max-width: 24em");
		HBox.setHgrow(col1,  Priority.ALWAYS);
		VBox.setVgrow(listPossible, Priority.ALWAYS);

		VBox filterNode = null;
		if (filter!=null) {
			filterNode = new VBox(5);
			filter.addFilter(this, filterNode);
		}
		if (filterNode!=null)
			col1.getChildren().add(col1.getChildren().size()-1, filterNode);

		col2 = new VBox(genericDescr,lbNotPossible);
		col2.setFillWidth(true);
		col2.setMaxHeight(Double.MAX_VALUE);
		VBox.setVgrow(lbNotPossible, Priority.NEVER);

	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
    	listPossible.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> selectionChanged(n));
	}

	//-------------------------------------------------------------------
	protected void selectionChanged(T n) {
		Pane box = getDescriptionNode(n);
		logger.log(Level.ERROR, "Selected {0} and show description node {1}", n, box);
		col2.getChildren().setAll(box, lbNotPossible);
		VBox.setVgrow(box, Priority.ALWAYS);
		help.getChildren().setAll(col1, col2);

		Possible poss = control.canBeSelected(n);
		logger.log(Level.ERROR, "  poss= {0}/{2}  btnCtrl={1}", poss, btnCtrl, poss.getMostSevere());
		if (btnCtrl!=null) {
			btnCtrl.setDisabled(CloseType.OK, !poss.get());
		}
		if (poss.getState()==State.REQUIREMENTS_NOT_MET) {
			List<String> problems = poss.getUnfulfilledRequirements().stream().map(r -> resolver.apply(r)).collect(Collectors.toList());
			lbNotPossible.setText(String.join(", ", problems));
		} else if (!poss.get() && poss.getMostSevere()!=null)
			lbNotPossible.setText(poss.getMostSevere().getMessage());
		else
			lbNotPossible.setText(null);
	}

	//-------------------------------------------------------------------
	protected Pane getDescriptionNode(T selected) {
		genericDescr.setData(selected);
		return genericDescr;
	}

	//-------------------------------------------------------------------
	protected void updateLayout( ) {
		WindowMode mode = ResponsiveControlManager.getCurrentMode();
		Pane box = getDescriptionNode(listPossible.getSelectionModel().getSelectedItem());
		box.setMaxHeight(Double.MAX_VALUE);
		col2.getChildren().setAll(box, lbNotPossible);
		VBox.setVgrow(box, Priority.ALWAYS);
		if (mode==WindowMode.MINIMAL) {
			help = new VBox(10, col1, col2);
			VBox.setVgrow(col1, Priority.ALWAYS);
			VBox.setVgrow(col2, Priority.ALWAYS);
			getChildren().setAll(help);
		} else {
			col2.setStyle("-fx-pref-width: 28em");
			help = new HBox(20, col1, col2);
			HBox.setHgrow(col1, Priority.SOMETIMES);
			HBox.setHgrow(box, Priority.SOMETIMES);
			getChildren().setAll(help);
		}
	}


	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ResponsiveControl#setResponsiveMode(org.prelle.javafx.WindowMode)
	 */
	@Override
	public void setResponsiveMode(WindowMode value) {
		updateLayout();
	}

	//-------------------------------------------------------------------
	public void setBaseFilter(Predicate<T> filter) {
		baseFilter = filter;
		refreshList();
	}

	//-------------------------------------------------------------------
	public T getSelected() {
		return listPossible.getSelectionModel().getSelectedItem();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.IRefreshableList#refreshList()
	 */
	@Override
	public void refreshList() {
		List<T> list = control.getAvailable();
		if (baseFilter!=null)
			list = list.stream().filter(baseFilter).collect(Collectors.toList());
		if (filter!=null)
			list = filter.applyFilter(list);

		Collections.sort(list, new Comparator<T>() {
			public int compare(T o1, T o2) {
				return COLLATOR.compare(o1.getName(), o2.getName());
			}
		});

		listPossible.getItems().setAll(list);
	}

	//-------------------------------------------------------------------
	public void setButtonControl(NavigButtonControl btnCtrl) {
		this.btnCtrl = btnCtrl;
	}

	//-------------------------------------------------------------------
	public void implSelect(T toSelect) {
		logger.log(Level.ERROR, "implSelect {0}", toSelect);

		btnCtrl.fireEvent(CloseType.OK);
	}

}
