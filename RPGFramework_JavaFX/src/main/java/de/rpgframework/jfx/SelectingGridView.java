package de.rpgframework.jfx;

import org.controlsfx.control.GridView;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.MultipleSelectionModel;
import javafx.scene.control.SelectionMode;

/**
 * @author prelle
 *
 */
public class SelectingGridView<T> extends GridView<T> {

    // --- Selection Model
    private ObjectProperty<MultipleSelectionModel<T>> selectionModel = new SimpleObjectProperty<MultipleSelectionModel<T>>(this, "selectionModel");

	//-------------------------------------------------------------------
	/**
	 */
	public SelectingGridView() {
         setSelectionModel(new StupidSimpleSingleSelectionModel<>(getItems()));
         getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
	}

	//-------------------------------------------------------------------
	/**
	 * @param items
	 */
	public SelectingGridView(ObservableList<T> items) {
		super(items);
        setSelectionModel(new StupidSimpleSingleSelectionModel<>(getItems()));
        getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
	}

    /**
     * Sets the {@link MultipleSelectionModel} to be used in the ListView.
     * Despite a ListView requiring a <b>Multiple</b>SelectionModel, it is possible
     * to configure it to only allow single selection (see
     * {@link MultipleSelectionModel#setSelectionMode(javafx.scene.control.SelectionMode)}
     * for more information).
     * @param value the MultipleSelectionModel to be used in this ListView
     */
    public final void setSelectionModel(MultipleSelectionModel<T> value) {
        selectionModelProperty().set(value);
    }

    /**
     * Returns the currently installed selection model.
     * @return the currently installed selection model
     */
    public final MultipleSelectionModel<T> getSelectionModel() {
        return selectionModel == null ? null : selectionModel.get();
    }

    /**
     * The SelectionModel provides the API through which it is possible
     * to select single or multiple items within a ListView, as  well as inspect
     * which items have been selected by the user. Note that it has a generic
     * type that must match the type of the ListView itself.
     * @return the selectionModel property
     */
    public final ObjectProperty<MultipleSelectionModel<T>> selectionModelProperty() {
        return selectionModel;
    }

}
