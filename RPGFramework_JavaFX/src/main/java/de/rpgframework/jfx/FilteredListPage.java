package de.rpgframework.jfx;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.prelle.javafx.ApplicationScreen;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.Mode;
import org.prelle.javafx.Page;
import org.prelle.javafx.ResponsiveControlManager;
import org.prelle.javafx.SymbolIcon;
import org.prelle.javafx.WindowMode;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.data.DataItem;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author prelle
 *
 */
public class FilteredListPage<T extends DataItem> extends Page implements IRefreshableList {

	private final static ResourceBundle RES = ResourceBundle.getBundle(FilteredListPage.class.getName());
	private final static Logger logger = RPGFrameworkJavaFX.logger;

	private class DataItemListCell<U extends DataItem> extends ListCell<U> {
		public void updateItem(U item, boolean empty) {
			super.updateItem(item, empty);
			if (empty) {
				setText(null);
			} else {
				setText(item.getName(Locale.getDefault()));
			}
		}
	}

	private Callback<ListView<T>, ListCell<T>> NAME_CELL_FACTORY = new Callback<ListView<T>, ListCell<T>>() {
		public ListCell<T> call(ListView<T> param) {
			return new DataItemListCell<T>();
		}
	};


	private Supplier<Collection<? extends T>> listProvider;
	private ObjectProperty<Callback<ListView<T>, ListCell<T>>> cellFactoryProperty = new SimpleObjectProperty<>(NAME_CELL_FACTORY);
	private ADescriptionPane<T> descPane;
	private AFilterInjector<T> filterInjector;

	// Secondary Header content
	private Button btnOpen;
	private TextField search;
	// Secondary content
	private FlowPane filterPane;
	// Primary content
	private HBox contentPane;
	private ListView<T> lvResult;

	private transient boolean busy;

	//-------------------------------------------------------------------
	public FilteredListPage(String title, Supplier<Collection<? extends T>> listProvider, ADescriptionPane<T> descPane) {
		super(title);
		setMode(Mode.BACKDROP);
		this.listProvider = listProvider;
		this.descPane = descPane;
		if (descPane==null)
			throw new NullPointerException("descPane");
		descPane.setStyle("-fx-background-color: transparent");

		initSecondaryHeader();
		initSecondaryContent();
		initPrimaryContent();
		initInteractivity();

		refreshList();
	}

	//-------------------------------------------------------------------
	public ObjectProperty<Callback<ListView<T>, ListCell<T>>> cellFactoryProperty() { return cellFactoryProperty; }
	public final void setCellFactory(Callback<ListView<T>, ListCell<T>> value) {
		cellFactoryProperty().set(value);
	}

	//-------------------------------------------------------------------
	public void setFilterInjector(AFilterInjector<T> value) {
		this.filterInjector = value;
		if (value!=null)
			value.addFilter(this, filterPane);
		refreshList();
	}

	//-------------------------------------------------------------------
	private void initSecondaryHeader() {
		btnOpen = new Button(null, new SymbolIcon("Filter"));
		btnOpen.setStyle("-fx-pref-width: 2em; -fx-border-width: 0px");

		search  = new TextField();
		search.setPromptText(ResourceI18N.get(RES, "search.prompt"));

		HBox head2nd = new HBox(btnOpen, search);
		setSecondaryHeader(head2nd);
	}

	//-------------------------------------------------------------------
	private void initSecondaryContent() {
		Label head = new Label(ResourceI18N.get(RES, "heading.type"));
		head.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);

		filterPane = new FlowPane();
		filterPane.setVgap(2);
		filterPane.setHgap(2);

		VBox content2nd = new VBox(head, filterPane);
		content2nd.setStyle("-fx-spacing: 0.5em; -fx-padding: 10px 0 10px 0");
		VBox.setMargin(content2nd, new Insets(10, 0, 10, 0));


		setSecondaryContent(content2nd);
	}

	//-------------------------------------------------------------------
	private void initPrimaryContent() {
		contentPane = new HBox(20);
		VBox.setVgrow(contentPane, Priority.ALWAYS);

		lvResult = new ListView<T>();
		lvResult.cellFactoryProperty().bind(cellFactoryProperty);
		lvResult.setMaxHeight(Double.MAX_VALUE);
		lvResult.setStyle("-fx-min-width: 20em; -fx-pref-width: 28em;  -fx-max-width: 28em");
		HBox.setHgrow(lvResult, Priority.SOMETIMES);
		contentPane.getChildren().add(lvResult);

		if (ResponsiveControlManager.getCurrentMode()!=WindowMode.MINIMAL) {
//			contentPane.getChildren().add(getInstance(null));
			contentPane.getChildren().add(descPane);
		}
		setContent(contentPane);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		// React to list selections
		lvResult.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null)
				showAction(n);
		});

		// React to searches
		search.textProperty().addListener( (ov,o,n) -> refreshList());
		// Enable open button
		btnOpen.setOnAction(ev -> toggleOpenClose());
	}

	//-------------------------------------------------------------------
	public boolean nameMatch(T item, String search) {
		return item.getName().toLowerCase().indexOf(search.toLowerCase())>-1;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.jfx.IRefreshableList#refreshList()
	 */
	@Override
	public void refreshList() {
		if (busy) return;

		try {
			busy = true;

		List<T> list = new ArrayList<T>(listProvider.get());
		logger.log(Level.DEBUG, "Calling "+listProvider+" returned "+list.size());
		// Match search keyword
		if (search.getText()!=null && !search.getText().isBlank()) {
			list = list.stream()
					.filter(crea -> nameMatch(crea, search.getText()))
					.collect(Collectors.toList());
		}
		logger.log(Level.DEBUG, "After name match "+list.size());

		if (filterInjector!=null) {
			list = filterInjector.applyFilter(list);
			logger.log(Level.DEBUG, "After filter "+list.size());
			filterInjector.updateChoices(list);
		}
		logger.log(Level.DEBUG, "After filter "+filterInjector+" "+list.size());

		Collections.sort(list, new Comparator<T>() {
			public int compare(T o1, T o2) {
				if (o1.getName()==null || o2.getName()==null)
					return 0;
				return Collator.getInstance().compare(o1.getName(), o2.getName());
			}
		});
		lvResult.getItems().setAll(list);
		} finally {
			busy = false;
		}
	}

	//-------------------------------------------------------------------
	protected void showAction(T value) {
		logger.log(Level.WARNING, "showAction("+value+") on "+descPane.getClass());
//		Node descPane = getInstance(value);
		descPane.setData(value);

		if (ResponsiveControlManager.getCurrentMode()==WindowMode.MINIMAL) {
			logger.log(Level.DEBUG, "Open page for "+descPane);
			contentPane.getChildren().remove(descPane);
			Page toOpen = new Page(null, descPane);
			FlexibleApplication.getInstance().openScreen(new ApplicationScreen(toOpen));
		} else {
			HBox.setHgrow(descPane, Priority.SOMETIMES);
			contentPane.getChildren().setAll(lvResult, descPane);
		}
	}
}
