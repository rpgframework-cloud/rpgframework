package de.rpgframework.jfx;

import de.rpgframework.genericrpg.chargen.SingleComplexDataItemController;
import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;
import de.rpgframework.jfx.rules.skin.BlankableValueControlSkin;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;
import javafx.scene.control.Skinnable;

/**
 *
 */
public class BlankableValueControl<D extends ComplexDataItem,V extends ComplexDataItemValue<D>> extends Control {

	private StringProperty placeholder = new SimpleStringProperty();
	private ObjectProperty<V> selected = new SimpleObjectProperty<V>();
	private ObjectProperty<SingleComplexDataItemController<D,V>> controller = new SimpleObjectProperty<SingleComplexDataItemController<D,V>>();

	//-------------------------------------------------------------------
	public BlankableValueControl(String placeholder) {
		this.placeholder.set(placeholder);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Control#createDefaultSkin()
	 */
	@Override
	public Skin<?> createDefaultSkin() {
		return new BlankableValueControlSkin<D,V>(this);
	}

	//-------------------------------------------------------------------
	public StringProperty placeholder() { return placeholder; }
	public BlankableValueControl<D,V> setPlaceholder(String value) { placeholder.set(value); return this; }
	public String getPlaceholder() { return placeholder.get(); }

	//-------------------------------------------------------------------
	public ObjectProperty<V> selected() { return selected; }
	public BlankableValueControl<D,V> setSelected(V value) { selected.set(value); return this; }
	public V getSelected() { return selected.get(); }

	//-------------------------------------------------------------------
	public ObjectProperty<SingleComplexDataItemController<D,V>> controller() { return controller; }
	public BlankableValueControl<D,V> setController(SingleComplexDataItemController<D,V> value) { controller.set(value); return this; }
	public SingleComplexDataItemController<D,V> getController() { return controller.get(); }

}
