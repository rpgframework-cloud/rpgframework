package de.rpgframework.jfx.rules;

import java.util.List;
import java.util.function.BiFunction;

import org.prelle.javafx.ResponsiveControl;
import org.prelle.javafx.WindowMode;

import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.items.AItemEnhancement;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.ItemEnhancementValue;
import de.rpgframework.genericrpg.items.PieceOfGear;
import de.rpgframework.jfx.rules.skin.EditCarriedItemPaneWideSkin;
import de.rpgframework.jfx.rules.skin.Properties;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;

/**
 *
 */
public class EditCarriedItemPane<T extends PieceOfGear<?, ?, ?, ?>, E extends AItemEnhancement> extends Control implements ResponsiveControl {

	private ObjectProperty<Node> primaryNode = new SimpleObjectProperty<Node>();
	private ObservableList<ItemCategory<?>> allowedCategories = FXCollections.observableArrayList();
	private ObservableMap<ItemCategory<?>, ObservableList<DataItem>> itemsInCategory = FXCollections.observableHashMap();
	private ObjectProperty<BiFunction<E, List<Choice>, Decision[]>> optionCallbackProperty = new SimpleObjectProperty<>();

	private CarriedItem<T> model;
	private ComplexDataItemController<E,ItemEnhancementValue<E>> enhanceCtrl;

	//-------------------------------------------------------------------
	public EditCarriedItemPane(CarriedItem<T> model, ComplexDataItemController<E,ItemEnhancementValue<E>> enhanceCtrl) {
		this.model = model;
		this.enhanceCtrl = enhanceCtrl;
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Control#createDefaultSkin()
	 */
	public Skin<?> createDefaultSkin() {
		return new EditCarriedItemPaneWideSkin<T,E>(this);
	}

	//-------------------------------------------------------------------
	public ObjectProperty<Node> primaryNodeProperty() { return primaryNode; }
	public Node getPrimaryNode() { return primaryNode.get(); }
	public EditCarriedItemPane<T,E> setPrimaryNode(Node value) { primaryNode.set(value); return this; }

	//-------------------------------------------------------------------
	public void refresh() {
	    getProperties().put(Properties.RECREATE, Boolean.TRUE);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ResponsiveControl#setResponsiveMode(org.prelle.javafx.WindowMode)
	 */
	@Override
	public void setResponsiveMode(WindowMode value) {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	public CarriedItem<T> getItem() { return model; }
	public ComplexDataItemController<E,ItemEnhancementValue<E>> getEnhancementController() { return enhanceCtrl; }

	//-------------------------------------------------------------------
	public ObjectProperty<BiFunction<E, List<Choice>, Decision[]>> optionCallbackProperty() { return optionCallbackProperty; }
	public BiFunction<E, List<Choice>, Decision[]> getOptionCallback() { return optionCallbackProperty.get(); }
	public EditCarriedItemPane<T,E> setOptionCallback(BiFunction<E, List<Choice>, Decision[]> value) { optionCallbackProperty.setValue(value); return this;}

}
