package de.rpgframework.jfx.rules.skin;

import java.lang.System.Logger;
import java.util.List;
import java.util.function.BiFunction;

import org.prelle.javafx.EditableLabel;

import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.Decision;
import de.rpgframework.genericrpg.items.AItemEnhancement;
import de.rpgframework.genericrpg.items.ItemEnhancementValue;
import de.rpgframework.genericrpg.items.PieceOfGear;
import de.rpgframework.jfx.ComplexDataItemControllerNode;
import de.rpgframework.jfx.rules.EditCarriedItemPane;
import javafx.collections.MapChangeListener;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 *
 */
public class EditCarriedItemPaneWideSkin<T extends PieceOfGear<?, ?, ?, ?>, E extends AItemEnhancement> extends SkinBase<EditCarriedItemPane<T,E>> {

	private final static Logger logger = System.getLogger(EditCarriedItemPaneWideSkin.class.getName());

	private VBox layout;
	private Label label;
	private Button btnEditTitle;
	private StackPane containerPrimaryNode;

	private ComplexDataItemControllerNode<E, ItemEnhancementValue<E>> enhancements;

	private MapChangeListener<Object, Object> propertiesMapListener = c -> {
        if (! c.wasAdded()) return;
        if (Properties.RECREATE.equals(c.getKey())) {
 //           refresh();
            getSkinnable().requestLayout();
            getSkinnable().getProperties().remove(Properties.RECREATE);
        }
    };

	//-------------------------------------------------------------------
	public EditCarriedItemPaneWideSkin(EditCarriedItemPane control) {
		super(control);
		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		label = new Label(getSkinnable().getItem().getNameWithoutRating());
		System.out.println("...EditCarriedItemPaneWideSkin: "+getSkinnable().getItem());

		enhancements = new ComplexDataItemControllerNode<>(super.getSkinnable().getEnhancementController());
		enhancements.setOptionCallback(null);

		containerPrimaryNode = new StackPane();
		containerPrimaryNode.setMinSize(400, 100);
		if (getSkinnable().getPrimaryNode()!=null) {
			containerPrimaryNode.getChildren().add(getSkinnable().getPrimaryNode());
		}
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		layout = new VBox(20, new Label("Test"),label,containerPrimaryNode, enhancements);
		layout.setStyle("-fx-background-color: rgba(255,255,255,0.9)");
		layout.setMaxSize(800, 400);

		getChildren().add(layout);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		getSkinnable().primaryNodeProperty().addListener( (ov,o,n) -> {
			if (o!=null)
				containerPrimaryNode.getChildren().remove( (Node)o);
			if (n!=null)
				containerPrimaryNode.getChildren().add(0, (Node)n);
		});

		enhancements.optionCallbackProperty().bind(getSkinnable().optionCallbackProperty());
	}

}
