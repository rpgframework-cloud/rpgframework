package de.rpgframework.jfx.rules;

import java.util.function.BiFunction;

import de.rpgframework.character.RuleSpecificCharacterObject;
import de.rpgframework.genericrpg.NumericalValueController;
import de.rpgframework.genericrpg.data.AttributeValue;
import de.rpgframework.genericrpg.data.IAttribute;
import de.rpgframework.jfx.rules.skin.AttributeTableSkin;
import de.rpgframework.jfx.rules.skin.Properties;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;

/**
 * @author Stefan Prelle
 *
 */
public class AttributeTable<A extends IAttribute> extends Control {
	
	public static class AttributeColumn<A extends IAttribute> {
		private boolean beforeValueColumn;
		private String title;
		private BiFunction<RuleSpecificCharacterObject<A,?,?,?>, A, Object> valueFactory;
		private BiFunction<A,Object, Node> componentFactory;
		
		public AttributeColumn(BiFunction<RuleSpecificCharacterObject<A,?,?,?>, A, Object> valueFact, BiFunction<A,Object, Node> componentFact) {
			beforeValueColumn = false;
			this.valueFactory = valueFact;
			this.componentFactory = componentFact;
		}
		public boolean isShowBeforeValueColumn() { return beforeValueColumn; }
		public BiFunction<RuleSpecificCharacterObject<A,?,?,?>, A, Object> getValueFactory() { return valueFactory; }
		public BiFunction<A, Object, Node> getComponentFactory() { return componentFactory; }
		public void setTitle(String value) { this.title = value; }
		public void setShowBeforeColumn(boolean value) { this.beforeValueColumn = true; }
		public String getTitle() {return title;}
	}

	public enum Mode {
		/**
		 * Show only the values, but don't allow any changes 
		 */
		SHOW_ONLY,
		/**
		 * Directly enter the attribute values
		 */
		DIRECT_INPUT,
		/**
		 * Allow raising and lowering attributes with a Generator. 
		 */
		GENERATE,
		/**
		 * Allow raising and lowering attributes with a Controller 
		 */
		CAREER,
	}
	
	@FXML
	private ObjectProperty<Mode> mode = new SimpleObjectProperty<Mode>();
	@FXML
	private A[] attributes;
	
	private ObjectProperty<RuleSpecificCharacterObject<A,?,?,?>> model = new SimpleObjectProperty<RuleSpecificCharacterObject<A,?,?,?>>();
	private ObjectProperty<IAttribute> selectedAttribute = new SimpleObjectProperty<IAttribute>();
	private ObjectProperty<NumericalValueController<A, AttributeValue<A>>> controller = new SimpleObjectProperty<NumericalValueController<A,AttributeValue<A>>>();
	
	private ObservableList<AttributeColumn> columns = FXCollections.observableArrayList();
	
	
	//-------------------------------------------------------------------
	public AttributeTable() {
	}
	
	//-------------------------------------------------------------------
	public AttributeTable(A[] attributes) {
		this.attributes = attributes;
		this.mode.set(Mode.SHOW_ONLY);
	}
	
	//-------------------------------------------------------------------
	public AttributeTable<A> setAttributes(A[] attributes) {
		return this;
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Control#createDefaultSkin()
	 */
	@Override
	public Skin<?> createDefaultSkin() {
		return new AttributeTableSkin(this);
	}
	
	//-------------------------------------------------------------------
	public Mode getMode() { return mode.get(); }
	public ObjectProperty<Mode> modeProperty() { return mode; }
	public AttributeTable<A> setMode(Mode value) { mode.set(value); return this; }

	public A[] getAttributes() { return attributes; }

	//-------------------------------------------------------------------
	public ObjectProperty<RuleSpecificCharacterObject<A,?,?,?>> modelProperty() { return model; }
	public RuleSpecificCharacterObject<A,?,?,?> getModel() { return model.get(); }
	public AttributeTable<A> setModel(RuleSpecificCharacterObject<A,?,?,?> value) { model.set(value); return this; }

	//-------------------------------------------------------------------
	public ObjectProperty<IAttribute> selectedAttributeProperty() { return selectedAttribute; }
	public IAttribute getSelectedAttribute() { return selectedAttribute.get(); }
	public AttributeTable<A> setSelectedAttribute(IAttribute value) { selectedAttribute.set(value); return this; }

	//-------------------------------------------------------------------
	public ObjectProperty<NumericalValueController<A, AttributeValue<A>>> controllerProperty() { return controller; }
	public NumericalValueController<A, AttributeValue<A>> getController() { return controller.get(); }
	public AttributeTable<A> setController(NumericalValueController<A, AttributeValue<A>> value) { controller.set(value); return this; }

	//-------------------------------------------------------------------
	public ObservableList<AttributeColumn> getColumns() { return columns; }

	//-------------------------------------------------------------------
   public void refresh() {
        getProperties().put(Properties.RECREATE, Boolean.TRUE);
    }

}
