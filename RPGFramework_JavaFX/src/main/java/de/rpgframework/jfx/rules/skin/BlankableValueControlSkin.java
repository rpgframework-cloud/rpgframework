package de.rpgframework.jfx.rules.skin;

import java.lang.System.Logger.Level;

import org.prelle.javafx.SymbolIcon;

import de.rpgframework.genericrpg.chargen.SingleComplexDataItemController;
import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;
import de.rpgframework.jfx.BlankableValueControl;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.SkinBase;
import javafx.scene.control.ToggleButton;
import javafx.scene.text.TextAlignment;

/**
 *
 */
public class BlankableValueControlSkin<D extends ComplexDataItem,V extends ComplexDataItemValue<D>> extends SkinBase<BlankableValueControl<D,V>> {

	private final String DASHED = "-fx-border-insets: 0;\n"
            + "-fx-border-width: 1;\n"
            + "-fx-stroke-dashed-offset: 5;\n"
            + "-fx-border-style: dashed;\n";

	private ToggleButton button;
	private SymbolIcon icoAdd;
	private SymbolIcon icoDel;

	//-------------------------------------------------------------------
	public BlankableValueControlSkin(BlankableValueControl<D,V> control) {
		super(control);
		initComponents();
		initLayout();
		initInteractivity();
		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		icoAdd = new SymbolIcon("add");
		icoDel = new SymbolIcon("delete");
		icoAdd.getStyleClass().add("mini-icon");
		icoDel.getStyleClass().add("mini-icon");
		button = new ToggleButton();
		button.setMaxWidth(Double.MAX_VALUE);
		button.setWrapText(true);
		button.setAlignment(Pos.CENTER);
		button.setTextAlignment(TextAlignment.CENTER);
		if (getSkinnable().getSelected()!=null) {
			button.setText(getSkinnable().getSelected().getNameWithoutRating());
		}
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		button.setMinSize(100, 50);
		getChildren().add(button);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		getSkinnable().selected().addListener( (ov,o,n) -> refresh());
		getSkinnable().controller().addListener( (ov,o,n) -> {
			refresh();
			button.setOnAction(ev -> clicked());
		});
		button.setOnAction(ev -> {
			if (getSkinnable().getController()!=null)
				getSkinnable().getController().selectClicked();
		});
	}

	//-------------------------------------------------------------------
	private void refresh() {
		V selected = getSkinnable().getSelected();
		SingleComplexDataItemController<D, V> ctrl = getSkinnable().getController();
		if (selected==null) {
			button.setGraphic(null);
			button.setStyle(DASHED);
			button.setText(getSkinnable().getPlaceholder());
			if (ctrl==null) {
				button.setDisable(true);
			} else {
				button.setDisable( !ctrl.canBeUsed() );
			}
		} else {
			button.setGraphic(icoDel);
			button.setStyle(null);
			button.setText(selected.getNameWithoutRating());
			if (ctrl==null) {
				button.setDisable(true);
			} else {
				button.setDisable(false);
			}
		}
	}

	//-------------------------------------------------------------------
	private void clicked() {
		System.getLogger(getClass().getPackageName()).log(Level.WARNING, "clicked");
		V selected = getSkinnable().getSelected();
		SingleComplexDataItemController<D, V> ctrl = getSkinnable().getController();
		System.getLogger(getClass().getPackageName()).log(Level.WARNING, "ctrl "+ctrl);
		if (ctrl==null) return;
		if (selected==null) {
			ctrl.selectClicked();
		} else {
			ctrl.deselect(selected);
			refresh();
		}
	}

}
