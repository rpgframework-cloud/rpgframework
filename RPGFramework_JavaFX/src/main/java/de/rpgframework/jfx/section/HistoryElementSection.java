package de.rpgframework.jfx.section;

import java.util.List;
import java.util.ResourceBundle;

import de.rpgframework.ResourceI18N;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.genericrpg.HistoryElement;
import de.rpgframework.genericrpg.data.CommonCharacter;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.jfx.cells.HistoryElementListCell;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.util.StringConverter;

/**
 * @author Stefan Prelle
 *
 */
public class HistoryElementSection extends ListSection<HistoryElement> {

	private RoleplayingSystem rules;
	
	private final static ResourceBundle RES = ResourceBundle.getBundle(HistoryElementSection.class.getPackageName()+".Section");
	private ObjectProperty<StringConverter<Modification>> converter = new SimpleObjectProperty<>();
	
	private CommonCharacter<?, ?, ?, ?> model;
//	protected ListView<HistoryElement> list;

	//-------------------------------------------------------------------
	/**
	 * @param provider
	 * @param title
	 * @param content
	 */
	public HistoryElementSection() {
		super(ResourceI18N.get(RES, "section.history.title"));
		list.setCellFactory(lv -> new HistoryElementListCell(RES));
//		setAddButton(null);
//		initComponents();
//		initLayout();
//		list.setStyle("-fx-min-width: 35em; -fx-pref-width: 50em; -fx-pref-height: 55em;");
		list.setMaxHeight(Double.MAX_VALUE);
	}
	
//	//-------------------------------------------------------------------
//	private void initComponents() {
//		list = new ListView<HistoryElement>();
//		list.setCellFactory(lv -> new HistoryElementListCell(UI, converter, rules));
//	}
//	
//	//-------------------------------------------------------------------
//	private void initLayout() {
//		setContent(list);
//	}
//
//	//-------------------------------------------------------------------
//	public void setData(List<HistoryElement> data) {
//		list.getItems().setAll(data);
//		refresh();
//	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.Section#refresh()
	 */
	@Override
	public void refresh() {
		list.refresh();
	}

//	//-------------------------------------------------------------------
//	public ReadOnlyObjectProperty<HistoryElement> selectedProperty() {
//		return list.getSelectionModel().selectedItemProperty();
//	}

	//-------------------------------------------------------------------
	public void onAdd() {
		
	}

	//-------------------------------------------------------------------
	public void onDelete(HistoryElement elem) {
		
	}

	//-------------------------------------------------------------------
	public void setData(List<HistoryElement> data) {
		list.getItems().setAll(data);
	}
}
