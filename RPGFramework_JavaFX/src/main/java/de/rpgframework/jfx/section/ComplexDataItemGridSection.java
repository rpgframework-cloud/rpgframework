package de.rpgframework.jfx.section;

import java.util.List;
import java.util.function.BiFunction;

import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;
import de.rpgframework.genericrpg.data.Decision;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * @author Stefan Prelle
 *
 */
public abstract class ComplexDataItemGridSection<T extends ComplexDataItem, V extends ComplexDataItemValue<T>> extends GridSection<V> {

	private ObjectProperty<BiFunction<T, List<Choice>, Decision[]>> optionCallbackProperty = new SimpleObjectProperty<>();

	//-------------------------------------------------------------------
	public ComplexDataItemGridSection(String title) {
		super(title);
	}
	
	//-------------------------------------------------------------------
	public ObjectProperty<BiFunction<T, List<Choice>, Decision[]>> optionCallbackProperty() { return optionCallbackProperty; }
	public BiFunction<T, List<Choice>, Decision[]> getOptionCallback() { return optionCallbackProperty.get(); }
	public void setOptionCallback(BiFunction<T, List<Choice>, Decision[]> value) { optionCallbackProperty.setValue(value); }

}
