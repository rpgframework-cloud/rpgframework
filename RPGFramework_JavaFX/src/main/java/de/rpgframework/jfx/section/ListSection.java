package de.rpgframework.jfx.section;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.List;

import org.prelle.javafx.Section;
import org.prelle.javafx.SymbolIcon;

import de.rpgframework.jfx.RPGFrameworkJavaFX;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionModel;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author Stefan Prelle
 *
 */
public abstract class ListSection<T> extends Section {

	protected final static Logger logger = RPGFrameworkJavaFX.logger;

	protected Button btnAdd;
	protected Button btnDel;
	protected ListView<T> list;

	protected ObjectProperty<T> showHelpFor = new SimpleObjectProperty<>();
    @FXML
	protected ObjectProperty<Node> headerNodeProperty = new SimpleObjectProperty<>();

	//-------------------------------------------------------------------
	public ListSection(String title) {
		super.setId(title);
		initListComponents();
		getStyleClass().add("list-section");
		setTitle(title);
		setContent(list);
		
		initListInteractivity();
	}

	//-------------------------------------------------------------------
	private void initListComponents() {
		list = new ListView<T>();
		list.setMaxHeight(Double.MAX_VALUE);
		btnAdd = new Button(null, new SymbolIcon("add"));
		btnDel = new Button(null, new SymbolIcon("delete"));
		btnDel.setDisable(true);
		getButtons().addAll(btnAdd, btnDel);
	}

	//-------------------------------------------------------------------
	protected void initListInteractivity() {
		showHelpFor.bind(list.getSelectionModel().selectedItemProperty());
		list.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> selectionChanged(o, n));
		btnAdd.setOnAction(ev -> onAdd());
		btnDel.setOnAction(ev -> onDelete(list.getSelectionModel().getSelectedItem()));
		
		headerNodeProperty.addListener( (ov,o,n) -> {
			if (n==null) {
				setContent(list);
			} else {
				VBox box = new VBox(5, n, list);
				box.setMaxHeight(Double.MAX_VALUE);
				list.setMaxHeight(Double.MAX_VALUE);
				VBox.setVgrow(list, Priority.ALWAYS);
				setContent(box);
			}
		});
	}

	//-------------------------------------------------------------------
	/**
	 * Override this method to get finer control over the Delete button
	 */
	protected void selectionChanged(T old, T neu) {
		btnDel.setDisable(neu==null);
	}

	//-------------------------------------------------------------------
	public void setData(List<T> data) {
		list.getItems().setAll(data);
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<T> showHelpForProperty() {
		return showHelpFor;
	}

	//-------------------------------------------------------------------
	public SelectionModel<T> getSelectionModel() {
		return list.getSelectionModel();
	}
	
	//-------------------------------------------------------------------
	public ListView<T> getListView() {
		return list;
	}

	//-------------------------------------------------------------------
	protected abstract void onAdd();

	//-------------------------------------------------------------------
	protected abstract void onDelete(T item);

	//-------------------------------------------------------------------
	protected void onSettings() {
		logger.log(Level.WARNING, "onSettings() not overloaded in "+getClass());
	}

	//-------------------------------------------------------------------
	public ObjectProperty<Node> headerNodeProperty() { return headerNodeProperty; }
	public Node getHeaderNode() { return headerNodeProperty.get(); }
	public Section setHeaderNode(Node value) { headerNodeProperty.set(value); return this; }

}
