package de.rpgframework.jfx.section;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;

import org.controlsfx.control.GridView;
import org.prelle.javafx.Section;
import org.prelle.javafx.SymbolIcon;

import de.rpgframework.jfx.RPGFrameworkJavaFX;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.SelectionModel;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author Stefan Prelle
 *
 */
public abstract class GridSection<T> extends Section {

	protected final static Logger logger = RPGFrameworkJavaFX.logger;

	protected Button btnAdd;
	protected Button btnDel;
	protected GridView<T> list;

	protected ObjectProperty<T> showHelpFor = new SimpleObjectProperty<>();
    @FXML
	protected ObjectProperty<Node> headerNodeProperty = new SimpleObjectProperty<>();

    private SelectionModel<T> selectionModel;
	protected transient List<IconGridCell> allCells = new ArrayList<>();

	//-------------------------------------------------------------------
	public GridSection(String title) {
		super.setId(title);
		initListComponents();
		getStyleClass().add("list-section");
		setTitle(title);
		setContent(list);
		
		initListInteractivity();
	}

	//-------------------------------------------------------------------
	private void initListComponents() {
		list = new GridView<T>();
		list.setMaxHeight(Double.MAX_VALUE);
		list.setHorizontalCellSpacing(10);
		list.setVerticalCellSpacing(10);
		selectionModel = new SingleSelectionModel<T>() {
			@Override
			protected T getModelItem(int index) {
				return list.getItems().get(index);
			}
			@Override
			protected int getItemCount() {
				return list.getItems().size();
			}
		};

		btnAdd = new Button(null, new SymbolIcon("add"));
		btnDel = new Button(null, new SymbolIcon("delete"));
		btnDel.setDisable(true);
		getButtons().addAll(btnAdd, btnDel);
	}

	//-------------------------------------------------------------------
	protected void initListInteractivity() {
		showHelpFor.bind(selectionModel.selectedItemProperty());
//		list.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> logger.log(Level.ERROR, "Selected "+n));
		btnAdd.setOnAction(ev -> onAdd());
		btnDel.setOnAction(ev -> onDelete(selectionModel.getSelectedItem()));
		
		headerNodeProperty.addListener( (ov,o,n) -> {
			if (n==null) {
				setContent(list);
			} else {
				VBox box = new VBox(5, n, list);
				box.setMaxHeight(Double.MAX_VALUE);
				list.setMaxHeight(Double.MAX_VALUE);
				VBox.setVgrow(list, Priority.ALWAYS);
				setContent(box);
			}
		});

		selectionModel.selectedIndexProperty().addListener( (ov,o,n) -> {
			for (IconGridCell cell : allCells) {
				if (cell.getIndex()==(int)o) cell.pseudoClassStateChanged(IconGridCell.PSEUDO_CLASS_SELECTED, false);
			}
		});
	}

	//-------------------------------------------------------------------
	public void setData(List<T> data) {
		list.getItems().setAll(data);
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<T> showHelpForProperty() {
		return showHelpFor;
	}

	//-------------------------------------------------------------------
	public SelectionModel<T> getSelectionModel() {
		return selectionModel;
	}
	
	//-------------------------------------------------------------------
	public GridView<T> getListView() {
		return list;
	}

	//-------------------------------------------------------------------
	protected abstract void onAdd();

	//-------------------------------------------------------------------
	protected abstract void onDelete(T item);

	//-------------------------------------------------------------------
	protected void onSettings() {
		logger.log(Level.WARNING, "onSettings() not overloaded in "+getClass());
	}

	//-------------------------------------------------------------------
	public ObjectProperty<Node> headerNodeProperty() { return headerNodeProperty; }
	public Node getHeaderNode() { return headerNodeProperty.get(); }
	public Section setHeaderNode(Node value) { headerNodeProperty.set(value); return this; }

}
