package de.rpgframework.jfx.section;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import org.controlsfx.control.GridView;
import org.prelle.javafx.Section;
import org.prelle.javafx.SymbolIcon;

import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.DataItemValue;
import de.rpgframework.jfx.RPGFrameworkJFXConstants;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionModel;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.image.Image;
import javafx.scene.layout.TilePane;

/**
 * @author prelle
 *
 */
public class IconSection<T extends DataItem, D extends DataItemValue<T>> extends Section {
	
	private final static Logger logger = System.getLogger(IconSection.class.getPackageName());
	
	private final Image EMPTY = new Image(RPGFrameworkJFXConstants.class.getResourceAsStream("Person.png"));

	final static int SIZE = 64;
	
	protected Function<T, Image> iconResolver;
	
	protected Button btnAdd;
	protected Button btnDel;
	protected IntegerProperty slots = new SimpleIntegerProperty(4);
	protected ObservableList<D> items = FXCollections.observableArrayList();
	protected SelectionModel<D> selectionModel = new SingleSelectionModel<D>() {
		@Override
		protected D getModelItem(int index) {
			return items.get(index);
		}
		@Override
		protected int getItemCount() {
			return items.size();
		}
	};
	private ObjectProperty<D> showHelpFor = new SimpleObjectProperty<>();
	private ObjectProperty<EventHandler<ActionEvent>> onAddProperty = new SimpleObjectProperty<>();
	private ObjectProperty<EventHandler<ActionEvent>> onDeleteProperty = new SimpleObjectProperty<>();
	

	private GridView<D> tiles;
//	private ImageView[] iViews;
	
	//-------------------------------------------------------------------
	public IconSection(Function<T, Image> iconResolver, String title) {
		this.iconResolver = iconResolver;
		setTitle(title);
		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	public ObservableList<D> getItems() { return items; }

	//-------------------------------------------------------------------
	public IntegerProperty slotsProperty() { return slots; }
	public int getSlots() { return slots.get(); }
	public IconSection<T,D> setSlots(int value) { slots.set(value); return this; }

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<D> showHelpForProperty() {
		return showHelpFor;
	}

	
	//-------------------------------------------------------------------
	private void initComponents() {
//		tiles = new TilePane(10,10);
//		tiles.setPrefTileHeight(SIZE+16);
//		tiles.setPrefTileWidth(SIZE);
		tiles = new GridView<>(items);
		tiles.setHorizontalCellSpacing(10);
		tiles.setVerticalCellSpacing(10);
		tiles.setCellWidth(SIZE+4);
		tiles.setCellHeight(SIZE+16);
//		tiles.setCellFactory( gv -> new IconGridCell<T,D>(this));

		
//		iViews = new ImageView[20];
		
		btnAdd = new Button(null, new SymbolIcon("add"));
		btnDel = new Button(null, new SymbolIcon("delete"));
		btnDel.setDisable(true);
		getButtons().addAll(btnAdd, btnDel);
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		setContent(tiles);
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		slots.addListener( (ov,o,n) -> refresh());
		items.addListener(new ListChangeListener<D>() {
			@Override
			public void onChanged(Change<? extends D> changes) {
				logger.log(Level.INFO, "Items changed");
				refresh();
				
			}});
		btnAdd.setOnAction(ev -> onAdd(ev));
		btnDel.setOnAction(ev -> onDelete(ev));
		
		
//		card.pseudoClassStateChanged(PSEUDO_CLASS_SELECTED, !oldState);
//		if (oldState) {
//			selectionModel.clearSelection(handles.indexOf(charac));
//		} else {
//			selectionModel.select(handles.indexOf(charac));
//		}
//
	}
	
	//-------------------------------------------------------------------
	public void refresh() {
		logger.log(Level.INFO, "refresh");
//		List<Label> toSet = new ArrayList<>();
//		for (int i=0; i<slots.get(); i++) {
//			if (iViews[i]==null) {
//				iViews[i] = new ImageView();
//				iViews[i].setFitWidth(SIZE);
//				iViews[i].setFitHeight(SIZE);
//				iViews[i].setStyle("-fx-border-width: 1px; -fx-border-color: pink; -fx-background-color: grey;");
//			}
//			DataItemValue<T> item = null;
//			if (i<items.size())
//				item = items.get(i);
//			if (item==null)
//				iViews[i].setImage(EMPTY);
//			else {
//
//				if (iconResolver != null) {
//					Image img = iconResolver.apply(item.getResolved());
//					if (img!=null)
//						iViews[i].setImage(img);
//					else
//						iViews[i].setImage(null);
//				}
//			}
//			
//			Label ivLabel = new Label(null, iViews[i]);
//			ivLabel.setContentDisplay(ContentDisplay.TOP);
//			ivLabel.setStyle("-fx-font-size: small");
//			if (item!=null) {
//				ivLabel.setText(item.getNameWithRating());
//				ivLabel.setTooltip(new Tooltip(item.getNameWithRating()));
//			}
//			
//			toSet.add(ivLabel);
//		}
		
		//tiles.getChildren().setAll(toSet);
		
	}
	
	//-------------------------------------------------------------------
	protected void onAdd(ActionEvent ev) {
		if (onAddProperty.get()!=null)
			onAddProperty.get().handle(ev);
	}

	//-------------------------------------------------------------------
	protected void onDelete(ActionEvent ev) {
		if (onDeleteProperty.get()!=null)
			onDeleteProperty.get().handle(ev);

	}

	//-------------------------------------------------------------------
	public void setOnAddAction(EventHandler<ActionEvent> handler) {
		onAddProperty.set(handler);
	}

	//-------------------------------------------------------------------
	public void setOnDeleteAction(EventHandler<ActionEvent> handler) {
		onDeleteProperty.set(handler);
	}

	//-------------------------------------------------------------------
	public boolean select(D item) {
		selectionModel.select(item);
		return selectionModel.getSelectedItem()==item;
	}

	//-------------------------------------------------------------------
	public SelectionModel<D> getSelectionModel() {
		return selectionModel;
	}

}