package de.rpgframework.jfx;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.MultipleSelectionModel;
import javafx.scene.control.SelectionMode;

/**
 * @author prelle
 *
 */
public class StupidSimpleSingleSelectionModel<T> extends MultipleSelectionModel<T> {

	private final static Logger logger = RPGFrameworkJavaFX.logger;

	private ObservableList<T> list;
	private ObservableList<Integer> selectedIndices;
	private ObservableList<T> selectedItems;

	//-------------------------------------------------------------------
	public StupidSimpleSingleSelectionModel(ObservableList<T> list) {
		this.list = list;
		selectedIndices = FXCollections.observableArrayList();
		selectedItems   = FXCollections.observableArrayList();

		list.addListener(new InvalidationListener() {
			public void invalidated(Observable observable) {
				refreshList();
			}
		});
		list.addListener(new ListChangeListener<T>() {
			public void onChanged(Change<? extends T> c) {
				refreshList();
			}});
	}

	//-------------------------------------------------------------------
	private void refreshList() {
		selectedIndices.clear();
		// Update with new indices
		for (T tmp : new ArrayList<T>(selectedItems)) {
			int index = list.indexOf(tmp);
			if (index==-1) {
				selectedItems.remove(tmp);
			} else {
				selectedIndices.add(index);
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.MultipleSelectionModel#getSelectedIndices()
	 */
	@Override
	public ObservableList<Integer> getSelectedIndices() {
		return selectedIndices;
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.MultipleSelectionModel#getSelectedItems()
	 */
	@Override
	public ObservableList<T> getSelectedItems() {
		return selectedItems;
	}

	@Override
	public void selectIndices(int index, int... indices) {
		selectedIndices.clear();
		selectedItems.clear();
		selectedItems.add(list.get(index));
		selectedIndices.add(index);
		for (int tmp : indices) {
			selectedItems.add(list.get(tmp));
			selectedIndices.add(tmp);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.MultipleSelectionModel#selectAll()
	 */
	@Override
	public void selectAll() {
		selectedItems.setAll(list);
		refreshList();
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.MultipleSelectionModel#selectFirst()
	 */
	@Override
	public void selectFirst() {
		if (list.isEmpty())
			return;
		if (selectedIndices.contains(0))
			return;
		select(0);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.MultipleSelectionModel#selectLast()
	 */
	@Override
	public void selectLast() {
		if (list.isEmpty())
			return;
		if (selectedIndices.contains(list.size()-1))
			return;
		select(list.size()-1);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.SelectionModel#clearAndSelect(int)
	 */
	@Override
	public void clearAndSelect(int index) {
		selectedIndices.clear();
		selectedItems.clear();
		select(index);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.SelectionModel#select(int)
	 */
	@Override
	public void select(int index) {
		logger.log(Level.INFO, "select "+index+"  already="+selectedIndices);
		if (selectedIndices.contains(index))
			return;
		if (getSelectionMode()==SelectionMode.SINGLE) {
			clearSelection();
		}
		if (index>=list.size())
			return;
		T val = list.get(index);
		selectedItems.add(val);
		selectedIndices.add(index);
		setSelectedIndex(index);
		setSelectedItem(val);
		logger.log(Level.INFO, "Result: indices="+selectedIndices+", items="+selectedItems+"   selected="+getSelectedIndex());
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.SelectionModel#select(java.lang.Object)
	 */
	@Override
	public void select(T obj) {
		logger.log(Level.INFO, "select "+obj);
		if (!list.contains(obj))
			return;
		if (getSelectionMode()==SelectionMode.SINGLE) {
			clearSelection();
		}

		int index = list.indexOf(obj);
		selectedItems.add(obj);
		selectedIndices.add(index);
		setSelectedIndex(index);
		setSelectedItem(obj);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.SelectionModel#clearSelection(int)
	 */
	@Override
	public void clearSelection(int index) {
		if (isSelected(index)) {
			selectedIndices.remove( (Integer)index);
			selectedItems.remove( list.get(index) );
		}
		if (index==getSelectedIndex()) {
			setSelectedIndex(-1);
			setSelectedItem(null);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.SelectionModel#clearSelection()
	 */
	@Override
	public void clearSelection() {
		logger.log(Level.INFO, "clearSelection");
		setSelectedIndex(-1);
		setSelectedItem(null);
		selectedIndices.clear();
		selectedItems.clear();
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.SelectionModel#isSelected(int)
	 */
	@Override
	public boolean isSelected(int index) {
		return selectedIndices.contains(index);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.SelectionModel#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return selectedIndices.isEmpty();
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.SelectionModel#selectPrevious()
	 */
	@Override
	public void selectPrevious() {
		int prev = getSelectedIndex()-1;
		if (prev>=0) {
			select(prev);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.SelectionModel#selectNext()
	 */
	@Override
	public void selectNext() {
		int next = getSelectedIndex()+1;
		if (next>=list.size()) {
			select(next);
		}
	}

}
