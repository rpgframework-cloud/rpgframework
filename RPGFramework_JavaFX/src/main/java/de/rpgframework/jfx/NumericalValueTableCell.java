package de.rpgframework.jfx;

import java.util.function.Function;

import de.rpgframework.genericrpg.NumericalValue;
import de.rpgframework.genericrpg.NumericalValueController;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.TableCell;

/**
 * @author Stefan
 *
 * T = Type (e.g. Attribute), C = ValueType (e.g. AttributeValue), R = RowType
 */
public class NumericalValueTableCell<T,R extends NumericalValue<T>> extends TableCell<R, Number> {

	private ObjectProperty<NumericalValueController<T,R>> ctrl;
	/**
	 * Use table cell value instead of NumericalValue
	 */
	private boolean useItem;
	private Function<R, Boolean> visibilityCallback;

	//--------------------------------------------------------------------
	public NumericalValueTableCell(ObjectProperty<NumericalValueController<T,R>> ctrl, Function<R, Boolean> visibilityCallback) {
		this.ctrl = ctrl;
		this.visibilityCallback = visibilityCallback;
	}

	//--------------------------------------------------------------------
	public NumericalValueTableCell(ObjectProperty<NumericalValueController<T,R>> ctrl) {
		this.ctrl = ctrl;
	}

	//--------------------------------------------------------------------
	public NumericalValueTableCell(NumericalValueController<T,R> data) {
		this.ctrl = new SimpleObjectProperty<NumericalValueController<T,R>>(data);
	}

	//--------------------------------------------------------------------
	public NumericalValueTableCell(NumericalValueController<T,R> data, boolean useItem) {
		this.ctrl = new SimpleObjectProperty<NumericalValueController<T,R>>(data);
		this.useItem = useItem;
	}

	//--------------------------------------------------------------------
	public NumericalValueTableCell(NumericalValueController<T,R> data, Function<R, Boolean> visibilityCallback) {
		this.ctrl = new SimpleObjectProperty<NumericalValueController<T,R>>(data);
		this.visibilityCallback = visibilityCallback;
	}

	//--------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(Number item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setGraphic(null);
		} else {
			if (getTableRow()!=null && getTableRow().getItem()!=null) {
				NumericalValueField<T,R> val = new NumericalValueField<T,R>( ()->getTableRow().getItem(), ctrl.get());
				val.setController(ctrl.get());
				if (useItem) {
//					val.setOverrideValue((Integer) item);

					val.refresh();
				}
				setGraphic(val);

				if (visibilityCallback==null) {
					setVisible(true);
				} else {
					setVisible(visibilityCallback.apply(getTableRow().getItem()));
				}
			}
		}
	}
}
