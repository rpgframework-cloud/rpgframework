package de.rpgframework.jfx;

import java.lang.System.Logger.Level;
import java.util.function.BiConsumer;
import java.util.function.Function;

import org.prelle.javafx.NodeWithTitle;
import org.prelle.javafx.ResponsiveControl;
import org.prelle.javafx.ResponsiveControlManager;
import org.prelle.javafx.WindowMode;

import de.rpgframework.character.RuleSpecificCharacterObject;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModificationChoice;
import de.rpgframework.genericrpg.modification.ModifiedObjectType;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.jfx.skin.DataItemDetailsPaneSkin;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;
import javafx.scene.image.Image;

/**
 * @author prelle
 *
 */
@SuppressWarnings("rawtypes")
public class DataItemDetailsPane<T extends DataItem> extends Control implements ResponsiveControl {

	private final static String DEFAULT_STYLE_CLASS = "item-details-pane";

	@FXML
	private ObjectProperty<RuleSpecificCharacterObject> model = new SimpleObjectProperty<>();
	private ObservableList<T> items = FXCollections.emptyObservableList();
	private ObjectProperty<Function<T, Image>> imageConverter = new SimpleObjectProperty<>();
	private ObjectProperty<Function<Modification, String>> modifConverter = new SimpleObjectProperty<>();
	private ObjectProperty<Function<ModifiedObjectType, String>> refTypeConverter = new SimpleObjectProperty<>();
	private ObjectProperty<Function<Choice, String>> choiceConverter = new SimpleObjectProperty<>();
	private ObjectProperty<BiConsumer<T,Choice>> decisionHandler = new SimpleObjectProperty<>();
	private ObjectProperty<BiConsumer<T,ModificationChoice>> modDecisionHandler = new SimpleObjectProperty<>();
	private ObjectProperty<NodeWithTitle> customNode1 = new SimpleObjectProperty<>();
	private ObjectProperty<NodeWithTitle> customNode2 = new SimpleObjectProperty<>();

	private ObjectProperty<T> selectedItem = new SimpleObjectProperty<T>();
	private ObjectProperty<ComplexDataItemValue<ComplexDataItem>> showChoicesWithValue = new SimpleObjectProperty<>();
	private ObjectProperty<DataItem> showHelpFor = new SimpleObjectProperty<DataItem>();

	private ObjectProperty<WindowMode> layoutMode = new SimpleObjectProperty<>(WindowMode.EXPANDED);

	private BooleanProperty showDecisionColumn = new SimpleBooleanProperty(false);
	private BooleanProperty showStatsColumn = new SimpleBooleanProperty(true);
	private BooleanProperty showModificationsInDescription = new SimpleBooleanProperty(true);

	private transient Function<Requirement,String> reqR;
	private transient Function<Modification,String> modR;

	//-------------------------------------------------------------------
	/**
	 */
	public DataItemDetailsPane(Function<Requirement,String> reqR, Function<Modification,String> modR) {
		getStyleClass().add(DEFAULT_STYLE_CLASS);
		this.reqR = reqR;
		this.modR = modR;
		if (ResponsiveControlManager.getCurrentMode()!=null)
			layoutMode.set(ResponsiveControlManager.getCurrentMode());
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Control#createDefaultSkin()
	 */
	@Override
	public Skin<?> createDefaultSkin() {
		return new DataItemDetailsPaneSkin<T>(this,reqR,modR);
	}

	//-------------------------------------------------------------------
	public ObjectProperty<RuleSpecificCharacterObject> modelProperty() { return model;}
	public RuleSpecificCharacterObject getModel() { return model.get();}
	public DataItemDetailsPane<T> setModel(RuleSpecificCharacterObject value) { model.set(value); return this;}

//	//-------------------------------------------------------------------
//	public ObjectProperty<ComplexDataItemValue<ComplexDataItem>> showChoicesWithValueProperty() { return showChoicesWithValue;}
	public ComplexDataItemValue<ComplexDataItem> getShowChoicesWithValue() { return showChoicesWithValue.get();}
//	public DataItemDetailsPane<T> setShowChoicesWithValue(ComplexDataItemValue<ComplexDataItem> value) { showChoicesWithValue.set(value); return this;}

	//-------------------------------------------------------------------
	public ObjectProperty<T> selectedItemProperty() { return selectedItem;}
	public T getSelectedItem() { return selectedItem.get();}
	public DataItemDetailsPane<T> setSelectedItem(T value) { selectedItem.set(value); return this;}

	//-------------------------------------------------------------------
	public ObservableList<T> itemsProperty() { return items;}

	//-------------------------------------------------------------------
	public ObjectProperty<Function<T, Image>> imageConverterProperty() { return imageConverter;}
	public Function<T, Image> getImageConverter() { return imageConverter.get();}
	public DataItemDetailsPane<T> setImageConverter(Function<T, Image> value) { imageConverter.set(value); return this; }

	//-------------------------------------------------------------------
	public ObjectProperty<Function<Modification, String>> modificationConverterProperty() { return modifConverter;}
	public Function<Modification, String> getModificationConverter() { return modifConverter.get();}
	public DataItemDetailsPane<T> setModificationConverter(Function<Modification, String> value) { modifConverter.set(value); return this; }

	//-------------------------------------------------------------------
	public ObjectProperty<Function<ModifiedObjectType, String>> referenceTypeConverterProperty() { return refTypeConverter;}
	public Function<ModifiedObjectType, String> getReferenceTypeConverter() { return refTypeConverter.get();}
	public DataItemDetailsPane<T> setReferenceTypeConverter(Function<ModifiedObjectType, String> value) { refTypeConverter.set(value); return this; }

	//-------------------------------------------------------------------
	public ObjectProperty<Function<Choice, String>> choiceConverterProperty() { return choiceConverter;}
	public Function<Choice, String> getChoiceConverter() { return choiceConverter.get();}
	public DataItemDetailsPane<T> setChoiceConverter(Function<Choice, String> value) { choiceConverter.set(value); return this; }

	//-------------------------------------------------------------------
	public ObjectProperty<BiConsumer<T,Choice>> decisionHandlerProperty() { return decisionHandler;}
	public BiConsumer<T,Choice> getDecisionHandler() { return decisionHandler.get();}
	public DataItemDetailsPane<T> setDecisionHandler(BiConsumer<T,Choice> value) { decisionHandler.set(value); return this; }

	//-------------------------------------------------------------------
	public ObjectProperty<BiConsumer<T,ModificationChoice>> modDecisionHandlerProperty() { return modDecisionHandler;}
	public BiConsumer<T,ModificationChoice> getModDecisionHandler() { return modDecisionHandler.get();}
	public DataItemDetailsPane<T> setModDecisionHandler(BiConsumer<T,ModificationChoice> value) { modDecisionHandler.set(value); return this; }

	//-------------------------------------------------------------------
	public ObjectProperty<NodeWithTitle> customNode1Property() { return customNode1;}
	public NodeWithTitle getCustomNode1() { return customNode1.get();}
	public DataItemDetailsPane<T> setCustomNode1(NodeWithTitle value) { customNode1.set(value); return this;}

	//-------------------------------------------------------------------
	public ObjectProperty<NodeWithTitle> customNode2Property() { return customNode2;}
	public NodeWithTitle getCustomNode2() { return customNode2.get();}
	public DataItemDetailsPane<T> setCustomNode2(NodeWithTitle value) { customNode2.set(value); return this;}

	//--------------------------------------------------------------------
	public BooleanProperty showDecisionColumnProperty() { return showDecisionColumn; }
	public Boolean  isShowDecisionColumn() { return showDecisionColumn.get(); }
	public DataItemDetailsPane<T> setShowDecisionColumn(Boolean value) { showDecisionColumn.set(value); return this; }

	//--------------------------------------------------------------------
	public BooleanProperty showStatsColumnProperty() { return showStatsColumn; }
	public Boolean  isShowStatsColumn() { return showStatsColumn.get(); }
	public DataItemDetailsPane<T> setShowStatsColumn(Boolean value) { showStatsColumn.set(value); return this; }

	//--------------------------------------------------------------------
	public BooleanProperty showModificationsInDescriptionProperty() { return showModificationsInDescription; }
	public Boolean  isShowModificationsInDescription() { return showModificationsInDescription.get(); }
	public DataItemDetailsPane<T> setShowModificationsInDescription(Boolean value) { showModificationsInDescription.set(value); return this; }

	//-------------------------------------------------------------------
	public ObjectProperty<WindowMode> layoutModeProperty() { return layoutMode;}

	//-------------------------------------------------------------------
	public ObjectProperty<DataItem> showHelpForProperty() { return showHelpFor;}
	public DataItemDetailsPane<T> setShowHelpFor(DataItem value) { showHelpFor.set(value); return this;}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ResponsiveControl#setResponsiveMode(org.prelle.javafx.WindowMode)
	 */
	@Override
	public void setResponsiveMode(WindowMode value) {
		RPGFrameworkJavaFX.logger.log(Level.WARNING, getId()+": mode changed to "+value);
		layoutMode.set(value);
	}

}
