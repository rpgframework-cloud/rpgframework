package de.rpgframework.jfx;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.function.Supplier;

import org.prelle.javafx.SymbolIcon;

import de.rpgframework.genericrpg.NumericalValue;
import de.rpgframework.genericrpg.NumericalValueController;
import de.rpgframework.genericrpg.data.ASkillValue;
import de.rpgframework.genericrpg.data.AttributeValue;
import de.rpgframework.genericrpg.items.ItemAttributeNumericalValue;
import javafx.beans.property.ObjectProperty;
import javafx.collections.MapChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SkinBase;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.util.Callback;

public class NumericalValueFieldSkin<T,V extends NumericalValue<T>> extends SkinBase<NumericalValueField<T, V>> {

	private final static Logger logger = System.getLogger("prelle.jfx");

	private HBox layout;
	private Button dec;
	private Button inc;
//	private TextField tfValue;
	private Label lbValue;
	private int minWidthEm;
	private Integer overrideValue;

	//--------------------------------------------------------------------
	public NumericalValueFieldSkin(NumericalValueField<T, V> parent) {
		super(parent);
		getSkinnable().getProperties().addListener(new MapChangeListener<Object, Object>(){

			@Override
			public void onChanged(MapChangeListener.Change<? extends Object, ? extends Object> change) {
				logger.log(Level.WARNING, "onChanged {0}", change);
				if (String.valueOf(change.getKey()).equals(NumericalValueField.REFRESH)) {
					refresh();
			        getSkinnable().getProperties().put(NumericalValueField.REFRESH, Boolean.FALSE);
				}
			}});
		initComponents();
		initLayout();
		initStyle();
		initInteractivity();
		dec.setDisable(true);
		inc.setDisable(true);

		refresh();
	}

	//--------------------------------------------------------------------
	private void initComponents() {
		dec  = new Button(null, new SymbolIcon("remove"));
		inc  = new Button(null, new SymbolIcon("add"));
//		dec  = new Button("\uE0C6");
//		inc  = new Button("\uE0C5");
//		tfValue = new TextField();
//		tfValue.setPrefColumnCount(2);
//		tfValue.setEditable(false);
//		tfValue.setFocusTraversable(false);
		lbValue = new Label();
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		layout = new HBox();
		layout.getChildren().clear();
		layout.setAlignment(Pos.CENTER);
//		if (useLabel)
			layout.getChildren().addAll(dec, lbValue, inc);
//		else
//			getChildren().addAll(dec, tfValue, inc);
		HBox.setHgrow(lbValue, Priority.SOMETIMES);
//		HBox.setHgrow(tfValue, Priority.SOMETIMES);

//		HBox.setMargin(tfValue, new Insets(0,2,0,2));
		HBox.setMargin(lbValue, new Insets(1,2,1,2));
		HBox.setMargin(dec, new Insets(2,3,0,4));
		HBox.setMargin(inc, new Insets(2,4,0,3));

		getChildren().add(layout);
	}

	//--------------------------------------------------------------------
	private void initStyle() {
		inc.getStyleClass().add("mini-button");
		dec.getStyleClass().add("mini-button");
		lbValue.setStyle("-fx-alignment:center");
//		if (minWidthEm>0) {
////			setStyle("-fx-background-color: lime;");
//			lbValue.setStyle("-fx-min-width: "+minWidthEm+"em; -fx-alignment:center");
//			tfValue.setStyle("-fx-min-width: "+minWidthEm+"em; -fx-alignment:center");
//		} else {
////			setStyle("-fx-background-color: pink;");
//			lbValue.setStyle("-fx-pref-width: 3em");
//			tfValue.setStyle("-fx-pref-width: 3em");
//		}
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		dec.setOnAction(ev -> {
			V value = (getSkinnable().getValueFactory()!=null)?getSkinnable().getValueFactory().get():null;
			getSkinnable().getController().decrease(value);
			refresh();
			if (getSkinnable().getOnAction()!=null) getSkinnable().getOnAction().handle(ev);
		});
		inc.setOnAction(ev -> {
			V value = (getSkinnable().getValueFactory()!=null)?getSkinnable().getValueFactory().get():null;
			getSkinnable().getController().increase(value);
			refresh();
			if (getSkinnable().getOnAction()!=null) getSkinnable().getOnAction().handle(ev);
		});

		getSkinnable().controllerProperty().addListener( (ov,o,n) -> refresh());
		getSkinnable().valueFactoryProperty().addListener( (ov,o,n) -> refresh());
//		if (control!=null)
//			control.addListener( (ov,o,n) -> refresh());
//
//		tfValue.focusedProperty().addListener( (ov,o,n) -> {
//			if (n) {
//				ActionEvent ev = new ActionEvent(tfValue, null);
//				if (onAction!=null) onAction.handle(ev);
//			}
//		});
	}

	//--------------------------------------------------------------------
	public void refresh() {
//		logger.log(Level.WARNING, "refresh( {0}, ctrl={1} )", getSkinnable().getValueFactory().get(), getSkinnable().getController());
		String content = "-";
		V value = (getSkinnable().getValueFactory()!=null)?getSkinnable().getValueFactory().get():null;
		if (value!=null) {
			if (getSkinnable().getConverter()!=null) {
				content = getSkinnable().getConverter().call(value);
			} else if (value instanceof AttributeValue) {
				content = ((AttributeValue)value).getDisplayString();
			} else if (value instanceof ItemAttributeNumericalValue) {
				content = String.valueOf( ((ItemAttributeNumericalValue)value).getModifiedValue() );
			} else if (value instanceof ASkillValue) {
				content = String.valueOf( ((ASkillValue)value).getModifiedValue() );
			} else
				content = String.valueOf(value.getDistributed());
		}
//		logger.log(Level.ERROR, "value is {0}, content is {1}, converter was {2}", value, content, getSkinnable().getConverter());
		lbValue.setText(content);

		NumericalValueController<T, V> control = getSkinnable().getController();
		if (control!=null) {
			dec.setManaged(true);
			inc.setManaged(true);
			dec.setVisible(true);
			inc.setVisible(true);
			dec.setDisable(!control.canBeDecreased(value).get());
			inc.setDisable(!control.canBeIncreased(value).get());
		} else {
			dec.setManaged(false);
			inc.setManaged(false);
			dec.setVisible(false);
			inc.setVisible(false);
		}

	}

}