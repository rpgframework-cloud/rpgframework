package de.rpgframework.jfx.cells;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;

import org.controlsfx.control.GridCell;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.jfx.CharacterHandleBox;
import de.rpgframework.jfx.SelectingGridView;
import javafx.collections.ListChangeListener;
import javafx.css.PseudoClass;
import javafx.event.EventHandler;
import javafx.scene.AccessibleRole;
import javafx.scene.input.MouseEvent;

/**
 * @author prelle
 *
 */
public class CharacterGridCell extends GridCell<CharacterHandle> {
	
	private final static Logger logger = System.getLogger(CharacterGridCell.class.getPackageName());
    private static final PseudoClass PSEUDO_CLASS_SELECTED =
            PseudoClass.getPseudoClass("selected");

	private CharacterHandleBox layout;
	
	//-------------------------------------------------------------------
	public CharacterGridCell(SelectingGridView<CharacterHandle> gv) {
        setAccessibleRole(AccessibleRole.LIST_ITEM);
		layout = new CharacterHandleBox();
		initInteractivity();
		if (gv.getSelectionModel()!=null) {
			addSelectionModelListener(gv);
		}
		
		getStyleClass().add("grid-cell");
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		layout.setOnMouseClicked(new EventHandler<MouseEvent>() {
	        public void handle(MouseEvent event) {
	        	 logger.log(Level.INFO, "Event "+event.getEventType()+" from "+event.getSource());
	            // TODO Auto-generated method stub
	            if (event.getClickCount() == 1) {
	                  //do something when it's clicked
	            	logger.log(Level.DEBUG, "Clicked "+getItem());
	            	if (getGridView() instanceof SelectingGridView) {
	            		boolean oldState = ((SelectingGridView<CharacterHandle>)getGridView()).getSelectionModel().isSelected(getIndex());
	            		pseudoClassStateChanged(PSEUDO_CLASS_SELECTED, !oldState);
	            		if (oldState) {
	            			((SelectingGridView<CharacterHandle>)getGridView()).getSelectionModel().clearSelection(getIndex());
	            		} else {
	            			((SelectingGridView<CharacterHandle>)getGridView()).getSelectionModel().select(getIndex());
	            		}
	            	}
	            }
	            if (event.getClickCount() >= 2) {
	                  //do something when it's clicked
	            	logger.log(Level.DEBUG, "Double Clicked "+getItem());
	            }
	         }
	      }); 
	}
	
	//-------------------------------------------------------------------
	private void addSelectionModelListener(SelectingGridView<CharacterHandle> gv) {
		gv.getSelectionModel().getSelectedItems().addListener(new ListChangeListener<CharacterHandle>() {

			@Override
			public void onChanged(Change<? extends CharacterHandle> c) {
				CharacterHandle index = getItem();
				while (c.next()) {
					if (c.wasRemoved()) {
						if (c.getRemoved().contains(index)) {
							pseudoClassStateChanged(PSEUDO_CLASS_SELECTED, false);
						}
					}
					if (c.wasAdded()) {
						if (c.getAddedSubList().contains(index)) {
							pseudoClassStateChanged(PSEUDO_CLASS_SELECTED, true);
						}
					}
				}
			}});
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(CharacterHandle item, boolean empty) {
		super.updateItem(item, empty);
		
//		logger.log(Level.INFO, "updateItem: "+)this);
//		logger.log(Level.INFO, "updateItem: "+this.getStyleClass());
		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			layout.setHandle(item);
			setGraphic(layout);
		}
	}
	
}
