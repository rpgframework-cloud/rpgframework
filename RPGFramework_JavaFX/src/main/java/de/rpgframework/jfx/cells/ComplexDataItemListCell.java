package de.rpgframework.jfx.cells;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.function.Function;
import java.util.function.Supplier;

import org.prelle.javafx.JavaFXConstants;

import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.Possible.State;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;
import de.rpgframework.genericrpg.data.GenericCore;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.jfx.Selector;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class ComplexDataItemListCell<T extends ComplexDataItem> extends ListCell<T> {

	private final static Logger logger = System.getLogger(ComplexDataItemListCell.class.getName());

	protected Supplier<ComplexDataItemController<T, ? extends ComplexDataItemValue<T>>> controlProv;
	private Function<T, String> costStringGetter;
	private Function<Requirement, String> reqResolver;
	private Selector<T, ? extends ComplexDataItemValue<T>> selector;

	protected Label lbName;
	private Label lbCost;
	protected Label lbSource;
	protected HBox line1;
	protected VBox layout;

	//-------------------------------------------------------------------
	public ComplexDataItemListCell() {
		lbName = new Label();
		lbCost= new Label();
		line1  = new HBox(20,lbName,lbCost);
		lbSource = new Label();
		lbName.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		lbSource.getStyleClass().add(JavaFXConstants.STYLE_TEXT_TERTIARY);
		HBox.setHgrow(lbName, Priority.ALWAYS);
		lbName.setMaxWidth(Double.MAX_VALUE);

		layout = new VBox(line1, lbSource);
	}

	//-------------------------------------------------------------------
	/**
	 * Use this constructor when using the cell in a controller or generator
	 */
	public ComplexDataItemListCell(Supplier<ComplexDataItemController<T, ? extends ComplexDataItemValue<T>>> controlProv) {
		this();
		this.controlProv = controlProv;

		if (controlProv!=null) {
			this.setOnDragDetected(event -> dragStarted(event));
			this.setOnMouseClicked(event -> clicked(event));
		}
	}

	//-------------------------------------------------------------------
	/**
	 * Use this constructor when using the cell in a controller or generator
	 */
	public ComplexDataItemListCell(Supplier<ComplexDataItemController<T, ? extends ComplexDataItemValue<T>>> controlProv, Function<Requirement, String> req) {
		this();
		this.controlProv = controlProv;
		this.reqResolver = req;

		if (controlProv!=null) {
			this.setOnDragDetected(event -> dragStarted(event));
			this.setOnMouseClicked(event -> clicked(event));
		}
	}

	//-------------------------------------------------------------------
	/**
	 * Use this constructor when using the cell in a controller or generator
	 */
	public ComplexDataItemListCell(Supplier<ComplexDataItemController<T, ? extends ComplexDataItemValue<T>>> controlProv, Function<Requirement, String> req, Selector<T, ? extends ComplexDataItemValue<T>> selector) {
		this();
		this.controlProv = controlProv;
		this.reqResolver = req;
		this.selector = selector;

		if (controlProv!=null) {
			this.setOnDragDetected(event -> dragStarted(event));
			this.setOnMouseClicked(event -> clicked(event));
		}
	}

	//-------------------------------------------------------------------
	/**
	 * Use this constructor when using the cell outside a controller context
	 */
	public ComplexDataItemListCell(Function<T, String> costStringGetter) {
		this();
		this.costStringGetter = costStringGetter;

		if (controlProv!=null) {
			this.setOnDragDetected(event -> dragStarted(event));
			this.setOnMouseClicked(event -> clicked(event));
		}
	}

	//-------------------------------------------------------------------
	/**
	 * return a list of ToDoElement keys to ignore for highlighting
	 */
	protected List<String> getIgnoreKeys() {
		return List.of();
	}

	//-------------------------------------------------------------------
	public void updateItem(T item, boolean empty) {
		super.updateItem(item, empty);
		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			lbName.setText(item.getName());
			lbSource.setText(String.join(", ", GenericCore.getBestPageReferenceLongNames(item, Locale.getDefault())));
			if (controlProv!=null && controlProv.get()!=null) {
				try {
					Possible poss = controlProv.get().canBeSelected(item);
					lbName.setDisable(!poss.get());
					setUserData(!poss.get());
					ToDoElement showMess = poss.getMostSevereExcept(getIgnoreKeys());
					lbSource.setStyle((poss.get() || showMess==null)?"":"-fx-text-fill: highlight");

					if ( (poss.get() || showMess==null) && poss.getState()!=State.REQUIREMENTS_NOT_MET) {
						lbSource.setText(String.join(", ", GenericCore.getBestPageReferenceShortNames(item, Locale.getDefault())));
//					lbSource.setText(null);
					} else {
						if (poss.getState()==State.REQUIREMENTS_NOT_MET) {
							if (poss.getUnfulfilledRequirements()!=null && !poss.getUnfulfilledRequirements().isEmpty()) {
								List<String> reqStrings = new ArrayList<>();
								for (Requirement r : poss.getUnfulfilledRequirements()) {
									reqStrings.add( (reqResolver!=null)?reqResolver.apply(r):String.valueOf(r));
								}
								lbSource.setText( String.join(", ", reqStrings));
								lbSource.setStyle("-fx-text-fill: highlight");
							}
						} else if (poss.getMostSevere()!=null) {
							lbSource.setText(showMess.getMessage(Locale.getDefault()));
						}
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				lbSource.setText(String.join(", ", GenericCore.getBestPageReferenceShortNames(item, Locale.getDefault())));
				lbName.setDisable(false);
				setUserData(true);
				lbSource.setStyle("");
			}
			if (controlProv!=null && controlProv.get()!=null) {
				String costStr = controlProv.get().getSelectionCostString(item);
				if (costStr!=null) {
					lbCost.setText(costStr);
				} else {
					float cost = controlProv.get().getSelectionCost(item);
					if (String.valueOf(cost).endsWith(".0")) {
						lbCost.setText(String.valueOf((int) cost));
					} else {
						lbCost.setText(String.valueOf(cost));
					}
				}
			} else if (costStringGetter!=null) {
				lbCost.setText(costStringGetter.apply(item));
			}
			setGraphic(layout);
		}
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		logger.log(Level.WARNING, "Drag started "+event.getSource());
		Node source = (Node) event.getSource();

		/* drag was detected, start a drag-and-drop gesture*/
		/* allow any transfer mode */
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		/* Put a string on a dragboard */
		ClipboardContent content = new ClipboardContent();
		if (getItem()==null)
			return;
		content.putString(getItem().getTypeString()+":"+getItem().getId());
		db.setContent(content);

		/* Drag image */
		WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
		db.setDragView(snapshot);

		event.consume();
	}

	//-------------------------------------------------------------------
	private void clicked(MouseEvent event) {
		logger.log(Level.WARNING, "clicked {0} and selector is {1}",getItem(), selector);
		if (event.getClickCount()!=2)
			return;
		if (getItem()==null)
			return;
		logger.log(Level.INFO, "doubleclicked "+getItem());

		if (selector!=null) {
			selector.implSelect(getItem());
			logger.log(Level.ERROR, "Selected "+getItem()+"?");
		} else {
			logger.log(Level.ERROR, "No selector connected");
		}

//		ComplexDataItemController<T,? extends ComplexDataItemValue<T>> ctrl = controlProv.get();
//		T toSelect = getItem();
//		if (!ctrl.getChoicesToDecide(toSelect).isEmpty()) {
//			// Yes, user must choose
//			List<Choice> options = ctrl.getChoicesToDecide(toSelect);
//			logger.log(Level.DEBUG, "called getChoicesToDecide returns {0} choices", options.size());
//			if (optionsCallback != null) {
//				Platform.runLater(() -> {
//					logger.log(Level.DEBUG, "call getOptionCallback");
//					Decision[] decisions = optionsCallback.apply(toSelect, options);
//					if (decisions != null) {
//						logger.log(Level.WARNING, "call select(option, decision[{0}])", decisions.length);
//						OperationResult<? extends ComplexDataItemValue<T>> res = ctrl.select(toSelect, decisions);
//						if (res.wasSuccessful()) {
//							logger.log(Level.INFO, "Selecting {0} with options was successful", toSelect);
//						} else {
//							logger.log(Level.WARNING, "Selecting {0} with options failed: {1}", toSelect, res.getError());
//							AlertManager.showAlertAndCall(javafx.scene.control.Alert.AlertType.ERROR, "Failed adding", res.getError());
//						}
//					}
//				});
//			} else {
//				logger.log(Level.ERROR, "Item {0} has choices to make, but no GUI callback defined", toSelect.getId());
//				AlertManager.showAlertAndCall(javafx.scene.control.Alert.AlertType.ERROR, "Can not add via double click","Item "+toSelect.getId()+" has choices to make, but no GUI callback defined");
//			}
//		} else {
//			controlProv.get().select(getItem());
//		}
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @param optionsCallback the optionsCallback to set
//	 */
//	public void setOptionsCallback(BiFunction<T, List<Choice>, Decision[]> optionsCallback) {
//		this.optionsCallback = optionsCallback;
//	}


}
