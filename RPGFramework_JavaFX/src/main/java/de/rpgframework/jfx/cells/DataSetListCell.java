package de.rpgframework.jfx.cells;

import java.util.Locale;

import de.rpgframework.genericrpg.LicenseManager;
import de.rpgframework.genericrpg.data.CommonCharacter;
import de.rpgframework.genericrpg.data.DataSet;
import javafx.beans.property.BooleanProperty;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

public class DataSetListCell extends ListCell<DataSet> {

	private CommonCharacter<?, ?, ?, ?> model;
	private CheckBox cbSelected;
	private Label name;
	private HBox languages;
	private HBox layout;

	private boolean updating;

	//--------------------------------------------------------------------
	public DataSetListCell(CommonCharacter<?, ?, ?, ?> model) {
		this.model = model;
		cbSelected = new CheckBox();
		name = new Label();
		languages  = new HBox(5);
		name.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(name, Priority.ALWAYS);
		layout = new HBox(5, cbSelected, name, languages);
		if (model==null)
			layout.getChildren().remove(cbSelected);
	}

	//--------------------------------------------------------------------
	public BooleanProperty selectedCheckboxProperty() {
		return cbSelected.selectedProperty();
	}

	//--------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(DataSet item, boolean empty) {
		super.updateItem(item, empty);
		updating = true;
		if (empty) {
			setGraphic(null);
		} else {
			setGraphic(layout);
			name.setText(item.getName(Locale.getDefault()));
			cbSelected.setUserData(item.getID());
//			cbSelected.setSelected(model.isExplicitPluginPermitted(item.getID()));
			cbSelected.selectedProperty().addListener( (ov,o,n) -> {
				if (updating)
					return;
				if (n) {
					model.getDataSets().add(DataSetListCell.this.getItem().getID());
				} else {
					model.getDataSets().remove(DataSetListCell.this.getItem().getID());
				}
			});
			languages.getChildren().clear();
			for (Locale loc : item.getLocales()) {
				String lang = loc.getLanguage();
				Label icon = new Label("  "+lang);
				if (lang.equals("de")) {
					icon = new Label("  "+String.valueOf(Character.toChars(127465))+String.valueOf(Character.toChars(127466)));
				} else if (lang.equals("en")) {
					StringBuffer sb = new StringBuffer();
				    sb.append(Character.toChars(127482));
				    sb.append(Character.toChars(127480));
					icon = new Label("   "+String.valueOf(Character.toChars(127482))+String.valueOf(Character.toChars(127480)));
				}
				languages.getChildren().add(icon);
				if (LicenseManager.hasLicense(item, loc)) {
					icon.setStyle("-fx-font-family: \"Segoe UI Symbol\"; -fx-font-size: 150%; -fx-text-fill: green");
				} else {
					icon.setStyle("-fx-font-family: \"Segoe UI Symbol\"; -fx-font-size: 150%;");
				}

			}
		}
		updating = false;
	}
}