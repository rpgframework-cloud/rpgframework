/**
 * 
 */
package de.rpgframework.jfx.cells;

import de.rpgframework.genericrpg.chargen.ai.LevellingProfileValue;
import javafx.scene.control.ListCell;

/**
 * @author prelle
 *
 */
public class LevellingProfileValueCell extends ListCell<LevellingProfileValue> {

	//-------------------------------------------------------------------
	public LevellingProfileValueCell() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(LevellingProfileValue item, boolean empty) {
		super.updateItem(item, empty);
		
		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			setText(item.getModifyable().getName());
		}
	}
	
}
