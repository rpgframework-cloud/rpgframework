package de.rpgframework.jfx.cells;

import java.util.function.Consumer;
import java.util.function.Supplier;

import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.SymbolIcon;

import de.rpgframework.character.RuleSpecificCharacterObject;
import de.rpgframework.genericrpg.data.Choice;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class ChoiceCell extends ListCell<Choice> {
	
	private StringConverter<Choice> conv;
	private Supplier<RuleSpecificCharacterObject> modelSupp;
	private Consumer<Choice> editHandler;
	
	private Button btnEdit;
	private Label choiceText;
	private Label decisionText;
	private HBox layout;
	
	//-------------------------------------------------------------------
	public ChoiceCell(StringConverter<Choice> conv, Supplier<RuleSpecificCharacterObject> modelSupp, Consumer<Choice> callback) {
		this.conv = conv;
		this.modelSupp = modelSupp;
		this.editHandler = callback;
		if (editHandler==null)
			throw new NullPointerException("callback for editing may not be NULL");
		
		btnEdit = new Button(null, new SymbolIcon("edit"));
		choiceText = new Label();
		choiceText.setWrapText(true);
		choiceText.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		decisionText = new Label();
		
		VBox box = new VBox(5, choiceText, decisionText);
		layout = new HBox(10, btnEdit, box);
		
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void refresh() {
		RuleSpecificCharacterObject model = modelSupp.get();
		if (model==null) {
			btnEdit.setStyle("-fx-background-color: orange");
			decisionText.setText("?model missing?");
		} else {
			if (model.hasDecisionBeenMade(getItem().getUUID())) {
				btnEdit.setStyle("-fx-background-color: transparent");
			} else {
				btnEdit.setStyle("-fx-background-color: red");
			}
			if (model.getDecision(getItem().getUUID())!=null) {
				decisionText.setText(String.valueOf(model.getDecision(getItem().getUUID())));
			} else {
				decisionText.setText("?");
			}
		}
		
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnEdit.setOnAction(ev -> {
			// Call 
			editHandler.accept(getItem());
			refresh();
		});
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(Choice item, boolean empty) {
		super.updateItem(item, empty);
		
		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			choiceText.setText( (conv!=null)?conv.toString(item):String.valueOf(item) );
			refresh();
			setGraphic(layout);
		}
	}
}
