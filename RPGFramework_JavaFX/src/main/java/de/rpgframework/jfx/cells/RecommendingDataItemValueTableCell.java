package de.rpgframework.jfx.cells;

import de.rpgframework.genericrpg.chargen.RecommendationState;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.DataItemValue;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;

/**
 * @author prelle
 *
 */
public class RecommendingDataItemValueTableCell<T extends DataItem,V extends DataItemValue<T>> extends TableCell<V,RecommendationState> {

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(RecommendationState item, boolean empty) {
		super.updateItem(item, empty);
		if (empty) {
			setGraphic(null);
			setText(null);
		} else {
			if (item==null || item==RecommendationState.NEUTRAL) {
				setGraphic(null);
			} else if (item==RecommendationState.RECOMMENDED) {
				setGraphic(new Label("*"));
			} else if (item==RecommendationState.UNRECOMMENDED) {
				setGraphic(new Label("!"));
			}
		}
	}
}
