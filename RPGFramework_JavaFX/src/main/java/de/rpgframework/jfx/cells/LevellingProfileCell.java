/**
 * 
 */
package de.rpgframework.jfx.cells;

import de.rpgframework.genericrpg.chargen.ai.LevellingProfile;
import javafx.scene.control.ListCell;

/**
 * @author prelle
 *
 */
public class LevellingProfileCell extends ListCell<LevellingProfile> {

	//-------------------------------------------------------------------
	public LevellingProfileCell() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(LevellingProfile item, boolean empty) {
		super.updateItem(item, empty);
		
		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			setText(item.getName());
		}
	}
	
}
