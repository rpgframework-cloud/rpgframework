package de.rpgframework.jfx.cells;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.function.BiFunction;
import java.util.function.Supplier;

import org.controlsfx.control.action.Action;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.SymbolIcon;

import de.rpgframework.genericrpg.NumericalValueController;
import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.chargen.RecommendationState;
import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.Separator;
import javafx.scene.control.Tooltip;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;

/**
 * +-----------------------------------------------
 * | Name   : (Recom) Name       .... (Lock)
 * | Decis. : <optional>
 * | Content: <optional>
 * | Flags  :
 * +-----------------------------------------------
 * | Action :
 * +-----------------------------------------------
 *
 * @author Stefan Prelle
 */
public class ComplexDataItemValueListCell<T extends ComplexDataItem, V extends ComplexDataItemValue<T>> extends ListCell<V> {

	public final static String ICON_PEN = "\uE1C2";

	public class CellAction extends Action {
		private String icon;
		private BiFunction<CellAction,V, OperationResult<?>> handler;
		public CellAction(String name, String icon, String tooltip, BiFunction<CellAction,V, OperationResult<?>> handler) {
			super(name);
			setEventHandler( (event) -> handler.apply(this, ComplexDataItemValueListCell.this.getItem()));
			Button btn = new Button(icon);
			btn.getStyleClass().add(JavaFXConstants.STYLE_MINI_BUTTON);
			setGraphic(btn);
			if (tooltip!=null) {
				btn.setTooltip(new Tooltip(tooltip));
			}
			this.handler  = handler;
		}
		public OperationResult<?> executeAction(V data) {
			return handler.apply(this, data);
		}
	}

	private final static String NORMAL_STYLE = "complex-data-item-value-cell";

	protected final static Logger logger = System.getLogger(ComplexDataItemValueListCell.class.getPackageName());

	protected Supplier<ComplexDataItemController<T, V>> controlProvider;
	private List<CellAction> actions = new ArrayList<>();
	private List<Button> actionButtons = new ArrayList();
	protected List<Node> extraActionLineNodes = new ArrayList();

	protected HBox nameLine;
	protected Label decision;
	protected HBox decisionLine;
	protected TilePane flagLine;
	protected Separator sep = new Separator(Orientation.HORIZONTAL);
	protected HBox actionLine;

	protected VBox lines;

	protected Label lblRecom;
	protected Label name;
	protected Label lblLock;

	protected Button btnDec;
	protected Label  lblVal;
	protected Button btnInc;
	protected HBox largeDecInc;

	//-------------------------------------------------------------------
	public ComplexDataItemValueListCell(Supplier<ComplexDataItemController<T, V>> ctrlProv) {
		this.controlProvider = ctrlProv;
		initComponents();
		initStyle();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		// Name Line
		lblRecom= new Label();
		name    = new Label();
		lblLock = new Label(null, new SymbolIcon("lock"));

		// Decision Line
		decision = new Label();
		decisionLine = new HBox(decision);

		// Flag Line
		flagLine =new TilePane();

		// Action Line
		btnDec  = new Button("\uE0C6");
		lblVal  = new Label("?");
		btnInc  = new Button("\uE0C5");
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		name.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		lblVal.getStyleClass().add(JavaFXConstants.STYLE_HEADING2);
		btnDec.getStyleClass().add(JavaFXConstants.STYLE_MINI_BUTTON);
		btnInc.getStyleClass().add(JavaFXConstants.STYLE_MINI_BUTTON);

		getStyleClass().add(NORMAL_STYLE);
	}

	//-------------------------------------------------------------------
	private static Region buffer() {
		Region buf = new Region();
		buf.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(buf, Priority.ALWAYS);
		return buf;
	}

	//-------------------------------------------------------------------
	protected void initLayout() {
		// Name line
		lblLock.setMaxWidth(50);
		nameLine = new HBox(5);

		// Action Line
		actionLine = new HBox(5);
		actionLine.setMaxWidth(Double.MAX_VALUE);
		actionLine.getStyleClass().add(JavaFXConstants.STYLE_TEXT_CAPTION);


		lines = new VBox(2);
		lines.getChildren().addAll(nameLine, decisionLine, flagLine, actionLine);
		lines.getChildren().add(sep);

		largeDecInc = new HBox(2, btnDec, lblVal, btnInc);
		btnDec.setStyle("-fx-padding: 2px");
		lblVal.setStyle("-fx-padding: -5px 0 -8px 0");
		btnInc.setStyle("-fx-padding: 2px");
		largeDecInc.setStyle("-fx-padding: 0px");
		largeDecInc.setAlignment(Pos.CENTER);
		largeDecInc.setFillHeight(false);

		nameLine.setMaxWidth(Double.MAX_VALUE);
		nameLine.getChildren().addAll(lblRecom, name, buffer(), lblLock, largeDecInc);

//		nameLine.setStyle("-fx-background-color: pink");
//		flagLine.setStyle("-fx-background-color: yellow");
//		decisionLine.setStyle("-fx-background-color: brown");
//		actionLine.setStyle("-fx-background-color: green");
		setAlignment(Pos.TOP_LEFT);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnInc .setOnAction(event -> {
			logger.log(Level.DEBUG, "Increase "+this.getItem());
			ComplexDataItemController<T, V> charGen = controlProvider.get();
			OperationResult<V> result = ((NumericalValueController<T, V>)charGen).increase(this.getItem());
			if (result.wasSuccessful()) {
				updateItem(this.getItem(), false);
				ComplexDataItemValueListCell.this.getListView().refresh();
			} else {
				FlexibleApplication.getInstance().showAlertAndCall(AlertType.ERROR, "Darn!", result.getError());
			}
		});
		btnDec .setOnAction(event -> {
			logger.log(Level.DEBUG, "Decrease "+this.getItem());
			ComplexDataItemController<T, V> charGen = controlProvider.get();
			OperationResult<V> result = ((NumericalValueController<T, V>)charGen).decrease(this.getItem());
			if (result.wasSuccessful()) {
				updateItem(this.getItem(), false);
				ComplexDataItemValueListCell.this.getListView().refresh();
			} else {
				FlexibleApplication.getInstance().showAlertAndCall(AlertType.ERROR, "Darn!", result.getError());
			}
		});
		this.setOnDragDetected(event -> dragStarted(event));
	}

	//-------------------------------------------------------------------
	public ComplexDataItemValueListCell<T, V> addAction(CellAction action) {
		actions.add(action);

		Button btn = new Button(action.icon);
		btn.getStyleClass().add(JavaFXConstants.STYLE_MINI_BUTTON);
		btn.setOnAction( ev -> action.executeAction(this.getItem()));
		return this;
	}

	//-------------------------------------------------------------------
	public void addExtraActionLine(Node node) {
		extraActionLineNodes.add(node);
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		logger.log(Level.DEBUG, "drag started for "+this.getItem());
		if (this.getItem()==null)
			return;

		//		logger.log(Level.DEBUG, "canBeDeselected = "+charGen.canBeDeselected(data));
		//		logger.log(Level.DEBUG, "canBeTrashed    = "+((charGen instanceof ResourceLeveller)?((ResourceLeveller)charGen).canBeTrashed(data):true));
		//		if (!charGen.canBeDeselected(data) && !((charGen instanceof ResourceLeveller)?((ResourceLeveller)charGen).canBeTrashed(data):true))
		//			return;

		Node source = (Node) event.getSource();
		logger.log(Level.DEBUG, "drag src = "+source);

		/* drag was detected, start a drag-and-drop gesture*/
		/* allow any transfer mode */
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		/* Put a string on a dragboard */
		ClipboardContent content = new ClipboardContent();
		//        String id = "skill:"+data.getModifyable().getId()+"|desc="+data.getDescription()+"|idref="+data.getIdReference();
		String id = "qualityval:"+this.getItem().getModifyable().getId();
		content.putString(id);
		db.setContent(content);

		/* Drag image */
		WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
		db.setDragView(snapshot);

		event.consume();
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(V item, boolean empty) {
		super.updateItem(item, empty);

		if (empty || item==null) {
			setText(null);
			setGraphic(null);
		} else {
			ComplexDataItemController<T, V> charGen = controlProvider.get();
			updateNameLine(item, charGen);
			updateDecisionLine(item, charGen);
			updateContentLine(item, charGen);
			updateFlagsLine(item);
			updateDecInc(item, charGen);
			updateActions(item, charGen);

//			StackPane stack = new StackPane(lines, largeDecInc);
//			stack.setStyle("-fx-background-color: white");
//			largeDecInc.setStyle("-fx-background-color: rgba(0,255,0,0.7)");
//			StackPane.setAlignment(largeDecInc, Pos.TOP_RIGHT);
			setGraphic(lines);
		}
	}


	//-------------------------------------------------------------------
	@SuppressWarnings("incomplete-switch")
	private void updateNameLine(V item, ComplexDataItemController<T, V> ctrl) {
		// Update recommendation
		RecommendationState recom = (ctrl!=null)?ctrl.getRecommendationState(item):null;
		if (recom==null || recom==RecommendationState.NEUTRAL) {
			lblRecom.setVisible(false);
			lblRecom.setManaged(false);
		} else {
			switch (recom) {
			case STRONGLY_RECOMMENDED:
				lblRecom.setText("*");
				lblRecom.setStyle("-fx-text-fill: green");
				break;
			case RECOMMENDED:
				lblRecom.setText("+");
				lblRecom.setStyle("-fx-text-fill: green");
				break;
			case UNRECOMMENDED:
				lblRecom.setText("-");
				lblRecom.setStyle("-fx-text-fill: red");
				break;
			}
		}

		// Update name
		name.setText(item.getNameWithoutRating());

		// Update lock symbol
		if (ctrl!=null) {
			Possible removeable = ctrl.canBeDeselected(item);
			if (removeable!=null) {
				lblLock.setVisible( !removeable.get());
				if (removeable.get()) { lblLock.setTooltip(null); }
				else {
					lblLock.setTooltip(new Tooltip("Auto-Injected"));
				}
			} else {
				lblLock.setVisible(false);
				logger.log(Level.ERROR, "Calling "+ctrl.getClass().getSimpleName()+".canBeDeselected returned NULL");
			}
		} else {
			lblLock.setVisible(false);
		}
	}

	//-------------------------------------------------------------------
	protected void updateDecisionLine(V item, ComplexDataItemController<T, V> ctrl) {
		// Only show description when there are decisions made
		decisionLine.setVisible( !item.getDecisions().isEmpty() );
		decisionLine.setManaged( !item.getDecisions().isEmpty() );
		if (ctrl!=null) {
			decision.setText(item.getDecisionString(Locale.getDefault(), ctrl.getModel()));
		}
	}

	//-------------------------------------------------------------------
	private void updateContentLine(V item, ComplexDataItemController<T, V> ctrl) {
		Parent content = getContentNode(item);
		if (content!=null) {
			lines.getChildren().setAll(nameLine,decisionLine,content,flagLine,actionLine,sep);
		} else {
			lines.getChildren().retainAll(nameLine,decisionLine,flagLine,actionLine,sep);
		}
	}

	//-------------------------------------------------------------------
	private void updateFlagsLine(V item) {
		flagLine.getChildren().clear();
		for (String flag : getFlagsStringsToShow(item)) {
			Label lbFlag = new Label(flag);
			lbFlag.getStyleClass().add("item-flag");
			flagLine.getChildren().add(lbFlag);
		}
		flagLine.setManaged(!flagLine.getChildren().isEmpty());
		flagLine.setVisible(!flagLine.getChildren().isEmpty());
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	private void updateDecInc(V item, ComplexDataItemController<T, V> ctrl) {
		T data = item.getModifyable();

		lblVal.setText(" "+String.valueOf(item.getDistributed())+" ");

		if (ctrl instanceof NumericalValueController && data.hasLevel()) {
			largeDecInc.setVisible(true);
			largeDecInc.setManaged(true);
			lblVal.setText(" "+String.valueOf(((NumericalValueController<T, V>) ctrl).getValue(item))+" ");
			btnDec.setDisable(!((NumericalValueController<T, V>) ctrl).canBeDecreased(item).get());
			btnInc.setDisable(!((NumericalValueController<T, V>) ctrl).canBeIncreased(item).get());
		} else {
			largeDecInc.setVisible(false);
			largeDecInc.setManaged(false);
		}
	}

	//-------------------------------------------------------------------
	protected boolean canPerformAction(V item, CellAction action) {
		return true;
	}

	//-------------------------------------------------------------------
	private void updateActions(V item, ComplexDataItemController<T, V> ctrl) {
		List<Button> replaceWith = new ArrayList<>();
		for (CellAction action : actions) {
			if (canPerformAction(item, action))	{
				replaceWith.add( getActionButton(action, item) );
			}
		}

		actionButtons.clear();
		actionButtons.addAll(replaceWith);
		actionLine.getChildren().setAll(replaceWith);

		if (!extraActionLineNodes.isEmpty()) {
			Region buf = new Region();
			buf.setMaxWidth(Double.MAX_VALUE);
			actionLine.getChildren().add(buf);
			actionLine.getChildren().addAll(extraActionLineNodes);
		}
	}

	//-------------------------------------------------------------------
	private Button getActionButton(CellAction action, V item) {
		Button button = new Button(action.getText(), action.getGraphic());
		button.getStyleClass().add(JavaFXConstants.STYLE_MINI_BUTTON);
		if (action.getGraphic()!=null)
			button.setText(null);
		if (action.getLongText()!=null) {
			button.setTooltip(new Tooltip(action.getLongText()));
		}
		button.setOnAction(ev -> action.executeAction(item));
		return button;
	}

	//-------------------------------------------------------------------
	protected Parent getContentNode(V ref) {
		return null;
	}
//
//	//-------------------------------------------------------------------
//	protected boolean canBeEdited(V ref) {
//		return false;
//	}
//
//	//-------------------------------------------------------------------
//	protected void editClicked(V ref) {
//		logger.log(Level.WARNING, "TODO: editClicked for "+getClass());
//
//		BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 0, "The developer forgot to support editing '"+getItem().getResolved().getTypeString()+"' or hide the edit button :)");
//	}

	//-------------------------------------------------------------------
	/**
	 * Return human-readable names of the flags that should show up on the
	 * cell
	 */
	public List<String> getFlagsStringsToShow(V item) {
		return List.of();
	}

}
