package de.rpgframework.jfx.cells;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Locale;
import java.util.function.Supplier;

import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.SymbolIcon;

import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.genericrpg.Possible;
import de.rpgframework.genericrpg.chargen.SelectedValueController;
import de.rpgframework.genericrpg.data.CommonCharacter;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;
import de.rpgframework.genericrpg.data.DataItem;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.Separator;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class SelectedValueListCell<V extends ComplexDataItemValue<? extends DataItem>> extends ListCell<V> {

	private final static String NORMAL_STYLE = "quality-cell";

	protected final static Logger logger = System.getLogger(SelectedValueListCell.class.getPackageName());

	protected Supplier<SelectedValueController<V>> controlProvider;
	protected CommonCharacter<?, ?, ?, ?> model;

	protected HBox layout;
	protected VBox bxCenter;
	protected Label name;
	private Label total;
	protected Label lblLock;
	private ImageView imgRecommended;

	protected Separator sep = new Separator(Orientation.HORIZONTAL);

	protected Label  lbDescr;

	protected HBox line1, line2;

	//-------------------------------------------------------------------
	public SelectedValueListCell(Supplier<SelectedValueController<V>> ctrlProv, CommonCharacter<?, ?, ?, ?> model) {
		this.controlProvider = ctrlProv;
		this.model = model;

		// Content
		layout  = new HBox(5);
		name    = new Label();
		name.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		total   = new Label();
		lbDescr = new Label();

		// Recommended icon
		imgRecommended = new ImageView();
//		imgRecommended = new ImageView(new Image(SR6Constants.class.getResourceAsStream("images/recommendation.png")));
		imgRecommended.setFitHeight(16);
		imgRecommended.setFitWidth(16);
		lblLock = new Label(null, new SymbolIcon("lock"));
		lblLock.setMaxWidth(50);

		initStyle();
		initLayout();
		initInteractivity();

		getStyleClass().add("qualityvalue-list-cell");
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		total.getStyleClass().add(JavaFXConstants.STYLE_TEXT_SECONDARY);
		name.setStyle("-fx-font-weight: bold");

		getStyleClass().add(NORMAL_STYLE);
	}

	//-------------------------------------------------------------------
	protected void initLayout() {
		Region buf = new Region();
		buf.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(buf, Priority.ALWAYS);
		line1 = new HBox(10);
		line1.getChildren().addAll(name, buf, lblLock);

		line2 = new HBox(5);
		line2.getChildren().addAll(lbDescr);
		line2.setAlignment(Pos.CENTER_LEFT);

		bxCenter = new VBox(2);
		bxCenter.getChildren().addAll(line1, line2);
		bxCenter.getChildren().add(sep);

		layout.getChildren().addAll(bxCenter);
		layout.setFillHeight(false);
		layout.setAlignment(Pos.BOTTOM_CENTER);

		bxCenter.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(bxCenter, Priority.ALWAYS);


		setAlignment(Pos.CENTER);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		this.setOnDragDetected(event -> dragStarted(event));
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		logger.log(Level.DEBUG, "drag started for "+this.getItem());
		if (this.getItem()==null)
			return;

		//		logger.log(Level.DEBUG, "canBeDeselected = "+charGen.canBeDeselected(data));
		//		logger.log(Level.DEBUG, "canBeTrashed    = "+((charGen instanceof ResourceLeveller)?((ResourceLeveller)charGen).canBeTrashed(data):true));
		//		if (!charGen.canBeDeselected(data) && !((charGen instanceof ResourceLeveller)?((ResourceLeveller)charGen).canBeTrashed(data):true))
		//			return;

		Node source = (Node) event.getSource();
		logger.log(Level.DEBUG, "drag src = "+source);

		/* drag was detected, start a drag-and-drop gesture*/
		/* allow any transfer mode */
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		/* Put a string on a dragboard */
		ClipboardContent content = new ClipboardContent();
		//        String id = "skill:"+data.getModifyable().getId()+"|desc="+data.getDescription()+"|idref="+data.getIdReference();
		String id = "qualityval:"+this.getItem().getModifyable().getId();
		content.putString(id);
		db.setContent(content);

		/* Drag image */
		WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
		db.setDragView(snapshot);

		event.consume();
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(V item, boolean empty) {
		super.updateItem(item, empty);

		if (empty || item==null) {
			setText(null);
			setGraphic(null);
		} else {
			SelectedValueController<V> charGen = controlProvider.get();

			name.setText(item.getNameWithoutRating());

			// Only show edit button, when there are choices to make
			//btnEdit.setVisible( !data.getChoices().isEmpty() );
			// Only show description when there are decisions made
			lbDescr.setVisible( !item.getDecisions().isEmpty() );
			lbDescr.setManaged( !item.getDecisions().isEmpty() );
			lbDescr.setText(item.getDecisionString(Locale.getDefault(), model));

//			imgRecommended.setVisible(charGen.isRecommended(item.getModifyable()));
			Possible removeable = charGen.canBeDeselected(item);
			if (removeable!=null) {
				lblLock.setVisible( !removeable.get());
				if (removeable.get()) { lblLock.setTooltip(null); }
				else {
					lblLock.setTooltip(new Tooltip("Auto-Injected"));
				}
			}

			setGraphic(layout);
		}
	}

	//-------------------------------------------------------------------
	protected void editClicked(V ref) {
		logger.log(Level.WARNING, "TODO: editClicked for "+getClass());

		BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 0, "The developer forgot to support editing '"+getItem().getResolved().getTypeString()+"' or hide the edit button :)");
	}

}
