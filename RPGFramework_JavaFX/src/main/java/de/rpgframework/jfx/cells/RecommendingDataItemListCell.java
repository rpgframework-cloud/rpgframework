package de.rpgframework.jfx.cells;

import de.rpgframework.genericrpg.chargen.RecommendationState;
import de.rpgframework.genericrpg.chargen.RecommendingController;
import de.rpgframework.genericrpg.data.DataItem;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;

/**
 * @author prelle
 *
 */
public class RecommendingDataItemListCell<T extends DataItem> extends ListCell<T> {

	private RecommendingController<T> controller;
	
	//-------------------------------------------------------------------
	public RecommendingDataItemListCell(RecommendingController<T> controller) {
		this.controller = controller;
	}

	public void updateItem(T item, boolean empty) {
		super.updateItem(item, empty);
		if (empty) {
			setGraphic(null);
			setText(null);
		} else {
			setText(item.getName());
			RecommendationState state = controller.getRecommendationState(item);
			if (state==null || state==RecommendationState.NEUTRAL) {
				setGraphic(null);
			} else if (state==RecommendationState.RECOMMENDED) {
				setGraphic(new Label("*"));
			} else if (state==RecommendationState.UNRECOMMENDED) {
				setGraphic(new Label("!"));
			}
		}
	}
}
