package de.rpgframework.jfx.cells;

import java.text.DateFormat;
import java.util.ResourceBundle;

import org.prelle.javafx.JavaFXConstants;

import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.genericrpg.HistoryElement;
import javafx.geometry.Orientation;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.Separator;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class HistoryElementListCell extends ListCell<HistoryElement> {
	
	private DateFormat FORMAT = DateFormat.getDateInstance(DateFormat.MEDIUM);
	
	private Label lbTitle;
	private Label lbGamemaster; 
	private Label lbRealDate;
	
	private TilePane ruleData;
	private VBox layout;

	//-------------------------------------------------------------------
	public HistoryElementListCell(ResourceBundle res) {
		initComponents();
		initLayout();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		lbTitle = new Label();
		lbTitle.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		lbGamemaster = new Label();
		lbGamemaster.getStyleClass().add(JavaFXConstants.STYLE_TEXT_SECONDARY);
		lbRealDate   = new Label();
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		layout = new VBox(2);
		// Line 1
		layout.getChildren().add(lbTitle);
		// Line 2
		Region buf = new Region();
		buf.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(buf, Priority.ALWAYS);
		HBox line2 = new HBox(5, lbRealDate, buf, lbGamemaster);
		layout.getChildren().add(line2);
		// Line 3
		ruleData = new TilePane(10,5);
		layout.setFillWidth(true);
		layout.getChildren().add(ruleData);
		layout.getChildren().add(new Separator(Orientation.HORIZONTAL));
	}

	//-------------------------------------------------------------------
	protected void updateRuleData(TilePane box, HistoryElement item) {
		box.getChildren().clear();
		box.getChildren().add(new Label(item.getTotalExperience()+""));
		box.getChildren().add(new Label(item.getTotalMoney()+""));
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(HistoryElement item, boolean empty) {
		super.updateItem(item, empty);
		if (empty) {
			setGraphic(null);
			setText(null);			
		} else {
			lbTitle.setText(item.getName());
			lbGamemaster.setText(String.join(",", item.getGamemasters()));
			lbRealDate.setText(FORMAT.format(item.getStart()));
			updateRuleData(ruleData, item);
			
			setGraphic(layout);
		}
	}

}
