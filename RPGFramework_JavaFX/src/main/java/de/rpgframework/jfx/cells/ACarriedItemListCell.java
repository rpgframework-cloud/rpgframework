package de.rpgframework.jfx.cells;

import java.lang.System.Logger.Level;
import java.util.function.Supplier;

import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.genericrpg.NumericalValue;
import de.rpgframework.genericrpg.NumericalValueController;
import de.rpgframework.genericrpg.chargen.ComplexDataItemController;
import de.rpgframework.genericrpg.chargen.OperationResult;
import de.rpgframework.genericrpg.items.CarriedItem;
import de.rpgframework.genericrpg.items.PieceOfGear;
import javafx.scene.control.Label;

/**
 *
 */
public abstract class ACarriedItemListCell<T extends PieceOfGear<?, ?, ?, ?>> extends ComplexDataItemValueListCell<T, CarriedItem<T>> {

	protected Label lbValue;

	//-------------------------------------------------------------------
	public ACarriedItemListCell(Supplier<ComplexDataItemController<T, CarriedItem<T>>> equipCtrl) {
		super( equipCtrl);
//		addAction(new CellAction(ICON_PEN, "tooltip", (act,item) -> editClicked(item)));
		addAction(new CellAction("Edit",ICON_PEN, "tooltip", (act,item) -> editClicked(item)));

		lbValue = new Label();
		addExtraActionLine(lbValue);

		setStyle("-fx-max-width: 22em");
	}

	//-------------------------------------------------------------------
	public abstract int getSinglePrice(CarriedItem<T> item);

	//-------------------------------------------------------------------
	public abstract String getModificationSourceString(Object modSource);

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(CarriedItem<T> item, boolean empty) {
		super.updateItem(item, empty);

		if (item != null) {
			int price = getSinglePrice(item) * (item.getCount()>1?item.getCount():1);
			lbValue.setText("$" + price);
			// Countable
			if (item.getResolved().isCountable() || item.getCount()>1) {
				lblVal.setText(String.valueOf(item.getCount()));
				largeDecInc.setVisible(true);
				largeDecInc.setManaged(true);
				btnDec.setDisable(!((NumericalValueController<T, NumericalValue<T>>)controlProvider.get()).canBeDecreased(item).get());
				btnInc.setDisable(!((NumericalValueController<T, NumericalValue<T>>)controlProvider.get()).canBeIncreased(item).get());
			} else {
				largeDecInc.setVisible(false);
				largeDecInc.setManaged(false);
			}
		}
	}

	//-------------------------------------------------------------------
	protected OperationResult<CarriedItem<T>> editClicked(CarriedItem<T> ref) {
		logger.log(Level.WARNING, "TODO: editClicked for "+getClass());

		BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 0, "The developer forgot to support editing '"+getItem().getResolved().getTypeString()+"' or hide the edit button :)");
		return new OperationResult<CarriedItem<T>>(ref);
	}

}
