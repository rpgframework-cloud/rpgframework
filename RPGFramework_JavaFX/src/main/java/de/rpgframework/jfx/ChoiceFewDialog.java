package de.rpgframework.jfx;

import java.lang.System.Logger.Level;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.OptionalNodePane;

import de.rpgframework.genericrpg.chargen.PartialController;
import de.rpgframework.genericrpg.data.Choice;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.Decision;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;

/**
 * Select one from a list of 2-3 choices
 * @author prelle
 *
 */
public class ChoiceFewDialog extends ManagedDialog {

	private String modText;
	private DataItem decideFor;
	private UUID choice;
	private List<?> options;
	private PartialController<?> control;
	private OptionalNodePane pane;
	private List<RadioButton> buttons;
	private ToggleGroup toggleGroup;

	private Decision decision;

	//-------------------------------------------------------------------
	public ChoiceFewDialog(DataItem decideFor, UUID choice, String modText, List<?> options, PartialController<?> control) {
		this.decideFor = decideFor;
		this.modText = modText;
		this.choice  = choice;
		this.options = options;
		this.control = control;
		if (control==null) throw new NullPointerException();
		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		toggleGroup = new ToggleGroup();
		buttons = new ArrayList<>();
		for (Object resolved : options) {
			String name = (resolved instanceof DataItem)?((DataItem)resolved).getName():String.valueOf(resolved);
			RadioButton cb = new RadioButton(name);
			cb.getStyleClass().add("base");
			toggleGroup.getToggles().add(cb);
			cb.setUserData(resolved);
			buttons.add(cb);
		}
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label lbModText = new Label(modText);

		VBox layout = new VBox(5, lbModText);
		for (RadioButton cb : buttons) {
			layout.getChildren().add(cb);
			Object resolved = cb.getUserData();
			if (resolved!=null && resolved instanceof DataItem) {
				Label desc = new Label( ((DataItem)resolved).getDescription() );
				desc.setWrapText(true);
				desc.setStyle("-fx-max-width: 35em");
				layout.getChildren().add(desc);
			}
		}

		super.setContent(layout);
		super.setTitle(decideFor.getName());
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		for (RadioButton btn : buttons) {
			btn.setOnAction(ev -> {
				Object resolved = btn.getUserData();
				RPGFrameworkJavaFX.logger.log(Level.WARNING, "Resolved {0}", resolved);
				String key = String.valueOf(resolved);
				if (resolved instanceof Enum) {
					key = ((Enum)resolved).name();
				} else if (resolved instanceof DataItem) {
					key = ((DataItem)resolved).getId();
				}

				RPGFrameworkJavaFX.logger.log(Level.WARNING, "Decide {0} for {1} in controller {2}", key, choice, control);
				decision = new Decision(choice, key);

				((PartialController<DataItem>)control).decide(decideFor, choice, decision);
			});

		}
	}

	//-------------------------------------------------------------------
	/**
	 * Return the decision that has been already send to the controller
	 */
	public Decision getDecision() {
		return decision;
	}

}
