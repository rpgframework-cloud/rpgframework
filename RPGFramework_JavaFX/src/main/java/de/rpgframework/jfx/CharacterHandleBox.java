package de.rpgframework.jfx;

import java.io.ByteArrayInputStream;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.ResourceBundle;

import org.prelle.javafx.AcrylicStackPane;
import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.SymbolIcon;

import de.rpgframework.ResourceI18N;
import de.rpgframework.character.Attachment;
import de.rpgframework.character.Attachment.Format;
import de.rpgframework.character.Attachment.Type;
import de.rpgframework.character.CharacterHandle;
import de.rpgframework.character.CharacterProvider;
import de.rpgframework.character.CharacterProviderLoader;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class CharacterHandleBox extends AcrylicStackPane {

	private final static ResourceBundle RES = ResourceBundle.getBundle(CharacterHandleBox.class.getName());

	public final static Logger logger = System.getLogger(RPGFrameworkJFXConstants.BASE_LOGGER_NAME);

	private CharacterHandle handle;

	private ImageView iView;
	private Label lbName;
	private Label lbDescription;
	private Button btnOpen;
	private Button btnDelete;
	private Button btnExport;
	private Button btnOpenDir;

	private HBox layout;
	private Label mode;

	//-------------------------------------------------------------------
	public CharacterHandleBox() {

		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		iView = new ImageView();
		iView.setFitWidth(96);
		iView.setFitHeight(96);
		iView.setImage(new Image(CharacterHandleBox.class.getResourceAsStream("Person.png")));

		lbName = new Label("Empty");
		lbName.getStyleClass().add(JavaFXConstants.STYLE_HEADING4);

		lbDescription = new Label("Empty");
		lbDescription.getStyleClass().add(JavaFXConstants.STYLE_TEXT_SECONDARY);

		btnOpen = new Button(null, new SymbolIcon("edit"));
		btnOpen.getStyleClass().add("app-bar-button");
		btnOpen.setTooltip(new Tooltip(ResourceI18N.get(RES, "tooltip.open")));
		btnDelete = new Button(null, new SymbolIcon("delete"));
		btnDelete.getStyleClass().add("app-bar-button");
		btnDelete.setTooltip(new Tooltip(ResourceI18N.get(RES, "tooltip.delete")));
		btnExport = new Button(null, new SymbolIcon("print"));
		btnExport.getStyleClass().add("app-bar-button");
		btnExport.setTooltip(new Tooltip(ResourceI18N.get(RES, "tooltip.export")));
		btnOpenDir = new Button(null, new SymbolIcon("folder"));
		btnOpenDir.getStyleClass().add("app-bar-button");
		btnOpenDir.setTooltip(new Tooltip(ResourceI18N.get(RES, "tooltip.openDir")));
		if ("android".equals(System.getProperty("javafx.platform"))) {
			btnOpenDir.setVisible(false);
		}
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		layout = new HBox(10);

		Region spacing = new Region();
		spacing.setMaxWidth(Double.MAX_VALUE);
		HBox bxActions = new HBox(btnOpen, btnExport, btnOpenDir, spacing, btnDelete);
		HBox.setHgrow(spacing, Priority.ALWAYS);

		Separator sep = new Separator(Orientation.HORIZONTAL);
		Region buf = new Region();
		buf.setMaxHeight(Double.MAX_VALUE);

		VBox content = new VBox(lbName, lbDescription, buf, sep, bxActions);
		VBox.setVgrow(buf, Priority.ALWAYS);

		layout.getChildren().addAll(iView, content);
		HBox.setHgrow(content, Priority.ALWAYS);
		content.setMaxWidth(Double.MAX_VALUE);
		content.setMaxHeight(Double.MAX_VALUE);

		setMaxWidth(Double.MAX_VALUE);

		getChildren().add(layout);

		// Career or Chargen
		mode = new Label("?");
		mode.getStyleClass().add("mini-button");
		mode.setStyle("-fx-text-fill: highlight; -fx-font-size: 200%; ");
		getChildren().add(mode);
		StackPane.setAlignment(mode, Pos.TOP_RIGHT);

		this.setMaxWidth(330);
		this.setMaxHeight(96);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnDelete.setOnAction(ev -> logger.log(Level.INFO, "Delete"));
	}

	//-------------------------------------------------------------------
	public void setOnDelete(EventHandler<ActionEvent> handler) {
		btnDelete.setOnAction(handler);
	}

	//-------------------------------------------------------------------
	public void setOnOpen(EventHandler<ActionEvent> handler) {
		btnOpen.setOnAction(handler);
	}

	//-------------------------------------------------------------------
	public void setOnExport(EventHandler<ActionEvent> handler) {
		btnExport.setOnAction(handler);
	}

	//-------------------------------------------------------------------
	public void setOnOpenDir(EventHandler<ActionEvent> handler) {
		btnOpenDir.setOnAction(handler);
	}

	//-------------------------------------------------------------------
	public void setImage(Image image) {
		iView.setImage(image);
	}

	//-------------------------------------------------------------------
	public void setHandle(CharacterHandle value) {
		this.handle = value;
		lbName.setText(value.getName());
//		System.err.println("CharacterHandleBox: call getShortDescription on "+value);
		lbDescription.setText(value.getShortDescription());
		CharacterProvider prov = CharacterProviderLoader.getCharacterProvider();
		byte[] imageBytes = null;
		try {
			Attachment attach = prov.getFirstAttachment(value, Type.CHARACTER, Format.IMAGE);
			if (attach!=null) {
				imageBytes = attach.getData();
			}
		} catch (Exception e) {
			logger.log(Level.ERROR, "Failed loading image attachment",e);
		}
		if (imageBytes==null) {
			iView.setImage(new Image(CharacterHandleBox.class.getResourceAsStream("Person.png")));
		} else {
			iView.setImage(new Image(new ByteArrayInputStream(imageBytes)));
		}

		if (handle.getCharacter()!=null) {
			mode.setText(  handle.getCharacter().isInCareerMode()?"\uD83D\uDCB9":"\uD83D\uDC76");
			mode.setTooltip(new Tooltip(ResourceI18N.get(RES, handle.getCharacter().isInCareerMode()?"tooltip.career":"tooltip.chargen")));

			btnExport.setVisible(true);
		} else {
			btnExport.setVisible(false);
		}
	}

}
