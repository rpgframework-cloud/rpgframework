/**
 *
 */
package de.rpgframework.jfx.wizard;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.chargen.CharacterGenerator;
import de.rpgframework.genericrpg.chargen.ai.LevellingProfile;
import de.rpgframework.genericrpg.chargen.ai.LevellingProfileValue;
import de.rpgframework.jfx.SelectionControllerNode;
import de.rpgframework.jfx.cells.LevellingProfileCell;
import de.rpgframework.jfx.cells.LevellingProfileValueCell;
import javafx.scene.image.Image;

/**
 * @author prelle
 *
 */
public class WizardPageProfiles extends WizardPage {

	private final static Logger logger = System.getLogger("splittermond.gen.wizard");

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(WizardPageProfiles.class.getPackageName()+".WizardPages");

	private CharacterGenerator charGen;

	private SelectionControllerNode<LevellingProfile, LevellingProfileValue> controller;
//	private OptionalDescriptionPane contentWithDescr;

	//-------------------------------------------------------------------
	public WizardPageProfiles(Wizard wizard, CharacterGenerator charGen) {
		super(wizard);
		if (charGen==null)
			throw new NullPointerException();
		this.charGen = charGen;
		initComponents();
		initLayout();
		initStyle();
		initInteractivity();

		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(ResourceI18N.get(RES,"wizard.profiles.title"));

		controller = new SelectionControllerNode<LevellingProfile, LevellingProfileValue>(charGen.getProfileController());
		controller.setAvailableCellFactory(lv -> new LevellingProfileCell());
		controller.setSelectedCellFactory(lv -> new LevellingProfileValueCell());
	}

	//-------------------------------------------------------------------
	private void initLayout() {
//		lvAvailable.setStyle("-fx-min-width: 25em;  -fx-pref-width: 35em");
//		lvSelected.setStyle("-fx-min-width: 25em;  -fx-pref-width: 35em");
//
//		HBox content = new HBox(20);
//		content.setMaxHeight(Double.MAX_VALUE);
//		content.getChildren().addAll(lvAvailable, lvSelected);
////		content.setStyle("-fx-max-width: 35em;  -fx-pref-width: 35em");
//		setImageInsets(new Insets(-40,0,0,0));

//		contentWithDescr = new OptionalDescriptionPane(controller, new Label());
		setContent(controller);
//
//		// Page Image
//		setImageInsets(new Insets(-40,0,0,0));
		Image img = null;
		String fname = "wizard/Attribute.jpg";
		logger.log(Level.DEBUG, "Load "+fname);
//		InputStream in = SpliMoCharGenJFXConstants.class.getResourceAsStream(fname);
//		if (in!=null) {
//			img = new Image(in);
//		} else
//			logger.warn("Missing image at "+fname);
//		setImage(img);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		controller.selectedProperty();
//		bxPrimary.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
////			bxSecondary.getSelectionModel().clearSelection();
//			refreshDescription(n);
//			});
//		bxSecondary.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
////			bxPrimary.getSelectionModel().clearSelection();
//			refreshDescription(n);
//			});
	}

	//-------------------------------------------------------------------
	private void refresh() {
		controller.refresh();
//		lvAvailable.getItems().clear();
//		lvAvailable.getItems().addAll(AITool.getProfiles());
//		lvSelected.getItems().clear();
////		lvSelectProfiles.getItems().addAll(SplitterMondCore.getSkills());
	}

}
