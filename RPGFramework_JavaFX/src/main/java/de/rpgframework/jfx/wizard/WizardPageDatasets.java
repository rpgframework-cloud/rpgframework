package de.rpgframework.jfx.wizard;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.OptionalNodePane;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.chargen.DataSetMode;
import de.rpgframework.genericrpg.chargen.IGeneratorWrapper;
import de.rpgframework.genericrpg.data.CommonCharacter;
import de.rpgframework.genericrpg.data.CommonCharacter.DataSetControl;
import de.rpgframework.genericrpg.data.DataSet;
import de.rpgframework.jfx.GenericDescriptionVBox;
import de.rpgframework.jfx.cells.DataSetListCell;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 *
 */
public class WizardPageDatasets extends WizardPage {

	private final static Logger logger = System.getLogger(WizardPageDatasets.class.getPackageName());
	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(WizardPageDatasets.class.getPackageName()+".WizardPages");

	private DataSet core;
	private List<DataSet> available;
	private IGeneratorWrapper<?,?,?> charGen;

	private ChoiceBox<DataSetMode> cbMode;
	private ListView<DataSet> listSelection;
	private OptionalNodePane optional;
	private GenericDescriptionVBox descr;

	//-------------------------------------------------------------------
	public WizardPageDatasets(Wizard wizard, IGeneratorWrapper<?,?,?> model, List<DataSet> sets, DataSetMode...modes) {
		super(wizard);
		setTitle(ResourceI18N.get(RES,"wizard.datasets.title"));
		this.charGen   = model;
		for (DataSet set : sets) {
			if (set.getID().equals("CORE")) {
				core = set;
				break;
			}
		}
		this.available = sets.stream().filter(s -> s!=core).collect(Collectors.toList());
		Collections.sort(available, new Comparator<DataSet>() {

			@Override
			public int compare(DataSet ds1, DataSet ds2) {
				try {
					int i = Integer.compare(ds1.getType().ordinal(), ds2.getType().ordinal());
					if (i!=0) return i;
					i = Integer.compare(ds1.getReleased(), ds2.getReleased());
					if (i!=0) return i;
					return ds1.getName(Locale.getDefault()).compareTo(ds2.getName(Locale.getDefault()));
				} catch (Exception e) {
					logger.log(Level.ERROR, "Error comparing DataSet", e);
				}
				return 0;
			}
		});

		initComponents(modes);
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents(DataSetMode...modes) {
		Label hdSource = new Label(ResourceI18N.get(RES,"wizard.datasets.available"));
		Label hdTarget = new Label(ResourceI18N.get(RES,"wizard.datasets.selected"));
		hdSource.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		hdTarget.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		cbMode = new ChoiceBox<>(FXCollections.observableArrayList(modes));
		cbMode.setConverter(new StringConverter<DataSetMode>() {
			public String toString(DataSetMode mode) {
				return (mode==null)?"?":ResourceI18N.get(RES,"wizard.datasets.mode."+mode.name().toLowerCase());
			}
			public DataSetMode fromString(String val) {return null; }
		});
		listSelection = new ListView<DataSet>() {};
		listSelection.getItems().setAll(available);
		//listSelection.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
//		listSelection.setCellFactory(lv -> new ListCell<DataSet>() {
//			{
//				selectedProperty().addListener( (ov,o,n) -> {if (n==true) showHelpFor(getItem());});
//			}
//			public void updateItem(DataSet item, boolean empty) {
//				super.updateItem(item, empty);
//				if (!empty) {
//					setText(item.getName(Locale.getDefault()));
//				} else {
//					setText(null);
//				}
//			}
//
//		});
		listSelection.setCellFactory(lv -> new DataSetListCell((CommonCharacter) charGen.getModel()));
//		listSelection.setSourceHeader(hdSource);
//		listSelection.setTargetHeader(hdTarget);
//		listSelection.getSourceItems().addAll(available);
		listSelection.setStyle("-fx-max-width: 50em");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label lbDescr  = new Label(ResourceI18N.get(RES,"wizard.datasets.descr"));
		lbDescr.setWrapText(true);
		Label lbMode = new Label(ResourceI18N.get(RES, "wizard.datasets.mode"));
		HBox bxModeLine = new HBox(10, lbMode, cbMode);
		bxModeLine.setAlignment(Pos.CENTER_LEFT);

		VBox content = new VBox(20, lbDescr, bxModeLine, listSelection);
		content.setId("WizardPageDataSets.content");

		descr = new GenericDescriptionVBox(null, null) ;
		descr.setShowModificationsInDescription(false);
		optional = new OptionalNodePane(content,descr);
		optional.setId("optional-node-pane-dataset");
		optional.setUseScrollPane(false);
		setContent(optional);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		listSelection.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			showHelpFor(n);
		});
//		listSelection.getSelectionModel().getSelectedItems().addListener( new ListChangeListener<DataSet>() {
//			public void onChanged(Change<? extends DataSet> c) {
//				List<String> active = ((CommonCharacter<?,?,?,?>)charGen.getModel()).getDataSets().selected;
//				active =
//						listSelection.getSelectionModel().getSelectedItems().stream().map(ds -> ds.getID()).collect(Collectors.toList());
//				System.out.println("----> WizardPageDataSets TAR "+listSelection.getSelectionModel().getSelectedItems());
//				System.out.println("----> WizardPageDataSets "+active);
////				if (!active.isEmpty())
//				 ((CommonCharacter<?,?,?,?>)charGen.getModel()).getDataSets().selected.clear();
//				 ((CommonCharacter<?,?,?,?>)charGen.getModel()).getDataSets().selected.addAll( active);
//			}});

		cbMode.valueProperty().addListener( (ov,o,n) -> {
			((CommonCharacter<?,?,?,?>)charGen.getModel()).getDataSets().mode=n;
			listSelection.setDisable(n!=DataSetMode.SELECTED);
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.WizardPage#pageVisited()
	 */
	@Override
	public void pageVisited() {
		DataSetControl control = ((CommonCharacter<?,?,?,?>)charGen.getModel()).getDataSets();
		cbMode.setValue(control.mode);
		ObservableList<DataSet> list = FXCollections.observableArrayList();
		for (String id : control.selected) {
			for (DataSet set : available) {
				if (set.getID().equals(id)) {
					list.add(set);
					break;
				}
			}
		}
		//listSelection.getSelectionModel().getSelectedItems().setAll(list);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.WizardPage#pageLeft()
	 */
	@Override
	public void pageLeft() {
		charGen.runProcessors();
	}

	//-------------------------------------------------------------------
	private void showHelpFor(DataSet set) {
		descr.setData(set.getName(Locale.getDefault()), null, set.getDescription(Locale.getDefault()));
	}
}
