package de.rpgframework.jfx.wizard;

import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.SymbolIcon;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;

/**
 * @author prelle
 *
 */
public class NumberUnitBackHeader extends HBox {
	
	private Label lbNumber;
	private Label lbUnit;
	private Label btnSettings;

	//-------------------------------------------------------------------
	public NumberUnitBackHeader(String unitName) {
		super(5);
		lbNumber = new Label("?");
		lbNumber.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		lbNumber.setStyle("-fx-text-fill: accent");
		lbUnit   = new Label(unitName);
		
		getChildren().addAll(lbNumber, lbUnit);
	}

	//-------------------------------------------------------------------
	public NumberUnitBackHeader(String unitName, boolean withSettings) {
		this(unitName);
		if (withSettings) {
			btnSettings = new Label(null,new SymbolIcon("setting"));
			Region buf = new Region();
			buf.setMaxWidth(Double.MAX_VALUE);
			getChildren().addAll(buf, btnSettings);
			HBox.setHgrow(buf, Priority.ALWAYS);
		}
	}

	//-------------------------------------------------------------------
	public void setValue(int value) {
		lbNumber.setText(String.valueOf(value));
	}

	//-------------------------------------------------------------------
	public void setValue(float value) {
		lbNumber.setText(String.format("%.2f", value));
	}

}
