package de.rpgframework.jfx.pane;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.TitledComponent;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.Reward;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class RewardPane extends VBox {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle("de.rpgframework.jfx.Panes");
	
	protected Reward reward;
	
	private TextField tfName;
	private TextField tfGamemaster;
	private DatePicker datePicker;

	//-------------------------------------------------------------------
	public RewardPane(Reward data) {
		this.reward = data;
		
		initComponents();
		initLayout();
		
		if (data==null) reward = new Reward();
		refresh();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	protected void initComponents() {
		tfName = new TextField();
		tfName.setPrefColumnCount(30);
		tfGamemaster = new TextField();		
		
		LocalDateTime now = LocalDateTime.now();
		datePicker = new DatePicker();
		datePicker.setValue(now.toLocalDate());
	}

	//-------------------------------------------------------------------
	protected void initLayout() {
		setSpacing(5);
		TitledComponent tcName       = new TitledComponent(ResourceI18N.get(RES, "pane.reward.name"), tfName).setTitleMinWidth(110d);
		TitledComponent tcGamemaster = new TitledComponent(ResourceI18N.get(RES, "pane.reward.gamemaster"), tfGamemaster).setTitleMinWidth(110d);
		TitledComponent tcRealData   = new TitledComponent(ResourceI18N.get(RES, "pane.reward.realdate"), datePicker).setTitleMinWidth(110d);
		
		getChildren().addAll(tcName, tcGamemaster, tcRealData);
	}

	//-------------------------------------------------------------------
	protected void initInteractivity() {
		tfName.textProperty().addListener( (ov,o,n) -> reward.setTitle(n));
		tfGamemaster.textProperty().addListener( (ov,o,n) -> reward.setGamemaster(n));
		datePicker.valueProperty().addListener( (ov,o,n) -> {
			reward.setDate(
				Date.from(	n.atStartOfDay(ZoneId.systemDefault()).toInstant() )
				);
			});
	}

	//-------------------------------------------------------------------
	public Reward getData() {
		return reward;
	}

	//-------------------------------------------------------------------
	private void refresh() {
		if (reward==null)
			return;
		
		tfName.setText( reward.getTitle() );
		tfGamemaster.setText( reward.getGamemaster() );
		datePicker.setValue( LocalDate.ofInstant( reward.getDate().toInstant(), ZoneId.systemDefault()));
	}

}
