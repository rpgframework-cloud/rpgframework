package de.rpgframework.jfx;

import java.util.List;
import java.util.function.Function;

import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.requirements.Requirement;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;

/**
 * Provide a spinner to browse through a list of data items
 * and add additional information
 *
 * @author prelle
 *
 */
public class DataItemListPane<T extends DataItem> extends DataItemPane<T> {
	@FXML
	private ObjectProperty<T> selectedItem = new SimpleObjectProperty<>();

	private ListView<T> list;

	//-------------------------------------------------------------------
	public DataItemListPane(Function<Requirement,String> reqR, Function<Modification,String> modR) {
		super(reqR,modR);
		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		list = new ListView<T>();
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		setSelectorNode(list);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		heightProperty().addListener( (ov,o,n) -> recalculateSpinnerHeight());
		widthProperty().addListener( (ov,o,n) -> recalculateSpinnerHeight());
		selectedItem.bind(list.getSelectionModel().selectedItemProperty());
	}

	//-------------------------------------------------------------------
	private void recalculateSpinnerHeight() {
		list.setPrefHeight(getHeight());
	}

	//-------------------------------------------------------------------
	public void setItems(List<T> data) {
		list.getItems().setAll(data);
	}

}
