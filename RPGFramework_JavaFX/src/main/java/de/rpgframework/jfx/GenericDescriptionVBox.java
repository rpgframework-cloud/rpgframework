package de.rpgframework.jfx;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Function;

import org.prelle.javafx.JavaFXConstants;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.DataItemValue;
import de.rpgframework.genericrpg.modification.DataItemModification;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.requirements.Requirement;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class GenericDescriptionVBox<T extends DataItem> extends ADescriptionPane<T> {

	private final static ResourceBundle RES = ResourceBundle.getBundle(GenericDescriptionVBox.class.getName());

	private Function<Requirement,String> requirementResolver;
	private Function<Modification,String> modificationResolver;
	private BooleanProperty showModificationsInDescription = new SimpleBooleanProperty(true);

	private Label hdRequires;
	private Label requirements;
	private Label hdModifies;
	private Label modifications;

//	protected VBox inner;

	//-------------------------------------------------------------------
	public GenericDescriptionVBox(Function<Requirement,String> requirementResolver, Function<Modification,String> modificationResolver) {
		this.requirementResolver = requirementResolver;
		this.modificationResolver= modificationResolver;
		if (requirementResolver==null) {
			requirementResolver = (req) -> req.toString();
		}
		if (modificationResolver==null) {
			modificationResolver = (req) -> req.toString();
		}

		initExtraComponents();
		initExtraLayout();
//		initInteractivity();
	}

	//-------------------------------------------------------------------
	public GenericDescriptionVBox(Function<Requirement,String> requirementResolver, Function<Modification,String> modificationResolver, T item) {
		this(requirementResolver, modificationResolver);
		setData(item);
		initExtraComponents();
		initExtraLayout();
	}

	//-------------------------------------------------------------------
	@Override
	protected void initExtraComponents() {
		requirements = new Label();
		requirements.setWrapText(true);
		modifications = new Label();
		modifications.setWrapText(true);
	}

	//-------------------------------------------------------------------
	@Override
	protected void initExtraLayout() {
//		Label hdDescription = new Label(ResourceI18N.get(RES, "label.descr"));
		hdRequires    = new Label(ResourceI18N.get(RES, "label.requires"));
		hdModifies    = new Label(ResourceI18N.get(RES, "label.modifications"));
//		hdDescription.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		hdRequires.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		hdModifies.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);

//		inner = new VBox(5,hdDescription, description, tfKey, taDescr, hdModifies, modifications, hdRequires, requirements);
		inner.getChildren().addAll(hdModifies, modifications, hdRequires, requirements);
		VBox.setMargin(hdModifies, new Insets(10, 0, 0, 0));
		VBox.setMargin(hdRequires, new Insets(10, 0, 0, 0));

//		setMaxHeight(Double.MAX_VALUE);
//		setStyle("-fx-pref-width: 20em");
//		setStyle("-fx-max-width: 30em");
//		getChildren().addAll(descTitle, descSources, getWithOrWithoutScrollPane());
	}

//	//-------------------------------------------------------------------
//	private Node getWithOrWithoutScrollPane() {
//		if (useScrollPane.get()) {
//			ScrollPane scroll = new ScrollPane(inner);
//			scroll.setFitToWidth(true);
//			scroll.setMinHeight(200);
//			scroll.setMaxHeight(Double.MAX_VALUE);
//			VBox.setVgrow(scroll, Priority.ALWAYS);
//			return scroll;
//		} else {
//			return inner;
//		}
//	}
//
//	//-------------------------------------------------------------------
//	protected void initInteractivity() {
//		useScrollPaneProperty().addListener( (ov,o,n) -> {
//			getChildren().setAll(descTitle, descSources, getWithOrWithoutScrollPane());
//		});
//
//		description.setOnMouseEntered(ev -> enterDescription());
//		taDescr.setOnMouseExited(ev -> exitDescription());
//		taDescr.textProperty().addListener( (ov,o,n) -> customDescriptionChanged(item, n));
//	}

	//-------------------------------------------------------------------
	public void setData(DataItemValue<T> data) {
		if (data==null) {
			descTitle.setText(null);
			descSources.setText(null);
			requirements.setText(null);
			modifications.setText(null);
			return;
		}
		setData(data.getModifyable());
	}

	//-------------------------------------------------------------------
	public void setData(T data) {
		super.setData(data);
//		System.err.println("GenericDescriptionVBox.setData: "+data);
		if (data==null) {
			requirements.setText(null);
			modifications.setText(null);
			return;
		}

		// Eventually show requirements
		if ((data instanceof ComplexDataItem) && !((ComplexDataItem)data).getRequirements().isEmpty()) {
			hdRequires.setVisible(true);
			hdRequires.setManaged(true);
			requirements.setVisible(true);
			requirements.setManaged(true);
			List<String> list = new ArrayList<>();
			for (Requirement req : ((ComplexDataItem)data).getRequirements()) {
				if (requirementResolver!=null)
					list.add(requirementResolver.apply(req));
				else
					list.add(String.valueOf(req));
			}
			requirements.setText(String.join(", ", list));
		} else {
			hdRequires.setVisible(false);
			hdRequires.setManaged(false);
			requirements.setVisible(false);
			requirements.setManaged(false);
		}
		//Eventually show modifications
		if ((data instanceof ComplexDataItem) && !((ComplexDataItem)data).getOutgoingModifications().isEmpty() && showModificationsInDescription.get()) {
			hdModifies.setVisible(true);
			hdModifies.setManaged(true);
			modifications.setVisible(true);
			modifications.setManaged(true);
			List<String> list = new ArrayList<>();
			for (Modification req : ((ComplexDataItem)data).getOutgoingModifications()) {
				if ((req instanceof DataItemModification) && String.valueOf(((DataItemModification)req).getReferenceType()).equals("HOOK") ) continue;
				String toAdd = (modificationResolver!=null)
						?modificationResolver.apply(req)
						:String.valueOf(req);
				if (toAdd!=null) list.add(toAdd);
			}
			modifications.setText(String.join(", ", list));
		} else {
			hdModifies.setVisible(false);
			hdModifies.setManaged(false);
			modifications.setVisible(false);
			modifications.setManaged(false);
		}
	}

	//-------------------------------------------------------------------
	public void setData(String title, String source, String desc) {
		descTitle.setText(title);
		descSources.setText(source);
//		System.err.println("GenericDescriptionVBox.setData: "+desc);
		RPGFrameworkJavaFX.parseMarkupAndFillTextFlow(description, desc);
		hdRequires.setVisible(false);
		hdRequires.setManaged(false);
		hdModifies.setVisible(false);
		hdModifies.setManaged(false);
		requirements.setVisible(false);
		requirements.setManaged(false);
		modifications.setVisible(false);
		modifications.setManaged(false);
	}

	//-------------------------------------------------------------------
	public void setResolver(Function<Requirement,String> requirementResolver) {
		this.requirementResolver = requirementResolver;
	}

	//-------------------------------------------------------------------
	public void setModificationResolver(Function<Modification,String> modResolver) {
		this.modificationResolver = modResolver;
	}

	//--------------------------------------------------------------------
	public BooleanProperty showModificationsInDescriptionProperty() { return showModificationsInDescription; }
	public Boolean  isShowModificationsInDescription() { return showModificationsInDescription.get(); }
	public GenericDescriptionVBox setShowModificationsInDescription(Boolean value) { showModificationsInDescription.set(value); return this; }

	//--------------------------------------------------------------------
	public BooleanProperty useScrollPaneProperty() { return useScrollPane; }
	public Boolean  isUseScrollPane() { return useScrollPane.get(); }
	public GenericDescriptionVBox setUseScrollPane(Boolean value) { useScrollPane.set(value); return this; }
//
//	//--------------------------------------------------------------------
//	private void enterDescription() {
//		description.setVisible(false);
//		description.setManaged(false);
//		tfKey.setVisible(false);
//		tfKey.setManaged(false);
//		taDescr.setVisible(true);
//		taDescr.setManaged(true);
//	}
//
//	//--------------------------------------------------------------------
//	private void exitDescription() {
//		description.setVisible(true);
//		description.setManaged(true);
//		tfKey.setVisible(true);
//		tfKey.setManaged(true);
//		taDescr.setVisible(false);
//		taDescr.setManaged(false);
//	}
//
//	//--------------------------------------------------------------------
//	private void customDescriptionChanged(DataItem item, String text) {
//		if (text==null || text.isBlank()) return;
//		System.getLogger(getClass().getPackageName()).log(Level.INFO, "customDescriptionChanged");
//		if (item==null) return;
//		String key = item.getTypeString()+"."+item.getId().toLowerCase()+".desc";
//		item.getDescription(Locale.getDefault());
//		if (CustomResourceManagerLoader.getInstance()!=null) {
//			DataSet set = item.getFirstParent(Locale.getDefault());
//			CustomResourceManagerLoader.getInstance().setProperty(set.getRules(), key, Locale.getDefault(), text);
//		}
//	}

}
