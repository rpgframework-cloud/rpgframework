package de.rpgframework.jfx;

/**
 * @author prelle
 *
 */
public interface IRefreshableList {

	//-------------------------------------------------------------------
	public void refreshList();

}
