package de.rpgframework.jfx;

import java.util.List;

import de.rpgframework.genericrpg.data.DataItem;
import javafx.scene.layout.Pane;

/**
 * @author prelle
 *
 */
public abstract class AFilterInjector<T extends DataItem> {

	//-------------------------------------------------------------------
	/**
	 * Replace the current selection of elements in ChoiceBoxes by
	 * values that exist in the given dataset
	 * @param available Dataset to use
	 */
	public abstract void updateChoices(List<T> available);

	public abstract void addFilter(IRefreshableList page, Pane filterPane);

	public abstract List<T> applyFilter(List<T> input);

}
