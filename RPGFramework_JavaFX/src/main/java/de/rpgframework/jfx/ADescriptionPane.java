package de.rpgframework.jfx;

import java.lang.System.Logger.Level;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

import org.prelle.javafx.JavaFXConstants;

import de.rpgframework.ResourceI18N;
import de.rpgframework.core.CustomResourceManagerLoader;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.data.DataSet;
import de.rpgframework.genericrpg.data.PageReference;
import de.rpgframework.jfx.attach.PDFViewerServiceFactory;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextFlow;

/**
 * @author prelle
 *
 */
public abstract class ADescriptionPane<T extends DataItem> extends VBox {

	private final static ResourceBundle RES = ResourceBundle.getBundle(GenericDescriptionVBox.class.getName());

	protected BooleanProperty useScrollPane = new SimpleBooleanProperty(true);

	protected Label descTitle;
	protected Label descSources;
	private T item;
	protected TextFlow description;
	private TextField tfKey;
	private TextArea taDescr;

	/** Layout */
	protected VBox inner;

	//-------------------------------------------------------------------
	protected ADescriptionPane() {
		getStyleClass().add("description-pane");
		initComponents();
		initExtraComponents();
		initLayout();
		initExtraLayout();
		initInteractivity();
		taDescr.setVisible(false);
		taDescr.setManaged(false);
	}

	//-------------------------------------------------------------------
	private final void initComponents() {
		descTitle = new Label("");
//		descTitle = new Label();
		descTitle.setWrapText(true);
		descTitle.getStyleClass().add(JavaFXConstants.STYLE_HEADING3);
		descSources = new Label("");
		descSources.setWrapText(true);

		description = new TextFlow();
		taDescr     = new TextArea();
		tfKey       = new TextField();
		tfKey.setVisible(false);
		tfKey.setManaged(false);
	}
	//-------------------------------------------------------------------
	protected void initExtraComponents() {};

	//-------------------------------------------------------------------
	protected final void initLayout() {
		Label hdDescription = new Label(ResourceI18N.get(RES, "label.descr"));
		hdDescription.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
		inner = new VBox(5,hdDescription, description, tfKey, taDescr);

		setMaxHeight(Double.MAX_VALUE);
		setStyle("-fx-pref-width: 20em");
		setStyle("-fx-max-width: 30em");
		getChildren().addAll(descTitle, descSources, getWithOrWithoutScrollPane());
	}
	//-------------------------------------------------------------------
	protected void initExtraLayout() {};

	//-------------------------------------------------------------------
	private Node getWithOrWithoutScrollPane() {
		if (useScrollPane.get()) {
			ScrollPane scroll = new ScrollPane(inner);
			scroll.setFitToWidth(true);
			scroll.setMinHeight(200);
			scroll.setMaxHeight(Double.MAX_VALUE);
			VBox.setVgrow(scroll, Priority.ALWAYS);
			return scroll;
		} else {
			return inner;
		}
	}

	//-------------------------------------------------------------------
	protected void initInteractivity() {
		useScrollPaneProperty().addListener( (ov,o,n) -> {
			getChildren().setAll(descTitle, descSources, getWithOrWithoutScrollPane());
		});

//		description.setOnMouseEntered(ev -> enterDescription());
//		taDescr.setOnMouseExited(ev -> exitDescription());
		taDescr.textProperty().addListener( (ov,o,n) -> customDescriptionChanged(item, n));
	}

	//--------------------------------------------------------------------
	public BooleanProperty useScrollPaneProperty() { return useScrollPane; }
	public Boolean  isUseScrollPane() { return useScrollPane.get(); }
	public ADescriptionPane<T> setUseScrollPane(Boolean value) { useScrollPane.set(value); return this; }


	//-------------------------------------------------------------------
	public void setData(String title, String source, String desc) {
		descTitle.setText(title);
		descSources.setText(source);
		RPGFrameworkJavaFX.parseMarkupAndFillTextFlow(description, desc);
	}

	//-------------------------------------------------------------------
	public void setData(T data) {
		this.item = data;
		if (data==null) {
			descTitle.setText(null);
			descSources.setText(null);
			description.getChildren().clear();
			return;
		}

		// Eventually open a PDF
		Optional<PageReference> optPageRef = data.getPageReferences()
				.stream()
				.filter(pr -> pr.getLanguage().equals(Locale.getDefault().getLanguage()))
				.findFirst();
		PageReference pageRef = optPageRef.isPresent()?optPageRef.get():null;
		if (pageRef!=null) {
			PDFViewerServiceFactory.create().ifPresent(service -> {
				service.show(pageRef.getProduct().getRules(), pageRef.getProduct().getID(),
						pageRef.getLanguage(), pageRef.getPage());
			});
		}

		descTitle.setText(data.getName(Locale.getDefault()));
		descSources.setText(RPGFrameworkJavaFX.createSourceText(data));
		tfKey.setVisible(!data.hasLicense(Locale.getDefault()));
		tfKey.setManaged(!data.hasLicense(Locale.getDefault()));
		tfKey.setText(data.getTypeString()+"."+data.getId().toLowerCase()+".desc");
		RPGFrameworkJavaFX.parseMarkupAndFillTextFlow(description, data.getDescription(Locale.getDefault()));

		// Write custom daza
		if (CustomResourceManagerLoader.getInstance()!=null) {
			DataSet set = item.getFirstParent(Locale.getDefault());
			String key = item.getTypeString()+"."+item.getId().toLowerCase()+".desc";
			String custom = CustomResourceManagerLoader.getInstance().getProperty(set.getRules(), key, Locale.getDefault());
			taDescr.setText(custom);
		}
	}
//	public abstract void setHideTitleAndSources(boolean hide);

	public void setTitle(String title) {
		descTitle.setText(title);
	}

	public void setSources(String value) {
		descSources.setText(value);
	}

	//--------------------------------------------------------------------
	private void enterDescription() {
		description.setVisible(false);
		description.setManaged(false);
		tfKey.setVisible(false);
		tfKey.setManaged(false);
		taDescr.setVisible(true);
		taDescr.setManaged(true);
	}

	//--------------------------------------------------------------------
	private void exitDescription() {
		description.setVisible(true);
		description.setManaged(true);
		tfKey.setVisible(true);
		tfKey.setManaged(true);
		taDescr.setVisible(false);
		taDescr.setManaged(false);
	}

	//--------------------------------------------------------------------
	private void customDescriptionChanged(DataItem item, String text) {
		if (text==null || text.isBlank()) return;
		System.getLogger(getClass().getPackageName()).log(Level.INFO, "customDescriptionChanged");
		if (item==null) return;
		String key = item.getTypeString()+"."+item.getId().toLowerCase()+".desc";
		item.getDescription(Locale.getDefault());
		text = text.replaceAll("\n", "<br/>");
		
		if (CustomResourceManagerLoader.getInstance()!=null) {
			DataSet set = item.getFirstParent(Locale.getDefault());
			CustomResourceManagerLoader.getInstance().setProperty(set.getRules(), key, Locale.getDefault(), text);
		}

		if (text!=null && !text.isBlank()) {
			RPGFrameworkJavaFX.parseMarkupAndFillTextFlow(description, text);
		}
	}

}
