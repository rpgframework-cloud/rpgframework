package de.rpgframework.jfx;

import java.util.List;

import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public abstract class ComplexDataItemListFilter<T extends ComplexDataItem, V extends ComplexDataItemValue<T>> extends VBox {

	protected ComplexDataItemControllerNode<T, V> parent;
	
	//-------------------------------------------------------------------
	public ComplexDataItemListFilter(ComplexDataItemControllerNode<T, V> parent) {
		this.parent = parent;
	}
	
	//-------------------------------------------------------------------
	/**
	 * Called from the GUI when changes happen from outside
	 */
	public abstract void applyFilter();

}
