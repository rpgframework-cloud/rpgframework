package de.rpgframework.jfx.fxml;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.function.Function;

import de.rpgframework.genericrpg.data.ComplexDataItem;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.jfx.RPGFrameworkJavaFX;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.text.TextFlow;

/**
 * @author stefa
 *
 */
public class GenericDescriptionBoxController {

    @FXML
    private Label descTitle;
    @FXML
    private Label descSources;
    @FXML
    private TextFlow description;
    @FXML
    private Label hdRequires;
    @FXML
    private Label requires;

	//-------------------------------------------------------------------
	public GenericDescriptionBoxController() {
	}

	//-------------------------------------------------------------------
	@FXML
    void initialize() {
        assert descTitle   != null : "fx:id=\"descTitle\" was not injected: check your FXML file 'GenericDescriptionBox.fxml'.";
        assert descSources != null : "fx:id=\"descSources\" was not injected: check your FXML file 'GenericDescriptionBox.fxml'.";
        assert description != null : "fx:id=\"description\" was not injected: check your FXML file 'GenericDescriptionBox.fxml'.";
    }

	//-------------------------------------------------------------------
	public void setData(DataItem data, Function<Requirement,String> requirementResolver) {
		if (descTitle!=null)
			descTitle.setText(data.getName());
		if (descSources!=null)
			descSources.setText(RPGFrameworkJavaFX.createSourceText(data));

		descTitle.setText(data.getName());
		RPGFrameworkJavaFX.parseMarkupAndFillTextFlow(description, data.getDescription(Locale.getDefault()));
		
		if ((data instanceof ComplexDataItem) && !((ComplexDataItem)data).getRequirements().isEmpty()) {
			hdRequires.setVisible(true);
			hdRequires.setManaged(true);
			requires.setVisible(true);
			requires.setManaged(true);
			List<String> list = new ArrayList<>();
			for (Requirement req : ((ComplexDataItem)data).getRequirements()) {
				list.add(requirementResolver.apply(req));
			}
			requires.setText(String.join(", ", list));
		} else {
			hdRequires.setVisible(false);
			hdRequires.setManaged(false);
			requires.setVisible(false);
			requires.setManaged(false);
		}
	}

}
