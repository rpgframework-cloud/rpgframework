package de.rpgframework.jfx.fxml;

import java.lang.System.Logger.Level;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.function.Function;
import java.util.function.Supplier;

import org.prelle.javafx.ApplicationScreen;
import org.prelle.javafx.Page;
import org.prelle.javafx.ResponsiveControlManager;
import org.prelle.javafx.WindowMode;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.jfx.ADescriptionPane;
import de.rpgframework.jfx.RPGFrameworkJavaFX;
import javafx.fxml.FXML;
import javafx.scene.control.ListCell;

/**
 * @author prelle
 *
 */
public class ExtendedFilteredListPageController<T extends DataItem, C extends ListCell<T>, P extends ADescriptionPane<T>> extends FilteredListPageController<T> {


	private transient Class<P> descriptionPaneClass;

	//-------------------------------------------------------------------
	@FXML
	public void initialize() {
		// Don't show description in minimal mode
		description.setVisible(ResponsiveControlManager.getCurrentMode()!=WindowMode.MINIMAL);
		description.setManaged(ResponsiveControlManager.getCurrentMode()!=WindowMode.MINIMAL);

		// React to list selections
		lvResult.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null)
				showAction(n);
		});

		// React to searches
		search.textProperty().addListener( (ov,o,n) -> refreshList());
		// Enable open button
		btnOpen.setOnAction(ev -> page.toggleOpenClose());
	}

	//-------------------------------------------------------------------
	protected void showAction(T value) {
		description.setVisible(ResponsiveControlManager.getCurrentMode()!=WindowMode.MINIMAL);
		description.setManaged(ResponsiveControlManager.getCurrentMode()!=WindowMode.MINIMAL);

		if (ResponsiveControlManager.getCurrentMode()==WindowMode.MINIMAL) {
			try {
				Page toOpen = new Page(value.getName());
				P box = descriptionPaneClass.getConstructor().newInstance();
				box.setData(value);
				toOpen.setContent(box);
				page.getAppLayout().getApplication().openScreen(new ApplicationScreen(toOpen));
			} catch (Exception e) {
				RPGFrameworkJavaFX.logger.log(Level.ERROR, "Error opening "+descriptionPaneClass.getName(),e);
			}
		} else {
			try {
				P box = descriptionPaneClass.getConstructor().newInstance();
				box.setData(value);
				description.getChildren().add(box);
			} catch (Exception e) {
				RPGFrameworkJavaFX.logger.log(Level.ERROR, "Error opening "+descriptionPaneClass.getName(),e);
			}
		}
	}

	//-------------------------------------------------------------------
	public void setComponent(Page page, Supplier<Collection<T>> listProvider, Function<Requirement,String> resolver, Function<Modification,String> mResolver, Class<C> listCellClass, Class<P> descriptionPaneClass) {
		super.setComponent(page, listProvider, resolver, mResolver);
		this.descriptionPaneClass = descriptionPaneClass;
		page.getHeader().setText(ResourceI18N.get(resources, "page.title"));

		boolean withDescription = ResponsiveControlManager.getCurrentMode()!=WindowMode.MINIMAL;

		// Configure list
		description.setManaged(withDescription);
		description.setVisible(withDescription);
		// React to double clicks on creatures
		lvResult.setCellFactory(lv -> {
			try {
				C cell = listCellClass.getConstructor().newInstance();
				cell.setOnMouseClicked(ev -> {
					if (ev.getClickCount()==2) {
						showAction(cell.getItem());
					}
				});
				return cell;
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException | SecurityException e) {
				RPGFrameworkJavaFX.logger.log(Level.ERROR, "Error creating listcell",e);
				return null;
			}
			});
	}

}
