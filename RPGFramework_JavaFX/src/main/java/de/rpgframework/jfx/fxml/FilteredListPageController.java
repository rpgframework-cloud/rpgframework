package de.rpgframework.jfx.fxml;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.prelle.javafx.ApplicationScreen;
import org.prelle.javafx.Page;
import org.prelle.javafx.ResponsiveControlManager;
import org.prelle.javafx.WindowMode;

import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.jfx.GenericDescriptionVBox;
import de.rpgframework.jfx.RPGFrameworkJavaFX;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class FilteredListPageController<T extends DataItem> {

	private final static Logger logger = RPGFrameworkJavaFX.logger;

	protected transient Page page;
	protected transient Supplier<Collection<T>> listProvider;
	protected transient Function<Requirement,String> resolver;
	protected transient Function<Modification,String> modResolver;

	@FXML
    protected ResourceBundle resources;
	@FXML
	protected FlowPane filterPane;
	@FXML
	protected ListView<T> lvResult;
	@FXML
	protected GenericDescriptionBoxController descriptionController;
	@FXML
	protected VBox description;

	@FXML
	protected Button btnOpen;
	@FXML
	protected TextField search;

	//-------------------------------------------------------------------
	@FXML
	public void initialize() {
		// Don't show description in minimal mode
		description.setVisible(ResponsiveControlManager.getCurrentMode()!=WindowMode.MINIMAL);
		description.setManaged(ResponsiveControlManager.getCurrentMode()!=WindowMode.MINIMAL);

		// React to list selections
		lvResult.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null)
				showAction(n);
		});

		// React to searches
		search.textProperty().addListener( (ov,o,n) -> refreshList());
		// Enable open button
		btnOpen.setOnAction(ev -> page.toggleOpenClose());

		// React to double clicks on creatures
		lvResult.setCellFactory(lv -> {
			ListCell<T> cell = new ListCell<T>() {
				public void updateItem(T item, boolean empty) {
					super.updateItem(item, empty);
					if (empty) {
						setText(null);
					} else {
						setText(item.getName(Locale.getDefault()));
					}
				}
			};
			cell.setOnMouseClicked(ev -> {
				if (ev.getClickCount()==2) {
					showAction(cell.getItem());
				}
			});
			return cell;
			});
	}

	//-------------------------------------------------------------------
	protected List<T> filterItems(List<T> input) {
		return input;
	}

	//-------------------------------------------------------------------
	protected void refreshList() {
		List<T> list = new ArrayList<T>(listProvider.get());
		// Match search keyword
		if (search.getText()!=null && !search.getText().isBlank()) {
			list = list.stream()
					.filter(crea -> crea.getName().toLowerCase().indexOf(search.getText().toLowerCase())>-1)
					.collect(Collectors.toList());
		}

		Collections.sort(list, new Comparator<T>() {
			public int compare(T o1, T o2) {
				return Collator.getInstance().compare(o1.getName(), o2.getName());
			}
		});
		lvResult.getItems().setAll(list);
	}

	//-------------------------------------------------------------------
	protected void showAction(T value) {
		logger.log(Level.INFO, "Show FeatureType "+value);

		description.setVisible(ResponsiveControlManager.getCurrentMode()!=WindowMode.MINIMAL);
		description.setManaged(ResponsiveControlManager.getCurrentMode()!=WindowMode.MINIMAL);

		if (ResponsiveControlManager.getCurrentMode()==WindowMode.MINIMAL) {
			try {
				VBox box = new GenericDescriptionVBox(resolver, modResolver, value);
				Page toOpen = new Page(null, box);
				page.getAppLayout().getApplication().openScreen(new ApplicationScreen(toOpen));
			} catch (Exception e) {
				logger.log(Level.ERROR, "Error opening FeatureTypeDescriptionPage",e);
			}
		} else {
			descriptionController.setData(value, resolver);
		}
	}

	//-------------------------------------------------------------------
	public void setComponent(Page page, Supplier<Collection<T>> listProvider, Function<Requirement,String> resolver, Function<Modification,String> modResolver) {
		logger.log(Level.INFO, "setComponent()");
		this.page = page;
		this.listProvider = listProvider;
		this.resolver     = resolver;
		this.modResolver  = modResolver;
		//page.getHeader().setText(ResourceI18N.get(resources, "page.title"));

		boolean withDescription = ResponsiveControlManager.getCurrentMode()!=WindowMode.MINIMAL;
		description.setManaged(withDescription);
		description.setVisible(withDescription);

		refreshList();
	}

}
