package de.rpgframework.jfx.fxml;

import java.io.IOException;
import java.util.Collection;
import java.util.ResourceBundle;
import java.util.function.Function;
import java.util.function.Supplier;

import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.Page;

import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.jfx.ADescriptionPane;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ListCell;

/**
 * @author prelle
 *
 */
public class RPGFrameworkScreenLoader {

//	//-------------------------------------------------------------------
//	public static VBox loadGenericDescriptionBox(DataItem item, Function<Requirement,String> resolver) {
//		FXMLLoader loader = new FXMLLoader(
//				RPGFrameworkScreenLoader.class.getResource("GenericDescriptionVBox.fxml"),
//				ResourceBundle.getBundle("de.rpgframework.jfx.fxml.GenericDescriptionVBox")
//				);
//		FXMLLoader.setDefaultClassLoader(FlexibleApplication.class.getClassLoader());
//		VBox ret;
//		try {
//			ret = loader.load();
//			((GenericDescriptionBoxController)loader.getController()).setData(item, resolver);
//		} catch (IOException e) {
//			ret = new VBox();
//		}
//		ret.setId("description-box");
//		return ret;
//	}
//
//	//-------------------------------------------------------------------
//	@SuppressWarnings("unchecked")
//	public static <T extends DataItem> Page loadFilteredListPage(Function<Requirement,String> resolver, Supplier<Collection<T>> listProvider) throws IOException {
//		FXMLLoader loader = new FXMLLoader(
//				RPGFrameworkScreenLoader.class.getResource("FilteredListPage.fxml"),
//				ResourceBundle.getBundle("de.rpgframework.jfx.FilteredListPage")
//				);
//		FXMLLoader.setDefaultClassLoader(FlexibleApplication.class.getClassLoader());
//		Page ret = loader.load();
//		ret.setId("filtered");
//		((FilteredListPageController<T>)loader.getController()).setComponent(
//				ret,
//				listProvider, resolver
//				);
//		return ret;
//	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	public static <T extends DataItem> Page loadExtendedFilteredListPage(
			Function<Requirement,String> resolver,
			Function<Modification,String> mResolver,
			Supplier<Collection<T>> listProvider,
			Class<ListCell<T>> lcClazz,
			Class<ADescriptionPane<T>> dpClazz) throws IOException {
		FXMLLoader loader = new FXMLLoader(
				RPGFrameworkScreenLoader.class.getResource("FilteredListPage.fxml"),
				ResourceBundle.getBundle("de.rpgframework.jfx.FilteredListPage")
				);
		ExtendedFilteredListPageController controller = new ExtendedFilteredListPageController<>();
		loader.setController(controller);
		FXMLLoader.setDefaultClassLoader(FlexibleApplication.class.getClassLoader());
		Page ret = loader.load();
		ret.setId("filtered");
		controller.setComponent(
				ret,
				listProvider, resolver, mResolver,
				lcClazz, dpClazz
				);
		return ret;
	}

}
