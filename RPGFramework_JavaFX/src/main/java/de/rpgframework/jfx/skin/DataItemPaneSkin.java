package de.rpgframework.jfx.skin;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.function.Function;

import org.prelle.javafx.ResponsiveControlManager;
import org.prelle.javafx.layout.ResponsiveBox;

import de.rpgframework.genericrpg.data.DataItem;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.requirements.Requirement;
import de.rpgframework.jfx.DataItemDetailsPane;
import de.rpgframework.jfx.DataItemPane;
import de.rpgframework.jfx.RPGFrameworkJavaFX;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.Region;

/**
 * Provide a spinner to browse through a list of data items
 * and add additional information
 *
 * @author prelle
 *
 */
public class DataItemPaneSkin<T extends DataItem> extends SkinBase<DataItemPane<T>> {

	private final static Logger logger = RPGFrameworkJavaFX.logger;

	private DataItemDetailsPane<T> details;
//	private ScrollPane detailsScroll;

	private ResponsiveBox layout;

//	private Region regSpinner;
//	private Region regDetail;

	//-------------------------------------------------------------------
	public DataItemPaneSkin(DataItemPane<T> control, Function<Requirement,String> req, Function<Modification,String> mod) {
		super(control);
		initComponents(req,mod);
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents(Function<Requirement,String> reqR, Function<Modification,String> modR) {
		details = new DataItemDetailsPane<T>(reqR, modR);
		details.setMaxHeight(Double.MAX_VALUE);
		details.showModificationsInDescriptionProperty().bind(getSkinnable().showModificationsInDescriptionProperty());
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		layout = new ResponsiveBox();
//		layout.setStyle("-fx-background-color: rgb(100,0,0)");
		layout.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		reLayout();
//		layout.setMaxHeight(Double.MAX_VALUE);
//		layout.setMaxWidth(Double.MAX_VALUE);
//		GridPane.setVgrow(details, Priority.ALWAYS);
//		GridPane.setHgrow(details, Priority.ALWAYS);
		getChildren().add(layout);
	}

	//-------------------------------------------------------------------
	private void reLayout() {
		logger.log(Level.INFO, "reLayout="+ResponsiveControlManager.getCurrentMode());
		layout.getChildren().clear();

		Region regSpinner = (Region) getSkinnable().getSelectorNode();
//		regSpinner.setStyle("-fx-background-color: cyan");
		layout.getChildren().add(regSpinner);
		layout.getChildren().add(details);
//		layout.setStyle("-fx-background-color: lightgreen");
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		getSkinnable().selectorNodeProperty().addListener( (ov,o,n) -> {
			reLayout();
		});
		details.selectedItemProperty().bind(getSkinnable().selectedItemProperty());
//		details.selectedItemValueProperty().bind(getSkinnable().selectedItemValueProperty());
		details.showDecisionColumnProperty().bind(getSkinnable().showDecisionColumnProperty());
		details.showStatsColumnProperty().bind(getSkinnable().showStatsColumnProperty());
//		details.showHelpForProperty().bind(getSkinnable().showHelpForProperty());

		details.modificationConverterProperty().bind(getSkinnable().modificationConverterProperty());
		details.referenceTypeConverterProperty().bind(getSkinnable().referenceTypeConverterProperty());
		details.choiceConverterProperty().bind(getSkinnable().choiceConverterProperty());


		getSkinnable().layoutModeProperty().addListener( (ov,o,n) -> {
			logger.log(Level.INFO, "getSkinnable().layoutMode changed to "+n);
			reLayout();
		});

		//details.modelProperty().bind(getSkinnable().modelProperty());
		details.decisionHandlerProperty().bind(getSkinnable().decisionHandlerProperty());
		details.modDecisionHandlerProperty().bind(getSkinnable().modDecisionHandlerProperty());

		details.customNode1Property().bind(getSkinnable().customNode1Property());

//		getSkinnable().heightProperty().addListener( (ov,o,n) -> {
//			logger.log(Level.INFO, "Height changed to "+n);
////			if (getSkinnable().getSelectorNode() instanceof Region) {
////				Region spinner = (Region)getSkinnable().getSelectorNode();
////				if (ResponsiveControlManager.getCurrentMode()==WindowMode.MINIMAL) {
////					spinner.setPrefHeight( ((Double)n)/2);
////				} else {
////					spinner.setPrefHeight( (Double)n );
////				}
////			}
//			});
		getSkinnable().prefHeightProperty().addListener( (ov,o,n) -> logger.log(Level.INFO, "Pref Height changed to "+n));
	}

}
