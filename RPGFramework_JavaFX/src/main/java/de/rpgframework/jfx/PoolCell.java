package de.rpgframework.jfx;

import de.rpgframework.genericrpg.Pool;
import javafx.scene.control.TableCell;
import javafx.scene.control.Tooltip;

/**
 * @author Stefan
 *
 * T = Type (e.g. Attribute), C = ValueType (e.g. AttributeValue), R = RowType
 */
public class PoolCell<V,T> extends TableCell<V, Pool<T>> {

	//--------------------------------------------------------------------
	public PoolCell() {
	}

	//--------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(Pool<T> item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setText(null);
			setTooltip(null);
		} else {
			setText(item.toString());
			setTooltip(new Tooltip(item.toExplainString()));
		}
	}
}
