package de.rpgframework.jfx.pages;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.ManagedDialog;

import de.rpgframework.ResourceI18N;
import de.rpgframework.character.RuleSpecificCharacterObject;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.genericrpg.export.CharacterExportPlugin;
import de.rpgframework.genericrpg.export.ExportPluginRegistry;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.TilePane;

/**
 * @author prelle
 *
 */
@SuppressWarnings("rawtypes")
public class CharacterExportPluginSelectorPane<C extends RuleSpecificCharacterObject<?, ?, ?,?>> extends TilePane {

	private final static Logger logger = System.getLogger(CharacterExportPluginSelectorPane.class.getPackageName());

	private final static ResourceBundle RES = ResourceBundle.getBundle(CharacterViewLayout.class.getName());

	private C character;
	private List<Button> buttons;
	private Node toClose;

	//-------------------------------------------------------------------
	public CharacterExportPluginSelectorPane(C charac) {
		this.character = charac;
		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		buttons = new ArrayList<>();
		logger.log(Level.INFO, "Loading plugins");
		List<CharacterExportPlugin> plugins = ExportPluginRegistry.getCharacterExportPlugins(character);
		logger.log(Level.INFO, "Loaded plugins = "+plugins);
		for (CharacterExportPlugin plugin : plugins) {
			Button button = new Button(plugin.getName(Locale.getDefault()));
			button.setContentDisplay(ContentDisplay.TOP);
			button.setUserData(plugin);
			button.setWrapText(true);
			button.setStyle("-fx-border-width: 0px; -fx-font-size: 150%; -fx-max-width: 150px");
			// Try to get an image
			byte[] imgData = plugin.getIcon();
			if (imgData!=null) {
				ByteArrayInputStream bis = new ByteArrayInputStream(imgData);
				ImageView iView = new ImageView(new Image(bis));
				iView.setPreserveRatio(true);
				iView.setFitHeight(150);
				button.setGraphic(iView);
			}
			buttons.add(button);
		}
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		setHgap(20);
		setVgap(20);
		getChildren().addAll(buttons);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		for (Button btn : buttons) {
			btn.setOnAction( ev -> export( (CharacterExportPlugin<C>)btn.getUserData() ));
		}
	}

	//-------------------------------------------------------------------
	private void export(CharacterExportPlugin<C> plugin) {
		logger.log(Level.WARNING, "Export ''{0}'' using ''{1}''/{2}", character.getName(), plugin.getName(Locale.getDefault()), plugin.getClass().getSimpleName());

		try {
			CharacterExportPluginConfigPane config = new CharacterExportPluginConfigPane(plugin);
			CloseType closed = FlexibleApplication.getInstance()
					.showAlertAndCall(new ManagedDialog(
							ResourceI18N.get(RES, "dialog.exportpluginselector.configs"),
							config, CloseType.OK, CloseType.CANCEL), null);
			logger.log(Level.INFO, "Dialog closed with {0}",closed);
			if (closed == CloseType.CANCEL) {
				return;
			}

			byte[] data = null;
			try {
				data = plugin.createExport(character);
			} catch (Exception e) {
				e.printStackTrace();
				logger.log(Level.ERROR, "Error in export plugin", e);
				Platform.runLater( () -> {
					BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 2,ResourceI18N.get(RES, "dialog.exportpluginselector.export_except"),e);
				});
				return;
			} finally {
				logger.log(Level.DEBUG, "Close selector pane: "+toClose);
				if (toClose!=null)
					FlexibleApplication.getInstance().close(toClose, CloseType.OK);
			}
			logger.log(Level.INFO, "Export data created - try to save it now");
			try {
				String baseDir = System.getProperty("eden.basedir");
				Path temp = Paths.get(baseDir, "pdfs", character.getName()+plugin.getFileType());
				if (!Files.exists(temp.getParent()))
					Files.createDirectories(temp.getParent());
				//Path temp = Files.createTempFile(character.getName(), plugin.getFileType());
				logger.log(Level.INFO, "Write to " + temp);
				Files.write(temp, data);
				logger.log(Level.INFO, "Wrote to " + temp);
				BabylonEventBus.fireEvent(BabylonEventType.OPEN_FILE, temp);
				Platform.runLater( () -> {
					BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 0,ResourceI18N.format(RES, "dialog.exportpluginselector.export_ok", temp.toString()));
				});
			} catch (IOException e) {
				logger.log(Level.ERROR, "Failed creating PDF file: "+e);
				e.printStackTrace();
			}
		} finally {
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @param toClose the toClose to set
	 */
	public void setToClose(Node toClose) {
		this.toClose = toClose;
	}

}
