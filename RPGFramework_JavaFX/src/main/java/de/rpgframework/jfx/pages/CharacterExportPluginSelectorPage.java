package de.rpgframework.jfx.pages;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.FlexibleApplication;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.Page;

import de.rpgframework.ResourceI18N;
import de.rpgframework.character.RuleSpecificCharacterObject;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.genericrpg.export.CharacterExportPlugin;
import de.rpgframework.genericrpg.export.ExportPluginRegistry;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.TilePane;

/**
 * @author prelle
 *
 */
@SuppressWarnings("rawtypes")
public class CharacterExportPluginSelectorPage<C extends RuleSpecificCharacterObject<?, ?, ?,?>> extends Page {

	private final static Logger logger = System.getLogger(CharacterExportPluginSelectorPage.class.getPackageName());

	private final static ResourceBundle RES = ResourceBundle.getBundle(CharacterViewLayout.class.getName());

	private C character;
	private List<Button> buttons;
	private TilePane tiles;

	//-------------------------------------------------------------------
	public CharacterExportPluginSelectorPage(C charac) {
		super(ResourceI18N.get(RES, "dialog.exportpluginselector.title"), null);
		this.character = charac;
		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		buttons = new ArrayList<>();
		List<CharacterExportPlugin> plugins = ExportPluginRegistry.getCharacterExportPlugins(character);
		for (CharacterExportPlugin plugin : plugins) {
			Button button = new Button(plugin.getName(Locale.getDefault()));
			button.setContentDisplay(ContentDisplay.TOP);
			button.setUserData(plugin);
			button.setWrapText(true);
			button.setStyle("-fx-border-width: 0px; -fx-font-size: 150%; -fx-max-width: 150px");
			// Try to get an image
			byte[] imgData = plugin.getIcon();
			if (imgData!=null) {
				ByteArrayInputStream bis = new ByteArrayInputStream(imgData);
				ImageView iView = new ImageView(new Image(bis));
				iView.setPreserveRatio(true);
				iView.setFitHeight(150);
				button.setGraphic(iView);
			}
			buttons.add(button);
		}
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		tiles = new TilePane(20, 20);
		tiles.getChildren().addAll(buttons);

		setContent(tiles);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		for (Button btn : buttons) {
			btn.setOnAction( ev -> export( (CharacterExportPlugin<C>)btn.getUserData() ));
		}
	}

	//-------------------------------------------------------------------
	private void export(CharacterExportPlugin<C> plugin) {
		logger.log(Level.INFO, "Export ''{0}'' using ''{1}''/{2}", character.getName(), plugin.getName(Locale.getDefault()), plugin.getClass());

		try {
			CharacterExportPluginConfigPane config = new CharacterExportPluginConfigPane(plugin);
			CloseType closed = FlexibleApplication.getInstance()
					.showAlertAndCall(new ManagedDialog(
							ResourceI18N.get(RES, "dialog.exportpluginselector.configs"),
							config, CloseType.OK, CloseType.CANCEL), null);
			if (closed == CloseType.CANCEL) {
				return;
			}

			byte[] data = null;
			try {
				data = plugin.createExport(character);
			} catch (Throwable e) {
				e.printStackTrace();
				logger.log(Level.ERROR, "Error in export plugin", e);
				return;
			}
			try {
				Path temp = Files.createTempFile(character.getName(), plugin.getFileType());
				Files.write(temp, data);
				logger.log(Level.WARNING, "Wrote to " + temp);
				BabylonEventBus.fireEvent(BabylonEventType.OPEN_FILE, temp);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} finally {
			logger.log(Level.DEBUG, "Close selector pane");
//			FlexibleApplication.getInstance().close(this, CloseType.OK);
			FlexibleApplication.getInstance().closeScreen();
		}
	}

}
