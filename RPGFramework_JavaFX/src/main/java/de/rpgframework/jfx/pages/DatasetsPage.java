package de.rpgframework.jfx.pages;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;
import java.util.ResourceBundle;

import org.prelle.javafx.OptionalNodePane;
import org.prelle.javafx.Page;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.LicenseManager;
import de.rpgframework.genericrpg.data.CommonCharacter;
import de.rpgframework.genericrpg.data.DataSet;
import de.rpgframework.genericrpg.data.GenericCore;
import de.rpgframework.jfx.cells.DataSetListCell;
import de.rpgframework.reality.BoughtItem;
import de.rpgframework.reality.CatalogItem;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

/**
 *
 */
public class DatasetsPage extends Page {

	private final static Logger logger = System.getLogger(DatasetsPage.class.getPackageName());

	private final static ResourceBundle RES = ResourceBundle.getBundle(DatasetsPage.class.getPackageName()+".Pages");

	private ListView<DataSet> listView;

	//-------------------------------------------------------------------
	public DatasetsPage() {
		super(ResourceI18N.get(RES, "page.datasets.title"));
		initComponents();
		initLayout();
		initInteractivity();
		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		listView = new ListView<DataSet>() {};
		listView.setMaxHeight(Double.MAX_VALUE);
		listView.setCellFactory(lv -> new DataSetListCell(null));
		listView.setStyle("-fx-max-width: 50em");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Text pre  = new Text(ResourceI18N.get(RES,"page.datasets.descr.pre"));
		pre.setStyle("-fx-fill: -fx-text-base-color");
		Text mid  = new Text(" "+ResourceI18N.get(RES,"page.datasets.descr.mid")+" ");
		mid.setStyle("-fx-fill: green");
		Text post = new Text(ResourceI18N.get(RES,"page.datasets.descr.post"));
		post.setStyle("-fx-fill: -fx-text-base-color; -fx-font-size: 14px;");
		TextFlow flow = new TextFlow(pre, mid, post);
		flow.setLineSpacing(10);
		VBox layout = new VBox(flow, listView);
		layout.setMaxHeight(Double.MAX_VALUE);
		VBox.setVgrow(listView, Priority.ALWAYS);
		setContent(layout);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		listView.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n==null) {
				return;
			}
		});
	}

	//-------------------------------------------------------------------
	public void refresh() {
		listView.getItems().setAll( GenericCore.getDataSets() );
		Collections.sort(listView.getItems(), new Comparator<DataSet>() {

			@Override
			public int compare(DataSet ds1, DataSet ds2) {
				try {
					int i = Integer.compare(ds1.getType().ordinal(), ds2.getType().ordinal());
					if (i!=0) return i;
					i = Integer.compare(ds1.getReleased(), ds2.getReleased());
					if (i!=0) return i;
					return ds1.getName(Locale.getDefault()).compareTo(ds2.getName(Locale.getDefault()));
				} catch (Exception e) {
					logger.log(Level.ERROR, "Error comparing DataSet", e);
				}
				return 0;
			}
		});
	}

}
