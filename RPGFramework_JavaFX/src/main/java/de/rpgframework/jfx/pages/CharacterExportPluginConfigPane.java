package de.rpgframework.jfx.pages;

import java.io.File;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.ResourceBundle;

import org.prelle.javafx.FlexibleApplication;

import de.rpgframework.ConfigOption;
import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.export.CharacterExportPlugin;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class CharacterExportPluginConfigPane extends VBox {
	
	private final static Logger logger = System.getLogger(CharacterExportPluginSelectorPane.class.getPackageName());
	
	public final static ResourceBundle RES = ResourceBundle.getBundle(CharacterViewLayout.class.getName());
	
	private CharacterExportPlugin<?> plugin;

	//-------------------------------------------------------------------
	public CharacterExportPluginConfigPane(CharacterExportPlugin<?> plugin) {
		this.plugin = plugin;
		initComponents();
		initLayout();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);

		int i=0;
		for (ConfigOption option : plugin.getConfiguration()) {
			// DUmmy
			if (option==null) {
				logger.log(Level.ERROR,"STOP HERE - Option is NullPointer");
				System.exit(1);
			}
//			// Ignore the config option that stores templates, since it is already being displayed
//			if (option.getLocalId().equals("template"))
//				continue;
			i++;
			grid.add(new Label(option.getName(Locale.getDefault())), 0, i);
			switch (option.getType()) {
			case TEXT:
				TextField text = new TextField(String.valueOf(option.getValue()));
				text.setId(option.getId());
				text.setUserData(option);
				text.textProperty().addListener( (ov,o,n) -> option.set(n));
				grid.add(text, 1,i);
				break;
			case PASSWORD:
				PasswordField pass = new PasswordField();
				pass.setText(String.valueOf(option.getValue()));
				pass.setId(option.getId());
				pass.setUserData(option);
				pass.textProperty().addListener( (ov,o,n) -> option.set(n));
				grid.add(pass, 1,i);
				break;
			case CHOICE:
				ChoiceBox<Object> choice = new ChoiceBox<>();
				choice.setConverter(new StringConverter<Object>() {
					public String toString(Object object) { return option.getOptionName(object, Locale.getDefault()); }
					public Object fromString(String string) {return null; }
				});
				choice.getItems().addAll(option.getChoiceOptions());
				choice.setValue(option.getValue());
				choice.setId(option.getId());
				choice.setUserData(option);
				choice.valueProperty().addListener( (ov,o,n) -> option.set(n));
				grid.add(choice, 1,i);
				break;
			case DIRECTORY:
				Path oldPath = Paths.get(String.valueOf(option.getValue()));
				if (!Files.exists(oldPath)) {
					String newVal = System.getProperties().getProperty("user.home");
					logger.log(Level.WARNING,"Correct invalid path "+oldPath+" to "+newVal);
					((ConfigOption<String>)option).set( newVal);
				}
				TextField dir = new TextField(String.valueOf(option.getValue()));
				Button dirSelect = new Button(ResourceI18N.get(RES, "button.select"));
				HBox dirLine = new HBox(5);
				dirLine.getChildren().addAll(dir, dirSelect);
				dir.setId(option.getId());
				dir.setUserData(option);
				grid.add(dirLine, 1,i);
				dirSelect.setOnAction(event -> {
					DirectoryChooser chooser = new DirectoryChooser();
					File oldValue = null;
					try {
						oldValue = new File((String) option.getValue());
					} catch (NullPointerException e) {
						/*Dann eben nicht*/
						logger.log(Level.WARNING,"Error opening "+option.getValue()+": "+e);
					}
					if (oldValue == null || !oldValue.exists()) {
						oldValue = new File(System.getProperties().getProperty("user.home"));
					}
					chooser.setInitialDirectory(oldValue);
					File selected = chooser.showDialog(FlexibleApplication.getInstance().getAppLayout().getScene().getWindow());
					if (selected != null) {
						dir.setText(selected.getAbsolutePath());
						option.set(selected.getAbsolutePath());
					}

				});
				break;
			case BOOLEAN:
				CheckBox box = new CheckBox();
				box.setId(option.getId());
				box.setUserData(option);
				box.setSelected((boolean)option.getValue());
				box.selectedProperty().addListener( (ov,o,n) -> option.set(n));
				grid.add(box, 1,i);
				break;
			default:
				logger.log(Level.ERROR,"TODO: implement "+option.getType()+" config option");
			}			
		}
		
		getChildren().add(grid);
	}

}
