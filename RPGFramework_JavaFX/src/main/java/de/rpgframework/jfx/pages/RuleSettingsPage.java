package de.rpgframework.jfx.pages;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.lang.reflect.Method;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.stream.Collectors;

import org.prelle.javafx.JavaFXConstants;
import org.prelle.javafx.Page;

import de.rpgframework.ResourceI18N;
import de.rpgframework.genericrpg.chargen.CharacterController;
import de.rpgframework.genericrpg.chargen.Rule;
import de.rpgframework.genericrpg.chargen.RuleValue;
import de.rpgframework.jfx.RPGFrameworkJFXConstants;
import de.rpgframework.jfx.RPGFrameworkJavaFX;
import javafx.geometry.Insets;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author stefa
 *
 */
public class RuleSettingsPage extends Page {

	private final static PropertyResourceBundle RES = RPGFrameworkJFXConstants.UI;

	private final static Logger logger = RPGFrameworkJavaFX.logger;

	private CharacterController<?,?> control;
	private List<Rule.EffectOn> allowedCategories;
	private GridPane grid;

	//-------------------------------------------------------------------
	/**
	 * @param title
	 */
	public RuleSettingsPage(CharacterController<?,?> ctrl, List<Rule.EffectOn> categories) {
		super(ResourceI18N.get(RES, "page.rules"));
		this.control = ctrl;
		this.allowedCategories = categories;
		initComponents();
		setData(ctrl);
		initLayout();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		grid = new GridPane();
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		grid.setVgap(5);
		grid.setHgap(10);
		ScrollPane scroll = new ScrollPane(grid);
		scroll.setMaxHeight(Double.MAX_VALUE);
		scroll.setFitToWidth(true);
		VBox.setVgrow(scroll, Priority.ALWAYS);

		setContent(scroll);
	}

	//-------------------------------------------------------------------
	public void setData(CharacterController<?, ?> ctrl)  {
		List<RuleValue> values = ctrl.getRuleController().getValues();
		// Sort rules
		values = values.stream()
			.filter(r -> allowedCategories.contains( r.getRule().getAffected()))
			.sorted(new Comparator<RuleValue>() {
				public int compare(RuleValue o1, RuleValue o2) {
					int cmp = Integer.compare(o1.getRule().getAffected().ordinal(), o2.getRule().getAffected().ordinal());
					if (cmp!=0) return cmp;
					return o1.getRule().getName(Locale.getDefault()).compareTo(o2.getRule().getName(Locale.getDefault()));
				}
			})
			.collect(Collectors.toList());

		grid.getChildren().clear();
		Rule.EffectOn lastCategory = null;
		int i=-1;
		for (RuleValue rVal : values) {
			logger.log(Level.DEBUG, "  rule "+rVal);
			i++;
			if (rVal.getRule().getAffected()!=lastCategory) {
				Label heading = new Label(rVal.getRule().getAffected().getName());
				heading.getStyleClass().add(JavaFXConstants.STYLE_HEADING5);
				GridPane.setMargin(heading, new Insets(5, 0, 0, 0));
				grid.add(heading, 0,i, 2,1);
				i++;
				lastCategory = rVal.getRule().getAffected();
			}
			Rule rule = rVal.getRule();
			Label lbName = new Label(rule.getName(Locale.getDefault()));
			lbName.setWrapText(true);
			grid.add(lbName, 0,i);

			if (!rVal.isEditable()) {
				Label lbLock = new Label("x\uD83D\uDD12");
				grid.add(lbLock, 1,i);
			}

			switch (rule.getType()) {
			case BOOLEAN:
				CheckBox check = new CheckBox();
				check.setDisable(!rVal.isEditable());
				check.setSelected((boolean)rVal.getValue());
				grid.add(check, 2,i);
				check.selectedProperty().addListener( (ov,o,n) -> {
					logger.log(Level.INFO, "Rule "+rVal+" changed to "+n);
					control.getRuleController().setRuleValue(rule, Boolean.valueOf(n));
					ctrl.runProcessors();
				});
				break;
			case INTEGER:
				TextField intField = new TextField(String.valueOf(rVal.getValue()));
				intField.setUserData(rule);
				intField.setPrefColumnCount(4);
				intField.setStyle("-fx-min-width: 3.5em");
				intField.setEditable(rVal.isEditable());
				grid.add(intField, 2,i);
				intField.textProperty().addListener( (ov,o,n) -> {
					logger.log(Level.INFO, "Rule "+rVal+" changed to "+n);
					control.getRuleController().setRuleValue(rule, Integer.parseInt(n));
					ctrl.runProcessors();
				});
				break;
			case FLOAT:
				intField = new TextField(String.valueOf(rVal.getValue()));
				intField.setUserData(rule);
				intField.setPrefColumnCount(4);
				intField.setStyle("-fx-min-width: 3.5em");
				intField.setEditable(rVal.isEditable());
				grid.add(intField, 2,i);
				intField.textProperty().addListener( (ov,o,n) -> {
					logger.log(Level.INFO, "Rule "+rVal+" changed to "+n);
					control.getRuleController().setRuleValue(rule, n);
					ctrl.runProcessors();
				});
				break;
			case ENUM:
				ChoiceBox<Object> enumField = new ChoiceBox<>();
				for (Enum<?> tmp : rule.getEnumClassToUse().getEnumConstants() ) {
					enumField.getItems().add(tmp);
				}
				enumField.setValue(rule.getDefaultAsEnumValue());
				enumField.setConverter(new StringConverter<Object>() {
					public String toString(Object val) {
						try {
							Method method = val.getClass().getMethod("getName", Locale.class);
							return (String) method.invoke(val, Locale.getDefault());
						} catch (NoSuchMethodException e) {
							return String.valueOf(val);
						} catch (Exception e) {
							return "?Error?";
						}
					}
					public Object fromString(String string) {return null;}
				});
				enumField.setUserData(rule);
				enumField.setStyle("-fx-min-width: 8em");
				enumField.setDisable(!rVal.isEditable());
				grid.add(enumField, 2,i);
				enumField.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
					logger.log(Level.INFO, "Rule "+rVal+" changed to "+n);
					control.getRuleController().setRuleValue(rule, n);
					ctrl.runProcessors();
				});
				break;
			default:
				logger.log(Level.ERROR, "No support for type "+rule.getType());
				System.err.println("RuleChoiceBox: No support for rule type "+rule.getType());
			}
		}
	}

}
