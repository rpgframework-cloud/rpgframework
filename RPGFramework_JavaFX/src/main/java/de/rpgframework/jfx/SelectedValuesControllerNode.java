package de.rpgframework.jfx;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.prelle.javafx.ResponsiveControl;
import org.prelle.javafx.WindowMode;

import de.rpgframework.genericrpg.chargen.SelectedValueController;
import de.rpgframework.genericrpg.data.ComplexDataItemValue;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.requirements.Requirement;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Skin;
import javafx.util.Callback;

/**
 * @author prelle
 *
 */
public class SelectedValuesControllerNode<V extends ComplexDataItemValue<?>> extends Control implements ResponsiveControl {

	private final static Logger logger = RPGFrameworkJavaFX.logger;

	private static final String DEFAULT_STYLE_CLASS = "selection-control";

	private ObjectProperty<SelectedValueController<V>> control;

	private ObjectProperty<ObservableList<V>> availableProperty = new SimpleObjectProperty<ObservableList<V>>(FXCollections.observableArrayList());
	private ObjectProperty<ObservableList<V>> selectedProperty = new SimpleObjectProperty<ObservableList<V>>(FXCollections.observableArrayList());

	private ObjectProperty<Callback<ListView<V>, ListCell<V>>> cellFactoryProperty;
	private ObjectProperty<Predicate<V>> availableFilterProperty = new SimpleObjectProperty<>();
	private ObjectProperty<Predicate<V>> selectedFilterProperty = new SimpleObjectProperty<>();
	private ObjectProperty<Node> selectedListHeadProperty = new SimpleObjectProperty<>();

	private BooleanProperty showHeadingsProperty;
	private StringProperty  availableHeadingProperty;
	private StringProperty  selectedHeadingProperty;
	private StringProperty  availablePlaceholderProperty;
	private StringProperty  selectedPlaceholderProperty;

	private ObjectProperty<V> showHelpForProperty;
	private StringProperty  availableStyleProperty;
	private StringProperty  selectedStyleProperty;
	private ObjectProperty<Function<Requirement,String>> requirementResolver = new SimpleObjectProperty<>();
	private ObjectProperty<Function<Modification,String>> modificationResolver = new SimpleObjectProperty<>();

	//-------------------------------------------------------------------
	public SelectedValuesControllerNode(SelectedValueController<V> controller) {
		this.control = new SimpleObjectProperty<SelectedValueController<V>>(controller);
		cellFactoryProperty = new SimpleObjectProperty<>();
		showHeadingsProperty = new SimpleBooleanProperty(true);
		availableHeadingProperty = new SimpleStringProperty(RPGFrameworkJFXConstants.UI.getString("label.available"));
		selectedHeadingProperty  = new SimpleStringProperty(RPGFrameworkJFXConstants.UI.getString("label.selected"));
		availablePlaceholderProperty = new SimpleStringProperty();
		selectedPlaceholderProperty  = new SimpleStringProperty();
		showHelpForProperty = new SimpleObjectProperty<>();
		availableStyleProperty = new SimpleStringProperty();
		selectedStyleProperty  = new SimpleStringProperty();

		getStyleClass().setAll(DEFAULT_STYLE_CLASS);

		this.control.addListener( (ov,o,n) -> refresh());

//		filterNodeProperty.addListener( (ov,o,n) -> {
//			if (n!=null) {
//				n.setUnfiltered(this.control.get().getAvailable());
//			}
//		});

		refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Control#createDefaultSkin()
	 */
	@Override
	public Skin<?> createDefaultSkin() {
		return new SelectedValuesControllerTwoColumnSkin<V>(this);
	}

	//-------------------------------------------------------------------
	public ObjectProperty<SelectedValueController<V>> getControllerProperty() { return control;}
	public SelectedValueController<V> getController() { return control.get();}
	public void setController(SelectedValueController<V> value) { control.set(value); }

	//-------------------------------------------------------------------
	public void refresh() {
		if (control.get()==null) {
			logger.log(Level.WARNING, "No controller yet");
			return;
		}
		if (logger.isLoggable(Level.DEBUG))
			logger.log(Level.DEBUG, "ComplexDataItemControllerNode.refresh: "+control.get().getSelected());
		try {
			availableProperty.get().setAll(
					control.get().getAvailable()
					.stream()
					.sorted( (o1,o2) -> o1.getNameWithoutRating().compareTo(o2.getNameWithoutRating()))
					.collect(Collectors.toList()));
			if (selectedFilterProperty.get()!=null)
				selectedProperty.get().setAll(control.get().getSelected().stream().filter(getSelectedFilter()).collect(Collectors.toList()));
			else
				selectedProperty.get().setAll(control.get().getSelected());
//			logger.log(Level.DEBUG, "ComplexDataItemControllerNode.refresh2: "+selectedProperty.get());
		} finally {
			logger.log(Level.DEBUG, "ComplexDataItemControllerNode.refresh: done");
		}
	}

	//-------------------------------------------------------------------
	public ObjectProperty<ObservableList<V>> availableProperty() { return availableProperty; }
	public ObservableList<V> getAvailable() { return availableProperty.get(); }
//	public void setAvailable(ObservableList<T> value) { availableProperty.setValue(value); }

	//-------------------------------------------------------------------
	public ObjectProperty<ObservableList<V>> selectedProperty() { return selectedProperty; }
	public ObservableList<V> getSelected() { return selectedProperty.get(); }
//	public void setSelected(ObservableList<V> value) { selectedProperty.setValue(value); }

	//-------------------------------------------------------------------
	public ObjectProperty<Callback<ListView<V>, ListCell<V>>> cellFactoryProperty() { return cellFactoryProperty; }
	public Callback<ListView<V>, ListCell<V>> getCellFactory() { return cellFactoryProperty.get(); }
	public void setCellFactory(Callback<ListView<V>, ListCell<V>> value) { cellFactoryProperty.setValue(value); }

	//-------------------------------------------------------------------
	public ObjectProperty<Predicate<V>> selectedFilterProperty() { return selectedFilterProperty; }
	public Predicate<V> getSelectedFilter() { return selectedFilterProperty.get(); }
	public void setSelectedFilter(Predicate<V> value) { selectedFilterProperty.setValue(value); }

	//-------------------------------------------------------------------
	public ObjectProperty<Node> selectedListHeadProperty() { return selectedListHeadProperty; }
	public Node getSelectedListHead() { return selectedListHeadProperty.get(); }
	public void setSelectedListHead(Node value) { selectedListHeadProperty.setValue(value); }

	//-------------------------------------------------------------------
	public BooleanProperty showHeadingsProperty() { return showHeadingsProperty; }
	public boolean getShowHeadings() { return showHeadingsProperty.get(); }
	public void setShowHeadings(boolean value) { showHeadingsProperty.set(value); }

	//-------------------------------------------------------------------
	public StringProperty availableHeadingProperty() { return availableHeadingProperty; }
	public String getAvailableHeading() { return availableHeadingProperty.get(); }
	public void setAvailableHeading(String value) { availableHeadingProperty.set(value); }

	//-------------------------------------------------------------------
	public StringProperty selectedHeadingProperty() { return selectedHeadingProperty; }
	public String getSelectedHeading() { return selectedHeadingProperty.get(); }
	public void setSelectedHeading(String value) { selectedHeadingProperty.set(value); }

	//-------------------------------------------------------------------
	public StringProperty availablePlaceholderProperty() { return availablePlaceholderProperty; }
	public String getAvailablePlaceholder() { return availablePlaceholderProperty.get(); }
	public void setAvailablePlaceholder(String value) { availablePlaceholderProperty.set(value); }

	//-------------------------------------------------------------------
	public StringProperty selectedPlaceholderProperty() { return selectedPlaceholderProperty; }
	public String getSelectedPlaceholder() { return selectedPlaceholderProperty.get(); }
	public void setSelectedPlaceholder(String value) { selectedPlaceholderProperty.set(value); }

	//-------------------------------------------------------------------
	public ObjectProperty<V> showHelpForProperty() { return showHelpForProperty; }
	public V getShowHelpFor() { return showHelpForProperty.get(); }
	public void setShowHelpFor(V value) { showHelpForProperty.setValue(value); }

	//-------------------------------------------------------------------
	public StringProperty availableStyleProperty() { return availableStyleProperty; }
	public String getAvailableStyle() { return availableStyleProperty.get(); }
	public void setAvailableStyle(String value) { availableStyleProperty.set(value); }

	//-------------------------------------------------------------------
	public StringProperty selectedStyleProperty() { return selectedStyleProperty; }
	public String getSelectedStyle() { return selectedStyleProperty.get(); }
	public void setSelectedStyle(String value) { selectedStyleProperty.set(value); }

	//-------------------------------------------------------------------
	public ObjectProperty<Function<Requirement,String>> requirementResolverProperty() { return requirementResolver; }
	public Function<Requirement,String> getRequirementResolver() { return requirementResolver.get(); }
	public SelectedValuesControllerNode<V> setRequirementResolver(Function<Requirement,String> value) { requirementResolver.setValue(value); return this;}

	//-------------------------------------------------------------------
	public ObjectProperty<Function<Modification,String>> modificationResolverProperty() { return modificationResolver; }
	public Function<Modification,String> getModificationResolver() { return modificationResolver.get(); }
	public SelectedValuesControllerNode<V> setModificationResolver(Function<Modification,String> value) { modificationResolver.setValue(value); return this;}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ResponsiveControl#setResponsiveMode(org.prelle.javafx.WindowMode)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void setResponsiveMode(WindowMode value) {
		System.err.println("ComplexDataItemControllerNode.setResponsive("+value+")");
		((SelectedValuesControllerTwoColumnSkin<V>)getSkin()).setResponsiveMode(value);
	}

}
