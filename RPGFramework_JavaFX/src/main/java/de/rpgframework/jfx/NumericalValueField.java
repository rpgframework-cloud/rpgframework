package de.rpgframework.jfx;

import java.lang.System.Logger;
import java.util.function.Supplier;

import org.prelle.javafx.skin.AcrylicPaneSkin;

import de.rpgframework.genericrpg.NumericalValue;
import de.rpgframework.genericrpg.NumericalValueController;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;
import javafx.util.Callback;

public class NumericalValueField<T,V extends NumericalValue<T>> extends Control {

	private final static Logger logger = System.getLogger("prelle.jfx");
	public static final String REFRESH = "refreshKey";

	private ObjectProperty<Supplier<V>> supplier   = new SimpleObjectProperty<>();
	private ObjectProperty<NumericalValueController<T,V>> control = new SimpleObjectProperty<>();
	private ObjectProperty<Callback<V, String>> converter = new SimpleObjectProperty<>();
//	private int minWidthEm;
//	private boolean useLabel = true;
//	private Integer overrideValue;

	private ObjectProperty<EventHandler<ActionEvent>> onAction = new SimpleObjectProperty<>();

	//--------------------------------------------------------------------
	public NumericalValueField() {
	}

	//--------------------------------------------------------------------
	public NumericalValueField(Supplier<V> valueProvider) {
		this.supplier.set(valueProvider);
	}

	//--------------------------------------------------------------------
	public NumericalValueField(Supplier<V> valueProvider, NumericalValueController<T,V> ctrl) {
		if (valueProvider==null) throw new NullPointerException("valueProvider");
		this.supplier.set(valueProvider);
		this.control.set(ctrl);
	}

	//--------------------------------------------------------------------
	public Skin<?> createDefaultSkin() {
		return new NumericalValueFieldSkin<>(this);
	}

	//--------------------------------------------------------------------
	public ObjectProperty<Supplier<V>> valueFactoryProperty() { return supplier; }
	public Supplier<V> getValueFactory() { return supplier.get(); }
	public NumericalValueField<T,V> setValueFactory(Supplier<V> value) { supplier.set(value); return this; }

	//--------------------------------------------------------------------
	public ObjectProperty<NumericalValueController<T,V>> controllerProperty() { return control; }
	public NumericalValueController<T,V> getController() { return control.get(); }
	public NumericalValueField<T,V> setController(NumericalValueController<T,V> value) { control.set(value); return this; }

	//--------------------------------------------------------------------
	public ObjectProperty<Callback<V, String>> converterProperty() { return converter; }
	public Callback<V, String> getConverter() { return converter.get(); }
	public NumericalValueField<T,V> setConverter(Callback<V, String> value) { converter.set(value); return this; }

	//--------------------------------------------------------------------
	public void refresh() {
        getProperties().put(AcrylicPaneSkin.REFRESH, Boolean.TRUE);
	}

	//--------------------------------------------------------------------
	public ObjectProperty<EventHandler<ActionEvent> > onActionProperty() { return onAction; }
	public EventHandler<ActionEvent>  getOnAction() { return onAction.get(); }
	public void setOnAction(EventHandler<ActionEvent> handler) {
		this.onAction.set(handler);
	}

}