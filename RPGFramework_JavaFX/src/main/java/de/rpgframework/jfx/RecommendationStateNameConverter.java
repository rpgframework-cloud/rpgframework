package de.rpgframework.jfx;

import java.util.Locale;

import de.rpgframework.genericrpg.chargen.RecommendationState;
import de.rpgframework.genericrpg.chargen.RecommendingController;
import de.rpgframework.genericrpg.data.DataItem;
import javafx.util.Callback;

/**
 * 
 */
public class RecommendationStateNameConverter<D extends DataItem> implements Callback<D, String> {

	private RecommendingController<D> ctrl;
	
	//-------------------------------------------------------------------
	public RecommendationStateNameConverter(RecommendingController<D> ctrl) {
		this.ctrl = ctrl;
	}

	//-------------------------------------------------------------------
	protected String getBaseName(D data) {
		if (data==null) return "-";
		return data.getName(Locale.getDefault());
	}

	//-------------------------------------------------------------------
	@Override
	public String call(D data) {
		String name = getBaseName(data);
		RecommendationState state = ctrl.getRecommendationState(data);
		if (state==null) return data.getName(Locale.getDefault());
		switch (state) {
		case STRONGLY_RECOMMENDED: return name+" \uE113";
		case RECOMMENDED: return name+" \uE1CE";
		case UNRECOMMENDED: return name+" \uE15B";
		default:
		}
		return name;
	}

}
