package de.rpgframework.jfx;

/**
 * @author prelle
 *
 */
public interface Constants {
	
	public final static String PREFERENCE_PATH = "/de/rpgframework";

	public final static String LAST_OPEN_DIR = PREFERENCE_PATH+"/lastDir/open";
	public final static String LAST_SAVE_DIR = PREFERENCE_PATH+"/lastDir/save";
	public final static String PROP_LAST_OPEN_IMAGE_DIR = "image";
	public final static String PROP_LAST_SAVE_PRINT_DIR = "print";

}
