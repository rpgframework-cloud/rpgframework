open module de.rpgframework.javafx {
	exports de.rpgframework.jfx;
	exports de.rpgframework.jfx.cells;
	exports de.rpgframework.jfx.fxml;
	exports de.rpgframework.jfx.pane;
	exports de.rpgframework.jfx.pages;
	exports de.rpgframework.jfx.rules;
	exports de.rpgframework.jfx.section;
	exports de.rpgframework.jfx.wizard;

	requires transitive javafx.extensions;
	requires javafx.base;
	requires transitive javafx.controls;
	requires transitive javafx.graphics;
	requires transitive de.rpgframework.core;
	requires transitive de.rpgframework.rules;
//	requires transitive de.rpgframework.products;
	requires javafx.fxml;
	requires java.xml;
	requires java.prefs;
	requires org.controlsfx.controls;
	requires transitive java.desktop;
	requires javafx.swing;
	requires rpgframework.pdfviewer;
	requires lombok;
}