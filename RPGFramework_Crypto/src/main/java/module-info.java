
module de.rpgframework.core {
	requires bcprov.jdk15on;
	requires bcpg.jdk15on;

	exports de.rpgframework.crypto;
}