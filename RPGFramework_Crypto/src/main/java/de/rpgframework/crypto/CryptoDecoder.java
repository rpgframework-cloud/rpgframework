package de.rpgframework.crypto;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.Security;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.bcpg.ArmoredInputStream;

/**
 *
 */
public class CryptoDecoder {

	private final static Logger logger = System.getLogger(CryptoDecoder.class.getPackageName());

	private final static String PUBKEY = "-----BEGIN PGP MESSAGE-----\n"+
"Version: BCPG v1.51\n"+
"\n"+
"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC4TxJp1lJNR7R56s8DzwLOAu7b\n"+
"mOayZaZgTrQDemaDbimgXX74Eho+WR6h84Tiw4aJgsa5FEaFPjHPYo0+PsieHDB3\n"+
"LA/pBdLiahvO+3oI9vFWlDlTXywmNqRTcNDjmtH+gHGqIj/BJ5WQ/zgxubxNL6qZ\n"+
"15/VOgrNJth632PKNQIDAQAB\n"+
"=Iuo2\n"+
"-----END PGP MESSAGE-----";

	private static Cipher rsa;
	private static PublicKey publicKey;

	//--------------------------------------------------------------------
	static {
		// Prepare cryptography
		try {
			Security.addProvider(new BouncyCastleProvider());
			rsa = Cipher.getInstance("RSA","BC");
			publicKey = readKey();
		} catch (IOException e) {
			logger.log(Level.ERROR, "Could not decode public key for licenses",e);
		} catch (Throwable e) {
			logger.log(Level.ERROR, "Could not initialize crypto framework - license management not working",e);
		}

	}

	//--------------------------------------------------------------------
	private static PublicKey readKey() throws IOException {
		try {
			ByteArrayInputStream bin = new ByteArrayInputStream(PUBKEY.getBytes());
			InputStream in = new ArmoredInputStream(bin);
			byte[] encodedPublicKey = new byte[in.available()];
			in.read(encodedPublicKey);
			in.close();
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(
					encodedPublicKey);
			return keyFactory.generatePublic(publicKeySpec);
		} catch (Exception e) {
			throw new IOException(e);
		}
	}

	//--------------------------------------------------------------------
	public static String decode(String licenseString) {
		byte[] encoded = Base64.getDecoder().decode(licenseString.getBytes());
		try {
			rsa.init(Cipher.DECRYPT_MODE, publicKey);
			byte[] decoded = rsa.doFinal(encoded);
			return new String(decoded, StandardCharsets.UTF_8);
		} catch (InvalidKeyException | IllegalBlockSizeException | BadPaddingException | StringIndexOutOfBoundsException e) {
			throw new IllegalArgumentException("Invalid license.",e);
		}
	}

}
